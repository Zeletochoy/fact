#Simple wrapper for xbuild
MSBUILD=xbuild

DOC_DIR=doc
DOC_DOXYFILE_USER=$(DOC_DIR)/Doxyfile.user
DOC_DOXYFILE_DEV=$(DOC_DIR)/Doxyfile.dev

deploy:
	xbuild src/Fact/Fact.sln /p:configuration=Deploy

debug:
	xbuild src/Fact/Fact.sln /p:configuration=Debug

$(DOC_TARGET): deploy

doc: $(DOC_DIR)/dev $(DOC_DIR)/user

$(DOC_DIR)/user:
	doxygen $(DOC_DOXYFILE_USER)
$(DOC_DIR)/dev:
	doxygen $(DOC_DOXYFILE_DEV)

clean:
	xbuild src/Fact/Fact.sln /target:clean /p:configuration=Deploy

clean-doc:
	$(RM) -rf $(DOC_DIR)/dev $(DOC_DIR)/user

distclean: clean clean-doc
