﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer.Display
{
    public class Admin
    {
        public static string Managers()
        {
            string panels = "";
            panels += "<div class=\"page-header\">\n";
            panels += "  <h2>Fact Managers</h2>\n";
            panels += "  <table class=\"table table-striped\">\n";
             panels += "<tr><th> Manager name </th><th> Actions </th><th>Status</th></tr>";
            List<string> managers = Global.GetManagers();
            managers.Add("127.0.0.1");
            foreach (string manager in managers)
            {
                if (!Global.PingManager(manager))
                {
                    panels += "<tr class=\"danger\"><th>" + manager + "</th><th>" +
                        Tools.Button.GenerateButton("Shutdown", "Default.aspx?command=ShutdownManager---" + manager) +
                        "</th><th>" + "" + "</th></tr>";
                }
                else
                {
                    panels += "<tr class=\"success\"><th>" + manager + "</th><th>" +
                       Tools.Button.GenerateButton("Shutdown", "Default.aspx?command=ShutdownManager---" + manager) +
                       "</th><th>" + "" + "</th></tr>";
                }
            }
            panels += "  </table>";
            panels += Tools.Button.GenerateButton("Add manager", "Default.aspx?command=StartNewManager");
            panels += "</div>";
            return panels;
        }
        public static string AmdinLogin()
        {
            string panels = "";
            panels += "<div class=\"page-header\">\n";
            panels += "<h1>Login</h1>\n";
            panels += "<form method=\"post\">";
            panels += "<div class=\"input-group\">";
            panels += "<span class=\"input-group-addon\">Login</span>";
            panels += "<input type=\"text\" name=\"login\" class=\"form-control\" placeholder=\"Login\">";
            panels += "<input type=\"password\" placeholder=\"Password\" name=\"password\" class=\"form-control\" placeholder=\"Password\">";
            panels += "</div>";
            panels += "<button type=\"submit\" class=\"btn btn-default\">Login</button>";
            panels += "</form>";
            panels += "</div>";
            return panels;
        }
        public static string AmdinCreate()
        {
            string panels = "";
            panels += "<div class=\"page-header\">\n";
            panels += "<h1>Login</h1>\n";
            panels += "<form method=\"post\">";
            panels += "<div class=\"input-group\">";
            panels += "<span class=\"input-group-addon\">Create user</span>";
            panels += "<input type=\"text\" name=\"create_login\" class=\"form-control\" placeholder=\"Login\">";
            panels += "<input type=\"password\" name=\"create_password\" class=\"form-control\" placeholder=\"Password\">";
            panels += "</div>";
            panels += "<button type=\"submit\" class=\"btn btn-default\">Create</button>";
            panels += "</form>";
            panels += "</div>";
            return panels;
        }
        public static string AdminPanel()
        {
            string panels = "";
            panels += "<div class=\"page-header\">\n";
            panels += "  <h1>Administration</h1>\n";
            panels += Managers();
            panels += "</div>";
            return panels;
        }

		public static string AdminCommand()
		{
			string panels = "";
			panels += "<div class=\"page-header\">\n";
			panels += "<h1>Worker shell</h1>\n";
			panels += "<form method=\"post\">";
			panels += "<textarea name=\"send_to_worker_script\"class=\"form-control\" rows=\"12\">" + "" + "</textarea>";
			panels += "<button type=\"submit\" class=\"btn btn-default\">Do</button>";
			panels += "</form>";
			panels += "</div>";
			return panels;
		}

		public static string AdminCommandWorker()
		{
			return "";
		}
    }
}
