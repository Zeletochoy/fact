﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Xml.Linq;

namespace FactWebServer.Display
{
    public class Project
    {

        public static int GetExpectedSubmissions(string Directory)
        {
            try
            {
                if (System.IO.Directory.Exists(Directory + "/Submissions"))
                {
                    List<string> values = new List<string>();
                    foreach (string dir in System.IO.Directory.GetDirectories(Directory + "/Submissions"))
                    {
	                        try
	                        {
	                            values.Add(System.IO.Path.GetFileNameWithoutExtension(dir));
	                        }
	                        catch { }
                    }
                    values.Sort(string.CompareOrdinal);
                    if (values.Count > 1)
                    {
						int count = 0;

						foreach (string file in System.IO.Directory.GetFiles(Directory + "/Submissions/" + values[values.Count - 2]))
						{
							if (file.EndsWith(".ff"))
							{
								count++;
							}
						}
						return count;
                    }
                }
            }
            catch { }
            return 0;
        }

        public static string GetLastSubmission(string Directory)
        {
            try
            {
                if (System.IO.Directory.Exists(Directory + "/Submissions"))
                {
                    List<string> values = new List<string>();
                    foreach (string dir in System.IO.Directory.GetDirectories(Directory + "/Submissions"))
                    {
                        try
                        {
                            values.Add(System.IO.Path.GetFileNameWithoutExtension(dir));
                        }
                        catch { }
                    }
                    values.Sort(string.CompareOrdinal);
                    return values[values.Count - 1];
                }
            }
            catch { }
            return "";
        }

        public static string Description(string Directory)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string name = "";
            try
            {
                if (System.IO.File.Exists(Directory + "/project.fw"))
                {
                    FactWebPage project = new FactWebPage(Directory + "/project.fw");
                    if (project["description"] != "")
                    {
                        string descfile = Directory + "/" + project["description"];
                        if (System.IO.File.Exists(descfile))
                        {
                            panels += "<div class=\"page-header\">\n";
                            panels += "  <h2>Description</h2>\n";
                            panels += System.IO.File.ReadAllText(descfile);
                            panels += "</div>";
                        }
                        else
                        {
                            panels += "<div class=\"page-header\">\n";
                            panels += "  <h2>Description</h2>\n";
                            panels += Tools.Log.GenerateErrorPanel("Impossible to load the description", "The description file '" + project["description"] + "' could not be found");
                            panels += "</div>";
                        }
                    }

                }
            }
            catch { }
            return panels;
        }

        public static string BuildInfo(string Directory, bool IsAdmin)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string BuildDir = GetLastSubmission(Directory);
            panels += "<div class=\"page-header\">\n";
            panels += "  <h2>Build status</h2>\n";
            if (BuildDir != "")
                panels += "  <h3>" + BuildDir.Replace('_', ' ') + "</h3>\n";
            panels += "</div>\n";

            try
            {
                if (!System.IO.Directory.Exists(Directory + "/Submissions"))
                {
                    panels += "<div>\n";
                    panels += "Initialization:";
                    panels += Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>(FactWebServer.Tools.ProgressBar.Type.Warning, 10));
                    panels += "</div>";
                }
                else if (!System.IO.Directory.Exists(Directory + "/Artifacts"))
                {
                    panels += "<div>\n";
                    panels += "Initialization:";
                    panels += Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>(FactWebServer.Tools.ProgressBar.Type.Warning, 50));
                    panels += "</div>";
                }
                else
                {
                    panels += "<div>\n";
                    panels += "Initialization:";
                    panels += Tools.ProgressBar.GenerateProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>(FactWebServer.Tools.ProgressBar.Type.Success, 100));
                    panels += "</div>";
                }
            }
            catch { }

			int submissioncount = 0;
			try
			{
				foreach (string file in System.IO.Directory.GetFiles(Directory + "/Submissions/" + BuildDir))
				{ if (file.EndsWith(".ff")) { submissioncount++; } }
			}
			catch { }

			int expectedsubmission = GetExpectedSubmissions(Directory);
            try
            {
                if (!System.IO.Directory.Exists(Directory + "/Submissions") ||
                    !System.IO.Directory.Exists(Directory + "/Artifacts") ||
                    !System.IO.Directory.Exists(Directory + "/Submissions/" + BuildDir))
                {
                    panels += "<div>\n";
                    panels += "Retriving packages:";
                    panels += Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>(FactWebServer.Tools.ProgressBar.Type.Danger, 1));
                    panels += "</div>";
                }
                else if (expectedsubmission == 0)
                {
                    panels += "<div>\n";
                    panels += "Retriving packages:";
                    panels += Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>(FactWebServer.Tools.ProgressBar.Type.Info, 100));
                    panels += "</div>";
                }
                else if (expectedsubmission < submissioncount)
                {

                    panels += "<div>\n";
                    panels += "Retriving packages:";
                    panels +=
                        Tools.ProgressBar.GenerateStripedProgressBar(
                            expectedsubmission,
                            new KeyValuePair<Tools.ProgressBar.Type, int>(
                            FactWebServer.Tools.ProgressBar.Type.Info,
                            submissioncount));
                    panels += "</div>";
                }
                else
                {
                    panels += "<div>\n";
                    panels += "Retriving packages:";
                    panels +=
                        Tools.ProgressBar.GenerateProgressBar(
                            100,
                            new KeyValuePair<Tools.ProgressBar.Type, int>(
                            FactWebServer.Tools.ProgressBar.Type.Success,
                            100));
                    panels += "</div>";
                }
            }
            catch { }



			int artifactcount = 0;
			try
			{
				foreach (string file in System.IO.Directory.GetFiles(Directory + "/Artifacts/" + BuildDir))
				{ if (file.EndsWith(".ff")) { artifactcount++; } }
			}
			catch { }

            try
            {
                if (!System.IO.Directory.Exists(Directory + "/Submissions") ||
                    !System.IO.Directory.Exists(Directory + "/Artifacts") ||
                    !System.IO.Directory.Exists(Directory + "/Submissions/" + BuildDir) ||
                    !System.IO.Directory.Exists(Directory + "/Artifacts/" + BuildDir) ||
				    submissioncount == 0)
                {
                    panels += "<div>\n";
                    panels += "Generating test artifacts:";
                    panels += Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>(FactWebServer.Tools.ProgressBar.Type.Danger, 1));
                    panels += "</div>";
                }
				else if (artifactcount < submissioncount)
                {

                    panels += "<div>\n";
                    panels += "Generating test artifacts:";
                    panels +=
                        Tools.ProgressBar.GenerateStripedProgressBar(
							submissioncount,
                            new KeyValuePair<Tools.ProgressBar.Type, int>(
                                FactWebServer.Tools.ProgressBar.Type.Info,
								artifactcount));
                    panels += "</div>";
                }
				else 
				{
					panels += "<div>\n";
					panels += "Generating test artifacts:";
					panels +=
						Tools.ProgressBar.GenerateProgressBar(
							100,
							new KeyValuePair<Tools.ProgressBar.Type, int>(
							FactWebServer.Tools.ProgressBar.Type.Success,
							100));
					panels += "</div>";
				}

            }
            catch { }
            try
            {
                if (IsAdmin)
                {
                    panels += "<div>\n";
                    panels += Tools.Button.GenerateButton("Start a new build", "Default.aspx?command=CreateSubmission---" + System.IO.Path.GetFileNameWithoutExtension(Directory));
                    panels += "</div>\n";
                }
            }
            catch { }
            panels += "</div>\n";
            return panels;

        }

        public static string BuildLists(string Directory, bool IsAdmin)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string name = System.IO.Path.GetFileNameWithoutExtension(Directory);
            panels += "<div class=\"page-header\">\n";
            panels += "  <h2>Tests results</h2>\n";
            panels += "<table class=\"table table-striped\">\n";
            try
            {
                if (System.IO.Directory.Exists(Directory + "/Artifacts"))
                {
                    List<string> values = new List<string>();
                    foreach (string dir in System.IO.Directory.GetDirectories(Directory + "/Artifacts"))
                    {
                        try
                        {
                            values.Add(System.IO.Path.GetFileNameWithoutExtension(dir));
                        }
                        catch { }
                    }
                    values.Sort(string.CompareOrdinal);
                    if (IsAdmin)
                    { panels += "<tr><th> Build number </th><th> Build name </th><th>Global score</th><th>Actions</th></tr>"; }
                    else { panels += "<tr><th> Build number </th><th> Build name </th><th>Global score</th></tr>"; }
                    for (int n = 0; n < values.Count; n++)
                    {
                        panels += "<tr>";
                        panels += "<th>" + n.ToString() + "</th><th>" + "<a href=\"Default.aspx?projectbuild=" + name + "---" + values[n] + "\">" + values[n].Replace('_', ' ') + "</a>" + "</th>";
                        if (System.IO.File.Exists(Directory + "/Artifacts/" + values[n] + "/score.fw"))
                        {
                            FactWebPage webpage = new FactWebPage(Directory + "/Artifacts/" + values[n] + "/score.fw");
                            int pass = webpage.GetPropertyAsInteger("__score");
                            int total = webpage.GetPropertyAsInteger("__total");
                                panels += "<th>" + Tools.ProgressBar.GenerateProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>[]
                                {
                                    new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Success, pass),
                                    new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Danger, 100 - pass)
                                }) + "</th>";
                        }
                        else
                        {
                            panels += "<th>" + Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>[]
                            {
                                new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Success, 50),
                                new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Danger, 50)
                            }) + "</th>";
                            Global.Add_Worker(new Worker.ScoreComputing(Directory));
                        }
                        

                        if (IsAdmin)
                        {
                            panels += "<th>" +
                                	Tools.Button.GenerateSmallButton("Delete submission", "Default.aspx?deletesubmission=" + name + "---" + values[n]) +
									Tools.Button.GenerateSmallButton("Restart submission", "Default.aspx?restartsubmission=" + name + "---" + values[n]) +
									Tools.Button.GenerateSmallButton("Recompute result", "Default.aspx?recomputesubmission=" + name + "---" + values[n]) +
                                "</th>";
                        }
                        panels += "</tr>";
                    }
                   
                }
            }
            catch { }
            panels += "</tables>";
            panels += "</div>";
            return panels;
        }

        public static string GenerateSubmissionGroup(string Parent, FactWebPage.Group Data, bool Show)
        {
			if (Data ["__type"] == "group")
			{
				string panels = "";
				string score = Data ["__score"];
				panels += "<tr>";
				if (Parent != "") {
					panels += "<th>" + Parent + "/" + Data.Name + "</th>";
				} else {
					panels += "<th>" + Data.Name + "</th>";
				}
				if (score == "") {
					panels += "<th></th>";
				} else {
					try {
						int iscore = int.Parse (score);
						panels += "<th>" +
							Tools.ProgressBar.GenerateProgressBar ((new KeyValuePair<Tools.ProgressBar.Type, int>[] {
							new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Success, iscore),
							new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Danger, 100 - iscore)
						})) +
							" " +
							score.ToString () + "%" + "</th>";
					} catch {
						panels += "<th>" + score.ToString () + "%" + "</th>";
					}
                   
                    
                  
				}
				panels += "</tr>";
				foreach (FactWebPage.Group group in Data.Groups) {
                    
                  
					panels += GenerateSubmissionGroup (Data.Name, group, false);
				}
				return panels;
			}
			else if (Show)
			{
				if (Data ["__type"] == "fail")
				{
					string panels = "";
					panels += "<tr><th>" + Parent + "/" + Data.Name +  "</th>" + "<th></th>" + "</tr>";
					return panels;
				}
			}
            return "";
        }

        public static string Submission(string Directory, string Build, string Submission)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string name = "";
            name = System.IO.Path.GetFileNameWithoutExtension(Directory);
            panels += "<div class=\"page-header\">\n";
            panels += "<h1>" + name + ": " + Submission + "(" + Build.Replace('_', ' ') + ")</h1>\n";
            panels += "</div>\n";
            panels += "<div class=\"page-header\">\n";
            panels += "<h2>Ranking:</h2>\n";
            panels += "<h2>Summary:</h2>\n";
            string submission = Directory + "/" + Build + "/"  + Submission;
            if (System.IO.File.Exists(Directory + "/Artifacts/" + Build + "/" + Submission + ".fw"))
            {
                FactWebPage page = new FactWebPage(Directory + "/Artifacts/" + Build + "/" + Submission + ".fw");
                if (page["__empty"] == "yes")
                {
                    panels += Tools.Log.GenerateWarningPanel(
                        "Empty submission",
                        "Your original submission is empty. " +
                        "If your are using a CVS check if you have correctly push your modifications.");
                }
                else
                {
                    if (page.GetPropertyAsInteger("__total") == 0)
                    {
                        panels += Tools.Log.GenerateWarningPanel(
                        "Testsuite was not responding",
                        "The tests associated to your submissions have been killed." +
                        "This usually occurs because your program has requieredd too much ressources, " +
                        "or because of an invalid behavior." +
                        "If you think this appears because of an internal problem, please contact the administrator.");
                    }
                    else
                    {
                        panels += "<table class=\"table table-striped\">\n";
                        panels += "<tr><th> Testsuite </th><th>Score</th></tr>";
                        foreach (FactWebPage.Group g in page.Groups)
                        {
                            panels += GenerateSubmissionGroup("", g, true);
                        }
                        panels += "</table>\n";
                    }
                }
            }
            panels += "<h2>Achievements:</h2>\n";

            panels += "</div>\n";

            return panels;
        }

        public static string Build(string Directory, string Build, bool IsAdmin)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string name = "";
            name = System.IO.Path.GetFileNameWithoutExtension(Directory);
            panels += "<div class=\"page-header\">\n";
            panels += "<h1>" + name + ": " + Build.Replace('_', ' ') + "</h1>\n";
            panels += "</div>\n";
            panels += "<div class=\"page-header\">\n";
            panels += "<h2>Submissions:</h2>\n";
            panels += "<table class=\"table table-striped\">\n";
            try
            {
                if (System.IO.Directory.Exists(Directory + "/Artifacts/" + Build))
                {
                    List<string> values = new List<string>();
                    foreach (string dir in System.IO.Directory.GetFiles(Directory + "/Artifacts/" + Build))
                    {
                        try
                        {
							if (dir.EndsWith(".ff"))
							{
                            	values.Add(System.IO.Path.GetFileName(dir));
							}
                        }
                        catch { }
                    }
                    values.Sort(string.CompareOrdinal);

					if (IsAdmin)
					{
						panels += "<tr><th> Submission Index </th><th> Submission name </th><th>Score</th><th>Actions</th></tr>";
					}
					else
					{
                    	panels += "<tr><th> Submission Index </th><th> Submission name </th><th>Score</th></tr>";
					}
					bool sended = false;
                    for (int n = 0; n < values.Count; n++)
                    {
                        string builditemname = values[n];
                        string rawname = System.IO.Path.GetFileNameWithoutExtension(builditemname);

                        if (builditemname.EndsWith(".ff"))
                        {
                            panels += "<tr>";
                            panels += "<th>" + n.ToString() + "</th><th>" + "<a href=\"Default.aspx?projectsubmit=" + name + "---" + Build + "---" + rawname + "\">" + rawname.Replace('_', ' ') + "</a>" + "</th>";

							if (!System.IO.File.Exists(Directory + "/Artifacts/" + Build + "/" + rawname + ".fw"))
                            {
                                panels += "<th>" + Tools.ProgressBar.GenerateStripedProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>[]
                                {
                                    new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Success, 50),
                                    new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Danger, 50)
                                }
                                );
                                
								if (!sended)
								{
									Global.Add_Worker(new Worker.ScoreComputing(Directory));
									sended = true;
								}
                            }
                            else
                            {
								FactWebPage page = new FactWebPage(Directory + "/Artifacts/" + Build + "/" + rawname + ".fw");
                                int score = page.GetPropertyAsInteger("__score");
                                int total = page.GetPropertyAsInteger("__total");
                                if (page["__empty"] == "yes")
                                {
                                    panels += "<th>" + Tools.ProgressBar.GenerateProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>[] { new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Info, 100), }) + "</th>";
                                }
                                else if (total == 0)
                                {
                                    panels += "<th>" + Tools.ProgressBar.GenerateProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>[] { new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Warning, 100), }) + "</th>";
                                }
                                else
                                {
                                    panels += "<th>" + Tools.ProgressBar.GenerateProgressBar(new KeyValuePair<Tools.ProgressBar.Type, int>[]
                                    {
                                        new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Success, score),
                                        new KeyValuePair<Tools.ProgressBar.Type, int>(Tools.ProgressBar.Type.Danger, 100 - score)
                                    }
                                    );
                                }
								if (IsAdmin)
								{
									panels += "<th>" + Tools.Button.GenerateSmallButton("Restart", "Default.aspx?restartspecsubmission=" + Directory + "---" + Build + "---" + rawname) + "</th>";
								}
                            }

							panels += "</tr>";
                        }
                    }

                }
            }
            catch { }
            panels += "</tables>";

            panels += "</div>\n";
            return panels;
        }

        public static string Summary(string Directory, bool IsAdmin)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string name = "";
            if (System.IO.File.Exists(Directory + "/project.fw"))
            {
                FactWebPage project = new FactWebPage(Directory + "/project.fw");
                if (project["name"] != "") { name = project["name"]; }
                if (name == "") { try { name = System.IO.Path.GetFileNameWithoutExtension(Directory); } catch { } }
                panels += "<div class=\"page-header\">\n";
                panels += "  <h1>" + name + "</h1>\n";


                panels += Description(Directory);
                panels += Tools.Button.GenerateButton("Open project " + name, "Default.aspx?project=" + System.IO.Path.GetFileNameWithoutExtension(Directory));
                if (IsAdmin)
                { panels += Tools.Button.GenerateButton("Delete project " + name, "Default.aspx?deleteproject=" + System.IO.Path.GetFileNameWithoutExtension(Directory)); }
            }
            else
            {
                if (name == "") { try { name = System.IO.Path.GetFileNameWithoutExtension(Directory); } catch { } }
                panels += "<div class=\"page-header\">\n";
                panels += "<h1>" + name + "</h1>\n";
                panels += "</div>\n";

                panels += Tools.Log.GenerateWarningPanel("Project file not found", "the file 'project.fw' has not been found for the project " + name);
                if (IsAdmin)
                {
					panels += Tools.Button.GenerateButton("Delete project " + name, "Default.aspx?deleteproject=" + System.IO.Path.GetFileNameWithoutExtension(Directory)); 

				}
			}Global.Add_Worker(new Worker.ScoreComputing(Directory));
            return panels;
        }

        public static string TestScript(string Directory, bool IsAdmin)
        {
            if (Directory == "") { return ""; }
            if (!IsAdmin) { return ""; }

            string panels = "";
            panels += "<div class=\"page-header\">\n";
            panels += "  <h1>Test Script</h1>\n";
            string text = "";
            if (System.IO.File.Exists(Directory + "/test.ft"))
            {
                try
                {
                    text = System.IO.File.ReadAllText(Directory + "/test.ft");
                }catch{}
            }
            panels += "<form method=\"post\">";
            panels += "<textarea name=\"test_script\"class=\"form-control\" rows=\"12\">" + text + "</textarea>";
            panels += "<button type=\"submit\" class=\"btn btn-default\">Update</button>";
            panels += "</form>";
            panels += "</div>";
            return panels;
        }

        public static string Details(string Directory, bool IsAdmin)
        {
            if (Directory == "") { return ""; }
            string panels = "";
            string name = "";
            if (System.IO.File.Exists(Directory + "/project.fw"))
            {
                FactWebPage project = new FactWebPage(Directory + "/project.fw");
                if (project["name"] != "") { name = project["name"]; }
                if (name == "") { try { name = System.IO.Path.GetFileNameWithoutExtension(Directory); } catch { } }
                panels += "<div class=\"page-header\">\n";
                panels += "  <h1>" + name + "</h1>\n";

                if (IsAdmin && !System.IO.File.Exists(Directory + "/test.ft")) 
                {
                    panels += Tools.Log.GenerateErrorPanel("Missing test suite", "The testsuite has not been found: test.ft");
                }
                panels += TestScript(Directory, IsAdmin);
                panels += Description(Directory);
                panels += BuildInfo(Directory, IsAdmin);
                panels += BuildLists(Directory, IsAdmin);
            }
            else
            {
                if (name == "") { try { name = System.IO.Path.GetFileNameWithoutExtension(Directory); } catch { } }
                panels += "<div class=\"page-header\">\n";
                panels += "<h1>" + name + "</h1>\n";
                panels += "</div>\n";

                panels += Tools.Log.GenerateWarningPanel("Project file not found", "the file 'project.fw' has not been found for the project " + name);
            }
            return panels;
        }
    }
}
