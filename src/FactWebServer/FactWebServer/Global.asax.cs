﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml.Linq;
using System.Collections.Generic;

namespace FactWebServer
{
	public partial class Global : System.Web.HttpApplication
    {
        static Queue<Worker.Worker> _Tasks = new Queue<FactWebServer.Worker.Worker>();
        static System.Threading.Thread _Worker;
        static string _RootDir = "";
        static DataBase _Database = null;
        static string GitName(string git)
        {
            string name = "";
            for (int x = git.Length - 1; x >= 0; x--)
            {
                if (git[x] == '/' ||
                    git[x] == '\\') 
                {
                    break;
                }
                name = git[x] + name;
            }
            return name;
        }
        static Fact.Plugin.Plugin LoadPlugin(string PluginDir, string Name)
        {
            try
            {
                Fact.Plugin.Plugin Plugin = Fact.Plugin.Plugin.LoadFromFile(PluginDir, Name);
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile(PluginDir, Name.ToLower()); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile("../" + PluginDir, Name); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile("../" + PluginDir, Name.ToLower()); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile(_RootDir + "/" + PluginDir, Name); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile(_RootDir + "/" + PluginDir, Name.ToLower()); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile(_RootDir + "/bin/" + PluginDir, Name); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile(_RootDir + "/bin/" + PluginDir, Name.ToLower()); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile("bin/" + PluginDir, Name); }
                if (Plugin == null) { Plugin = Fact.Plugin.Plugin.LoadFromFile("bin/" + PluginDir, Name.ToLower()); }
                return Plugin;
            }
            catch { return null; }
        }

        public static void Add_Worker(Worker.Worker Worker)
        {
            lock (_Tasks)
            {
                if (_Tasks.Contains(Worker)) { return; }
                _Tasks.Enqueue(Worker);
            }
        }

        public static string DoScript(string script, params string[] args)
        {
            string cmdline = "";
            for (int n = 0; n < args.Length; n++)
            {
                cmdline += args[n] + " ";
            }
            cmdline = cmdline.Trim();
            try
            {
                string file = script;
                if (!System.IO.File.Exists(file)) { file = _RootDir + "/" + script; }
                if (!System.IO.File.Exists(file)) { file = _RootDir + "/Scripts/" + script; }
                if (!System.IO.File.Exists(file)) { return Fact.Runtime.Process.Execute(file, cmdline).StdOut; }
            }
            catch { return ""; }
            return "";
        }

        void _work()
        {
            while (true)
            {
                bool done = false;
				try
				{
                    
	                Worker.Worker worker = null;
	                lock (_Tasks)
	                {
	                    if (_Tasks.Count > 0)
	                    {
	                        worker = _Tasks.Dequeue();
                            done = true;
	                    }
	                }
	                if (worker != null)
	                {
						try { worker.Setup(); } catch { }
						try { worker.Start(); } catch { }
						try {  worker.Shutdown(); } catch { }
	                }
				}
				catch { }

                if (!done)
                {
                    System.Threading.Thread.Sleep(100);
                }
            }
        }
        static List<string> _Managers = new List<string>();
        public static void RemoveManager(string Name)
        {
            try
            {
                lock (_Managers)
                {
                    _Managers.Remove(Name);
                }
            }
            catch { }
        }

        public static void AddManager(string Name)
        {
            lock (_Managers)
            {
                if (Name != "" && !_Managers.Contains(Name))
                {
                    _Managers.Add(Name);
                }
            }
        }

        public static List<string> GetManagers()
        {
            lock (_Managers) 
            {
                return new List<string>(_Managers);
            }
            return new List<string>();
        }

        public static void DoCommand(string Name, string Args)
        {
            switch (Name)
            {
                case "StartNewManager": StartNewManagerAsync(); return;
                case "ShutdownManager": ShutdownManagerAsync(Args); return;
                case "CreateSubmission": CreateSubmissionAsync(Args); return;
                default: return;
            }
        }

        public static void ShutdownManagerAsync(string Manager)
        {
            string value = Manager;
            System.Threading.Thread thread = new System.Threading.Thread(
                () => { string cpy = value;  ShutdownManager(cpy); });
            thread.IsBackground = true;
            thread.Start();
        }

        public static void ShutdownManager(string Manager)
        {
            try
            {
                FactWebPage global = new FactWebPage(_RootDir + "/Config/global.fw");
                string manager = DoScript(global["on_shutdown_manager"], Manager);
                RemoveManager(manager);
            }
            catch { }
        }

        public static void StartNewManagerAsync()
        {
            System.Threading.Thread thread = new System.Threading.Thread(
                () => { StartNewManager(); });
            thread.IsBackground = true;
            thread.Start();
        }

        public static void StartNewManager()
        {
            try
            {
                FactWebPage global = new FactWebPage(_RootDir + "/Config/global.fw");
                string manager = DoScript(global["on_create_manager"]);
                AddManager(manager);
            }
            catch { }
        }

        static Fact.Plugin.Plugin _Manager = null;
        static Fact.Plugin.Plugin _PackageCreate = null;
		static Fact.Plugin.Plugin _PackageRemove = null;

        static string _Error = "";
        static public string DisplayError() 
        {
            if (_Error != "")
            {
                return Tools.Log.GenerateErrorPanel("Server error", _Error);
            }
            return "";
        }

        static public void CreateSubmissionAsync(string Directory)
        {
            string value = Directory;
            System.Threading.Thread thread = new System.Threading.Thread(
                () => { string cpy = value;  CreateSubmission(cpy); });
            thread.IsBackground = true;
            thread.Start();
        }

		static string _ComputeFileHash(string File)
		{
			try
			{
				byte[] hash = System.Security.Cryptography.SHA512.Create ().ComputeHash (System.IO.File.ReadAllBytes(File));
				string hashs = "";
				for (int n = 0; n < hash.Length; n++)
				{
					hashs += hash [n].ToString () + ":";
				}
				return hashs;
			}
			catch {

			}
			return "";
		}

		static public string ExistingFile(string File, string Directory)
		{
			string hash = _ComputeFileHash (File);
			string filename = System.IO.Path.GetFileNameWithoutExtension (File);
			Fact.Log.Info ("Looking for an existing hash for " + filename);
			foreach (string dir in System.IO.Directory.GetDirectories(Directory + "/Submissions/"))
			{
				try
				{
					string dirname = System.IO.Path.GetFileNameWithoutExtension (dir);
					if (System.IO.Directory.Exists (Directory + "/Artifacts/" + dirname))
					{
						if (System.IO.File.Exists (Directory + "/Artifacts/" + dirname + "/" + filename + ".ff")) 
						{
							FactWebPage page = new FactWebPage (Directory + "/Artifacts/" + dirname + "/" + filename + ".fw");
							if (page.GetPropertyAsInteger ("__total") == 0 &&
                                page["__empty"] != "yes")
                            { continue; }
							if (!System.IO.File.Exists (Directory + "/Submissions/" + dirname + "/" + filename + ".hash")) 
							{
								Fact.Log.Warning ("Missing hash file:\"" + Directory + "/Submissions/" + dirname + "/" + filename + ".hash" + "\"");
								Fact.Log.Warning ("Server files may be corrupted");
								try
								{
									System.IO.File.WriteAllText(
										Directory + "/Submissions/" + dirname + "/" + filename + ".hash",
										_ComputeFileHash(Directory + "/Submissions/" + dirname + "/" + filename + ".ff"));
								}
								catch
								{
								}
							}

							if (System.IO.File.Exists (Directory + "/Submissions/" + dirname + "/" + filename + ".hash")) 
							{
								string oldhash = System.IO.File.ReadAllText(Directory + "/Submissions/" + dirname + "/" + filename + ".hash");
								if (oldhash.Trim () == hash)
								{
									Fact.Log.Info ("The fact package submitted match an old fact package");
									return Directory + "/Artifacts/" + dirname + "/" + filename + ".ff";
								}
							}
						}
					}
				}
				catch { }
			}
			return "";
		}
        static public void CreateSubmission(string Directory)
        {
            string SubmissionName = Directory;

            Directory = _RootDir + "/Projects/" + Directory;
            try
            {
                
                DateTime time = DateTime.Now;
                string month = time.Month.ToString(); if (month.Length == 1) { month = "0" + month; }
                string day = time.Day.ToString(); if (day.Length == 1) { day = "0" + day; }
                string hour = time.Hour.ToString(); if (hour.Length == 1) { hour = "0" + hour; }
                string minutes = time.Minute.ToString(); if (minutes.Length == 1) { minutes = "0" + minutes; }
                string second = time.Second.ToString(); if (second.Length == 1) { second = "0" + second; }
                string name =
                    "y_" + time.Year.ToString() +
                    "_m_" + month +
                    "_d_" + day +
                    "_h_" + hour +
                    "_m_" + minutes +
                    "_s_" + second +
                    "_ms_" + time.Millisecond.ToString();
                string nameid = name;
                Fact.Tools.RecursiveMakeDirectory(Directory + "/Submissions/" + name);
                Fact.Tools.RecursiveMakeDirectory(Directory + "/Artifacts/" + name);
                Fact.Tools.RecursiveMakeDirectory(Directory + "/Config");
				int managerIndex = 0;
				int sended = 0;
				int quota = 0;
				bool keepgitinfo = false;
                string SubmissionDir = Directory + "/Submissions/" + name;
				string ArtifactDir = Directory + "/Artifacts/" + name;

				List<string> _ActiveManagers = new List<string>();
				string remove = "";
				try
				{
					lock (_Managers)
					{
						for (int n = 0; n < _Managers.Count; n++)
						{
							if (PingManager(_Managers[n])) { _ActiveManagers.Add(_Managers[n]); Fact.Log.Verbose("Manager " + _Managers[n] + " is responding"); }
						}
					}
				}
				catch{}

                try
                {
                   
                    Fact.Log.Verbose("Cloning repositories ...");
                    FactWebPage global = new FactWebPage(Directory + "/config.fw");
					try
					{
						if (global["quota"] != ""){ quota = int.Parse(global["quota"]); }
					}
					catch{}
					remove = global["remove"];
                    DoScript( global["on_clone_repository"]);
                    try
                    {
                        string repositoriesfile = global["repositories"];
                        if (repositoriesfile == "") { Fact.Log.Error("Invalid repository file"); }
                        if (System.IO.File.Exists(Directory + "/" + repositoriesfile))
                        { repositoriesfile = Directory + "/" + repositoriesfile; }
                        else if (System.IO.File.Exists(_RootDir + "/" + repositoriesfile))
                        { repositoriesfile = _RootDir + "/" + repositoriesfile; }

                        if (System.IO.File.Exists(repositoriesfile))
                        {
                            string[] repositories = System.IO.File.ReadAllLines(repositoriesfile);
                            foreach (string _ in repositories)
                            {
                                Fact.Log.Verbose("Cloning " + _ + " ...");
                                try
                                {
                                    string factpackage =
                                        Directory +
                                        "/Submissions/" +
                                        name + "/" +
                                        System.IO.Path.GetFileNameWithoutExtension(GitName(_)) + ".ff";
                                    _PackageCreate.Run(_, factpackage);



									try
									{
										if (remove != "")
										{
											Fact.Log.Info("Try to remove " + factpackage + "/" + remove + " directory in package");
											_PackageRemove.Run(factpackage + "/" + remove,  factpackage + ".tmp");
											Fact.Tools.RecursiveDelete(factpackage);
											Fact.Tools.Move(factpackage + ".tmp", factpackage);
											Fact.Log.Info("Removed " + remove + " directory in package");
										}

										if (!keepgitinfo)
										{
											Fact.Log.Info("Try to remove " + factpackage + "/.git directory in package");
											_PackageRemove.Run(factpackage + "/.git",  factpackage + ".tmp");
											Fact.Tools.RecursiveDelete(factpackage);
											Fact.Tools.Move(factpackage + ".tmp", factpackage);
											Fact.Log.Info("Removed .git directory in package");
										}
									} catch { }
									try
									{
										if (System.IO.File.Exists(factpackage))
										{
											if (quota > 0)
											{
												System.IO.FileInfo info = new System.IO.FileInfo(factpackage);
												if (quota < info.Length)
												{
													try
													{ System.IO.File.Delete(factpackage); }
													catch{}

													Fact.Log.Info("Overquota " + factpackage + " ignored");
												}
											}
										}
									}
									catch { }
                                }
                                catch { }
                            }
                        }
                        else
                        {
                            if (repositoriesfile == "") { Fact.Log.Error("repository file " + repositoriesfile + " not found"); }
 
                        }
                    }
                    catch { }
                    DoScript(global["on_clone_cloned"]);

                    try
                    {
                        Fact.Log.Verbose("Sending repositories ...");
                        if (_Manager == null)
                        {
                            try
                            {
                                _Manager = LoadPlugin("system/manager", "Manager");
                            }
                            catch { }
                        }
                        if (_Managers != null)
                        {
                            string task = "";
                            if (System.IO.File.Exists(Directory + "/test.ft")) { task = System.IO.File.ReadAllText(Directory + "/test.ft"); }
                            if (System.IO.File.Exists(Directory + "/tests.ft")) { task = System.IO.File.ReadAllText(Directory + "/tests.ft"); }
                            if (System.IO.File.Exists(_RootDir + "/test.ft"))
                            {
                                Fact.Log.Warning("The default testsuite will be used");
                                task = System.IO.File.ReadAllText(_RootDir + "/test.ft"); 
                            }

							int tries = 0;
                                foreach (string file in System.IO.Directory.GetFiles(SubmissionDir))
                                {
							restart:
									managerIndex++;
									if (managerIndex >= _ActiveManagers.Count) 
									{
										Fact.Log.Info("Synchronize...");
										int recieveCount = 0;
										int timeout = 0;
										while (recieveCount +  _ActiveManagers.Count * 4 < sended)
										{
											recieveCount = 0;
											if (timeout == 60)
											{
												Fact.Log.Warning("Global timeout 10 minutes without response");
												sended = recieveCount;
												break;
											}
											foreach (string artifact in System.IO.Directory.GetFiles(ArtifactDir))
											{
												if (artifact.EndsWith(".ff"))
													recieveCount++;
											}
											Fact.Log.Info("Got " + recieveCount.ToString() + "/" + sended.ToString());

											Fact.Log.Info("Wait 10s for pending jobs ...");
											System.Threading.Thread.Sleep(10000);
											timeout++;
										}
										managerIndex = 0;
									}

                                    if (file.EndsWith(".ff"))
                                    {
                                        string package_name = file;
                                        try { package_name = System.IO.Path.GetFileName(package_name); }
                                        catch { }
										string existingfile = ExistingFile(file, Directory);
										if (System.IO.File.Exists(existingfile))
										{
											Fact.Log.Info("reusing " + existingfile);
                                            Fact.Tools.Copy(existingfile, Directory + "/Artifacts/" + nameid + "/" + package_name);
											goto endoftask;
										}
                                        DoScript(global["on_send_repository"], SubmissionName, file, _ActiveManagers[managerIndex]);
                                        if (_ActiveManagers != null)
                                        {
                                            try
                                            {
												

                                                
												if (System.IO.File.Exists(ArtifactDir + "/" + package_name))
												{
												 	Fact.Log.Verbose("Skipping file " + file + "...");
													goto endoftask;
												}
                                                string tempfile = Fact.Internal.Information.GetTempFileName();
                                                string content = task
                                                                .Replace("#SUBMISSION", SubmissionName)
                                                                .Replace("#PACKAGE_NAME", package_name)
                                                                .Replace("#BUILD_ARTIFACTS_DIRECTORY", Directory + "/Artifacts/" + nameid)
                                                                .Replace("#BUILD_SUBMISSION_DIRECTORY", Directory + "/Submissions/" + nameid)
                                                                .Replace("#ARTIFACTS_DIRECTORY", Directory + "/Artifacts")
                                                                .Replace("#SUBMISSION_DIRECTORY", Directory + "/Submissions")
                                                                .Replace("#PROJECT_DIRECTORY", Directory)
                                                                .Replace("#PACKAGE", file)
                                                                .Replace("#MANAGER", _ActiveManagers[managerIndex]);
                                                Fact.Log.Info("Sending task: \n" + content);
                                                System.IO.File.WriteAllText(SubmissionDir + "/" + tempfile + ".ft", content);
                                                int value = _Manager.Run("--ip", _ActiveManagers[managerIndex], "--sendtask", SubmissionDir + "/" + tempfile + ".ft");
                                                if (value != 0)
												{
													tries++;
													Fact.Log.Verbose("Impossible send " + file + " to " +  _ActiveManagers[managerIndex]);
													System.Threading.Thread.Sleep(5000);
													if (tries < _ActiveManagers.Count)
														goto restart;
													else
													{
														Fact.Log.Warning("Ignoring file " + file);
													}
												}
												Fact.Log.Verbose("Send " + file + " to " +  _ActiveManagers[managerIndex]);
												sended++;
                                            }
                                            catch { }
                                        }
                                        
                                        

                                    }
							endoftask:;
								tries = 0;
                                }
                        }
                    }
                    catch { }

                }
                catch { }
            }
            catch { }
        }

		static public void CreateSpecificSubmissionAsync(string Directory, string Build, string FileName)
		{
			string vdir = Directory;
			string vbuild = Build;
			string vfile = FileName;
			System.Threading.Thread thread = new System.Threading.Thread(
				() => { string cdir = vdir; string cbuild = vbuild; string cfile = vfile;  CreateSpecificSubmissionAsync(cdir, cbuild, cfile); });
			thread.IsBackground = true;
			thread.Start();
		}

		static public void CreateSpecificSubmission(string Directory, string Build, string FileName)
		{
			string SubmissionName = Directory;

			Directory = _RootDir + "/Projects/" + Directory;
			string SubmissionDir = Directory + "/Submissions/" + Build;
			string ArtifactDir = Directory + "/Artifacts/" + Build;
			int managerIndex = 0;
			List<string> _ActiveManagers = new List<string>();
			if (_Managers != null)
			{
				string task = "";
				if (System.IO.File.Exists(Directory + "/test.ft")) { task = System.IO.File.ReadAllText(Directory + "/test.ft"); }
				if (System.IO.File.Exists(Directory + "/tests.ft")) { task = System.IO.File.ReadAllText(Directory + "/tests.ft"); }
				if (System.IO.File.Exists(_RootDir + "/test.ft"))
				{
					Fact.Log.Warning("The default testsuite will be used");
					task = System.IO.File.ReadAllText(_RootDir + "/test.ft"); 
				}

				int tries = 0;
				string file = SubmissionDir + "/" + FileName + ".ff";
					restart:
						managerIndex++;
					if (managerIndex >= _ActiveManagers.Count) 
					{
						Fact.Log.Info("Synchronize...");
						return;
					}

					if (file.EndsWith(".ff"))
					{
						string package_name = file;

						try { package_name = System.IO.Path.GetFileName(package_name); }
						catch { }
						string existingfile = ExistingFile(file, Directory);
						if (System.IO.File.Exists(existingfile))
						{
							Fact.Log.Info("reusing " + existingfile);
							Fact.Tools.Copy (existingfile, Directory + "/Artifacts/" + Build + "/" + package_name);
							goto endoftask;
						}
						if (_ActiveManagers != null)
						{
							try
							{
								
								if (System.IO.File.Exists(ArtifactDir + "/" + package_name))
								{
									Fact.Log.Verbose("Skipping file " + file + "...");
									goto endoftask;
								}
								string tempfile = Fact.Internal.Information.GetTempFileName();
								string content = task
									.Replace("#SUBMISSION", SubmissionName)
										.Replace("#PACKAGE_NAME", package_name)
										.Replace("#BUILD_ARTIFACTS_DIRECTORY", Directory + "/Artifacts/" + Build)
										.Replace("#BUILD_SUBMISSION_DIRECTORY", Directory + "/Submissions/" + Build)
										.Replace("#ARTIFACTS_DIRECTORY", Directory + "/Artifacts")
										.Replace("#SUBMISSION_DIRECTORY", Directory + "/Submissions")
										.Replace("#PROJECT_DIRECTORY", Directory)
										.Replace("#PACKAGE", file)
										.Replace("#MANAGER", _ActiveManagers[managerIndex]);
								Fact.Log.Info("Sending task: \n" + content);
								System.IO.File.WriteAllText(SubmissionDir + "/" + tempfile + ".ft", content);
								int value = _Manager.Run("--ip", _ActiveManagers[managerIndex], "--sendtask", SubmissionDir + "/" + tempfile + ".ft");
								if (value != 0)
								{
									tries++;
									Fact.Log.Verbose("Impossible send " + file + " to " +  _ActiveManagers[managerIndex]);
									System.Threading.Thread.Sleep(5000);
									if (tries < _ActiveManagers.Count)
										goto restart;
									else
									{
										Fact.Log.Warning("Ignoring file " + file);
									}
								}
								Fact.Log.Verbose("Send " + file + " to " +  _ActiveManagers[managerIndex]);
							}
							catch { }
						}



					}
					endoftask:;
			}
		}

        static public bool PingManager(string name)
        {
            try
            {
                Fact.Plugin.Plugin Manager = LoadPlugin("system/manager", "Manager");
                return Manager.Run("--ping", "--ip", name) == 0;
            }
            catch { return false; }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            _RootDir = this.Server.MapPath(".");
            _Worker = new System.Threading.Thread(_work);
            _Worker.IsBackground = true;
            _Worker.Start();

            // Initialize directory
            _RootDir = Server.MapPath(".");

            // Initialize the database
            if (!System.IO.File.Exists(_RootDir + "/Config/passwords.fw"))
            {
                try
                {
                    System.IO.File.Create(_RootDir + "/Config/passwords.fw");
                }
                catch { _Error = "Impossible to create the password database"; }
            }

            try
            {
                Global._Database = new DataBase(_RootDir + "/Config/passwords.fw");
            }
            catch { _Error = "Impossible to load the password database"; }
            // Fact load global system manager
            try
            {
                _Manager = LoadPlugin("system/manager", "Manager");
                if (_PackageCreate == null) { throw new NullReferenceException(); }
            }
            catch { _Error = "Impossible to load fact system manager plugin.\n"; }

            // Fact load global package create extension
            try
            {
                _PackageCreate = LoadPlugin("package/create", "Create");
                if (_PackageCreate == null) { throw new NullReferenceException(); }
            }
            catch { _Error += "Impossible to load fact package create plugin.\n"; }

			// Fact load global package create extension
			try
			{
				_PackageRemove = LoadPlugin("package/remove", "Remove");
				if (_PackageCreate == null) { throw new NullReferenceException(); }
			}
			catch { _Error += "Impossible to load fact package remove plugin.\n"; }

            try
            {
                string file = "";
                FactWebPage global = new FactWebPage(_RootDir + "/Config/global.fw");
                string managers = global["managers"];
                foreach (string _ in System.IO.File.ReadAllLines(_RootDir + "/" + managers)) { AddManager(_); }
            }
            catch { _Error += "Impossible to load the manager list.\n"; }
        }

        class DataBase
        {
            string _File = "";
            string[] _Logins = new string[0];
            public DataBase(string File)
            {
                _File = File;
                _Logins = System.IO.File.ReadAllLines(_File);
                
            }
            static string Hash(string login, string password)
            {
                string hash = login + "__fact__login__" + password;
                byte[] hashname = System.Text.Encoding.UTF8.GetBytes(hash);
                System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512.Create();
                byte[] bytes = sha512.ComputeHash(hashname);
                string text = "";
                for (int n = 0; n < bytes.Length; n++)
                {
                    text += ":" + bytes[n].ToString();
                }
                return text;
            }

            public bool Check(string login, string password)
            {
                lock (this)
                {
                    string hash = Hash(login, password);
                    for (int n = 0; n < _Logins.Length; n++)
                    {
                        if (_Logins[n] == hash)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            public bool Empty() { return _Logins.Length == 0; }

            public void AddLogin(string login, string password)
            {
                string hash = Hash(login, password);
                if (!Check(login, password))
                {
                    lock (this)
                    {
                        List<string> values = new List<string>();
                        for (int n = 0; n < _Logins.Length; n++)
                        {
                            values.Add(_Logins[n]);
                        }
                        values.Add(hash);
                        _Logins = values.ToArray();
                        try
                        {
                            System.IO.File.WriteAllLines(_File, _Logins);
                        }
                        catch { }
                    }
                }
                return;
            }
        }

 
        public static bool CheckLogin(string Login, string Password)
        {
            if (_Database == null)
            {
                try { _Database = new DataBase(_RootDir + "/Config/passwords.fw"); }
                catch { }
            }
            if (_Database != null)
            {
                return _Database.Check(Login, Password);
            }
            return false;
        }

        public static void AddLogin(string Login, string Password)
        {
            if (_Database == null)
            {
                try { _Database = new DataBase(_RootDir + "/Config/passwords.fw"); }
                catch { }
            }
            if (_Database != null)
            {
                _Database.AddLogin(Login, Password);
            }
           
        }

        public static bool EmptyLogin()
        {
            if (_Database == null)
            {
                try { _Database = new DataBase(_RootDir + "/Config/passwords.fw"); }
                catch { }
            }
            if (_Database != null)
            {
                return _Database.Empty();
            }
            return true;
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}