﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer.Worker
{
    public class ScoreComputing : Worker
    {
        string _Directory = "";
        public ScoreComputing(string Diretory)
        {
            _Directory = Diretory;
        }
        public ScoreComputing()
        { }

        static string GenerateTestResult(Fact.Test.Result.Result Result, out int passed, out int total)
        {
            passed = 0;
            total = 0;
            string testText = "";
            if (Result is Fact.Test.Result.Group)
            {
                testText += GenerateTestGroup((Fact.Test.Result.Group)Result, out passed, out total);
            }
            else
            {
                total = 1;
                testText += Result.Text + " {\n";
                if (Result is Fact.Test.Result.Passed) { testText += "__type=pass\n"; passed = 1; }
                else if (Result is Fact.Test.Result.Error) { testText += "__type=fail\n"; }
                else { testText += "__type=none\n"; }
                
                testText += "}\n";
            }
            return testText;
        }

        static string GenerateTestGroup(Fact.Test.Result.Group Group, out int passed, out int total)
        {
            passed = 0;
            total = 0;
            string gp = Group.Text + " {\n";
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                int subtest_passed = 0;
                int subtest_total = 0;
                gp += GenerateTestResult(result, out subtest_passed, out subtest_total);
                passed += subtest_passed;
                total += subtest_total;
            }
            int score = 0;
            if (total == 0)
            { score = 0; }
            else
            { score = (passed * 100) / (total); }
            gp += "__score=" + score.ToString() + "\n";
            gp += "__total=" + total.ToString() + "\n";
            gp += "__passed=" + passed.ToString() + "\n";

			gp += "__type=group\n";
            gp += "}\n";
            return gp;
        }

        public override void Start()
        {
			int global_pass = 0;
			int global_total = 0;

            foreach (string build in System.IO.Directory.GetDirectories(_Directory + "/Artifacts"))
            {
                foreach (string file in System.IO.Directory.GetFiles(build))
                {
                    try
                    {
                        // Directory + "/Artifacts/" + values[n] + "/score.fw";
                        if (file.EndsWith(".ff"))
                        {
                            string rawFileName = System.IO.Path.GetFileNameWithoutExtension(file);
                            int scoremax = 0;
                            int score = 0;
                            if (!System.IO.File.Exists((build + "/" + rawFileName + ".fw")))
                            {
                                Fact.Processing.Project project = new Fact.Processing.Project(file);
                                project.Load(file);
                                string projectdata = "";

                                foreach (Fact.Test.Result.Result result in project.GetTestResults())
                                {
                                    int total, passed;
									projectdata += GenerateTestResult(result, out passed, out  total);
									scoremax += total;
									score += passed;
                                }
								if (scoremax == 0)
								{
									projectdata += "__score=0\n";
									projectdata += "__total=0\n";
									projectdata += "__passed=0\n";
                                    if (project.Empty)
                                    {
                                        projectdata += "__empty=yes\n";
                                    }
                                    System.IO.File.WriteAllText(build + "/" + rawFileName + ".fw", projectdata);
								}
								else
								{
									int glob = (score * 100) / scoremax;


									projectdata += "__score=" + glob.ToString() + "\n";
									projectdata += "__total=" + scoremax.ToString() + "\n";
									projectdata += "__passed=" + score.ToString() + "\n";
                                    if (project.Empty)
                                    {
                                        projectdata += "__empty=yes\n";
                                    }
	                                System.IO.File.WriteAllText(build + "/" + rawFileName + ".fw", projectdata);
								}
                            }
                        }
                    }

                    catch { }
                }

                if (!System.IO.File.Exists("score.fw"))
                {
     
                }
            }
        }
    }
}
