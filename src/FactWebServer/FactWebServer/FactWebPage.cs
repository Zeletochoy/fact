﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer
{
    public class FactWebPage
    {
        public class Group
        {
            System.Collections.Generic.Dictionary<string, string> _properties = new System.Collections.Generic.Dictionary<string, string>();
            public string this[string property]
            {
                get { if (_properties.ContainsKey(property)) { return _properties[property]; } return ""; }
            }

            System.Collections.Generic.List<Group> _Groups = new System.Collections.Generic.List<Group>();
            public System.Collections.ObjectModel.ReadOnlyCollection<Group> Groups { get { return _Groups.AsReadOnly(); } } 

            internal void _ParseGroup(string[] lines, ref int index)
            {
                for (; index < lines.Length; index++)
                {
                    if (lines[index].Trim() == "}") { return; }
                    else if (lines[index].Trim().EndsWith("{")) 
                    {
                        string[] p = lines[index].Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
                        index++;
                        Group subgroup = new Group();
                        subgroup.Name = p[0];
                        subgroup._ParseGroup(lines, ref index);
                        _Groups.Add(subgroup);
                    }
                    else
                    {
                        string[] p = lines[index].Split('=');
                        if (p.Length == 2)
                        {
                            _properties[p[0].Trim().ToLower()] = p[1].Trim().ToLower();
                        }
                    }
                }
            }

            public string Name { get; set; }
        }

        System.Collections.Generic.Dictionary<string, string> _properties = new System.Collections.Generic.Dictionary<string, string>();

        System.Collections.Generic.List<Group> _Groups = new System.Collections.Generic.List<Group>();
        public System.Collections.ObjectModel.ReadOnlyCollection<Group> Groups { get { return _Groups.AsReadOnly(); } } 

        public FactWebPage(string file)
        {
            try
            {
                if (System.IO.File.Exists(file))
                {
                    string[] lines = System.IO.File.ReadAllLines(file);
                    for (int n = 0; n < lines.Length; n++)
                    {
                        if (lines[n].Trim().EndsWith("{"))
                        {
                            string[] p = lines[n].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            n++;
                            Group subgroup = new Group();
                            subgroup.Name = p[0];
                            subgroup._ParseGroup(lines, ref n);
                            _Groups.Add(subgroup);
                        }
                        else
                        {
                            string[] p = lines[n].Split('=');
                            if (p.Length == 2)
                            {
                                _properties[p[0].Trim().ToLower()] = p[1].Trim().ToLower();
                            }
                        }
                    }
                }
            }
            catch { }
        }

		public int GetPropertyAsInteger(string property)
		{
			try
			{
				string value = _properties [property];
				int score = 0;
				if (int.TryParse (value, out score))
				{
					return score;
				}
				return 0;
			}
			catch { }
			return 0;
		}

        public string this[string property]
        {
            get { if (_properties.ContainsKey(property)) { return _properties[property]; } return ""; }
        }


    }
}
