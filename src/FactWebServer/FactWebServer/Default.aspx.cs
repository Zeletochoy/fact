﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer
{
    public partial class _Default : System.Web.UI.Page
    {
        public string FindProjectDir(string name)
        {
            string root = this.Server.MapPath(".");
            if (!System.IO.Directory.Exists(root + "/Projects"))
            {
                return "";
            }
            try
            {
                foreach (string folder in System.IO.Directory.GetDirectories(root + "/Projects"))
                {
                    if (System.IO.Path.GetFileNameWithoutExtension(folder) == name)
                    { return folder; }

                }
            }
            catch
            {
               
            }
            try
            {
                foreach (string folder in System.IO.Directory.GetDirectories(root + "/Projects"))
                {
                    if (System.IO.Path.GetFileNameWithoutExtension(folder).ToLower() == name)
                    { return folder; }
                }
            }
            catch { }
            return "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool isAdmin = false;
            string rootDir = this.Server.MapPath(".");
			if (System.IO.File.Exists(rootDir + "/Config/admin.fw"))
            {
                FactWebPage adminpage = new FactWebPage(rootDir + "/Config/admin.fw");
                if (!isAdmin && adminpage["ip"] != "")
                {
                    System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(adminpage["ip"]);
					string userIP = Request.UserHostAddress;
					if (regex.Match (userIP).Success)
					{
						isAdmin = true;
					}
                }
                if (!isAdmin && adminpage["nocheck"] == "yes")
                {
                    isAdmin = true;
                }
            }
            else
            {
                isAdmin = true;
            }

            if (!isAdmin)
            {
                if (Global.EmptyLogin()) { isAdmin = true; }
                else
                {
                    try
                    {
                        if ((bool)(Session["isadmin"])) { isAdmin = true; }
                    }
                    catch { }
                }
            }

			if (isAdmin)
			{
				Session.Timeout = 240;
			}
            string request = Page.Request.Url.ToString();
            string tag = "";
            string baseName = "";
            for (int n = request.Length - 1; n > 0; n--)
            {
                if (request[n] == '/') { break ; }
                tag = request[n] + tag;
            }
            if (tag.StartsWith("Default.aspx?")) { tag = tag.Substring("Default.aspx?".Length); }
            if (tag.StartsWith("Default.aspx")) { tag = tag.Substring("Default.aspx".Length); }
            if (tag.StartsWith("default.aspx?")) { tag = tag.Substring("default.aspx?".Length); }
            if (tag.StartsWith("default.aspx")) { tag = tag.Substring("default.aspx".Length); }
            baseName = request.Substring(0, request.Length - tag.Length);
            if (baseName.EndsWith("?")) { baseName = baseName.Substring(0, baseName.Length - 1); }
            form1.InnerHtml = "";
            form1.InnerHtml += "<div class=\"container theme-showcase\">";
            //form1.InnerHtml += "<div class=\"page-header\">";
            //form1.InnerHtml += "<div class=\"container\">";
            form1.InnerHtml += "<div class=\"container bs-docs-container\">";

            if (isAdmin)
            {
                if (Request.Form["create_login"] != null && Request.Form["create_password"] != null)
                {
                    if (Request.Form["create_login"] != "" && Request.Form["create_password"] != "")
                    {
                        Global.AddLogin(
                            Request.Form["create_login"],
                            Request.Form["create_password"]);
                    }
                }
            }


            if (tag != "")
            {
                if (tag.Contains("="))
                {
                    string[] urlrequest = tag.Split('=');
                    if (urlrequest.Length != 2) { form1.InnerHtml += Tools.Log.GenerateErrorPanel("Invalid request", "Page not found"); }
                    switch (urlrequest[0])
                    {
                        case "command":
                            if (isAdmin)
                            {
                                string[] subvalue = urlrequest[1].Split(new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
                                if (subvalue.Length > 1) { Global.DoCommand(subvalue[0], subvalue[1]); }
                                else { Global.DoCommand(subvalue[0], ""); }
                                this.Response.Redirect("Default.aspx");
                            }
                            break;
                        case "admin":
                            if (isAdmin)
                            {
                                form1.InnerHtml += Global.DisplayError();
                                form1.InnerHtml += Display.Admin.AdminPanel();
                                form1.InnerHtml += Display.Admin.AmdinCreate();
                            }
                            break;
                        case "login":
                            {
                                form1.InnerHtml += Display.Admin.AmdinLogin();
                                if (Request.Form["login"] != null && Request.Form["password"] != null)
                                {
                                    if (Request.Form["login"] != "" && Request.Form["password"] != "")
                                    {
                                        if (Global.CheckLogin(Request.Form["login"], Request.Form["password"]))
                                        {
                                            Session.Add("isadmin", true);
                                            isAdmin = true;
                                            this.Response.Redirect("Default.aspx");
                                        }
                                        else
                                        {
                                            form1.InnerHtml += Tools.Log.GenerateWarningPanel("Invalid login or username", "Invalid login or username");
                                        }
                                    }
                                }
                            }
                            break;
                        case "deleteproject":
                            {
                                if (isAdmin)
                                {
                                    string[] subvalue = urlrequest[1].Split(new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
                                    if (subvalue.Length == 1)
                                    {
                                        Fact.Tools.RecursiveDelete(rootDir + "/Projects/" + subvalue[0]);
                                    }
                                    this.Response.Redirect("Default.aspx");
                                }
                            }
                            break;
						case "restartspecsubmission":
							{
								if (isAdmin) {
									string[] subvalue = urlrequest [1].Split (new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
									if (subvalue.Length == 3) {
										Fact.Tools.RecursiveDelete (rootDir + "/Projects/" + subvalue [0] + "/Artifacts/" + subvalue [1] + "/" + subvalue [2] + ".ff");
										Fact.Tools.RecursiveDelete (rootDir + "/Projects/" + subvalue [0] + "/Artifacts/" + subvalue [1] + "/" + subvalue [2] + ".fw");
										Fact.Tools.RecursiveDelete (rootDir + "/Projects/" + subvalue [0] + "/Artifacts/" + subvalue [1] + "/" + subvalue [2] + ".hash");
										Global.CreateSpecificSubmissionAsync (subvalue [0], subvalue [1], subvalue [2]);
									}
									this.Response.Redirect ("Default.aspx");
								}
							}
							break;
                        case "deletesubmission":
                            {
                                if (isAdmin)
                                {
                                    string[] subvalue = urlrequest[1].Split(new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
                                    if (subvalue.Length == 2)
                                    {
                                        Fact.Tools.RecursiveDelete(rootDir + "/Projects/" + subvalue[0] + "/Submissions/" + subvalue[1]);
                                        Fact.Tools.RecursiveDelete(rootDir + "/Projects/" + subvalue[0] + "/Artifacts/" + subvalue[1]);
                                    }
                                    this.Response.Redirect("Default.aspx");
                                }
                            }
                            break;
                        case "project":
                            if (Request.Form["test_script"] != null)
                            {
                                if (Request.Form["test_script"] != "")
                                {
                                    string dir = FindProjectDir(urlrequest[1]);
                                    try
                                    {
                                        System.IO.File.WriteAllText(dir + "/test.ft", Request.Form["test_script"]);
                                    }
                                    catch 
                                    {
                                        form1.InnerHtml +=  Tools.Log.GenerateWarningPanel("Warning", "Can't write on " + dir + "/test.ft");
                                    }
                                }
                            }
                            form1.InnerHtml += Display.Project.Details(FindProjectDir(urlrequest[1]), isAdmin);
                            break;
                        case "projectbuild":
                            {
                                string[] subvalue = urlrequest[1].Split(new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
                                if (subvalue.Length == 2)
                                {
                                    form1.InnerHtml += Display.Project.Build(FindProjectDir(subvalue[0]), subvalue[1], isAdmin);

                                }
                            }
                            break;
                        case "projectsubmit":
                            {
                                string[] subvalue = urlrequest[1].Split(new string[] { "---" }, StringSplitOptions.RemoveEmptyEntries);
                                if (subvalue.Length == 3)
                                {
                                    form1.InnerHtml += Display.Project.Submission(FindProjectDir(subvalue[0]), subvalue[1], subvalue[2]);

                                }
                            }
                            break;
                    }
                }
                else
                {
                    form1.InnerHtml += Tools.Log.GenerateErrorPanel("Invalid request", "Page not found");
                }
            }
            else
            {

                if (isAdmin)
                {
                    
                    if (Request.Form["create_project_name"] != null)
                    {
                        if (Request.Form["create_project_name"] != "")
                        {
                            string name = Request.Form["create_project_name"];
                            if (!System.IO.Directory.Exists(rootDir + "/Projects/" + name))
                            {
                                Fact.Tools.RecursiveMakeDirectory(rootDir + "/Projects/" + name);
                            }

                        }
                    }

                }
                if (isAdmin)
                {
                    
                    form1.InnerHtml += Tools.Button.GenerateButton("Administration panel", "Default.aspx?admin=global");
                    
                    form1.InnerHtml += "  <h1>Create project</h1>\n";
                    form1.InnerHtml += "<form method=\"post\">";
                    form1.InnerHtml += "<div class=\"input-group\">";
                    form1.InnerHtml += "<span class=\"input-group-addon\">Project name</span>";
                    form1.InnerHtml += "<input type=\"text\" name=\"create_project_name\" class=\"form-control\" placeholder=\"Project name\">";
                    form1.InnerHtml += "</div>";
                    form1.InnerHtml += "<button type=\"submit\" class=\"btn btn-default\">Create</button>";
                    form1.InnerHtml += "</form>";
                }

                string root = this.Server.MapPath(".");
                if (!System.IO.Directory.Exists(root + "/Projects"))
                {
                    form1.InnerHtml += Tools.Log.GenerateWarningPanel("Directory not found", "The directory '/Projects' has not been found adn will be create now.");
                    try
                    {
                        System.IO.Directory.CreateDirectory(root + "/Projects");
                    }
                    catch (Exception exp)
                    {
                        form1.InnerHtml += Tools.Log.GenerateErrorPanel("Directory not created", exp.ToString() + "Impossible to create the directory '/Projects'.");
                        return;

                    }
                }
                try
                {
                    foreach (string folder in System.IO.Directory.GetDirectories(root + "/Projects"))
                    {
                        try
                        {
                            form1.InnerHtml += Display.Project.Summary(folder, isAdmin);
                        }
                        catch { }
                    }
                }
                catch
                {
                    form1.InnerHtml += Tools.Log.GenerateWarningPanel("Projects not found", "Impossible to load all the projects stored in '/projects'.");
                }



            }


            form1.InnerHtml += "</div>";
            form1.InnerHtml += "</div>";
        }
    }
}
