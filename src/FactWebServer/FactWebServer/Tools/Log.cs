﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer.Tools
{
    public class Log
    {
        public static string GenerateErrorPanel(string Title, string Message)
        {
            return "<div class=\"panel panel-danger\">" +
            "<div class=\"panel-heading\">" +
            "<h3 class=\"panel-title\">" + Title + "</h3>" +
            "</div>" +
            "<div class=\"panel-body\">" +
            Message +
            "</div>" +
            "</div>";
        }
        public static string GenerateWarningPanel(string Title, string Message)
        {
            return "<div class=\"panel panel-warning\">" +
            "<div class=\"panel-heading\">" +
            "<h3 class=\"panel-title\">" + Title + "</h3>" +
            "</div>" +
            "<div class=\"panel-body\">" +
            Message +
            "</div>" +
            "</div>";
        }

        
    }
}
