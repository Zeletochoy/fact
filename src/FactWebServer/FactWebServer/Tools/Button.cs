﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer.Tools
{
    public class Button
    {
        public static string GenerateButton(string Text, string URL)
        {
            return "<p><a class=\"btn btn-default\" role=\"button\" href=\"" + URL + "\">" + Text + "</a></p>";
        }

        public static string GenerateSmallButton(string Text, string URL)
        {
            return "<p><a class=\"btn btn-default btn-xs\" role=\"button\" href=\"" + URL + "\">" + Text + "</a></p>";
        }
    }
}
