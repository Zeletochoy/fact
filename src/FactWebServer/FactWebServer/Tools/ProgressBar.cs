﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace FactWebServer.Tools
{
    public class ProgressBar
    {
        public enum Type
        {
            Success,
            Warning,
            Danger,
            Info,
        }
        public static string GenerateStripedProgressBar(params System.Collections.Generic.KeyValuePair<Type, int>[] values)
        {
            return GenerateStripedProgressBar(100, values);
        }
        public static string GenerateStripedProgressBar(int max, params System.Collections.Generic.KeyValuePair<Type, int>[] values)
        {
            float scaler = 1.0f;
            if (max > 0) { scaler = 100.0f / (float)max; }
            string resut = "<div class=\"progress progress-striped active\">";
            resut += "";
            for (int n = 0; n < values.Length; n++)
            {
                resut += "<div class=\"progress-bar ";
                switch (values[n].Key)
                {
                    case Type.Success: resut += "progress-bar-success"; break;
                    case Type.Warning: resut += "progress-bar-warning"; break;
                    case Type.Danger: resut += "progress-bar-danger"; break;
                }
                if (values[n].Value >= max) { resut += "\" style=\"width: 100%\"></div>"; }
                else { resut += "\" style=\"width: " + (int)((float)values[n].Value * scaler) + "%\"></div>"; }
            }
            resut += "</div>";
            return resut;
        }
        public static string GenerateProgressBar(params System.Collections.Generic.KeyValuePair<Type, int>[] values)
        {
            return GenerateProgressBar(100, values);
        }
        public static string GenerateProgressBar(int max, params System.Collections.Generic.KeyValuePair<Type, int>[] values)
        {
            float scaler = 1.0f;
            if (max > 0) { scaler = 100.0f / (float)max; }
            string resut = "<div class=\"progress\">";
            resut += "";
            for (int n = 0; n < values.Length; n++)
            {
                resut += "<div class=\"progress-bar ";
                switch (values[n].Key)
                {
                    case Type.Success: resut += "progress-bar-success"; break;
                    case Type.Warning: resut += "progress-bar-warning"; break;
                    case Type.Danger: resut += "progress-bar-danger"; break;
                }

                if (values[n].Value >= max) { resut += "\" style=\"width: 100%\"></div>"; }
                else { resut += "\" style=\"width: " + (int)((float)values[n].Value * scaler) + "%\"></div>"; }
            }
            resut += "</div>";
            return resut;
        }
    }
}
