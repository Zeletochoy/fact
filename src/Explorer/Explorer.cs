﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Explorer
{
    public class Explorer : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Explorer";
            }
        }

        public override int Run(string[] args)
        {
            MainView View = new MainView();
            System.Windows.Forms.Application.Run(View);
            return 0;
        }
    }
}
