﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install
{
    class Package
    {
        List<string> _Dependency = new List<string>();
        List<KeyValuePair<string, string>> _Files = new List<KeyValuePair<string, string>>();

        string _Hash = "";
        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }
        string _File = "";
        public string File { get { return _File; } }

        public Package(string File)
        {
            _File = File;
            if (!System.IO.File.Exists(File)) { return; }
            string[] lines = System.IO.File.ReadAllLines(File);
            if (lines.Length == 0) { return; }
            _Name = lines[0].Trim();
            for (int n = 1; n < lines.Length - 4; n += 5)
            {
                string hash = lines[n];
                string path = lines[n + 1];
                _Files.Add(new KeyValuePair<string, string>(hash, path));
                //FIX ME load dependencies
            }
        }

        public void AddFile(string Hash, string Location)
        {
            _Files.Add(new KeyValuePair<string, string>(Hash, System.IO.Path.GetFullPath(Location)));
        }

        public List<KeyValuePair<string, string>> GetFiles()
        {
            return new List<KeyValuePair<string, string>>(_Files);
        }

        public void Save()
        {
            if (System.IO.File.Exists(_File))
            { System.IO.File.Delete(_File); }
            List<string> lines = new List<string>();
            lines.Add(_Name);
            foreach (KeyValuePair<string, string> files in _Files)
            {
                lines.Add(files.Key);
                lines.Add(files.Value);
                lines.Add("-");
                lines.Add("-");
                lines.Add("-");
            }
            System.IO.File.WriteAllLines(_File, lines.ToArray());
        }
    }
}
