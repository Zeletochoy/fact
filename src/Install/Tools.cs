﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install
{
    static class Tools
    {
        static public string DefaultInstallDir()
        {
            try
            {
                string relativeDirectory = System.AppDomain.CurrentDomain.BaseDirectory;

                if (System.IO.File.Exists(relativeDirectory + "/FactExe.exe"))
                {
                    if (System.IO.Directory.Exists(relativeDirectory + "/../bin"))
                    {
                        if (System.IO.Directory.Exists(relativeDirectory + "/../../bin"))
                        {
                            string fullpath = System.IO.Path.GetFullPath(relativeDirectory + "../..");
                            if (System.IO.File.Exists(fullpath))
                            { return fullpath; }
                        }
                    }
                }
            }
            catch { }
            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                if (System.IO.Directory.Exists(@"C:\Program Files")) { return @"C:\Program Files\Fact"; }
                if (System.IO.Directory.Exists(@"C:\Program Files (x86)")) { return @"C:\Program Files (x86)\Fact"; }
            }
            else if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
            {
                if (System.IO.Directory.Exists(@"/usr/share")) { return @"/usr/share/fact"; }
                if (System.IO.Directory.Exists(@"/usr/bin")) { return @"/usr/bin/fact"; }
                if (System.IO.Directory.Exists(@"/bin")) { return @"/bin/fact"; }
            }
            else if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
            {
                if (System.IO.Directory.Exists(@"/usr/share")) { return @"/usr/share/fact"; }
                if (System.IO.Directory.Exists(@"/usr/bin")) { return @"/usr/bin/fact"; }
                if (System.IO.Directory.Exists(@"/bin")) { return @"/bin/fact"; }
            }
            return "/bin/fact";
        }

        static public void RegisterAssembly(string Name, string Location)
        {
            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                Fact.Log.Verbose("Registering " + Name + " into the Hive");
                // Classic update
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\.NETFramework\AssemblyFolders\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }
                // WOW64 update
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\.NETFramework\AssemblyFolders\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }

                // 3.5 hive
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Microsoft\.NETFramework\v3.5\AssemblyFoldersEx\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }
                {
                    string Hive = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\.NETFramework\v3.5\AssemblyFoldersEx\" + Name;
                    string Content = Location;
                    try
                    {
                        Microsoft.Win32.Registry.SetValue(Hive, "", Content);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to update the key: " + Hive);
                    }
                }
            }
        }

        static public void AddToPath(string Location)
        {
            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                string paths = System.Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.Machine);
                string[] binpaths = paths.Split(';');
                List<string> pathtoremove = new List<string>();
                foreach (string binpath in binpaths)
                {
                    if (binpath.ToLower().Trim() == (Location).ToLower().Trim())
                    {
                        Fact.Log.Verbose("The directory " + Location + " is already in the path");
                        return;
                    }
                }
                if (!paths.EndsWith(";")) { paths += ";"; }
                paths += Location;
                System.Environment.SetEnvironmentVariable("PATH", paths, EnvironmentVariableTarget.Machine);
                Fact.Log.Verbose("The directory " + Location + " is now in the path");
            }
        }
    }
}
