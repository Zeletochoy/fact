﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Install
{
    class Install : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Install";
            }
        }

       void EnumerateDistroFile(string Location,  List<string> Files)
       {
           foreach (string file in System.IO.Directory.GetFiles(Location))
           {
               string full = System.IO.Path.GetFullPath(file);
               if (file.EndsWith(".dll") || file.EndsWith(".exe"))
               {
                   if (file.EndsWith(".vshost.exe")) { continue; }
                   Files.Add(full);
               }
           }
           foreach (string directory in System.IO.Directory.GetDirectories(Location))
           {
               EnumerateDistroFile(directory, Files);
           }
       }

        Fact.Processing.Project CreateDistroPackage(string where)
        {
            where = System.IO.Path.GetFullPath(where);
            Fact.Processing.Project project = new Fact.Processing.Project(GetFactPackageName());
            List<string> files = new List<string>();
            EnumerateDistroFile(where, files);
            foreach (string file in files)
            {
                Fact.Processing.File factFile = new Fact.Processing.File(System.IO.File.ReadAllBytes(file), System.IO.Path.GetFileName(file));
                string dir = System.IO.Path.GetDirectoryName(file);
                dir = file.Substring(where.Length);
                if (dir.StartsWith("/")) { dir = dir.Substring(1); }
                project.AddFile("/bin/" + dir, factFile);
            }
            return project;
        }

        string GetFactPackageName()
        {
            return "Fact-" +
                    Fact.Internal.Information.FactVersion().Major + "-" +
                    Fact.Internal.Information.FactVersion().Minor + "-" +
                    Fact.Internal.Information.FactVersion().Build;
        }

        void AddToPathWindows(string Path, EnvironmentVariableTarget Target)
        {
            string paths = System.Environment.GetEnvironmentVariable("PATH", Target);
            string[] binpaths = paths.Split(';');

        }

        void AddToPath(string Path)
        {
            if (Fact.Internal.Information.CurrentOperatingSystem() ==
                Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
            {
                AddToPathWindows(Path, EnvironmentVariableTarget.Machine);
                AddToPathWindows(Path, EnvironmentVariableTarget.User);
                AddToPathWindows(Path, EnvironmentVariableTarget.Process);
            }
        }

        bool InstallPackage(string TargetPackage, string Location)
        {
            if (!System.IO.File.Exists(TargetPackage))
            {
                Fact.Log.Error("Missing package " + TargetPackage);
                return false;
            }
            string rootpackage = Location;
            Fact.Log.Verbose("Checking the package");
            Fact.Processing.Project packageLoaded = new Fact.Processing.Project("");
            try
            {
                packageLoaded.Load(TargetPackage);
            }
            catch
            {
                Fact.Log.Error("This package is not compatible with this version of fact");
                return false;
            }
            string package_hash = packageLoaded.Hash;
            if (System.IO.File.Exists(Location + "/info/packages/" + package_hash))
            {
                Fact.Log.Warning("This package has already been deployed on this system");
                Fact.Log.Warning("Nothing to do");
                return true;
            }

            List<Fact.Processing.File> _binFiles = packageLoaded.GetFiles("/bin", true);
            List<KeyValuePair<string, string>> _InstalledFiles = new List<KeyValuePair<string, string>>();
            // Installing files
            if (_binFiles.Count > 0)
            {
                if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
                {
                    Tools.AddToPath(Location + "/bin");
                }
            }
            Fact.Log.Verbose("Installing " + packageLoaded.Name);

            if (_binFiles.Count > 0)
            {
                Fact.Log.Verbose("Installing binary files");
                foreach (Fact.Processing.File file in _binFiles)
                {
                    string relativepath = file.Directory.Substring("/bin".Length);
                    string path = Location + "/bin/" + relativepath; 
                    if (System.IO.File.Exists(path))
                    {
                        Fact.Log.Verbose("Removing existing file " + path);
                    }
                    if (relativepath.ToLower() == "factexe.exe" || relativepath.ToLower() == "/factexe.exe")
                    {
                        Fact.Log.Verbose("Installing a new vesion of FactExe");
                        Fact.Log.Verbose("Installing file " + path);
                        file.Extract(path);
                        _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, path));
                        Fact.Log.Verbose("Creating short version file " + Location + "/bin/fact");
                        if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.MicrosoftWindows)
                        {
                            try
                            {
                                if (System.IO.File.Exists(Location + "/bin/fact.exe"))
                                {
                                    System.IO.File.Delete(Location + "/bin/fact.exe");
                                }
                            }
                            catch { Fact.Log.Error("Impossinle to update the short version of " + Location + "/bin/fact.exe"); }
                            System.IO.File.Copy(path, Location + "/bin/fact.exe");
                            _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, Location + "/bin/fact.exe"));
                        }
                        else
                        {
                            if (System.IO.File.Exists(Location + "/bin/fact"))
                            {
                                System.IO.File.Delete(Location + "/bin/fact");
                            }
                            System.IO.File.Copy(path, Location + "/bin/fact");
                            if (System.IO.Directory.Exists("/usr/bin"))
                            {
                                string mono = Fact.Internal.Information.GetMonoBinary();
                                if (System.IO.File.Exists(mono))
                                {
                                    mono = System.IO.Path.GetFullPath(mono);
                                    Fact.Log.Verbose("Creating launch script in the bin folder");
                                    try
                                    {
                                        if (System.IO.File.Exists("/usr/bin/fact")) { System.IO.File.Delete("/usr/bin/fact"); }
                                        StringBuilder launchscrip = new StringBuilder();
                                        launchscrip.AppendLine("#! /usr/bin/env sh");
                                        launchscrip.AppendLine(mono + " --runtime=\"v4.0\" " + Location + "/bin/fact" + " $@ ");
                                        System.IO.File.WriteAllText("/usr/bin/fact", launchscrip.ToString());
                                    }
                                    catch { }
                                }
                            }
                            _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, Location + "/bin/fact"));
                        }
                    }
                    else if (relativepath.ToLower() == "fact.dll" || relativepath.ToLower() == "/fact.dll")
                    {
                        Fact.Log.Verbose("Installing a new vesion of Fact Core");
                        Fact.Log.Verbose("Installing file " + path);
                        file.Extract(path);
                        _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, path));
                        Tools.RegisterAssembly("Fact", Location + "/bin");
                    }
                    else
                    {
                        Fact.Log.Verbose("Installing file " + path);
                        file.Extract(path);
                        _InstalledFiles.Add(new KeyValuePair<string, string>(file.ContentHash, path));
                    }

                }
            }
            Fact.Log.Verbose("Updating the information file");
            Fact.Tools.RecursiveMakeDirectory(rootpackage + "/info/packages");
            Package package = new Package(rootpackage + "/info/packages/" + packageLoaded.Hash);
            package.Name = packageLoaded.Name;
            foreach (KeyValuePair<string, string> file in _InstalledFiles)
            {
                package.AddFile(file.Key, file.Value);
            }
            package.Save();

            return true;
        }

        Dictionary<string, Package> LoadPackages(string Location)
        {
            if (!System.IO.Directory.Exists(Location + "/info/packages"))
            {
                return new Dictionary<string,Package>();
            }

            Dictionary<string, Package> _packages = new Dictionary<string, Package>();
            foreach (string file in System.IO.Directory.GetFiles(Location + "/info/packages"))
            {
                Package package = new Package(file);
                if (!_packages.ContainsKey(package.Name))
                {
                    _packages.Add(package.Name, package);
                }
            }
            return _packages;
        }
        bool DeletePackage(string TargetPackage, string Location)
        {
            try
            {
                Dictionary<string, Package> packages = LoadPackages(Location);
                if (packages.ContainsKey(TargetPackage))
                {
                    Fact.Log.Verbose("Removing the package: " + TargetPackage);
                    foreach (KeyValuePair<string, string> file in packages[TargetPackage].GetFiles())
                    {
                        Fact.Log.Verbose("Deleting file: " + file.Value);
                        Fact.Tools.RecursiveDelete(file.Value);
                    }
                    Fact.Tools.RecursiveDelete(packages[TargetPackage].File);
                    return true;
                }
                else
                {
                    Fact.Log.Warning("The package " + TargetPackage + " is not installed and will not be removed");
                    return true;
                }
            }
            catch
            {
                Fact.Log.Error("Impossible to delete the package: " + TargetPackage);
                return false;
            }
        }

        void ListPackages(string Location)
        {
            try
            {
                Dictionary<string, Package> packages = LoadPackages(Location);
                foreach (string pck in packages.Keys)
                {
                    Fact.Log.Verbose("Package: " + pck);
                }
            }
            catch { }
        }
        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();

            CommandLine.AddStringInput("fact-location", "set location of fact (the default one is usually good)", Tools.DefaultInstallDir());
            CommandLine.AddStringInput("package", "the package that should be installed", "");
            CommandLine.AddStringInput("create-package", "Create package that can be distributed (use this as arguement to create a package for the current instance of fact)", "");
            CommandLine.AddBooleanInput("remove", "Remove the package instead of installing it", false);
            CommandLine.AddBooleanInput("list", "List all the package installed on this system", false);

            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");

            if (!CommandLine.Parse(args))
            {
                CommandLine.PrintErrors();
                return 1;
            }
            if (CommandLine["help"].AsBoolean())
            {
                CommandLine.PrintUsage();
                return 0;
            }
            string createPackage = CommandLine["create-package"].AsString();
            if (createPackage != "")
            {
                string relativeDirectory = "";
                if (createPackage == "this") { relativeDirectory = System.AppDomain.CurrentDomain.BaseDirectory; }
                else { relativeDirectory = createPackage; }
                if (!System.IO.Directory.Exists(relativeDirectory))
                {
                    Fact.Log.Error("The specified directory " + relativeDirectory + " does not exist");
                    return 1;
                }
                if (CommandLine["package"].IsDefault)
                {
                    Fact.Log.Error("the option package must be use to specify the output package location");
                    return 1;
                }
                Fact.Log.Verbose("Creating the package");
                try
                {
                    Fact.Processing.Project project = CreateDistroPackage(relativeDirectory);
                    Fact.Log.Verbose("Saving the distribution package");
                    try
                    {
                        project.Save(CommandLine["package"].AsString(), true);
                    }
                    catch 
                    {
                        Fact.Log.Error("Unexpected error while saving he package");
                        return 1;
                    }
                }
                catch 
                {
                    Fact.Log.Error("Unexpected error while creating the package");
                    return 1;
                }
                return 0;
            }
            string location = CommandLine["fact-location"].AsString();
            string package =  CommandLine["package"].AsString();
            if (location == "")
            {
                Fact.Log.Error("Invalid installation directory");
                return 1;
            }

            Fact.Log.Verbose("Creating the folders");
            string rootlocation = location;
            Fact.Tools.RecursiveMakeDirectory(location);
            Fact.Tools.RecursiveMakeDirectory(location + "/bin");
            Fact.Tools.RecursiveMakeDirectory(location + "/info");

            Fact.Log.Verbose("Checking folders");
            if (!System.IO.Directory.Exists(location) ||
                !System.IO.Directory.Exists(location + "/bin") ||
                !System.IO.Directory.Exists(location + "/info"))
            {
                Fact.Log.Error("Impossible to create the installation folders in " + location);
                Fact.Log.Error("Check the permission and try to run this program with higher priviledges");
                return 1;
            }

            if (CommandLine["remove"].AsBoolean())
            {
                 if (!DeletePackage(package, location))
                 {
                     return 1;
                 }
                 return 0;
            }
            else if (CommandLine["list"].AsBoolean())
            {
                ListPackages(location);
                return 0;
            }
            if (!InstallPackage(package, location))
            {
                return 1;
            }
            
            return 0;
        }
    } 
}
