﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactC.Parser
{
    class Scope
    {
        internal static Fact.Code.Condition ParseIf(ref int position, List<Fact.Character.MetaToken> Tokens)
        {
            Fact.Code.Condition Condition = new Fact.Code.Condition();
            // Assume that the first char is a valid if
            position++;
            for (; position < Tokens.Count &&
            (Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
            Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
            (Tokens[position].Length > 0 && Tokens[position][0].Type == Fact.Character.MetaChar.BasicCharType.Comment));
            position++) ;
            // Assume that the a valid '(' is located here
            position++;
            string cond = "";
            for (; position < Tokens.Count; position++)
            {
                if (Tokens[position].Length > 0 &&
                    Tokens[position][0].Type != Fact.Character.MetaChar.BasicCharType.Comment)
                {
                    if (Tokens[position] == ")")
                    {
                        position++;
                        break;
                    }
                    else
                    {
                        cond += Tokens[position].ToString();
                    }
                }
            }
            Condition.ConditionExpr = new Fact.Code.Raw(cond);
            for (; position < Tokens.Count &&
            (Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
            Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
            (Tokens[position].Length > 0 && Tokens[position][0].Type == Fact.Character.MetaChar.BasicCharType.Comment));
            position++) ;

            if (Tokens[position] == "{")
            {
                Condition.OnTrue = ParseScope(ref position, Tokens);
            }
            else
            {
                //FIXME
            }

            for (; position < Tokens.Count &&
            (Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
            Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
            (Tokens[position].Length > 0 && Tokens[position][0].Type == Fact.Character.MetaChar.BasicCharType.Comment));
            position++) ;
            
            if (position < Tokens.Count && Tokens[position] == "else")
            {
                position++;
                
                for (; position < Tokens.Count &&
                    (Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                    Tokens[position].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                    (Tokens[position].Length > 0 && Tokens[position][0].Type == Fact.Character.MetaChar.BasicCharType.Comment));
                    position++) ;

                if (position < Tokens.Count)
                {
                    if (Tokens[position] == "if")
                    {
                        Fact.Code.Scope Scope = new Fact.Code.Scope();
                        Scope.Children.Add(ParseIf(ref position, Tokens));
                        Condition.OnFalse = Scope;
                    }
                    else if (Tokens[position] == "{")
                    {
                        Condition.OnFalse = ParseScope(ref position, Tokens);
                    }
                    else
                    {
                        //FIXME
                    }
                }
            }

            return Condition;
        }
        internal static Fact.Code.Scope ParseScope(ref int position, List<Fact.Character.MetaToken> Tokens)
        {
            Fact.Code.Scope Scope = new Fact.Code.Scope();
            int count = 1;
            // Assume that the first char is a valid {
            position++;
            for (; position < Tokens.Count && count > 0; position++)
            {
                if (Tokens[position].TokenType == Fact.Character.MetaChar.BasicTokenType.KeyWord)
                {
                    if (Tokens[position] == "if")
                    {
                        Scope.Children.Add(ParseIf(ref position, Tokens));
                        position--;
                    }
                    else if (Tokens[position] == "return")
                    {
                        position++;
                        string array = "";
                        for (; position < Tokens.Count; position++)
                        {
                            if (Tokens[position].Length > 0 &&
                                Tokens[position][0].Type != Fact.Character.MetaChar.BasicCharType.Comment)
                            {
                                if (Tokens[position] == ";")
                                {
                                    position++;
                                    break;
                                }
                                else
                                {
                                    array += Tokens[position].ToString();
                                }
                            }
                        }
                        Scope.Children.Add(new Fact.Code.Return(array.Trim()));
                    }
                }
                else if (Tokens[position] == "{") { Scope.Children.Add(ParseScope(ref position, Tokens)); position--; }
                else if (Tokens[position] == "}") { position++; break; }
                else
                {
                    string raw = Tokens[position].ToString().Trim();
                    if (raw != "")
                    {
                        Scope.Children.Add(new Fact.Code.Raw(raw));
                    }
                }
            }
            
            return Scope;
        }
    }
}
