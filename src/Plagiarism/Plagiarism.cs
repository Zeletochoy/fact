﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plagiarism
{
    public class Plagiarism : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Plagiarism";
            }
        }

        void ExtractFunctions(Fact.Code.CodeElement Element, List<Fact.Code.Function> Output)
        {
            if (Element == null) { return; }
            if (Element is Fact.Code.Function) 
            {
                Fact.Code.Function function = Element as Fact.Code.Function;
                if (function.FunctionBody != null) { Output.Add(function); return; }
            }
            if (Element is Fact.Code.Scope)
            {
                Fact.Code.Scope scope = Element as Fact.Code.Scope;
                if (scope.Children != null)
                {
                    foreach (Fact.Code.CodeElement elm in scope.Children)
                    {
                        ExtractFunctions(elm, Output);
                    }
                }
            }
        }

        Fact.Code.Function Purify(Fact.Code.Function Function)
        {
            // TODO
            return Function;
        }

        float Match(string A, string B)
        {
            return 0.0f;
        }

        float Match(Fact.Code.Scope A, string B)
        {
            return 0.0f;
        }

        float Match(Fact.Code.Scope A, Fact.Code.Scope B)
        {
            foreach (Fact.Code.CodeElement elmA in A.Children)
            {
                float max = 0.0f;
                foreach (Fact.Code.CodeElement elmB in B.Children)
                {
                    if (elmA is Fact.Code.Scope)
                    {
                        if (elmB is Fact.Code.Scope)
                        {
                            float score = Match(elmA as Fact.Code.Scope, elmB as Fact.Code.Scope);
                            if (score > max) { max = score; }
                            continue;
                        }
                        else
                        {
                        }
                    }
                    else if (elmA is Fact.Code.Condition)
                    {
                        if (elmB is Fact.Code.Condition)
                        {
                            /*
                            float score_cond = Match((elmA as Fact.Code.Condition).ConditionExpr, (elmB as Fact.Code.Condition).ConditionExpr);
                            float score_body_OnTrue = Match((elmA as Fact.Code.Condition).OnTrue, (elmB as Fact.Code.Condition).OnTrue);
                            float score_body_OnFalse = Match((elmA as Fact.Code.Condition).OnFalse, (elmB as Fact.Code.Condition).OnFalse);
                            float score = score_cond / 3.0f + score_body_OnTrue / 3.0f + score_body_OnFalse / 3.0f;
                        
                             */
                        }
                    }
                    else if (elmA is Fact.Code.Raw)
                    {
                        if (elmB is Fact.Code.Raw)
                        {
                            float score = Match((elmA as Fact.Code.Raw).Data, (elmB as Fact.Code.Raw).Data);
                            if (score > max) { max = score; }
                            continue;
                        }
                    }

                }
            }
            return 0.0f;
        }

        float Match(Fact.Code.Function A, Fact.Code.Function B)
        {
            if (A == null || B == null) { return 0.0f; }
            float value = Match(A, B);
            return value;
        }

        public override int Run(params string[] args)
        {
            Fact.Processing.Project ProjectA = null;
            Fact.Processing.Project ProjectB = null;
            Fact.Processing.Project Ignored = null;
            string Output = "";
            if (args.Length == 3)
            {
                ProjectA = new Fact.Processing.Project(""); ProjectA.Load(args[0]);
                ProjectB = new Fact.Processing.Project(""); ProjectB.Load(args[1]);
                Output = args[2];
            }
            else if (args.Length == 4)
            {
                ProjectA = new Fact.Processing.Project(""); ProjectA.Load(args[0]);
                ProjectB = new Fact.Processing.Project(""); ProjectB.Load(args[1]);
                Ignored = new Fact.Processing.Project(""); Ignored.Load(args[2]);
                Output = args[3];
            }
            else
            {
                Fact.Log.Error("Usage: [ProjectA] [ProjectB] [IgnoredProject] [OutputFile]");
                Fact.Log.Error("       [ProjectA] [ProjectB] [OutputFile]");
                return 1;
            }
            List<Fact.Code.Function> FunctionsA = new List<Fact.Code.Function>();
            List<Fact.Code.Function> FunctionsB = new List<Fact.Code.Function>();
            List<Fact.Code.Function> FunctionsIgnored = new List<Fact.Code.Function>();



            if (ProjectA == null || ProjectB == null) { Fact.Log.Error("Impossible to perform plagiarism check"); return 1; }
            Fact.Log.Verbose("Collecting functions in package " + ProjectA.Name);
            foreach (Fact.Processing.File file in ProjectA) { ExtractFunctions(file.MainBlock, FunctionsA); }
            foreach (Fact.Code.Function function in FunctionsA) { Fact.Log.Verbose("Extracted: " + function.ToString()); }
            Fact.Log.Verbose("Collecting functions in package " + ProjectB.Name);
            foreach (Fact.Processing.File file in ProjectB) { ExtractFunctions(file.MainBlock, FunctionsB); }
            foreach (Fact.Code.Function function in FunctionsB) { Fact.Log.Verbose("Extracted: " + function.ToString()); }
            if (Ignored != null)
            {
                Fact.Log.Verbose("Collecting the ignored code " + Ignored.Name);
                foreach (Fact.Processing.File file in Ignored) { ExtractFunctions(file.MainBlock, FunctionsIgnored); }
                foreach (Fact.Code.Function function in FunctionsIgnored) { Fact.Log.Verbose("Extracted: " + function.ToString()); }
            }
            Fact.Log.Verbose("Purify functions in package " + ProjectA.Name);
            for (int n = 0; n < FunctionsA.Count; n++) { FunctionsA[n] = Purify(FunctionsA[n]); }
            Fact.Log.Verbose("Purify functions in package " + ProjectB.Name);
            for (int n = 0; n < FunctionsB.Count; n++) { FunctionsB[n] = Purify(FunctionsB[n]); }
            if (Ignored != null)
            {
                Fact.Log.Verbose("Purify functions in package " + Ignored.Name);
                for (int n = 0; n < FunctionsIgnored.Count; n++) { FunctionsIgnored[n] = Purify(FunctionsIgnored[n]); }
            }
            Fact.Log.Verbose("Mathing phase");

            
            foreach (Fact.Code.Function functionA in FunctionsA)
            {
                // Check against the target package
                float max = 0.0f;
                Fact.Code.Function similar = null;
                foreach (Fact.Code.Function functionB in FunctionsB)
                {
                    float score = Match(functionA, functionB);
                    if (score > max) { max = score; similar = functionB; }
                }
                if (Ignored != null)
                {
                    // Check against the ignored package
                    float ignoredmax = 0.0f;
                    Fact.Code.Function ignoredsimilar = null;
                    foreach (Fact.Code.Function ignored in FunctionsIgnored)
                    {
                        float score = Match(functionA, ignored);
                        if (score > ignoredmax) { ignoredmax = score; ignoredsimilar = ignored; }
                    }
                    // Hard coded safety marging of 5%
                    if (ignoredmax >= max - 0.05f) { continue; }
                }

                //Save the matching

            }

            return 0;
        }
    }
}
