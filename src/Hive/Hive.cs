﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Hive
{
    public class Hive : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Hive";
            }
        }

        Dictionary<string, byte[]> _RamFragments = new Dictionary<string, byte[]>();
        HashSet<string> _DiskFragment = new HashSet<string>();

        string _DiskFolder = "";


        string ComputeHash(byte[] Data)
        {
            string hash = "";
            System.Security.Cryptography.SHA512 sha = System.Security.Cryptography.SHA512.Create();
            byte[] array = sha.ComputeHash(Data);
            for (int n = 0; n < array.Length; n++)
            {
                hash += array[n].ToString("X2");
            }
            return hash.ToLower();
        }

        void StoreFragment(byte[] Data)
        {
            string Hash = ComputeHash(Data);
            if (_DiskFragment != null) 
            {
                if(!System.IO.Directory.Exists(_DiskFolder + "/" + Hash[0] + "/" + Hash[1]))
                {
                    Fact.Tools.RecursiveMakeDirectory(_DiskFolder + "/" + Hash[0] + "/" + Hash[1]);
                }
                _DiskFragment.Add(Hash);
            }
        }

        void GetFragment()
        {

        }

        public override int Run(params string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("disk-folder", "The folder where the data will be stored on the filesystem (nothing means full ram)", "");
            CommandLine.AddStringInput("disk-space", "The maximal space that will be used on the disk in Mo (default is 1Go)", "1000");
            CommandLine.AddStringInput("ram-space", "The maximal space that will be used in the ram in Mo (default is 1Go)", "1000");
            CommandLine.AddStringInput("server", "The address of the server to use", "");
            CommandLine.AddStringInput("credential", "The credentail to use", "");


            if (!CommandLine.Parse(args))
            {
                CommandLine.PrintErrors();
                return 1;
            }

            if (CommandLine["help"].AsBoolean())
            {
                CommandLine.PrintUsage();
                return 0;
            }

            Fact.Log.Verbose("Creating hive client ...");
            string _CredentialFile = CommandLine["credential"].IsDefault ? "" : CommandLine["credential"];
            Fact.Directory.Credential credential = null;
            string server = CommandLine["server"].AsString();
            if (server == "")
            {
                Fact.Log.Error("No server specified");
                return 1;
            }
            if (_CredentialFile != "")
            {
                try
                {
                    credential = new Fact.Directory.Credential(_CredentialFile);
                }
                catch { Fact.Log.Error("Impossible to load the credential"); return 1; }
            }

            Fact.Network.Client Client = null;

            // We should find a better way than keeping the password in memory
            string Username = "";
            string Password = "";
            if (credential == null)
            {
                Fact.Log.Login(out Username, out Password);
                Client = new Fact.Network.Client(server, Username, Password);
            }
            else
            {
                Client = new Fact.Network.Client(server, credential);
            }

            string token = "";
            Fact.Log.Verbose("Register hive ...");
            try
            {
                token = Client.Call("HiveManager.Register") as String;
            }
            catch
            {
                Fact.Log.Error("Impossible to register the hive");
                return 1;
            }

            return 0;
        }
    }
}
