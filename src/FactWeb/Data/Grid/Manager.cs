﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Data.Grid
{
    class Manager
    {
        string _Address = "";
        string _UID = "";
        string _Name = "";
        public string UID { get { return _UID; } }
        public string Name { get { return _Name; } set { _Name = value; } }
        public string Address { get { return _Address; } set { _Address = value; } }

        public enum Status
        {
            Responding,
            Busy,
            NotResponding,
            Pairing
        }

        public Manager()
        {
            CurentStatus = Status.Pairing;
        }

        public Status CurentStatus { get; set; }

        public bool SendTaskAsFile(string File)
        {
            try
            {
                return Plugins.SystemManager.Run("--ip", _Address, "--sendtask", File) == 0;
            }
            catch { return false; }
        }

        public bool SendTaskAsString(string Task)
        {
            string temp_task = Fact.Internal.Information.GetTempFileName();
            System.IO.File.WriteAllText(temp_task, Task);
            bool execution_result = false;
            try
            {
                execution_result = Plugins.SystemManager.Run("--ip", _Address, "--sendtask", temp_task) == 0;
            }
            catch { execution_result = false; }
            Fact.Tools.RecursiveDelete(temp_task);
            return execution_result;
        }

        public bool Ping()
        {
            if (_Address == "") { return false; }
            try
            {
                bool success = Plugins.SystemManager.Run("--ping", "--ip", _Address) == 0;
                if (success) { CurentStatus = Status.Responding; }
                else { CurentStatus = Status.NotResponding; }
                return success;
            }
            catch { return false; }
        }
    }
}
