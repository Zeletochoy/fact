﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Data
{
    class Manager
    {
        string _GitRepositoryCreationScript = "";
        public string GitRepositoryCreationScript { get { return _GitRepositoryCreationScript; } set { _GitRepositoryCreationScript = value; } }
        public string CreateGitRepository(string RepositoryName) { return CreateRepository(_GitRepositoryCreationScript, RepositoryName); }

        string _SvnRepositoryCreationScript = "";
        public string SvnRepositoryCreationScript { get { return _SvnRepositoryCreationScript; } set { _SvnRepositoryCreationScript = value; } }
        public string CreateSvnRepository(string RepositoryName) { return CreateRepository(_SvnRepositoryCreationScript, RepositoryName); }

        string _FtpRepositoryCreationScript = "";
        public string FtpRepositoryCreationScript { get { return _FtpRepositoryCreationScript; } set { _FtpRepositoryCreationScript = value; } }
        public string CreateFtpRepository(string RepositoryName) { return CreateRepository(_FtpRepositoryCreationScript, RepositoryName); }


        string CreateRepository(string Script, string RepositoryName)
        {
            if (Script == "")
            {
                Fact.Log.Error("Impossible to create the repository " + RepositoryName);
            }
            try
            {
                string result = "";
                using (Fact.Runtime.Process Process = new Fact.Runtime.Process(Script, RepositoryName))
                {
                    result = Process.Run(10000).StdOut;
                }
                return result;
            }
            catch { return ""; }
        }


        static Manager _GlobalManager = new Manager();
        public static Manager GlobalManager { get { return _GlobalManager; } }
        List<Projects.Project> _Projects = new List<Projects.Project>();
        Dictionary<string, List<Projects.Project>> _ProjectsGroup = new Dictionary<string, List<Projects.Project>>();

        List<Grid.Manager> _Managers = new List<Grid.Manager>();
        Dictionary<string, List<Grid.Manager>> _ManagerGroup = new Dictionary<string, List<FactWeb.Data.Grid.Manager>>();


        Elisyan.Authentication.Group _Global;
        public Elisyan.Authentication.Group GlobalGroup { get { return _Global; } }
        Elisyan.Authentication.Group _Students;
        public Elisyan.Authentication.Group StudentGroup { get { return _Students; } }
        public Elisyan.Authentication.Group TeacherGroup { get { return _Teachers; } }
        Elisyan.Authentication.Group _Teachers;
        public Elisyan.Authentication.Group AdminGroup { get { return _Admin; } }
        Elisyan.Authentication.Group _Admin;

        static void GetAllGroupName(string Namespace, Elisyan.Authentication.Group Group, HashSet<string> Result)
        {
            Result.Add(Namespace + "/" + Group.Name);
            foreach (Elisyan.Authentication.Group gp in Group.GetSubGroups().Values)
            { GetAllGroupName(Namespace + "/" + Group.Name, gp, Result); }
        }

        static void GetAllUserName(Elisyan.Authentication.Group Group, HashSet<string> Result)
        {
            foreach (Elisyan.Authentication.User usr in Group.GetUsers().Values)
            {
                if (usr.DisplayedName != "") { Result.Add(usr.Name + "(" + usr.DisplayedName + ")"); }
                else { Result.Add(usr.Name); }
            }
            foreach (Elisyan.Authentication.Group gp in Group.GetSubGroups().Values)
            {
                GetAllUserName(gp, Result);
            }
        }

        public List<string> GetAllStudentName()
        {
            HashSet<string> result = new HashSet<string>();
            GetAllUserName(_Students, result);
            return new List<string>(result);
        }

        public Elisyan.Authentication.Group GetGroupFromName(string Name)
        {
            string[] decomp = Name.Split('/');
            int at = 0;
            Elisyan.Authentication.Group Group = _Global;
            while (at < decomp.Length)
            {
                foreach (Elisyan.Authentication.Group group in Group.GetSubGroups().Values)
                {
                    if (group.Name == decomp[at]) { Group = group; break; }
                }
                at++;
            }
            return Group;
        }

        public List<string> GetAllStudentGroupName()
        {
            HashSet<string> result = new HashSet<string>();
            foreach (Elisyan.Authentication.Group group in _Students.GetSubGroups().Values)
            {
                GetAllGroupName("Students", group, result);
            }
            return new List<string>(result);
        }


        public void RemoveProject(Projects.Project Project)
        {
            lock (this)
            {
                lock (_Projects)
                {
                    //TODO
                }
            }
        }

        public Dictionary<string, List<Grid.Manager>> GetManagersGroups()
        {
            lock (_ManagerGroup)
            {
                return new Dictionary<string, List<FactWeb.Data.Grid.Manager>>(_ManagerGroup);
            }
        }

        public Dictionary<string, List<Projects.Project>> GetProjectsGroups()
        {
            lock (_ProjectsGroup)
            {
                return new Dictionary<string, List<Projects.Project>>(_ProjectsGroup);
            }
        }

        public Data.Projects.Project CreateProject(string Name, string Group)
        {
            try
            {
                Projects.Project NewProject = null;
                lock (_ProjectsGroup)
                {
                    if (!_ProjectsGroup.ContainsKey(Group))
                    {
                        _ProjectsGroup.Add(Group, new List<Projects.Project>());
                    }
                    NewProject = new Projects.Project();
                    NewProject.Name = Name;
                    _ProjectsGroup[Group].Add(NewProject);
                }
                lock (_Projects)
                {
                    _Projects.Add(NewProject);
                    Fact.Log.Verbose("Add project " + NewProject.Name);
                    return NewProject;
                }
            }
            catch 
            {
                Fact.Log.Error("Unexpected error while creating the project");
            }
            return null;
        }

        public Grid.Manager BindManager(string Address, string Name, string Group)
        {
            try
            {
                Grid.Manager NewManager = null;
                lock (_ManagerGroup)
                {
                    if (!_ManagerGroup.ContainsKey(Group))
                    {
                        _ManagerGroup.Add(Group, new List<FactWeb.Data.Grid.Manager>());
                    }

                    foreach (Grid.Manager manager in _ManagerGroup[Group])
                    {
                        if (manager.Name == Name) { return null; }
                    }
                    NewManager = new FactWeb.Data.Grid.Manager();
                    NewManager.Address = Address;
                    NewManager.Name = Name;
                    _ManagerGroup[Group].Add(NewManager);
                }
                lock (_Managers)
                {
                    _Managers.Add(NewManager);
                    Fact.Log.Verbose("Add manager " + NewManager.Name);
                    return NewManager;
                }
            }
            catch
            {
                Fact.Log.Error("Unexpected error while creating the project");
            }
            return null;
        }

        public void RemoveManager(string Name)
        {
            lock (this)
            {
                lock (_Projects)
                {
                }
            }
        }

        Fact.Plugin.Plugin LocalManager;
        List<string> _LoadingErrors = new List<string>();
        List<string> _LoadingWarnings = new List<string>();

        public List<string> GetErrors()
        {
            List<string> errors = new List<string>();
            lock (_LoadingErrors)
            {
                foreach (string error in _LoadingErrors) { errors.Add(error); }
            }
            return errors;
        }

        public void Update(ref Elisyan.Job.Status Status)
        {
            List<Grid.Manager> ManagersCopy = null;
            lock (_Managers)
            {
                 ManagersCopy = new List<FactWeb.Data.Grid.Manager>(_Managers);
            }
            {
                foreach (Grid.Manager a_manager in ManagersCopy)
                {
                    a_manager.Ping();
                }
            }
            // TODO Perform all the needed update project launching ...
        }


        public void Save(ref Elisyan.Job.Status Status)
        {
            Fact.Log.Verbose("Backup procedure: Started");
            lock (this)
            {
                lock (_Projects)
                {
                    StringBuilder projects = new StringBuilder();
                    projects.AppendLine("<projects>");
                    foreach (Projects.Project project in _Projects)
                    {
                        try
                        {
                            int value = 0;
                            while (project.XMLLocation == "")
                            {
                                string location = "./data/config/projects/" + project.UID + "_" + value.ToString() + ".xml";
                                if (!System.IO.File.Exists(location))
                                {
                                    project.XMLLocation = location;
                                }
                                else
                                {
                                    value++;
                                }
                            }
                            projects.AppendLine("<project file=\"" + project.XMLLocation + "\"/>");
                            project.Save();
                        }
                        catch
                        {
                            Fact.Log.Error("Impossible to save the project.");
                        }
                    }
                    projects.AppendLine("</projects>");
                }

                try
                {
                    System.IO.FileStream stream = System.IO.File.Open("./data/user.hbd", System.IO.FileMode.OpenOrCreate);
                    _Global.Save(stream);
                    stream.Close();
                }
                catch { }
            }
            Fact.Log.Verbose("Backup procedure: Ended");
        }

        public void Init()
        {
            Fact.Log.Verbose("Initialization procedure: Started");
            lock (this)
            {
                {
                    // Initialize the folders
                    Fact.Tools.RecursiveMakeDirectory("./data/projects");
                    Fact.Tools.RecursiveMakeDirectory("./data/git");
                    Fact.Tools.RecursiveMakeDirectory("./data/cache");
                    Fact.Tools.RecursiveMakeDirectory("./data/directory");
                    Fact.Tools.RecursiveMakeDirectory("./data/config");
                    Fact.Tools.RecursiveMakeDirectory("./data/config/grid");
                    Fact.Tools.RecursiveMakeDirectory("./data/config/webserver");
                    Fact.Tools.RecursiveMakeDirectory("./data/config/projects");
                }

                {
                    // Initialize the projects
                    try
                    {
                        if (!System.IO.File.Exists("./data/config/projects/projects.xml"))
                        {
                            StringBuilder projects = new StringBuilder();
                            projects.AppendLine("<projects>");
                            projects.AppendLine("</projects>");
                            System.IO.File.WriteAllText("./data/config/projects/projects.xml", projects.ToString());
                        }
                        else
                        {
                            Fact.Parser.XML xmlparser = new Fact.Parser.XML();
                            xmlparser.Parse_File("./data/config/projects/projects.xml");
                            foreach (Fact.Parser.XML.Node project in xmlparser.Root.Children)
                            {
                                if (project.Type == "project")
                                {
                                    _Projects.Add(Projects.Project.LoadFromFile(project["file"]));
                                }
                            }
                        }
                    }
                    catch 
                    {
                        Fact.Log.Error("Unexpected error while loading the projects");
                    }
                }

                {
                    // Initialize the managers
                    try
                    {
                        if (!System.IO.File.Exists("./data/config/grid/managers.xml"))
                        {
                            Fact.Parser.XML xmlparser = new Fact.Parser.XML();

                            StringBuilder managers = new StringBuilder();
                            managers.AppendLine("<managers>");
                            foreach (Fact.Parser.XML.Node manager in xmlparser.Root.Children)
                            {
                                if (manager.Type == "manager")
                                {
                                    _Projects.Add(Projects.Project.LoadFromFile(manager["file"]));
                                }
                            }
                            managers.AppendLine("</managers>");
                            System.IO.File.WriteAllText("./data/config/grid/managers.xml", managers.ToString());
                        }
                        else
                        {
                            Fact.Parser.XML xmlparser = new Fact.Parser.XML();
                            xmlparser.Parse_File("./data/config/grid/managers.xml");
                            Console.WriteLine(xmlparser.Root.Type);
                            foreach (Fact.Parser.XML.Node project in xmlparser.Root.Children)
                            {
                                if (project.Type == "manager")
                                {
                                    _Projects.Add(Projects.Project.LoadFromFile(project["file"]));
                                }
                            }
                        }
                    }
                    catch
                    {
                        Fact.Log.Error("Unexpected error while initializing the managers"); 
                    }
                }

                try
                {
                    //Loading the manager
                    Plugins.SystemManager.RunAsync();
                    //Wait 3s while the system is starting
                    System.Threading.Thread.Sleep(10000);
                    //Testing the local manager
                    int Result = Plugins.SystemManager.Run("--ping", "--ip", "127.0.0.1");
                    if (Result != 0)
                    {
                        lock (_LoadingWarnings)
                        {
                            _LoadingWarnings.Add(
    @"The local fact manager does not answer after 10s:
- Run fact system manager --ip server_ip --ping
-- If the server answer ignore this message (this could happend if your system is slow or have many processor)
-- If the server does not answer check your configuration and restart the server");
                        }
                    }
                }
                catch
                {
                    lock (_LoadingErrors)
                    {
                        Fact.Log.Error("Impossible to start the local manager correctly");
                        _LoadingErrors.Add(
    @"The local fact manager has not been started correctly:
- Check if fact is correcly installed on your system
- Do not use sandbox on the fact webserver");
                    }
                }


                {
                    try
                    {
                        if (System.IO.File.Exists("./data/user.hbd"))
                        {
                            System.IO.FileStream stream = System.IO.File.Open("./data/user.hbd", System.IO.FileMode.Open);
                            _Global = Elisyan.Authentication.Group.Load(stream);
                            stream.Close();
                            foreach (Elisyan.Authentication.Group group in _Global.GetSubGroups().Values)
                            {
                                if (group.Name == "Students") { _Students = group; }
                                if (group.Name == "Teachers") { _Teachers = group; }
                                if (group.Name == "Administrators") { _Admin = group; }
                            }
                        }
                        if (_Global == null) { _Global = new Elisyan.Authentication.Group("Global"); }
                        if (_Students == null) { _Students = new Elisyan.Authentication.Group("Students"); _Global.AddSubGroup(_Students); }
                        if (_Teachers == null) { _Teachers = new Elisyan.Authentication.Group("Teachers"); _Global.AddSubGroup(_Teachers); }
                        if (_Admin == null) { _Admin = new Elisyan.Authentication.Group("Administrators"); _Global.AddSubGroup(_Admin); }

                        if (_Admin.GetSubGroups().Count == 0 && AdminGroup.GetUsers().Count == 0)
                        {
                            // Must be changed as soon as possible
                            _Admin.AddUser(new Elisyan.Authentication.User("admin", "admin"));
                        }
                    }
                    catch { Fact.Log.Error("Unexpected error while initializing the groups"); }
                
                }
                Fact.Log.Verbose("Initialization procedure: Ended");
            }
        }
    }
}
