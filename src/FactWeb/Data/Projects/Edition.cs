﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Data.Projects
{
    class Edition
    {
        DateTime _StartDate;
        DateTime _EndDate;


        public DateTime StartDate { get { return _StartDate; } set { _StartDate = value; } }
        public DateTime EndDate { get { return _EndDate; } set { _EndDate = value; } }

        bool _MustCreateRepositories = true;
        Dictionary<string, string> _RepositoriesCreated = new Dictionary<string, string>();

        List<Team> _Teams = new List<Team>();

        string _Name = "";
        public string Name { get { return _Name; } set { _Name = value; } }

        Elisyan.Authentication.Group _RegisteredStudents = new Elisyan.Authentication.Group("Students");
        public Elisyan.Authentication.Group RegisteredStudents { get { return _RegisteredStudents; } }

        Elisyan.Authentication.Group _ExcludedStudents = new Elisyan.Authentication.Group("Students");
        public Elisyan.Authentication.Group ExcludedStudents { get { return _ExcludedStudents; } }

        int _MinStudentPerGroup = 1;
        public int MinStudentPerGroup { get { return _MinStudentPerGroup; } set { if (_MinStudentPerGroup <= 1) { _MinStudentPerGroup = 1; } else { _MinStudentPerGroup = value; } } }
        int _MaxStudentPerGroup = 1;
        public int MaxStudentPerGroup { get { return _MaxStudentPerGroup; } set { if (_MaxStudentPerGroup <= 1) { _MaxStudentPerGroup = 1; } else { _MinStudentPerGroup = value; } } }

        public bool IsGroupProject { get { return _MaxStudentPerGroup > 1; } }

        bool _Validated = false;

        HashSet<string> _UpToDateStaticData = new HashSet<string>();

        public bool Validated { get { return _Validated && _EndDate < DateTime.Now; } set { if (!_Validated && value) { } _Validated = value; } }

        public List<Elisyan.Authentication.User> StudentWithoutTeam
        {
            get
            {
                /*
                if (!IsGroupProject) { return false; }
                foreach (Elisyan.Authentication.User user in _RegisteredStudents)
                {
                    if (!_ExcludedStudents.ContainUser(user))
                    {
                        foreach (Team team in _Teams)
                        {
                            if (!team.ContainsUser(user)) { }
                        }
                    }
                }
                */
                return new List<Elisyan.Authentication.User>();
            }
        }

        static volatile int _UID_count;
        string _Edition_UID = "";
        public string UID { get { return _Edition_UID; } }
        public Edition()
        {
            int value = System.Threading.Interlocked.Increment(ref _UID_count);
            _Edition_UID = "edition_" + value;
        }

        public void Update()
        {
            if (_StartDate != _EndDate)
            {
                if (DateTime.Now < _EndDate)
                {
                    if (_StartDate < DateTime.Now || (_StartDate - DateTime.Now).TotalMinutes < 30)
                    {
                        bool missing = false;
                        foreach (Elisyan.Authentication.User user in _RegisteredStudents)
                        {
                            if (!_ExcludedStudents.ContainUser(user.Name))
                            {
                                if (IsGroupProject)
                                {
                                    //TODO
                                }
                                else
                                {

                                    // Check if the student has a repository
                                    if (!_RepositoriesCreated.ContainsKey(user.Name))
                                    {
                                        // Must create the repository
                                        string repository = Data.Manager.GlobalManager.CreateGitRepository(user.Name);
                                        if (repository != "") { _RepositoriesCreated.Add(user.Name, repository); }
                                        else { missing = true; }
                                    }
                                }
                            }
                        }
                        if (missing)
                        {
                            Console.WriteLine("Some repositories have not been correctly created");
                        }
                    }
                }
            }
        }


        public void RemoveStaticData(List<string> Files, List<Grid.Manager> Managers)
        {
            //TODO
        }

        List<string> CloneStudents(List<KeyValuePair<string, string>> StudentsRepositories, Grid.Manager Manager)
        {
            List<KeyValuePair<string, string>> MissingRepositories = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> PresentRepositories = new List<KeyValuePair<string, string>>();
            //TODO
            return null;
        }

        void RunTests(List<string> StudentPackages, List<string> Tests, List<Grid.Manager> Managers)
        {
            List<string> output = new List<string>();
            int CurrentManager = 0;
            foreach (string package in StudentPackages)
            {
                bool MustUploadStaticData = false;
                if (_UpToDateStaticData.Contains(Managers[CurrentManager].UID))
                {
                    MustUploadStaticData = true;
                    _UpToDateStaticData.Add(Managers[CurrentManager].UID);
                }
                //TODO
                CurrentManager++;
                if (CurrentManager >= Managers.Count) { CurrentManager = 0; }
            }
            return;
        }

        List<string> MakeStudents(List<string> StudentPackages, List<Grid.Manager> Managers)
        {
            List<string> output = new List<string>();
            int CurrentManager = 0;
            foreach (string package in StudentPackages)
            {
                if (HasChanged(package))
                {
                    //TODO
                    CurrentManager++;
                    if (CurrentManager >= Managers.Count) { CurrentManager = 0; }
                }
                else if (!HasTestData(package))
                {
                    //TODO
                }
            }
            return output;
        }

        void MakeAndRun(List<string> StudentPackages, List<string> Tests, List<Grid.Manager> Managers)
        {
            List<string> Results = MakeStudents(StudentPackages, Managers);
            if (Results.Count != 0)
            {
                RunTests(Results, Tests, Managers);
            }
        }

        List<string> CompileTestSuites(List<string> TestSuites, out bool Changed)
        {
            //TODO
            Changed = true;
            return null;
        }

        void CleanMakeCache()
        {
            //TODO
        }

        void CleanTestResultsCache()
        {
            //TODO
        }

        void CleanCloneCache()
        {
            //TODO
        }

        void CleanTestSuitesCache()
        {
            //TODO
        }

        void CleanCache()
        {
            CleanCloneCache();
            CleanMakeCache();
            CleanTestResultsCache();
            CleanTestSuitesCache();
        }

        bool HasChanged(string File)
        {
            return true;
        }

        bool HasTestData(string File)
        {
            return false;
        }

        void StartTest()
        {
            //TODO

            bool TestSuitesChanged = false;
            List<string> Tests = CompileTestSuites(null, out TestSuitesChanged);
            if (TestSuitesChanged) { CleanTestResultsCache(); CleanTestSuitesCache(); }

            List<string> NewPackages = CloneStudents(null, null);
            if (NewPackages != null && NewPackages.Count != 0)
            {
                MakeAndRun(NewPackages, Tests, null);
            }
        }
    }
}
