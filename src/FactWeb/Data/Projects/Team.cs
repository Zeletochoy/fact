﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Data.Projects
{
    class Team
    {
        Elisyan.Authentication.User _Leader = null;
        Elisyan.Authentication.Group _Team = new Elisyan.Authentication.Group("Team");

        public bool ContainsUser(Elisyan.Authentication.User User)
        {
            return _Team.ContainUser(User.Name);
        }
    }
}
