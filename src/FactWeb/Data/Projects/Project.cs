﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Data.Projects
{
    class Project
    {
        string _XMLLocation = "";
        string _Name = "";
        string _UID = "";
        static volatile int _UID_count;
        public string Name { get { return _Name; } set { _Name = value; } }
        public string UID { get { return _UID; } }
        public string XMLLocation { get { return _XMLLocation; } set { _XMLLocation = value; } }
        List<Edition> _Editions = new List<Edition>();

        public List<Edition> GetEditions() { lock (_Editions) { return new List<Edition>(_Editions); } }

        public bool IsRegistered(Elisyan.Authentication.User User)
        {
            List<Edition> editions = GetEditions();
            foreach (Edition edition in editions) { if (edition.RegisteredStudents.ContainUser(User.Name) && !edition.ExcludedStudents.ContainUser(User.Name)) { return true; } }
            return false;
        }

        public static void CreateProject(string Name) 
        {
            Project p = new Project();
            p._Name = Name;
        }

        public static Project LoadFromFile(string File)
        {
            Project project = new Project();
            project._XMLLocation = File;
            try
            {
                Fact.Parser.XML XML = new Fact.Parser.XML();
                XML.Parse_File(File);
                project._Name = XML.Root["name"];
            }
            catch { }
            return project;
        }

        internal Project() 
        {
            int value = System.Threading.Interlocked.Increment(ref _UID_count);
            _UID = "project_" + value.ToString();
        }

        public void Update()
        {
            lock (_Editions)
            {
                foreach (Edition edition in _Editions)
                {
                    edition.Update();
                }
            }
        }

        public void CreateEdition(string Name)
        {
            lock (_Editions)
            {
                Edition edition = new Edition();
                edition.Name = Name;
                _Editions.Add(edition);
            }
        }

        public void Save()
        {
            StringBuilder project = new StringBuilder();
            project.AppendLine("<project name=\"" + _Name + "\">");
            foreach (Edition edition in _Editions)
            {
                project.AppendLine("<edition>");
                project.AppendLine("</edition>");
            }
            project.AppendLine("</project>");
            System.IO.File.WriteAllText(_XMLLocation, project.ToString());
        }
    }
}
