﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb
{
    static class Plugins
    {
        static Fact.Plugin.Plugin _SystemManager = null;
        public static Fact.Plugin.Plugin SystemManager 
        {
            get 
            {
                if (_SystemManager == null) 
                {
                    Fact.Log.Verbose("Initializing the System Manager plugin");
                    try
                    { 
                        foreach (Fact.Plugin.Plugin plugin in Fact.Plugin.Plugin.LoadFromFile("system/manager"))
                        {
                            if (plugin.Name.ToLower() == "manager")
                            {
                                Fact.Log.Verbose("System Manager plugin found");
                                _SystemManager = plugin;
                                break;
                            }
                        }
                    }
                    catch 
                    { 
                    }
                }
                return _SystemManager; 
            }
        }
    }
}
