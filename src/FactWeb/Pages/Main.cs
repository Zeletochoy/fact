﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages
{
    class Main : Page
    {

        public Main()
        {
            Name = "Fact";
            Location = "";
            Content = GenerateNews();
        }

        public Elisyan.Content.Content GenerateNews()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H1>News</H1>");
            // TODO Generate a per user news page
            return Body.ToArray();
        }
    }
}
