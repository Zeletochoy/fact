﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages.Projects
{
    class Edition : Page
    {
        Data.Projects.Edition _Edition = null;
        Data.Projects.Project _Project = null;



        public Edition(Data.Projects.Edition Edition, Data.Projects.Project Project)
        {
            _Edition = Edition;
            _Project = Project;
            Location = "projects/editions/" + Edition.UID + ".hx";
            Update();
        }

        public void Update()
        {
            Content = GenerateEditionDispatcher();
        }

        string FormatDateTime(DateTime Time)
        {
            return Time.Day + "/" + Time.Month + "/" + Time.Year + " " + Time.Hour + ":" + Time.Minute;
        }

        DateTime ParseDateTime(string Date)
        {
            try
            {
                int iday = DateTime.Now.Day;
                int imonth = DateTime.Now.Month;
                int iyear = DateTime.Now.Year;

                int ihour = 0;
                int iminutes = 0;
                string[] dayhour = Date.Split(new char[] { ' ', '\t', '\n', '\r', '+' }, StringSplitOptions.RemoveEmptyEntries);
                string day = "";
                string hour = "";
                if (dayhour.Length >= 1) { day = dayhour[0]; }
                if (dayhour.Length >= 2) { hour = dayhour[1]; }

                {
                    string[] splitted = day.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitted.Length >= 3)
                    {
                        int.TryParse(splitted[0], out iday);
                        int.TryParse(splitted[1], out imonth);
                        int.TryParse(splitted[2], out iyear);
                        if (iyear < 1000) { iyear += 2000; }
                    }
                }

                {
                    string[] splitted = hour.Split(new char[] { ':', 'h' }, StringSplitOptions.RemoveEmptyEntries);
                    if (splitted.Length >= 2)
                    {
                        int.TryParse(splitted[0], out ihour);
                        int.TryParse(splitted[1], out iminutes);
                    }
                }

                {
                    while (iminutes >= 60) { ihour++; iminutes -= 60; }
                    while (ihour >= 24) { iday++; ihour -= 24; }
                }
                return new DateTime(iyear, imonth, iday, ihour, iminutes, 0);
            }
            catch { return new DateTime(); }
        }

        public Elisyan.Content.Content GenerateAutomaticCorrectionPanel()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();

            Body.Add("<H2>Automatic correction</H2>");
            Body.Add("<H3>Test Suite</H3>");

            Elisyan.Content.Bootstrap.Alert CreationPanel = new Elisyan.Content.Bootstrap.Alert();
            Elisyan.Content.Form CreationForm = new Elisyan.Content.Form();
            CreationForm.Content = new Elisyan.Content.Content[]
            {
                new Elisyan.Content.Bootstrap.FormTextbox() { Name = "name", Text = "Name", Info = "Name" },              
                "</br>",
                new Elisyan.Content.FormSubmit() { Text = "Add test file" }
            };
            CreationForm.Action = new Elisyan.Action.Action(
                (Dictionary<string, string> Variables, Elisyan.Session Session) =>
                {
                    // TODO Add a new test file
                    return null;
                });
            CreationPanel.Content = new Elisyan.Content.Content[] { "Add a test file to the testsuite:</br>", CreationForm };
            Body.Add(CreationPanel);

            // TODO Add the automatic correction system
            return Body.ToArray();
        }

        static Elisyan.Content.Content GenerateSubGroupList(Elisyan.Authentication.Group Group)
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Elisyan.Content.Bootstrap.Table table = new Elisyan.Content.Bootstrap.Table();
            table.Columns.Add("Name");
            table.Columns.Add("");

            foreach (Elisyan.Authentication.Group subgroup in Group.GetSubGroups().Values)
            {

                Elisyan.Content.Bootstrap.Table.Row Row = new Elisyan.Content.Bootstrap.Table.Row();
                Row.Columns.Add(subgroup.Name);
                Row.Columns.Add(new Elisyan.Content.Bootstrap.Button() { Text = "Delete", Action = new Elisyan.Action.Action(() => { /* TODO */ return null; }) });
                table.Rows.Add(Row);
            }

            Body.Add(table);
            return Body.ToArray();
        }

        public Elisyan.Content.Content GenerateAdministrationPanel()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();

            Body.Add("<H2>Edition configuration</H2>");
            Body.Add("<H3>Information</H3>");
            Form CreationForm = new Form(
                (Dictionary<string, string> Variables) =>
                {
                    if (Variables.ContainsKey("name") && Variables["name"] != "") { _Edition.Name = Variables["name"]; }
                    if (Variables.ContainsKey("startdate") && Variables["startdate"] != "") { _Edition.StartDate = ParseDateTime(Variables["startdate"]); }
                    if (Variables.ContainsKey("enddate") && Variables["enddate"] != "") { _Edition.EndDate = ParseDateTime(Variables["enddate"]); }
                    Update();
                });
            CreationForm.FormType = Form.Type.Modification;
            CreationForm.AddTextBox("name", "Name", "Name");
            CreationForm.AddTextBox("startdate", FormatDateTime(_Edition.StartDate), "Start date");
            CreationForm.AddTextBox("enddate", FormatDateTime(_Edition.EndDate), "End date");
            CreationForm.InformationText = "Update the information of the edition:";
            CreationForm.ValidationText = "Update information";
            Body.Add(CreationForm);
            Body.Add("<H3>Registered students</H3>");
            Body.Add("<H4>Registered groups</H4>");
            Body.Add(GenerateSubGroupList(_Edition.RegisteredStudents));
            {
                Form form = new Form((Dictionary<string, string> Variables) =>
                {
                    Elisyan.Authentication.Group group = Data.Manager.GlobalManager.GetGroupFromName(Variables["value"]);
                    _Edition.RegisteredStudents.AddSubGroup(group);
                    Update();
                });
                form.InformationText = ("Add a new group to the registered groups");
                form.AddComboBox("value", "", Data.Manager.GlobalManager.GetAllStudentGroupName().ToArray());
                Body.Add(form);
            }
            Body.Add("<H4>Registered individual students</H4>");
            Body.Add("" /* Generate the student list */);
            {
                Form form = new Form((Dictionary<string, string> Variables) => { });
                form.InformationText = ("Add a new student to the registered students");
                form.AddComboBox("value", "", Data.Manager.GlobalManager.GetAllStudentName().ToArray());
                Body.Add(form);
            }
            Body.Add("<H4>Excluded groups</H4>");
            Body.Add(GenerateSubGroupList(_Edition.ExcludedStudents));
            {
                Form form = new Form((Dictionary<string, string> Variables) =>
                {
                    Elisyan.Authentication.Group group = Data.Manager.GlobalManager.GetGroupFromName(Variables["value"]);
                    _Edition.ExcludedStudents.AddSubGroup(group);
                    Update();
                });
                form.InformationText = ("Exclude a group from the registered groups");
                form.AddComboBox("value", "", Data.Manager.GlobalManager.GetAllStudentGroupName().ToArray());
                Body.Add(form);
            }
            Body.Add("<H4>Excluded individual students</H4>");
            Body.Add("" /* Generate the student list */);
            {
                Form form = new Form((Dictionary<string, string> Variables) => { });
                form.InformationText = ("Exclude a student from the registered students");
                form.AddComboBox("value", "", Data.Manager.GlobalManager.GetAllStudentName().ToArray());
                Body.Add(form);
            }

            if (_Edition.IsGroupProject)
            {
                Body.Add("<H3>Teams</H3>");

                Elisyan.Content.Bootstrap.Button Button = new Elisyan.Content.Bootstrap.Button();

                Body.Add("<H4>Students without team</H4>");

                Button.Text = "Create random team for students without team";
                Button.Action = new Elisyan.Action.Action(() => { Update(); return null; });
            }

            return Body.ToArray();
        }


        public Elisyan.Content.Content GenerateEditionDispatcher()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();

            if (_Edition.Validated)
            { Body.Add("<H2>" + _Project.Name + " <small> Validated </small></H2>"); }
            else if (_Edition.StartDate >= _Edition.EndDate)
            {
                Elisyan.Content.PerGroupContent Group = new Elisyan.Content.PerGroupContent();
                Group.AddSpecificContent(Data.Manager.GlobalManager.AdminGroup, "<H2>" + _Project.Name + " <small> Invalid Date (Please configure the date) </small></H2>");
                Group.AddSpecificContent(Data.Manager.GlobalManager.TeacherGroup, "<H2>" + _Project.Name + " <small> Invalid Date (Please configure the date) </small></H2>");
                Group.AddSpecificContent(Data.Manager.GlobalManager.StudentGroup, "<H2>" + _Project.Name + "</H2>");
                Group.DefaultContent = "";
                Body.Add(Group);
            }
            else if (_Edition.EndDate < DateTime.Now)
            { Body.Add("<H2>" + _Project.Name + " <small> Ended </small></H2>"); }
            else if (_Edition.StartDate > DateTime.Now)
            { Body.Add("<H2>" + _Project.Name + " <small> Start: " + FormatDateTime(_Edition.StartDate) + " </small></H2>"); }
            else
            { Body.Add("<H2>" + _Project.Name + " <small> " + FormatDateTime(_Edition.StartDate) + " - " + FormatDateTime(_Edition.EndDate) + " </small></H2>"); }

            {
                Elisyan.Content.Content AdministrationPanel = GenerateAdministrationPanel();
                Elisyan.Content.PerGroupContent Dispatch = new Elisyan.Content.PerGroupContent();
                Dispatch.AddSpecificContent(Data.Manager.GlobalManager.AdminGroup, AdministrationPanel);
                Dispatch.AddSpecificContent(Data.Manager.GlobalManager.TeacherGroup, AdministrationPanel);
                Dispatch.DefaultContent = "";
                Body.Add(Dispatch);
            }

            {
                Elisyan.Content.Content AutomaticCorrection = GenerateAutomaticCorrectionPanel();
                Elisyan.Content.PerGroupContent Dispatch = new Elisyan.Content.PerGroupContent();
                Dispatch.AddSpecificContent(Data.Manager.GlobalManager.AdminGroup, AutomaticCorrection);
                Dispatch.AddSpecificContent(Data.Manager.GlobalManager.TeacherGroup, AutomaticCorrection);
                Dispatch.DefaultContent = "";
                Body.Add(Dispatch);
            }

            return Body.ToArray();
        }
    }
}
