﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages.Projects
{
    class Main : Page
    {
        HashSet<string> _GeneratedProjectPages = new HashSet<string>();
        Elisyan.Content.PerGroupContent Projects = new Elisyan.Content.PerGroupContent();
        WebSite _Parent = null;
        public Main(WebSite Parent)
        {
            _Parent = Parent;
            Location = "projects.hx";
            Name = "Projects";
            Content = GenerateBody();
            Elisyan.Job.CreateTimer(Update, 1000 * 60 * 10, true);
        }

        public void Update(ref Elisyan.Job.Status Status)
        {
            Update();
        }

        public void Update()
        {
            Fact.Log.Verbose("Update Project Data");
            this.Content = GenerateBody();
        }

        public Elisyan.Content.Content GenerateBody()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H2>Projects</H2>");
            Elisyan.Content.PerGroupContent dispatch = new Elisyan.Content.PerGroupContent();
            dispatch.DefaultContent = new Elisyan.Content.Bootstrap.Alert()
            {
                Type = Elisyan.Content.Bootstrap.Type.Danger,
                Content = "Please, Log in to see the projects associated to your account."
            };

            Elisyan.Content.Content administratorPage = FullList();

            dispatch.AddSpecificContent(Data.Manager.GlobalManager.AdminGroup, administratorPage);
            dispatch.AddSpecificContent(Data.Manager.GlobalManager.TeacherGroup, administratorPage);
            foreach (Elisyan.Authentication.User user in Data.Manager.GlobalManager.StudentGroup)
            {
                dispatch.AddSpecificContent(user, GeneratePerUserList(user));
            }
            dispatch.AddSpecificContent(Data.Manager.GlobalManager.StudentGroup,
                new Elisyan.Content.Bootstrap.Alert()
            {
                Type = Elisyan.Content.Bootstrap.Type.Info,
                Content = "You have no project associated to your account."
            });
            Body.Add(dispatch);
            return Body.ToArray();
        }
        Elisyan.Content.Content GeneratePerUserList(Elisyan.Authentication.User User)
        {
            bool Initialized = false;
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Dictionary<string, List<Data.Projects.Project>> projects = Data.Manager.GlobalManager.GetProjectsGroups();
            foreach (string group in projects.Keys)
            {
                bool GroupInit = false;
               
                Elisyan.Content.Bootstrap.Table table = new Elisyan.Content.Bootstrap.Table();
                table.Columns.Add("Name");
                table.Columns.Add("Status");
                table.Columns.Add("");

                foreach (Data.Projects.Project project in projects[group])
                {
                    if (project.IsRegistered(User))
                    {
                        GroupInit = true;
                        Initialized = true;
                        if (!_GeneratedProjectPages.Contains(project.UID))
                        {
                            Project projectPage = new Project(project, _Parent);
                            _Parent.Register(projectPage);
                            _GeneratedProjectPages.Add(project.UID);
                        }
                        Elisyan.Content.Bootstrap.Table.Row row = new Elisyan.Content.Bootstrap.Table.Row();
                        row.Columns.Add(project.Name);
                        row.Columns.Add("-");
                        row.Columns.Add(new Elisyan.Content.Content[] 
                        {
                            new Elisyan.Content.Bootstrap.Button()
                            {
                                Action = Elisyan.Page.ActionNavigate("/projects/" + project.UID + ".hx"),
                                Text = "Open"
                            }
                        });
                        table.Rows.Add(row);
                    }
                }
                if (GroupInit)
                {
                    Body.Add("<H3>" + group + "</H3>");
                    Body.Add(table);
                }
            }
            if (Initialized)
            {
                return Body.ToArray();
            }
            else
            {
                return new Elisyan.Content.Bootstrap.Alert()
                {
                    Type = Elisyan.Content.Bootstrap.Type.Info,
                    Content = "You have no project associated to your account."
                };
            }
        }

        Elisyan.Content.Content FullList()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Dictionary<string, List<Data.Projects.Project>> projects = Data.Manager.GlobalManager.GetProjectsGroups();
            foreach (string group in projects.Keys)
            {
                Body.Add("<H3>" + group + "</H3>");
                Elisyan.Content.Bootstrap.Table table = new Elisyan.Content.Bootstrap.Table();
                table.Columns.Add("Name");
                table.Columns.Add("Status");
                table.Columns.Add("");

                foreach (Data.Projects.Project project in projects[group])
                {
                    if (!_GeneratedProjectPages.Contains(project.UID))
                    {
                        Project projectPage = new Project(project, _Parent);
                        _Parent.Register(projectPage);
                        _GeneratedProjectPages.Add(project.UID);
                    }
                    Elisyan.Content.Bootstrap.Table.Row row = new Elisyan.Content.Bootstrap.Table.Row();
                    row.Columns.Add(project.Name);
                    row.Columns.Add("-");
                    row.Columns.Add(new Elisyan.Content.Content[] 
                    {
                        new Elisyan.Content.Bootstrap.Button()
                        {
                            Action = Elisyan.Page.ActionNavigate("/projects/" + project.UID + ".hx"),
                            Text = "Open"
                        }
                    });
                    table.Rows.Add(row);
                }
                Body.Add(table);
            }

            {
                Form Form = new Form((Dictionary<string, string> Variables) =>
                {
                    object result = Data.Manager.GlobalManager.CreateProject(Variables["name"], Variables["group"]);
                    if (result == null) { Fact.Log.Error("Impossible to create the project"); }
                    Update();
                });
                Form.AddTextBox("name", "Name", "Name");
                Form.AddTextBox("group", "Group", "Group");

                Form.InformationText = "Add a project to the projects monitored by fact:";
                Form.ValidationText = "Add";
                Body.Add(Form);
            }
            return Body.ToArray();
        }

    }
}
