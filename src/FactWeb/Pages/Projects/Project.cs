﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages.Projects
{
    class Project : Page
    {
        Data.Projects.Project _Project = null;
        HashSet<string> _GeneratedEditionPages = new HashSet<string>();

        WebSite _Parent;
        public Project(Data.Projects.Project Project, WebSite Parent)
        {
            _Parent = Parent;
            _Project = Project;
            Location = "projects/" + Project.UID + ".hx";
            Update();
        }

        public void Update()
        {
            Content = CreateProjectDispacthPage();
        }



        Elisyan.Content.Content CreateProjectDispacthPage()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H2>" + _Project.Name + "</H2>");
            Elisyan.Content.PerGroupContent Dispatch = new Elisyan.Content.PerGroupContent();

            Elisyan.Content.Content AdministrationTools = CreateAdministrationPage();
            //Elisyan.Content.Content RegisterStudentsTools = CreateClassicPage();

            Dispatch.AddSpecificContent(Data.Manager.GlobalManager.AdminGroup, AdministrationTools);
            Dispatch.AddSpecificContent(Data.Manager.GlobalManager.TeacherGroup, AdministrationTools);

            List<Data.Projects.Edition> Editions = _Project.GetEditions();
            /* The validated editions are registered after in order to allow a student
             * register either to an old validated edition and a new edition to see the new edition
             */
            foreach (Data.Projects.Edition edition in Editions)
            {
                if (!edition.Validated)
                {
                    Dispatch.AddSpecificContent(edition.RegisteredStudents, CreateStudentPage(edition));
                }
            }
            foreach (Data.Projects.Edition edition in Editions)
            {
                if (edition.Validated)
                {
                    Dispatch.AddSpecificContent(edition.RegisteredStudents, CreateStudentPage(edition));
                }
            }
            Body.Add(Dispatch);
            return Body.ToArray();
        }

        Elisyan.Content.Content CreateStudentPage(Data.Projects.Edition Edition)
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            return Body.ToArray();
        }

        Elisyan.Content.Content CreateSummary()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H3>Summary</H3>");
          
            return Body.ToArray();
        }

        Elisyan.Content.Content CreateAdministrationPage()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H3>Editions</H3>");
            List<Data.Projects.Edition> editions = _Project.GetEditions();
            Body.Add("<H4>Current</H4>");
            {
                Elisyan.Content.Bootstrap.Table Table = new Elisyan.Content.Bootstrap.Table();
                foreach (Data.Projects.Edition edition in editions)
                {
                    if (!_GeneratedEditionPages.Contains(edition.UID))
                    {
                        _Parent.Register(new Edition(edition, _Project));
                        _GeneratedEditionPages.Add(edition.UID);
                    }
                    if (!edition.Validated)
                    {
                        Elisyan.Content.Bootstrap.Table.Row editionrow = new Elisyan.Content.Bootstrap.Table.Row();
                        if (edition.StartDate < DateTime.Now) { editionrow.Type = Elisyan.Content.Bootstrap.Type.Info; }
                        else { editionrow.Type = Elisyan.Content.Bootstrap.Type.Success; }
                        editionrow.Columns.Add(edition.Name);
                        editionrow.Columns.Add(new Elisyan.Content.Content[] 
                        {
                            new Elisyan.Content.Bootstrap.Button()
                            { Text = "Open", Action = Elisyan.Page.ActionNavigate("/projects/editions/" + edition.UID + ".hx") }
                        });
                        Table.Rows.Add(editionrow);
                    }
                }
                Body.Add(Table);
                {
                    Form Form = new Form((Dictionary<string, string> Variables) =>
                    {
                        _Project.CreateEdition(Variables["name"]);
                        Update();
                    });
                    Form.AddTextBox("name", "Name", "Name");
                    Form.InformationText = "Add a new edition to this project:";
                    Form.ValidationText = "Add";
                    Body.Add(Form);
                }
            }
            Body.Add("<H4>Archive</H4>");
            {
                Elisyan.Content.Bootstrap.Table Table = new Elisyan.Content.Bootstrap.Table();
                foreach (Data.Projects.Edition edition in editions)
                {
                    if (edition.Validated)
                    {
                        Elisyan.Content.Bootstrap.Table.Row editionrow = new Elisyan.Content.Bootstrap.Table.Row();
                        editionrow.Columns.Add(edition.Name);
                        editionrow.Columns.Add(new Elisyan.Content.Content[] 
                    {
                        new Elisyan.Content.Bootstrap.Button()
                        { Text = "Open", Action = Elisyan.Page.ActionNavigate("/projects/editions/" + edition.UID + ".hx") }
                    });
                        Table.Rows.Add(editionrow);
                    }
                }
                Body.Add(Table);
            }
            return Body.ToArray();
        }
    }
}
