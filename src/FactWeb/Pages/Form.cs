﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages
{
    class Form : Elisyan.Content.Content
    {

        public enum Type
        {
            Creation,
            Modification
        }
        Type _FormType = Type.Creation;
        public Type FormType { get { return _FormType; } set { _FormType = value; } }

        Elisyan.Content.Content _Array = null;

        string _ValidationText = "Submit";
        public string ValidationText { get { return _ValidationText; } set { _ValidationText = value;    } }
        string _InfoText = "";
        public string InformationText { get { return _InfoText; } set { _InfoText = value; } }

        public delegate void OnSubmit(Dictionary<string, string> Variables);
        OnSubmit _Action = null;
        public Form(OnSubmit OnSubmit)
        { _Action = OnSubmit; }

        List<Elisyan.Content.Content> _FormContent = new List<Elisyan.Content.Content>();
        public void AddTextBox(string Name, string Text, string Info, bool Hidden)
        {
            _FormContent.Add(new Elisyan.Content.Bootstrap.FormTextbox()
            {
                Text = Text,
                Name = Name,
                Info = Info,
                Hidden = false
            });
            Update();
        }
        public void AddTextBox(string Name, string Text, string Info)
        {
            AddTextBox(Name, Text, Info, false);
        }

        public void AddComboBox(string Name, string Text, params string[] Options)
        {
            Elisyan.Content.Bootstrap.FormCombobox ComboBox = new Elisyan.Content.Bootstrap.FormCombobox();
            foreach (string text in Options) { ComboBox.Options.Add(text); }
            ComboBox.Name = Name;
            _FormContent.Add(ComboBox);
            Update();
        }

        void Update()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Elisyan.Content.Bootstrap.Alert CreationPanel = new Elisyan.Content.Bootstrap.Alert();
            if (_FormType == Type.Creation) { CreationPanel.Type = Elisyan.Content.Bootstrap.Type.Info; }
            if (_FormType == Type.Modification) { CreationPanel.Type = Elisyan.Content.Bootstrap.Type.Success; }

            Elisyan.Content.Form CreationForm = new Elisyan.Content.Form();
            CreationForm.Content = new Elisyan.Content.Content[]
                {
                    _FormContent.ToArray(),
                    "</br>",
                    new Elisyan.Content.FormSubmit() { Text = _ValidationText }
                };
            CreationForm.Action = new Elisyan.Action.Action(
                (Dictionary<string, string> Variables, Elisyan.Session Session) =>
                {
                    _Action(Variables);
                    return Session.LastVisitedPage;
                });
            CreationPanel.Content = new Elisyan.Content.Content[] { _InfoText + ":</br>", CreationForm };
            Body.Add(CreationPanel);
            _Array = Body.ToArray();
        }
        public override string ToString(Elisyan.Session Session)
        {
            if (_Array != null) { return _Array.ToString(Session); }
            return "";
        }
    }
}
