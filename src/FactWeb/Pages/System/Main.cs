﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages.System
{
    class Main : Page
    {
        Elisyan.Content.PerGroupContent Dispatcher = new Elisyan.Content.PerGroupContent();
        Adapter Body = new Adapter();

        class Adapter : Elisyan.Content.Content
        {
            public Elisyan.Content.Content Child { get; set; }
            public override string ToString(Elisyan.Session Session)
            {
                return Child.ToString(Session);
            }
        }

        WebSite _Parent = null;
        public Main(WebSite Parent)
        {
            Name = "System";
            Location = "system.hx";
            _Parent = Parent;
            Content = Dispatcher;

            Update();
            Elisyan.Job.CreateTimer((ref Elisyan.Job.Status ignored) => { Update(); }, 10000, true);

            Dispatcher.DefaultContent =
                new Elisyan.Content.Bootstrap.Alert()
                {
                    Type = Elisyan.Content.Bootstrap.Type.Danger,
                    Content = "Annonymous users are not allowed to access to the fact server configuration section. Please Log in."
                };

            Dispatcher.AddSpecificContent(
             Data.Manager.GlobalManager.AdminGroup,
             Body);

            Dispatcher.AddSpecificContent(
                 Data.Manager.GlobalManager.TeacherGroup,
                 new Elisyan.Content.Bootstrap.Alert()
                 {
                     Type = Elisyan.Content.Bootstrap.Type.Danger,
                     Content = "Teachers are not allowed to access to the fact server configuration sections."
                 });
            Dispatcher.AddSpecificContent(
                Data.Manager.GlobalManager.StudentGroup,
                new Elisyan.Content.Bootstrap.Alert()
                {
                    Type = Elisyan.Content.Bootstrap.Type.Danger,
                    Content = "Students are not allowed to access to the fact server configuration sections."
                });
        }

        public void Update()
        {
            Fact.Log.Verbose("Update System data");
            Body.Child = SystemManagementSetup();
        }

        Elisyan.Content.Content SystemManagementSetup()
        {
            return new Elisyan.Content.Content[]
            {
                GrindManagementSetup(),
                UserManagementSetup()
            };
        }

        public static Elisyan.Content.Content GenerateUserInfo(Elisyan.Authentication.Group Group, WebSite Parent)
        {
            Dictionary<Elisyan.Authentication.Group, Group> _GroupPages = new Dictionary<Elisyan.Authentication.Group, Group>();
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Elisyan.Content.Bootstrap.Table table = new Elisyan.Content.Bootstrap.Table();
            table.Columns.Add("Name");
            table.Columns.Add("");

            foreach (Elisyan.Authentication.User subgroup in Group.GetUsers().Values)
            {

                Elisyan.Content.Bootstrap.Table.Row Row = new Elisyan.Content.Bootstrap.Table.Row();
                Row.Columns.Add(subgroup.Name);
                //Row.Columns.Add(new Elisyan.Content.Bootstrap.Button() { Text = "Open", Action = Elisyan.Page.ActionNavigate(Parent.Register(new Group(subgroup, Parent))) });
                table.Rows.Add(Row);
            }

            Body.Add(table);
            return Body.ToArray();
        }

        public static Elisyan.Content.Content GenerateSubGroupsInfo(Elisyan.Authentication.Group Group, WebSite Parent)
        {
            Dictionary<Elisyan.Authentication.Group, Group> _GroupPages = new Dictionary<Elisyan.Authentication.Group, Group>();
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Elisyan.Content.Bootstrap.Table table = new Elisyan.Content.Bootstrap.Table();
            table.Columns.Add("Name");
            table.Columns.Add("");

            foreach (Elisyan.Authentication.Group subgroup in Group.GetSubGroups().Values)
            {
                
                Elisyan.Content.Bootstrap.Table.Row Row = new Elisyan.Content.Bootstrap.Table.Row();
                Row.Columns.Add(subgroup.Name);
                Row.Columns.Add(new Elisyan.Content.Bootstrap.Button() { Text = "Open", Action = Elisyan.Page.ActionNavigate(Parent.Register(new Group(subgroup, Parent))) });
                table.Rows.Add(Row);
            }

            Body.Add(table);
            return Body.ToArray();
        }

        public static void AddSubGroup(Elisyan.Authentication.Group group, string Name)
        {
                group.AddSubGroup(new Elisyan.Authentication.Group(Name));
        }

        public static void AddUser(Elisyan.Authentication.Group group, string Name, string Password)
        {
            group.AddUser(new Elisyan.Authentication.User(Name, Password));
        }

        Elisyan.Content.Content UserManagementSetup()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();

            Body.Add("<H2>Users</H2>");
            Body.Add("<H3>Administrators</H3>");
            {
                Body.Add(GenerateSubGroupsInfo(Data.Manager.GlobalManager.AdminGroup, _Parent));
                Form Form = new Form((Dictionary<string, string> Variables) =>
                {
                    AddSubGroup(Data.Manager.GlobalManager.AdminGroup, Variables["name"]);
                    Update();
                });
                Form.AddTextBox("name", "Name", "Name");
                Form.InformationText = "Add a new group agreagting administrators:";
                Form.ValidationText = "Add";
                Body.Add(Form);
            }
            Body.Add("<H3>Teachers</H3>");
            {
                Body.Add(GenerateSubGroupsInfo(Data.Manager.GlobalManager.TeacherGroup, _Parent));
                Form Form = new Form((Dictionary<string, string> Variables) => 
                {
                        AddSubGroup(Data.Manager.GlobalManager.TeacherGroup, Variables["name"]);
                        Update();
                });
                Form.AddTextBox("name", "Name", "Name");
                Form.InformationText = "Add a new group agreagting teachers:";
                Form.ValidationText = "Add";
                Body.Add(Form);
            }
            Body.Add("<H3>Students</H3>");
            {
                Body.Add(GenerateSubGroupsInfo(Data.Manager.GlobalManager.StudentGroup, _Parent));
                Form Form = new Form((Dictionary<string, string> Variables) =>
                {
                    AddSubGroup(Data.Manager.GlobalManager.StudentGroup, Variables["name"]);
                    Update();
                });
                Form.AddTextBox("name", "Name", "Name");
                Form.InformationText = "Add a new group agreagting students:";
                Form.ValidationText = "Add";
                Body.Add(Form);
            }

            return Body.ToArray();
        }

        Elisyan.Content.Content GrindManagementSetup()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H2>Managers</H2>");

            Dictionary<string, List<Data.Grid.Manager>> managers = Data.Manager.GlobalManager.GetManagersGroups();
            foreach (string group in managers.Keys)
            {

                Body.Add("<H3>" + group + "</H3>");
                List<Data.Grid.Manager> submanagers = managers[group];
                Elisyan.Content.Bootstrap.Table table = new Elisyan.Content.Bootstrap.Table();
                table.Columns.Add("Name");
                table.Columns.Add("Address");
                table.Columns.Add("Status");
                table.Columns.Add("");

                foreach (Data.Grid.Manager manager in submanagers)
                {
                    Elisyan.Content.Bootstrap.Table.Row Row = new Elisyan.Content.Bootstrap.Table.Row();
                    Row.Columns.Add(manager.Name);
                    Row.Columns.Add(manager.Address);
                    if (manager.CurentStatus == FactWeb.Data.Grid.Manager.Status.Responding)
                    { Row.Type = Elisyan.Content.Bootstrap.Type.Success; Row.Columns.Add("Responding"); }
                    if (manager.CurentStatus == FactWeb.Data.Grid.Manager.Status.Busy)
                    { Row.Type = Elisyan.Content.Bootstrap.Type.Warning; Row.Columns.Add("Busy"); }
                    if (manager.CurentStatus == FactWeb.Data.Grid.Manager.Status.Pairing)
                    { Row.Type = Elisyan.Content.Bootstrap.Type.Warning; Row.Columns.Add("Pairing"); }
                    if (manager.CurentStatus == FactWeb.Data.Grid.Manager.Status.NotResponding)
                    { Row.Type = Elisyan.Content.Bootstrap.Type.Danger; Row.Columns.Add("Not Responding"); }



                    Row.Columns.Add(new Elisyan.Content.Bootstrap.Button()
                    {
                        Text = "Remove",
                        Action = new Elisyan.Action.Action(() => { /* TODO: remove the manager */ return null; })
                    });

                    table.Rows.Add(Row);
                }

                Body.Add(table);
            }

            {
                Form Form = new Form((Dictionary<string, string> Variables) => 
                {
                    object result = Data.Manager.GlobalManager.BindManager(Variables["address"], Variables["name"], Variables["group"]);
                    if (result == null) { Fact.Log.Error("Impossible to add the manager"); }
                    Update();
                });
                Form.AddTextBox("name", "Name", "Name");
                Form.AddTextBox("address", "Address", "Address");
                Form.AddTextBox("group", "Group", "Group");

                Form.InformationText = "Add an additional worker to the cluster used by the system:";
                Form.ValidationText = "Add";
                Body.Add(Form);
            }
            return Body.ToArray();
        }

        Elisyan.Content.Content RepositoryManagementSetup()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H2>Repositories</H2>");

            {
                Body.Add("<H3>Git repositories</H3>");
                Form Form = new Form((Dictionary<string, string> Variables) =>
                {
                    Data.Manager.GlobalManager.GitRepositoryCreationScript = Variables["script"];
                }
                );
                Form.FormType = Form.Type.Modification;
                Form.AddTextBox("script", "Script location", "Script location");
                Form.InformationText =
                    "Set the location of the script use to create git repositories.</br>" +
                    "The script will call use like: ./script (repository_name) (allowed_users)+</br>" +
                    "The script must return the full name of the repository.";
                Form.ValidationText = "Update";


            }

            {
                Body.Add("<H3>Svn repositories</H3>");
                Form Form = new Form((Dictionary<string, string> Variables) =>
                {
                    Data.Manager.GlobalManager.GitRepositoryCreationScript = Variables["script"];
                }
                );
                Form.FormType = Form.Type.Modification;
                Form.AddTextBox("script", "Script location", "Script location");
                Form.InformationText =
                    "Set the location of the script use to create SVN repositories.</br>" +
                    "The script will call use like: ./script (repository_name) (allowed_users)+</br>" +
                    "The script must return the full name of the repository.";
                Form.ValidationText = "Update";


            }

            {
                Body.Add("<H3>Ftp repositories</H3>");
                Form Form = new Form((Dictionary<string, string> Variables) =>
                {
                    Data.Manager.GlobalManager.GitRepositoryCreationScript = Variables["script"];
                }
                );
                Form.FormType = Form.Type.Modification;
                Form.AddTextBox("script", "Script location", "Script location");
                Form.InformationText =
                    "Set the location of the script use to create Ftp repositories.</br>" +
                    "The script will call use like: ./script (repository_name) (allowed_users)+</br>" +
                    "The script must return the full name of the repository.";
                Form.ValidationText = "Update";
            }

            return Body.ToArray();
        }
    }
}
