﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages.System
{
    class Group : Page
    {
        Elisyan.Authentication.Group _Group;
        WebSite _Parent;
        public Group(Elisyan.Authentication.Group Group, WebSite Parent)
        {
            _Parent = Parent;
            _Group = Group;
            Register = false;
            Name = Group.Name;
            Update();
        }

        void Update()
        {
            List<Elisyan.Content.Content> Body = new List<Elisyan.Content.Content>();
            Body.Add("<H1>" + _Group.Name + "</H1>");
            Body.Add("<H2>Groups</H2>");
            {
                Body.Add(Main.GenerateSubGroupsInfo(_Group, _Parent));
                {
                    Form Form = new Form((Dictionary<string, string> Variables) =>
                    {
                        Main.AddSubGroup(_Group, Variables["name"]);
                        Update();
                    });
                    Form.AddTextBox("name", "Name", "Name");
                    Form.InformationText = "Add a new group to " + _Group.Name + ":";
                    Form.ValidationText = "Add";
                    Body.Add(Form);
                }
            }
            Body.Add("<H2>Users</H2>");
            {
                Body.Add(Main.GenerateUserInfo(_Group, _Parent));
                {
                    Form Form = new Form((Dictionary<string, string> Variables) =>
                    {
                        Main.AddUser(_Group, Variables["name"], Variables["password"]);
                        Update();
                    });
                    Form.AddTextBox("name", "Name", "Name");
                    Form.AddTextBox("password", "Password", "Password", true);
                    Form.InformationText = "Add a user to " + _Group.Name + ":";
                    Form.ValidationText = "Add";
                    Body.Add(Form);
                }
            }
            Content = Body.ToArray();
        }
    }
}
