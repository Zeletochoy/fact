﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages
{
    class WebSite
    {
        Template Template = new Template();
        Elisyan.Network.Server Server = null;
        


        static WebSite()
        {

        }


        public WebSite()
        {
            Server = new Elisyan.Network.Server("http://*:8990/", "https://*:8991/");
            Server.Add(Elisyan.Bootstrap.Global.BootstrapDefaultStyle(), "/css/bootstrap.css");
            Server.Add(Elisyan.Bootstrap.Global.BootstrapDefaultScript(), "/js/bootstrap.js");
            Server.Add(Elisyan.JQuery.Global.JQueryDefaultScript(), "/js/jquery.js");

            Register(new Main());
            Register(new System.Main(this));
            Register(new Projects.Main(this));
            
            {
                PageNotFound PageNotFound = new PageNotFound();
                Elisyan.Page ElisyanPage = new Elisyan.Page();
                ElisyanPage.Head = "<link href=\"/css/bootstrap.css\" rel=\"stylesheet\"><script src=\"/js/jquery.js\"></script><script src=\"/js/bootstrap.js\"></script>";
                ElisyanPage.Body = Template.Wrap(PageNotFound, ElisyanPage);
                Server.PageNotFound = ElisyanPage;
            }

        }

        class PageNotFound : Page
        {
            public PageNotFound()
            {
                Content = new Elisyan.Content.Bootstrap.Alert()
                {
                    Content = "<H1>404 Not Found<small>The requested page does not exist on this server or you do not have acces to it</small></H1>",
                    Type = Elisyan.Content.Bootstrap.Type.Danger
                };
            }
        }

        public Elisyan.Page Register(Page Page)
        {
            try
            {
                Elisyan.Page ElisyanPage = new Elisyan.Page();
                ElisyanPage.Head = "<link href=\"/css/bootstrap.css\" rel=\"stylesheet\"><script src=\"/js/jquery.js\"></script><script src=\"/js/bootstrap.js\"></script>";
                ElisyanPage.Body = Template.Wrap(Page, ElisyanPage);
                if (Page.Register)
                {
                    if (Page.Location == "") { Server.MainPage = ElisyanPage; Server.Add(ElisyanPage, "/index.hx"); }
                    else { Server.Add(ElisyanPage, "/" + Page.Location); }
                }
                return ElisyanPage;
            }
            catch (Exception e)
            {
                Fact.Log.Exception(e);
            }
            return null;
        }

        public void Run()
        {
            Server.Run();
        }
    }
}
