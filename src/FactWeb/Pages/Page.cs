﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages
{
    class Page
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public Elisyan.Content.Content Content { get; set; }
        bool _Register = true;
        public bool Register  { get{ return _Register; } set { _Register = value; } }
    }
}
