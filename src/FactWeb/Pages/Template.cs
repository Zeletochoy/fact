﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb.Pages
{
    class Template
    {
        class PageContent : Elisyan.Content.Content
        {
            Page _Page = null;
            public PageContent(Page Page)
            { _Page = Page; }
            public override string ToString(Elisyan.Session Session)
            {
                return _Page.Content.ToString(Session);
            }
            public override string ToString()
            {
                return _Page.Content.ToString();
            }
        }
        public Template() { }
        public Elisyan.Content.Content Wrap(Page RealPage, Elisyan.Page Page)
        {
            try
            {
                Elisyan.Content.Bootstrap.CenterPanel Body = new Elisyan.Content.Bootstrap.CenterPanel();
                Elisyan.Content.Bootstrap.NavBar NavBar = new Elisyan.Content.Bootstrap.NavBar()
                    {
                        WebSiteName = "Fact",
                    };
                NavBar.Links.Add(new Elisyan.Content.Bootstrap.NavBar.Link() { Text = "Home", Action = Elisyan.Page.ActionNavigate("/index.hx") });
                NavBar.Links.Add(new Elisyan.Content.Bootstrap.NavBar.Link() { Text = "Projects", Action = Elisyan.Page.ActionNavigate("/projects.hx") });
                NavBar.Links.Add(new Elisyan.Content.Bootstrap.NavBar.Link() { Text = "System", Action = Elisyan.Page.ActionNavigate("/system.hx") });

                NavBar.LoginForm = new Elisyan.Page()
                {
                    Head = "<link href=\"css/bootstrap.css\" rel=\"stylesheet\"><script src=\"js/jquery.js\"></script><script src=\"js/bootstrap.js\"></script>",

                    Body = 
                    new Elisyan.Content.Bootstrap.CenterPanel()
                    {
                        Main = new Elisyan.Content.Form()
                        {
                            Action = new Elisyan.Action.Action(
                              (Dictionary<string, string> Variables, Elisyan.Session Session) =>
                              {
                                  try
                                  {
                                      Session.Authenticate(
                                          Data.Manager.GlobalManager.GlobalGroup,
                                          Variables["login"],
                                          Variables["password"]);
                                      return null;
                                  }
                                  catch (Exception e)
                                  {
                                      Fact.Log.Exception(e);
                                      return null;
                                  }
                              }),
                            Content = new Elisyan.Content.Content[]
                        {
                            "</br>",
                            new Elisyan.Content.Bootstrap.FormTextbox() { Name = "login", Info = "Login", Text = "Login" }, "</br>",
                            new Elisyan.Content.Bootstrap.FormTextbox() { Name = "password", Info = "Password", Text = "Password", Hidden = true }, "</br>",
                            new Elisyan.Content.FormSubmit() { Text = "Log In" }
                        }
                        },

                    }
                };

                Body.Main = new Elisyan.Content.Content[]
            {
                "</br>",
                NavBar,
                new PageContent(RealPage)
            };
                return Body;
            }
            catch (Exception e)
            {
                Fact.Log.Exception(e);
            }
            return null;
        }
    }
}
