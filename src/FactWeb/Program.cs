﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactWeb
{
    class Program
    {
        static void Main(string[] args)
        {
            Data.Manager.GlobalManager.Init();
            Elisyan.Job.CreateTimer(Data.Manager.GlobalManager.Update, 30 * 1000, true);
            Elisyan.Job.CreateTimer(Data.Manager.GlobalManager.Save, (60 * 1000), true);
            Pages.WebSite Website = new FactWeb.Pages.WebSite();
            Website.Run();
        }
    }
}
