﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fact.Character;
using Fact.Processing;
using Fact.Runtime;
using Fact.Test.Result;

namespace JavaCodingStyle.Format
{
    class Line
    {
        public static Result Check80Cols(MetaLine Line)
        {
            if (Line.File.Type == Fact.Processing.File.FileType.JavaSource)
            {
                if (Line.Length > 80)
                {
                    return new Fact.Test.Result.Error("More than 80 Columns", "A line must not have more than 80 Columns");
                }
            }
            return null;
        }
    }
}
