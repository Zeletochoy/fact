﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fact.Character;
using Fact.Processing;
using Fact.Runtime;
using Fact.Test.Result;

namespace JavaCodingStyle.Format
{
    class Str
    {
        public static Result CheckGenericNewLine(MetaChar Char)
        {
            if (Char.File.Type == Fact.Processing.File.FileType.JavaSource)
            {
                if (Char == '\\' &&
                    !Char.Next.Equals(null) &&
                    !Char.Previous.Equals(null) &&
                    Char.Next == 'n' &&
                    Char.Previous != '\\')
                {
                    return new Fact.Test.Result.Error("OS dependant 'new line'", "A 'new line' must not be os dependant.");
                }
            }
            return null;
        }
    }
}
