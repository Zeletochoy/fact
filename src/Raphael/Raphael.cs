﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Raphael
{
    class Raphael : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Raphael";
            }
        }

        static void CreateFolder(string Path)
        {
            try
            {
                if (Path == "") { return; }
                if (System.IO.Directory.Exists(Path)) { return; }
                if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(Path))) { CreateFolder(System.IO.Path.GetDirectoryName(Path)); }
                System.IO.Directory.CreateDirectory(Path);
            }
            catch { }
        }

        public static void MakeInstall(string path)
        {
            CreateFolder(path);
            Fact.Log.Verbose("Raphael will be installed in file: " + path + "/raphael.js");
            System.IO.File.WriteAllText(path + "/raphael.js", Properties.Resources.Raphael);
        }

        public override int Run(string[] args)
        {
            if (args.Length == 2 && args[0].ToLower() == "install")
            { 
                MakeInstall(args[1]);
                return 0; 
            }

            int Offset = 0;
            bool ShowLogo = true;
            if (args.Length > 1 && args[0] == "--NoLogo")
            {
                Offset++;
                ShowLogo = false;
            }
            if (args.Length == Offset + 2 &&
                System.IO.File.Exists(args[Offset]))
            {
                try
                {
                    string path = System.IO.Path.GetDirectoryName(args[Offset + 1]);
                    if (path == "") { path = "."; }
                    CreateFolder(path);
                    if (!System.IO.File.Exists(path + "/raphael.js"))
                    {
                        Fact.Log.Warning("No raphael.js file found. Raphael will be installed.");
                        MakeInstall(path);
                    }
                }
                catch { return 1; }
                Fact.Log.Verbose("Load package : " + args[Offset] + " ...");
                Fact.Processing.Project Project = new Fact.Processing.Project("");
                Project.Load(args[Offset]);
                Fact.Log.Verbose("Generate web page ...");


                Page Page = new Page(Project, ShowLogo);

                System.IO.File.WriteAllText(args[Offset + 1], Page.Generate());


                return 0;
            }

            Console.WriteLine("Usage : Raphael [--NoLogo] [--ShowEmptyFile] [--ShowEmptySet] package results");
            Console.WriteLine("        package: The package that contains the results");
            Console.WriteLine("        results: the path of the html page generated");
            Console.WriteLine("");
            Console.WriteLine("Usage : Raphael Install path");
            Console.WriteLine("        path: The path where Raphael must be installed");
            
            return 0;
        }
    }
}
