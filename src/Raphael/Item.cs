﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Raphael
{
    abstract class Item
    {
        public virtual int Width
        { get { return 0; } }
        public virtual int Height
        { get { return 10; } }
        public virtual string Generate()
        { return ""; }
    }
}
