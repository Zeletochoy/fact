﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Raphael
{
    class Page
    {
        List<Item> _Items = new List<Item>();
        Fact.Processing.Project _Project;
        string _Title = "";

        bool _ShowEmptyFile = false;
        bool _ShowEmptyTestSet = false;
        bool _ShowLogo = true;

        public Page(Fact.Processing.Project Project, bool ShowLogo)
        {
            _Title = Project.Name;
            _Project = Project;
            _ShowLogo = ShowLogo;
        }

        public string GenerateResultColorBarAttrib(Fact.Test.Result.Result result)
        {
            StringBuilder Builder = new StringBuilder();

            if (result is Fact.Test.Result.Error)
            {
                Builder.Append(".attr({fill: \"#EDD\"})");
                Builder.Append(".attr({stroke: \"#FEE\"})");
            }
            else if (result is Fact.Test.Result.Passed)
            {
                Builder.Append(".attr({fill: \"#DED\"})");
                Builder.Append(".attr({stroke: \"#DFD\"})");
            }
            else
            {
                Builder.Append(".attr({fill: \"#DDD\"})");
                Builder.Append(".attr({stroke: \"#EEE\"})");
            }

            return Builder.ToString();
        }

        public string GenerateResultColorBoxAttrib(Fact.Test.Result.Result result)
        {
            StringBuilder Builder = new StringBuilder();

            if (result is Fact.Test.Result.Error)
            {
                Builder.Append(".attr({fill: \"#A99\"})");
                Builder.Append(".attr({stroke: \"#A88\"})");
            }
            else if (result is Fact.Test.Result.Passed)
            {
                Builder.Append(".attr({fill: \"#9A9\"})");
                Builder.Append(".attr({stroke: \"#8A8\"})");
            }
            else
            {
                Builder.Append(".attr({fill: \"#999\"})");
                Builder.Append(".attr({stroke: \"#888\"})");
            }

            return Builder.ToString();
        }

        public int ComputeSize(string Info)
        {
            int count = 1;
            for (int n = 0; n < Info.Length; n++)
            {
                if (Info[n] == '\n') { count++; }
            }
            return count * 12;
        }

        public string FormatText(string Text)
        {
            return Text.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n").Replace("\"", "\\\"");
        }

        public void ComputeInfoRectangle(Fact.Test.Result.Result result, StringBuilder Code, ref int offset, long uid)
        {
            int size = 23;
            if (result.Info == "")
            {
                Code.Append("var descrect" + uid.ToString() + " = mainPaper.rect(50, " + (offset + 12).ToString() + ", 700, " + size.ToString() + ", 5)");
                Code.Append(GenerateResultColorBarAttrib(result));
                Code.Append(";");
                Code.Append("var txt = mainPaper.text(400, " + (offset + 12).ToString() + ", \"" + FormatText(result.Text) + "\")");
                Code.Append(";");
                return;
            }
            size += ComputeSize(result.Info);
            Code.Append("var descrect" + uid.ToString() + " = mainPaper.rect(50, " + (offset + 12).ToString() + ", 700, " + size.ToString() + ", 5)");
            Code.Append(GenerateResultColorBarAttrib(result));
            Code.Append(";");
            Code.Append("var filerect" + uid.ToString() + " = mainPaper.rect(50, " + offset.ToString() + ", 700, 24, 5)");
            Code.Append(GenerateResultColorBoxAttrib(result));
            Code.Append(";");
            Code.Append("var txt = mainPaper.text(400, " + (offset + 12).ToString() + ", \"" + FormatText(result.Text) + "\")");
            Code.Append(";");
            Code.Append("var txt = mainPaper.text(55, " + (offset + 18 + size / 2).ToString() + ", \"" + FormatText(result.Info) + "\")");
            Code.Append(".attr({\"text-anchor\": \"start\"})");
            Code.Append(";");
            offset += size + 20;
        }

        public string GenerateFileReport(List<Fact.Test.Result.Result> Results, Fact.Processing.File File, ref long Height)
        {
            long uid = 0;
            int offset = 0;
            return GenerateFileReport(Results, File, ref Height, ref uid, ref offset);
        }

        public string GenerateFileReport(List<Fact.Test.Result.Result> Results, Fact.Processing.File File, ref long Height, ref long Uid, ref int Offset)
        {
            StringBuilder Builder = new StringBuilder();
            string[] Lines = new string[0];
            if (File != null)
            {
                File.Rebuild();
                Lines = File.GetLines(false);
            }

            foreach (Fact.Test.Result.Result result in Results)
            {


                    if (result.FromLine == result.ToLine && result.FromLine > 0 && result.ToLine - 1 < Lines.Length && result.Info == "")
                    {
                        Builder.Append("var descrect" + Uid.ToString() + " = mainPaper.rect(50, " + (Offset + 12).ToString() + ", 700, 33, 5)");
                        Builder.Append(GenerateResultColorBarAttrib(result));
                        Builder.Append(";");
                        Builder.Append("var filerect" + Uid.ToString() + " = mainPaper.rect(50, " + Offset.ToString() + ", 700, 24, 5)");
                        Builder.Append(GenerateResultColorBoxAttrib(result));
                        Builder.Append(";");

                        Builder.Append("var txt = mainPaper.text(400, " + (Offset + 12).ToString() + ", \"" + result.Text + "\")");
                        Builder.Append(";");
                        Builder.Append("var txt = mainPaper.text(55, " + (Offset + 12 + 22).ToString() + ", \"" + result.FromLine.ToString() + ": " + Lines[result.FromLine - 1].Replace("\r", "").Replace("\\", "\\\\").Replace("\"", "\\\"") + "\")");
                        Builder.Append(".attr({\"text-anchor\": \"start\"})");
                        Builder.Append(";");
                        Offset += 55;
                    }
                    else if (result.FromLine == result.ToLine && result.FromLine > 0 && result.ToLine - 1 < Lines.Length && result.Info != "")
                    {

                        Builder.Append("var descrect" + Uid.ToString() + " = mainPaper.rect(50, " + (Offset + 12).ToString() + ", 700, 43, 5)");
                        Builder.Append(GenerateResultColorBarAttrib(result));
                        Builder.Append(";");
                        Builder.Append("var filerect" + Uid.ToString() + " = mainPaper.rect(50, " + Offset.ToString() + ", 700, 24, 5)");
                        Builder.Append(GenerateResultColorBoxAttrib(result));
                        Builder.Append(";");

                        Builder.Append("var txt = mainPaper.text(400, " + (Offset + 12).ToString() + ", \"" + result.Text + "\")");
                        Builder.Append(";");
                        Builder.Append("var txt = mainPaper.text(55, " + (Offset + 12 + 22).ToString() + ", \"" + result.Info + "\")");
                        Builder.Append(".attr({\"text-anchor\": \"start\"})");
                        Builder.Append(";");
                        Builder.Append("var txt = mainPaper.text(55, " + (Offset + 12 + 32).ToString() + ", \"" + result.FromLine.ToString() + ": " + Lines[result.FromLine - 1].Replace("\r", "").Replace("\\", "\\\\").Replace("\"", "\\\"") + "\")");
                        Builder.Append(".attr({\"text-anchor\": \"start\"})");
                        Builder.Append(";");

                        Offset += 65;
                        
                    }
                    else
                    {
                        ComputeInfoRectangle(result, Builder, ref Offset, Uid);
                    }

                    if (result is Fact.Test.Result.Group)
                    {

                        Fact.Test.Result.Group group = result as Fact.Test.Result.Group;
                        Builder.Append(GenerateFileReport(group.GetTestResults(), File, ref Height, ref Uid, ref Offset));
                    }

                    Uid++;
                

                
            }

            Height = Offset;
            return Builder.ToString();
        }

        public string Generate()
        {
            long TreeViewHeight = 0;

            foreach (Fact.Processing.File File in _Project)
            {
                if (_ShowEmptyFile || File.GetTestResults().Count > 0)
                {
                    TreeViewHeight += 25;
                }
            }

            if (TreeViewHeight < 600) { TreeViewHeight = 600; }

            StringBuilder ScriptBuilder = new StringBuilder();


            int TreeViewOffset = 0;
            long MaxHeight = TreeViewHeight;
            long uid = 0;

            if (_ShowEmptyFile || _Project.GetTestResults().Count > 0)
            {
                long Height = 0;
                ScriptBuilder.AppendLine("var rect" + uid.ToString() + " = treeviewPaper.rect(0, " + TreeViewOffset.ToString() + ", 200, 24, 5)");
                ScriptBuilder.AppendLine(".attr({fill: \"#AAA\"})");
                ScriptBuilder.AppendLine(".attr({stroke: \"#AAA\"})");
                ScriptBuilder.AppendLine(".attr({\"stroke-opacity\": 0})");
                ScriptBuilder.AppendLine(".mouseover( function () { rect" + uid + ".animate({fill: \"#CCC\"}, 200); } )");
                ScriptBuilder.AppendLine(".mouseout( function () { rect" + uid + ".animate({fill: \"#AAA\"}, 200); } )");
                ScriptBuilder.AppendLine(".click( function () { mainPaper.clear(); " + GenerateFileReport(_Project.GetTestResults(), null, ref Height) + " } )");
                ScriptBuilder.AppendLine(";");
                if (Height > MaxHeight)
                { MaxHeight = Height; }
                ScriptBuilder.AppendLine("var txt = treeviewPaper.text(100, " + (TreeViewOffset + 12).ToString() + ", \"Project\")");
                ScriptBuilder.AppendLine(".mouseover( function () { rect" + uid + ".animate({fill: \"#CCC\"}, 200); } )");
                ScriptBuilder.AppendLine(".mouseout( function () { rect" + uid + ".animate({fill: \"#AAA\"}, 200); } )");
                ScriptBuilder.AppendLine(".click( function () { mainPaper.clear(); " + GenerateFileReport(_Project.GetTestResults(), null, ref Height) + " } )");
                ScriptBuilder.AppendLine(";");
                TreeViewOffset += 25;
                uid++;
                if (Height > MaxHeight)
                { MaxHeight = Height; }
            }

            foreach (Fact.Processing.File File in _Project)
            {
                if (_ShowEmptyFile || File.GetTestResults().Count > 0)
                {
                    long Height = 0;
                    ScriptBuilder.AppendLine("var rect" + uid.ToString() + " = treeviewPaper.rect(0, " + TreeViewOffset.ToString() + ", 200, 24, 5)");
                    ScriptBuilder.AppendLine(".attr({fill: \"#AAA\"})");
                    ScriptBuilder.AppendLine(".attr({stroke: \"#AAA\"})");
                    ScriptBuilder.AppendLine(".attr({\"stroke-opacity\": 0})");
                    ScriptBuilder.AppendLine(".mouseover( function () { rect" + uid + ".animate({fill: \"#CCC\"}, 200); } )");
                    ScriptBuilder.AppendLine(".mouseout( function () { rect" + uid + ".animate({fill: \"#AAA\"}, 200); } )");
                    ScriptBuilder.AppendLine(".click( function () { mainPaper.clear(); " + GenerateFileReport(File.GetTestResults(), File, ref Height) + " } )");
                    ScriptBuilder.AppendLine(";");
                    if (Height > MaxHeight)
                    { MaxHeight = Height; }
                    ScriptBuilder.AppendLine("var txt = treeviewPaper.text(100, " + (TreeViewOffset + 12).ToString() + ", \"" + File.Directory + "/" + File.Name + "\")");
                    ScriptBuilder.AppendLine(".mouseover( function () { rect" + uid + ".animate({fill: \"#CCC\"}, 200); } )");
                    ScriptBuilder.AppendLine(".mouseout( function () { rect" + uid + ".animate({fill: \"#AAA\"}, 200); } )");
                    ScriptBuilder.AppendLine(".click( function () { mainPaper.clear(); " + GenerateFileReport(File.GetTestResults(), File, ref Height) + " } )");
                    ScriptBuilder.AppendLine(";");
                    TreeViewOffset += 25;
                    uid++;
                    if (Height > MaxHeight)
                    { MaxHeight = Height; }
                }
            }



            StringBuilder Builder = new StringBuilder();
            Builder.AppendLine("<!DOCTYPE html>");
            Builder.AppendLine("<html lang=\"en\">");
            Builder.AppendLine("<head>");
            Builder.AppendLine("<meta charset=\"utf-8\">");
            Builder.AppendLine("<title>" + _Title + "</title>");
            Builder.AppendLine("<script src=\"raphael.js\"></script>");
            Builder.AppendLine("<style type=\"text/css\" media=\"screen\">");
            Builder.AppendLine("#mainview{");
            Builder.AppendLine("min-width: 800px;");
            Builder.AppendLine("min-height: " + MaxHeight.ToString() + "px;");
            Builder.AppendLine("}");

            Builder.AppendLine("#treeview{");
            Builder.AppendLine("min-width: 200px;");
            Builder.AppendLine("min-height: " + MaxHeight.ToString() + "px;");
            Builder.AppendLine("}");
            Builder.AppendLine("#geninfo{");
            Builder.AppendLine("min-width: 200px;");
            Builder.AppendLine("min-height: 70px;");
            Builder.AppendLine("max-height: 80px;");
            Builder.AppendLine("}");
            Builder.AppendLine("</style>");
            Builder.AppendLine("<script>");
            Builder.AppendLine("window.onload = function () {");
            Builder.AppendLine("var mainPaper = Raphael(\"mainview\", 800, " + MaxHeight.ToString() + ");");
            Builder.AppendLine("var treeviewPaper = Raphael(\"treeview\", 200," + MaxHeight.ToString() + ");");


            Builder.Append(ScriptBuilder.ToString());

            Builder.AppendLine("};");
            Builder.AppendLine("</script>");
            Builder.AppendLine("</head>");
            Builder.AppendLine("<body bgcolor=\"#808080\" >");
            Builder.AppendLine("<div id=\"maindiv\">");
            Builder.AppendLine("<table>");
            Builder.AppendLine("<tr>");
            Builder.AppendLine("<th><div id=\"treeview\"></div></th>");
            Builder.AppendLine("<th><div id=\"mainview\"></div></th>");
            Builder.AppendLine("</tr>");
            if (_ShowLogo)
            {
                Builder.AppendLine("<tr><th><div id=\"geninfo\">Generated by F.A.C.T</div></th></tr>");
            }
            else
            {
                Builder.AppendLine("<tr><th><div id=\"geninfo\"></div></th></tr>");
            }
            Builder.AppendLine("</table>");

            Builder.AppendLine("</div>");
            Builder.AppendLine("</body>");
            Builder.AppendLine("</html>");
            return Builder.ToString();
        }
    }
}
