﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Javac
{
    class Javac : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Javac";
            }
        }

        public static string FindFactPackage(string fileName)
        {
            if (fileName == "") { return ""; }
            if (System.IO.Path.GetExtension(fileName).ToLower() == ".ff")
            { return fileName; }
            return FindFactPackage(System.IO.Path.GetDirectoryName(fileName));
        }

        public static string Extract(string fileName, string where)
        {
            if (!System.IO.File.Exists(fileName))
            {
                string factPackage = FindFactPackage(fileName);
                string factLocation = fileName.Substring(factPackage.Length);
                if (factLocation.StartsWith("/") || factLocation.StartsWith("\\"))
                {
                    factLocation = factLocation.Substring(1);
                }
                if (!System.IO.File.Exists(factPackage))
                {
                    Fact.Log.Warning("Fact package not found and ignored : " + factPackage);
                    return "";
                }
                Fact.Processing.Project Project = new Fact.Processing.Project("");
                Project.Load(factPackage);
                if (factLocation.EndsWith("/*"))
                {
                    factLocation = factLocation.Substring(0, factLocation.Length - 2);
                    Fact.Tools.RecursiveMakeDirectory(where + "/" + factLocation);
                    Project.ExtractDirectory(factLocation, where + "/" + factLocation, new Fact.Processing.File.FileType[] { Fact.Processing.File.FileType.JavaSource });
                }
                else
                {
                    try
                    {
                        Fact.Processing.File source = Project.GetFile(factLocation);
                        
                        if (source != null)
                        {
                            Fact.Tools.RecursiveMakeDirectory(where + "/" + source.Directory);
                            System.IO.File.WriteAllLines(where + "/" + source.Directory + "/" + source.Name, source.GetLines(false).ToArray());
                        }
                        else
                        {
                            Fact.Log.Warning("file not found " + factLocation + " in package " + factPackage);
                        }
                    }
                    catch { Fact.Log.Warning("Error while creating file : " + factLocation); }
                    
                }
                return "";
            }
            return fileName;
        }

        public void RecursiveLoad(string Directory, StringBuilder CommandLine)
        {
            foreach (string dir in System.IO.Directory.GetDirectories(Directory))
            {
                RecursiveLoad(dir, CommandLine);
            }
            foreach (string file in System.IO.Directory.GetFiles(Directory))
            {
                if (file != "")
                {
                    CommandLine.Append(" \"" + new System.IO.FileInfo(file).FullName + "\" ");
                }
            }
        }

        public override int Run(string[] args)
        {
            List<string> inputFile = new List<string>();
            string output = "out.exe";

            bool inputMode = false;
            bool outputMode = false;
            bool refMode = false;

            string Compiler = Fact.Internal.Information.GetJAVACBinary();

            for (int n = 0; n < args.Length; n++)
            {
                if (args[n] == "-i" || args[n] == "--Input")
                { inputMode = true; outputMode = false; refMode = false; continue; }
                if (args[n] == "-o" || args[n] == "--Output")
                { inputMode = false; outputMode = true; refMode = false; continue; }

                if (outputMode) { output = args[n]; }
                if (inputMode) { inputFile.Add(args[n]); }
            }

            string tmp_dir = Fact.Internal.Information.GetTempFileName();
            try
            {
                System.IO.Directory.CreateDirectory(tmp_dir);
            }
            catch { Fact.Log.Error("Impossible to create temp directory : " + tmp_dir);}

            StringBuilder CommandLine = new StringBuilder();

            foreach (string file in inputFile)
            {
                string result = Extract(file, tmp_dir);
                if (result != "")
                {
                    CommandLine.Append(" \"" + new System.IO.FileInfo(result).FullName + "\" ");
                }
            }

            RecursiveLoad(tmp_dir, CommandLine);
            
            Fact.Runtime.Process Process = new Fact.Runtime.Process(Compiler, CommandLine.ToString());
            Fact.Log.Verbose("Call javac " + CommandLine.ToString());
            Fact.Runtime.Process.Result Result = Process.Run(-1);
            if (Result.ExitCode != 0)
            {
                Fact.Log.Error("Javac: ");
                string[] outs = Result.StdOut.Split(new char[] { '\n', '\r' });
                foreach (string str in outs) { if (str.Length > 0) { Fact.Log.Error("  " + str); } }
                string[] errors = Result.StdErr.Split(new char[] { '\n', '\r' });
                foreach (string error in errors) { if (error.Length > 0) { Fact.Log.Error("  " + error); } }
            }

            foreach (string file in inputFile)
            {
                string result = FindFactPackage(file);
                if (result != "")
                {
                    string FactFile = file.Substring(result.Length);
                    while (FactFile.StartsWith("/")) { FactFile = FactFile.Substring(1); }

                    if (FactFile.EndsWith("/*"))
                    {
                        foreach (string fullfile in Fact.Tools.ExpandPath(tmp_dir + "/" + FactFile))
                        {
                            if (fullfile.EndsWith(".class"))
                            {
                                if (System.IO.File.Exists(fullfile))
                                {

                                    Fact.Log.Verbose("Reinjecting " + fullfile);
                                    Fact.Processing.Project Project = new Fact.Processing.Project("");
                                    Project.Load(result);
                                    Fact.Processing.File ClassFile = new Fact.Processing.File(
                                        System.IO.File.ReadAllBytes(fullfile),
                                        System.IO.Path.GetFileName(fullfile));
                                    ClassFile.Type = Fact.Processing.File.FileType.JavaClass;
                                    Project.AddFile(System.IO.Path.GetDirectoryName(FactFile),
                                    ClassFile);
                                    Project.Save(result);
                                }
                            }
                        }
                    }
                    else
                    {
                        string FactFileName = System.IO.Path.GetFileNameWithoutExtension(FactFile) + ".class";
                        FactFile = System.IO.Path.GetDirectoryName(FactFile) + "/" + FactFileName;
                        if (System.IO.File.Exists(tmp_dir + "/" + FactFile))
                        {
                            Fact.Log.Verbose("Reinjecting " + FactFile);
                            Fact.Processing.Project Project = new Fact.Processing.Project("");
                            Project.Load(result);
                            Fact.Processing.File ClassFile = new Fact.Processing.File(
                                    System.IO.File.ReadAllBytes(tmp_dir + "/" + FactFile),
                                    System.IO.Path.GetFileName(FactFile));
                            ClassFile.Type = Fact.Processing.File.FileType.JavaClass;
                            Project.AddFile(System.IO.Path.GetDirectoryName(FactFile),
                                ClassFile);
                            Project.Save(result);
                        }
                    }
                }
            }

            Fact.Tools.RecursiveDelete(tmp_dir);
            return 0;
        }
    }
}
