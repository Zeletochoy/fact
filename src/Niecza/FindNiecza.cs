﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Niecza
{
    static class FindNiecza
    {
        public static string GetNieczaBinary()
        {
            string file = Fact.Internal.Information.Where("niecza");

            if (file == "" && System.IO.File.Exists("system/internal/bin/niecza/run/Niecza.exe"))
            {
                file = "system/internal/bin/niecza/run/Niecza.exe";
            }
            if (file == "")
            {
                Fact.Log.Warning("Niecza has not been found.");
                Fact.Log.Warning("Niecza will be compiled now.");
                if (!System.IO.Directory.Exists("system/internal/bin"))
                {
                    Fact.Log.Warning("system/internal/bin directory not found (will be created).");
                    Fact.Tools.RecursiveMakeDirectory("system/internal/bin");
                }
                if (!System.IO.Directory.Exists("system/internal/package"))
                {
                    Fact.Log.Warning("system/internal/package directory not found (will be created).");
                    Fact.Tools.RecursiveMakeDirectory("system/internal/package");
                }

                if (System.IO.File.Exists("system/internal/package/niecza.ff"))
                {
                    Fact.Log.Warning("The package nieackza has already be downloaded.");
                    Fact.Log.Warning("Remove the old nieckza package.");
                    Fact.Tools.RecursiveDelete("system/internal/package/niecza.ff");
                }
                Fact.Log.Verbose("Looking for package create plugin ...");

                Fact.Plugin.Plugin create = Fact.Plugin.Plugin.LoadFromFile("package/create", "create");
                if (create == null)
                {
                    Fact.Log.Error("Impossible to find the package creation plugin.");
                    return "";
                }
                Fact.Log.Verbose("Try to clone Niecza from github ...");
                create.Run(new string[] { "https://github.com/sorear/niecza.git", "system/internal/package/niecza.ff" });
                if (!System.IO.File.Exists("system/internal/package/niecza.ff"))
                {
                    Fact.Log.Error("Impossible to clone Niecza from github.");
                    return "";
                }
                else
                {
                    Fact.Log.Verbose("Nieckza has been downloaded and packaged.");
                }
                Fact.Log.Verbose("Looking for package maker plugin ...");
                Fact.Plugin.Plugin make = Fact.Plugin.Plugin.LoadFromFile("make/make", "make");
                if (create == null)
                {
                    Fact.Log.Error("Impossible to find the package maker plugin.");
                    return "";
                }
                make.Run(new string[] { "system/internal/package/niecza.ff", "system/internal/package/niecza_builded.ff" });

                if (!System.IO.File.Exists("system/internal/package/niecza_builded.ff"))
                {
                    Fact.Log.Error("Compilation failled.");
                    return "";
                }

                Fact.Log.Verbose("Extract the compiler from the package ...");

                Fact.Processing.Project Project = new Fact.Processing.Project("build");
                Project.Load("system/internal/package/niecza_builded.ff");
                Fact.Processing.File Exe = null;
                string folder = "";
                Exe = Project.GetFile("niecza.exe");
                if (Exe == null) { Exe = Project.GetFile("bin/niecza.exe"); folder = "bin/"; }
                if (Exe == null) { Exe = Project.GetFile("niecza/niecza.exe"); folder = "niecza/"; }
                if (Exe == null) { Exe = Project.GetFile("niecza/bin/niecza.exe"); folder = "niecza/bin/"; }
                if (Exe == null) { Exe = Project.GetFile("bin/niecza"); folder = "bin/"; }
                if (Exe == null) { Exe = Project.GetFile("niecza/nieckza"); folder = "niecza/"; }
                if (Exe == null) { Exe = Project.GetFile("niecza/bin/niecza"); folder = "niecza/bin/"; }
                if (Exe == null) { Exe = Project.GetFile("niecza/run/Niecza.exe"); folder = "niecza/run/"; }
                if (Exe == null) { Exe = Project.GetFile("niecza/run/Niecza"); folder = "niecza/run/"; }
                if (Exe == null) { Exe = Project.GetFile("/niecza/run/Niecza.exe"); folder = "/niecza/run/"; }
                if (Exe == null) { Exe = Project.GetFile("/niecza/run/Niecza"); folder = "/niecza/run/"; }
                if (Exe == null)
                {
                    Fact.Log.Error("Final executable not found.");
                }
                else
                {
                    Fact.Tools.RecursiveMakeDirectory("system/internal/bin/niecza/run");
                    Fact.Log.Verbose("Compiler found in " + folder);
                    Project.ExtractDirectory(folder, "system/internal/bin/niecza/run");
                }

                Fact.Processing.File Lib = null;
                folder = "";
                Lib = Project.GetFile("CORE.setting");
                if (Lib == null) { Lib = Project.GetFile("niecza/lib/CORE.setting"); folder = "niecza/lib/"; }
                if (Lib == null) { Lib = Project.GetFile("/niecza/lib/CORE.setting"); folder = "/niecza/lib/"; }

                if (Lib == null)
                {
                    Fact.Log.Error("Niecza lib not found.");
                }
                else
                {
                    Fact.Tools.RecursiveMakeDirectory("system/internal/bin/niecza/lib");
                    Fact.Log.Verbose("Niecza lib in " + folder);
                    Project.ExtractDirectory(folder, "system/internal/bin/niecza/lib");
                }

                Fact.Log.Verbose("Clean diretory ...");



                if (file == "" && System.IO.File.Exists("system/internal/bin/niecza/run/Niecza.exe"))
                {
                    return "system/internal/bin/niecza/run/Niecza.exe";
                }

                return "";
            }
            return "";
        }
    }
}
