﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Niecza
{
    public class Niecza : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Niecza";
            }
        }

        public override int Run(string[] args)
        {
            string niecza = FindNiecza.GetNieczaBinary();

            List<string> inputFile = new List<string>();
            string output = "out.exe";

            bool inputMode = false;
            bool outputMode = false;
            bool refMode = false;

            

            for (int n = 0; n < args.Length; n++)
            {
                if (args[n] == "-i" || args[n] == "--Input")
                { inputMode = true; outputMode = false; refMode = false; continue; }
                if (args[n] == "-o" || args[n] == "--Output")
                { inputMode = false; outputMode = true; refMode = false; continue; }

                if (outputMode) { output = args[n]; }
                if (inputMode) { inputFile.Add(args[n]); }
            }

            return 0;
        }
    }
}
