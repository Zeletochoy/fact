﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buildfarm
{
    class ScriptEngine
    {
        Fact.Parser.XML.Node _Node = null;
        string _Server = "http://localhost";
        string _Port = "9089";
        public ScriptEngine(Fact.Parser.XML.Node Node)
        {
            _Node = Node;
        }
        public void Execute(Fact.Processing.Project Project)
        {
            foreach (Fact.Parser.XML.Node Command in _Node.Children)
            {
                try
                {
                    switch (Command.Type.ToLower())
                    {
                        case "copy":
                            {
                                string source = Command["source"].Trim();
                                if (source == "") { source = Command["src"].Trim(); }
                                if (source == "") { continue; }
                                string destination = Command["destination"].Trim();
                                if (destination == "") { destination = Command["dest"].Trim(); }
                                if (destination == "") { destination = Command["dst"].Trim(); }
                                if (destination == "") { continue; }
                                string destinationDirectory = System.IO.Path.GetDirectoryName(destination);
                                string destinationFilename = System.IO.Path.GetFileName(destination);
                                Fact.Processing.File file = Project.GetFile(source);
                                if (file != null)
                                {
                                    file = file.Copy(destinationFilename);
                                    Project.AddFile(destinationFilename, file.Copy());
                                }
                                break;
                            }
                        case "move":
                            {
                                string source = Command["source"].Trim();
                                if (source == "") { source = Command["src"].Trim(); }
                                if (source == "") { continue; }
                                string destination = Command["destination"].Trim();
                                if (destination == "") { destination = Command["dest"].Trim(); }
                                if (destination == "") { destination = Command["dst"].Trim(); }
                                if (destination == "") { continue; }
                                string destinationDirectory = System.IO.Path.GetDirectoryName(destination);
                                string destinationFilename = System.IO.Path.GetFileName(destination);
                                Fact.Processing.File file = Project.GetFile(source);
                                if (file != null)
                                {
                                    file = file.Copy(destinationFilename);
                                    Project.AddFile(destinationDirectory, file);
                                    Project.RemoveFile(source);
                                }
                                break;
                            }
                        case "remove-file":
                            {
                                string source = Command["source"].Trim();
                                if (source == "") { source = Command["src"].Trim(); }
                                if (source == "") { source = Command["file"].Trim(); }
                                if (source == "") { continue; }
                                Project.RemoveFile(source);
                                break;
                            }
                        case "remove-directory":
                            {
                                string source = Command["source"].Trim();
                                if (source == "") { source = Command["src"].Trim(); }
                                if (source == "") { source = Command["directory"].Trim(); }
                                if (source == "") { source = Command["dir"].Trim(); }
                                if (source == "") { continue; }
                                Project.RemoveDirectory(source);
                                break;
                            }
                        case "make":
                            {
                                Fact.Plugin.Plugin make = Fact.Plugin.Plugin.LoadFromFile("/make/make.dll")[0];
                                List<string> arguments = new List<string>();
                                string argumentsList = Command["args"];
                                if (argumentsList.Trim() == "") { argumentsList = Command["arguments"]; }
                                arguments.Add("--server");
                                arguments.Add(_Server + ":" + _Port);
                                make.Run(arguments.ToArray());
                                break;
                            }
                    }
                }
                catch { }
            }
        }
    }
}
