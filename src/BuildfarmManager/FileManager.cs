﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Buildfarm
{
    class FileManager
    {
        public FileManager()
        { }
        string _Location = "";
        public string Location
        {
            get { return _Location; }
            set
            {
                lock (this)
                {
                    if (value == _Location) { return; }
                    if (value == "")
                    {
                        Fact.Tools.RecursiveDelete(_Location);
                        _Location = "";
                        return;
                    }
                    else
                    {
                        Fact.Tools.RecursiveMakeDirectory(value);
                        if (_Location != "")
                        {
                            Fact.Tools.Move(_Location, value);
                            Fact.Tools.RecursiveDelete(_Location);
                        }
                        _Location = value;
                        if (!System.IO.Directory.Exists(_Location + "/data")) { Fact.Tools.RecursiveMakeDirectory(_Location + "/data"); }
                        if (!System.IO.Directory.Exists(_Location + "/project")) { Fact.Tools.RecursiveMakeDirectory(_Location + "/project"); }
                    }
                }
            }
        }

        public string Save(Fact.Processing.File File)
        {
            lock (this)
            {
                string hash = File.ContentHash;
                string folder1 = hash.Substring(0, 2);
                string folder2 = hash.Substring(2, 2);
                string filename = hash.Substring(4, hash.Length - 4);
                Fact.Tools.RecursiveMakeDirectory(_Location + "/data/" + folder1 + "/" + folder2);
                if (System.IO.File.Exists(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".data"))
                {
                    string count = System.IO.File.ReadAllText(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".refcount");
                    int countvalue = 0;
                    if (!int.TryParse(count, out countvalue)) { countvalue = 0; }
                    System.IO.File.Delete(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".refcount");
                    System.IO.File.WriteAllText(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".refcount", countvalue.ToString());
                }
                else
                {
                    System.IO.FileStream stream = System.IO.File.Open(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".data", System.IO.FileMode.OpenOrCreate);
                    File.Save(stream);
                    stream.Close();
                    System.IO.File.WriteAllText(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".refcount", "1");
                }
                return folder1 + "/" + folder2 + "/" + filename;
            }
        }

        public string Save(Fact.Processing.Project project)
        {
            lock (this)
            {
                string hash = project.Hash;
                string folder1 = hash.Substring(0, 2);
                string folder2 = hash.Substring(2, 2);
                string filename = hash.Substring(4, hash.Length - 4);
                Fact.Tools.RecursiveMakeDirectory(_Location + "/project/" + folder1 + "/" + folder2);
                if (System.IO.File.Exists(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".data"))
                {
                    string count = System.IO.File.ReadAllText( _Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".refcount");
                    int countvalue = 0;
                    if (!int.TryParse(count, out countvalue)) { countvalue = 0; }
                    System.IO.File.Delete(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".refcount");
                    System.IO.File.WriteAllText(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".refcount", countvalue.ToString());
                }
                else
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append("<project name='" +  project.Name + "'>");
                    foreach (Fact.Processing.File file in project)
                    {
                        string path = Save(file);
                        builder.Append("<file name='" +  System.Uri.EscapeDataString(file.Name) + "' directory='" + file.Directory + "'>");
                        builder.Append(file.ContentHash);
                        builder.Append("</file>");
                    }
                    builder.Append("</project>");
                    System.IO.File.WriteAllText(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".refcount", "0");
                    System.IO.File.WriteAllText(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".data", builder.ToString());
                }
                return folder1 + "/" + folder2 + "/" + filename;
            }
        }

        public Fact.Processing.File LoadFile(string Hash)
        {
            string folder1 = Hash.Substring(0, 2);
            string folder2 = Hash.Substring(2, 2);
            string filename = Hash.Substring(4, Hash.Length - 4);
            lock (this)
            {
                if (_Location == "") { return null; }
                if (!System.IO.File.Exists(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".data")) { return null; }
                System.IO.FileStream fileStream = System.IO.File.Open(_Location + "/data/" + folder1 + "/" + folder2 + "/" + filename + ".data", System.IO.FileMode.OpenOrCreate);
                Fact.Processing.File file = new Fact.Processing.File((byte[])null);
                file.Load(fileStream);
                fileStream.Close();
                return file;
            }
            return null;
        }

        public Fact.Processing.Project LoadProject(string Hash)
        {
            Fact.Log.Debug("Load project");
            string folder1 = Hash.Substring(0, 2);
            string folder2 = Hash.Substring(2, 2);
            string filename = Hash.Substring(4, Hash.Length - 4);
            lock (this)
            {
                if (_Location == "") { return null; }
                if (!System.IO.File.Exists(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".data")) { return null; }
                string data = System.IO.File.ReadAllText(_Location + "/project/" + folder1 + "/" + folder2 + "/" + filename + ".data");
                Fact.Parser.XML XML = new Fact.Parser.XML(); XML.Parse(data);
                Fact.Parser.XML.Node node = XML.Root.FindNode("project", false);
                Fact.Processing.Project project = new Fact.Processing.Project(node["name"].Trim());

                if (node != null)
                {
                    foreach (Fact.Parser.XML.Node filenode in node.Children)
                    {
                        string hash = filenode.Children[0].Text;
                        string name = filenode["name"];
                        string directory = filenode["directory"];
                        Fact.Processing.File file = LoadFile(hash);
                        if (file != null)
                        {
                            Fact.Log.Debug("file found: " + hash);
                            file.ForceChangeName(name);
                            project.AddFile(directory, file);
                        }
                    }
                }
                return project;
            }
        }
    }
}
