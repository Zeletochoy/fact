﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Setup
{
    public partial class Setup : Form
    {
        bool _IsShort = true;
        Slide _CurrentSlide = new Slide();
        Slide _OldSlide = new Slide();
        Slide _DisplayedSlide = new Slide();
        float _Transition = 0.0f;
        float _FrozenTimeslide = 0.0f;
        bool _MustSwitch = false;
        bool _CanSwitch = true;
        bool _PendingTransion = false;
        public Slide CurrentSlide { get { return _CurrentSlide; } set { _CurrentSlide = value; Invalidate(); } }
        float timeslide = 0.0f;
        int _MouseX = 0;
        int _MouseY = 0;
        int _TransitionFreeTime = 0;

        public Setup()
        {
            InitializeComponent();
        }

        void Render(System.Drawing.Graphics Graphics)
        {
            if (_TransitionFreeTime > 0) { _TransitionFreeTime--; }
            if (_CurrentSlide == null) { return; }
            if (_DisplayedSlide != null && _DisplayedSlide != _CurrentSlide)
            {
                if (!_PendingTransion) { _FrozenTimeslide = timeslide; }
                _PendingTransion = true;
                _MustSwitch = true;
                _OldSlide = _DisplayedSlide;
                if (_CanSwitch && _TransitionFreeTime == 0)
                {
                    _CurrentSlide._Parent = this;
                    _DisplayedSlide = _CurrentSlide;
                    try { _CurrentSlide._RaiseOnLoad(this, new EventArgs()); } catch { }
                    _TransitionFreeTime = 10;
                    _PendingTransion = false;
                    _CanSwitch = true;
                }
            }
            else
            {
                _PendingTransion = false;
                _MustSwitch = false;
            }
            if (_DisplayedSlide == null && _TransitionFreeTime == 0)
            {
                _PendingTransion = false;
                _CanSwitch = true;
                _DisplayedSlide = _CurrentSlide;
                _CurrentSlide._Parent = this;
                try { _CurrentSlide._RaiseOnLoad(this, new EventArgs()); } catch { }
                _TransitionFreeTime = 10;
            }
            // resize the display if needed
            if (_DisplayedSlide.Type == Slide.DisplayType.Splash && !_IsShort)
            {
                Size = new Size(540, 333);
                _IsShort = true;
            }
            else if (_DisplayedSlide.Type != Slide.DisplayType.Splash && _IsShort)
            {
                // Assuming the current location is the center of the screen
                // we want to be relative to the current position
                int screencenterx = Location.X + Size.Width / 2;
                int screencentery = Location.Y + Size.Height / 2;
                Size = new Size(600, 800);
                int newlocationx = screencenterx - Size.Width / 2;
                int newlocationy = screencentery - Size.Height / 2;
                if (newlocationx < 0) { newlocationx = 0; }
                if (newlocationy < 0) { newlocationy = 0; }
                try { Location = new Point(newlocationx, newlocationy); } catch { }
                _IsShort = false;
            }

            // Compute the basic guide for the display
            Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

            int width = Size.Width;
            int height = Size.Height;
            int centerx = Size.Width / 2;
            int centery = Size.Height / 2;

            Font fonttitle = new Font("arial", 96, GraphicsUnit.Pixel);
            Font font = new Font("arial", 64, GraphicsUnit.Pixel);
            Font fontsmall = new Font("arial", 14, GraphicsUnit.Pixel);



            if (_DisplayedSlide.Type == Slide.DisplayType.Splash) { Graphics.DrawString(_DisplayedSlide.Title, fonttitle, System.Drawing.Brushes.White, new Point(centerx - (_DisplayedSlide.Title.Length * (int)(fonttitle.Size / 1.6f)) / 2, centery - (int)(fonttitle.Size / 2.0f))); }
            else { Graphics.DrawString(_DisplayedSlide.Title, font, System.Drawing.Brushes.White, new Point((int)(font.Size / 1.6f) / 2, (int)font.Size / 3)); }

            // Now check if we have to display the loading bar
            bool displayLoading = _DisplayedSlide.Processing;
            if (displayLoading)
            {
                _CanSwitch = false;
                for (int n = 0; n < 7; n++)
                {
                    float virtualx = 0.0f;
                    // We are not doing a loop on the blob if a transition is pending
                    if (_PendingTransion)
                    {
                        virtualx = (timeslide - _FrozenTimeslide) + ((float)n / 7.0f) / 5.0f;                        
                    }
                    else
                    {
                        virtualx = timeslide + ((float)n / 7.0f) / 5.0f;
                        while (virtualx > 1.0f) { virtualx -= 1.0f; }
                    }
                   
                    // Remap X according to a quadratic function
                    virtualx -= 0.5f;
                    virtualx = virtualx * 2.0f;
                    virtualx = virtualx * virtualx * virtualx * virtualx * virtualx;
                    virtualx /= 2.0f;
                    virtualx += 0.5f;
                    // apply a static offset
                    virtualx += ((float)(n - 3) / 7.0f) / 5.0f;
                    

                    int x = (int)(virtualx * width);
                    // If the last blob is hidden and we are in a pending transition then we can unlock the transition
                    if (n == 0 && _PendingTransion && x > width + 12)
                    { _CanSwitch = true; }
                    Graphics.FillRectangle(System.Drawing.Brushes.WhiteSmoke, new Rectangle(x, centery - 3 + centery / 2, 6, 6));
                }
            }

            if (_DisplayedSlide.Type != Slide.DisplayType.Splash)
            {
                
                if (_MouseX > width - 22 && _MouseY < 20) { Graphics.DrawString("x", fontsmall, Brushes.White, new Point(width - 20, 5)); }
                else { Graphics.DrawString("x", fontsmall, Brushes.DarkGray, new Point(width - 20, 5)); }

                int OldY = 0;
                int Y = (int)(font.Size / 3) + (int)(font.Size * 1.5f) ;
                int X = (int)(font.Size / 1.6f) / 2 + 12;
                for (int n = 0; n < _DisplayedSlide.Children.Count; n++)
                {
                    Y += _DisplayedSlide.Children[n].Draw(X, Y, width, height, Graphics);
                    Y += 12;
                    _DisplayedSlide.Children[n]._Selected = (_MouseY >= OldY && _MouseY <= Y);
                    OldY = Y;
                }

                if (_DisplayedSlide.Bottom != null)
                {
                    Y = height - 60;
                    OldY = Y;
                    Y += _DisplayedSlide.Bottom.Draw(X, Y, width, height, Graphics);
                    _DisplayedSlide.Bottom._Selected = (_MouseY >= OldY && _MouseY <= Y);
                }
            }
        }

        private void Setup_Load(object sender, EventArgs e)
        {

        }

        private void Setup_Paint(object sender, PaintEventArgs e)
        {
            Render(e.Graphics);
        }

        private void update_Tick(object sender, EventArgs e)
        {
            timeslide += 0.01f;
            if (timeslide > 100.0f) { timeslide -= 100.0f; }
            Invalidate();
        }

        private void Setup_MouseMove(object sender, MouseEventArgs e)
        {
            _MouseX = e.X;
            _MouseY = e.Y;
        }

        private void Setup_MouseClick(object sender, MouseEventArgs e)
        {
            int width = Size.Width;
            int height = Size.Height;
            int centerx = Size.Width / 2;
            int centery = Size.Height / 2;

            if (_MouseX > width - 22 && _MouseY < 20)
            {
                if (_CurrentSlide == null) { Close(); }
            }
            if (_CurrentSlide == null) { return; }

            if (_CurrentSlide.Type != Slide.DisplayType.Splash)
            {
                for (int n = 0; n < _CurrentSlide.Children.Count; n++)
                {
                    if (_CurrentSlide.Children[n]._Selected) { _CurrentSlide.Children[n].Click(); }
                }
                if (_CurrentSlide.Bottom != null)
                {
                    if (_CurrentSlide.Bottom._Selected) { _CurrentSlide.Bottom.Click(); }
                }
            }
        }
    }
}