﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Setup
{
    public class Item
    {
        internal Slide _Parent = null;
        internal bool _Selected = false;
        public Item() { }
        public virtual int Draw(int X, int Y, int Width, int Height, System.Drawing.Graphics Graphics) { return 0; }
        public virtual void Click() { }

    }
}
