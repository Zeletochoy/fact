﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Setup
{
    static class Program
    {
        /// <summary>
        /// Entry point
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Player.Player player = new Player.Player("http://project-fact.com/Downloads/info.xml");
            Setup setup = new Setup();
            /*
            Slide splashscreen = new Slide();
            splashscreen.Title = "Fact";
            splashscreen.Type = Slide.DisplayType.Classic;
            Items.TextBlock text = new Items.TextBlock(
                "End User Licence Agreement",
                @"Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files, to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
- The Software is provided 'as is', without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders. be liable for any tort or otherwise, arising from, out of or in connection with the Software or the use or other dealings in the Software.
- Except as contained in this notice, the name of Raphaël Boissel or Jean-Luc Bounthong shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization from Raphaël Boissel or Jean-Luc Bounthong.",
                300);
            splashscreen.AddChild(text);

            Items.CheckBox agreed = new Items.CheckBox("I've read and I Agree");
            splashscreen.AddChild(agreed);
            Items.Button next = new Items.Button("Next");
            splashscreen.AddChild(next);
             */
            setup.CurrentSlide = player.Play();

            Application.Run(setup);
        }
    }
}
