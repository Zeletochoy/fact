﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Setup.Items
{
    class TextBlock : Item
    {
        Font _Fonttitle = new Font("arial", 24, GraphicsUnit.Pixel);
        Font _Font = new Font("arial", 14, GraphicsUnit.Pixel);
        string _Title = "";
        string _Text = "";
        int _Size = 0;
        public TextBlock(string Title, string Text, int Size) { _Title = Title; _Text = Text; _Size = Size; }
        public override int Draw(int X, int Y, int Width, int Height, System.Drawing.Graphics Graphics)
        {
            int Size = 0;

            if (_Title != "")
            {
                Graphics.DrawString(_Title, _Fonttitle, System.Drawing.Brushes.WhiteSmoke, new Point(X, Y + (int)_Fonttitle.Size / 3));
                Y += ((int)_Fonttitle.Size / 3) + (int)_Fonttitle.Size + 10;
                Size += ((int)_Fonttitle.Size / 3) + (int)_Fonttitle.Size + 10;
            }

            if (_Text != "")
            {
                Graphics.DrawString(_Text, _Font, System.Drawing.Brushes.WhiteSmoke, new Rectangle(X + 3, Y + (int)_Font.Size / 3, Width - (X * 2  + 6), 300));
                Y += _Size;
                Size += _Size;
            }
            return Size;
        }
    }
}
