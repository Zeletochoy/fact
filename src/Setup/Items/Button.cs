﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Setup.Items
{
    class Button : Item
    {
        Font _Fonttitle = new Font("arial", 24, GraphicsUnit.Pixel);
        Font _Font = new Font("arial", 14, GraphicsUnit.Pixel);
        Pen _Pen = new Pen(System.Drawing.Brushes.WhiteSmoke, 2.0f);
        bool _Enabled = false; public bool Enabled { get { return _Enabled; } set { _Enabled = value; } }
        string _Text = "";
        public Button(string Text) { _Text = Text; }
        public event EventHandler OnClick;
        public override int Draw(int X, int Y, int Width, int Height, System.Drawing.Graphics Graphics)
        {
            int Size = 0;
            int length = (int)(_Text.Length * _Font.Size);
            bool reallySellected = _Selected && (true);
            if (_Text != "")
            {
                if (reallySellected && _Enabled) { Graphics.FillRectangle(System.Drawing.Brushes.Gray, new Rectangle(X + (Width - X * 2) - length - 16, Y + 5, length + 6, (int)_Font.Size + 5)); }
                Graphics.DrawRectangle(_Pen, new Rectangle(X + (Width - X * 2) - length - 16, Y + 5, length + 6, (int)_Font.Size + 5));
                if (!_Enabled) { Graphics.DrawString(_Text, _Font, System.Drawing.Brushes.Gray, new Rectangle(X + (Width - X * 2) - length - 16, Y + 6, length + 6, (int)_Font.Size + 2), new StringFormat() { Alignment = StringAlignment.Center }); }
                else { Graphics.DrawString(_Text, _Font, System.Drawing.Brushes.WhiteSmoke, new Rectangle(X + (Width - X * 2) - length - 16, Y + 6, length + 6, (int)_Font.Size + 2), new StringFormat(){ Alignment = StringAlignment.Center }); }
                Size += (int)_Font.Size / 3 + (int)_Font.Size;
            }
            return Size;
        }

        public override void Click()
        {
            if (_Selected) { if (OnClick != null) { OnClick(this, new EventArgs()); } }
        }
    }
}
