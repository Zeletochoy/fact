﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Setup.Items
{
    class CheckBox : Item
    {
        Font _Fonttitle = new Font("arial", 24, GraphicsUnit.Pixel);
        Font _Font = new Font("arial", 14, GraphicsUnit.Pixel);
        Pen _Pen = new Pen(System.Drawing.Brushes.WhiteSmoke, 2.0f);
        bool _Checked = false;
        string _Text = "";
        public CheckBox(string Text) { _Text = Text; }
        public override int Draw(int X, int Y, int Width, int Height, System.Drawing.Graphics Graphics)
        {
            int Size = 0;

            if (_Text != "")
            {
                Graphics.DrawRectangle(_Pen, new Rectangle(X + 4, Y + 4, 15, 15));
                if (_Checked)
                {
                    if (_Selected) { Graphics.FillRectangle(System.Drawing.Brushes.White, new Rectangle(X + 7, Y + 7, 9, 9)); }
                    else { Graphics.FillRectangle(System.Drawing.Brushes.LightGray, new Rectangle(X + 7, Y + 7, 9, 9)); }
                }
                else
                {
                    if (_Selected) { Graphics.FillRectangle(System.Drawing.Brushes.Gray, new Rectangle(X + 7, Y + 7, 9, 9)); }
                }
                Graphics.DrawString(_Text, _Font, System.Drawing.Brushes.WhiteSmoke, new Rectangle(X + 25, Y + (int)_Font.Size / 3, Width - (X * 2  + 6), 300));
                Size += (int)_Font.Size / 3 + (int)_Font.Size;
            }
            return Size;
        }

        public override void Click()
        {
            if (_Selected) { _Checked = !_Checked; }
        }
    }
}
