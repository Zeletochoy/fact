﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Setup
{
    public class Slide
    {
        public enum DisplayType
        {
            Splash,
            Classic,
        }
        internal Setup _Parent = null; public Setup Parent { get { return _Parent; } }
        private string _Title = ""; public string Title { get { return _Title; } set { _Title = value; } }
        DisplayType _Type = DisplayType.Splash; public DisplayType Type { get { return _Type; } set { _Type = value; } }
        List<Item> _Children = new List<Item>(); public List<Item> Children { get { return _Children; } }
        Item _Bottom = null; public Item Bottom { get { return _Bottom; } set { _Bottom = value; } }
        bool _Processing = false; public bool Processing { get { return _Processing; } set { _Processing = value; } }
        public void AddChild(Item Item) { Item._Parent = this; _Children.Add(Item); }
        public event EventHandler OnLoad;
        internal void _RaiseOnLoad(object Sender, EventArgs EventArgs) { if (OnLoad != null) { OnLoad(Sender, EventArgs); } }
        Slide _Next = null; public Slide Next { get { return _Next; } set { _Next = value; } }
    }
}
