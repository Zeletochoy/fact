﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Setup.Player
{
    class Player
    {
        Slide fetching = new Slide();
        Slide splash = new Slide();
        string _TempFolder = "./";
        string _InfoFile = "";
        public Player(string URL)
        {
            _InfoFile = URL;
            splash.Title = "Fact";
            splash.Type = Slide.DisplayType.Splash;
            splash.OnLoad += splash_OnLoad;
            splash.Processing = true;
            fetching.Title = "Fact";
            
            Items.TextBlock fetchinginfo = new Items.TextBlock("Fetching data from the server", "The system is curently fetching the latest information on the server. It does not download the software itself, only information on it. Please wait it won't take long", 150);
            fetching.AddChild(fetchinginfo);
            fetching.Type = Slide.DisplayType.Classic;
            fetching.Processing = true;
            fetching.OnLoad += fetching_OnLoad;
        }

        void fetching_OnLoad(object sender, EventArgs e)
        {
            Job.Create((Job.Status status) => {
            
            if (System.IO.File.Exists(_TempFolder + ".temp-fact-info.xml")) { System.IO.File.Delete(_TempFolder + ".temp-fact-info.xml"); }
            Dowload(_InfoFile, _TempFolder + ".temp-fact-info.xml");
            if (!System.IO.File.Exists(_TempFolder + ".temp-fact-info.xml"))
            {
                goto fail;
            }
            string XML = System.IO.File.ReadAllText(_TempFolder + ".temp-fact-info.xml");
            fetching.Parent.CurrentSlide = Run(XML);
            return;
        fail:
            Slide error = new Slide();
            error.Title = "Fact";
            error.Type = Slide.DisplayType.Classic;
            Items.TextBlock errordescription = new Items.TextBlock("Error", "Impossible to fetch the installation data from the server. Please check your internet connection and retry in few minutes.", 100);
            error.AddChild(errordescription);
            fetching.Parent.CurrentSlide = error;
            });
        }

        void splash_OnLoad(object sender, EventArgs e)
        {
             splash.Parent.CurrentSlide = fetching;
        }

        public Slide Play()
        {
            return splash;
        }

        public void SetRegistryKey(string Key, string ValueName, string Value)
        {
            try
            {
                Microsoft.Win32.Registry.SetValue(Key, ValueName, Value);
            }
            catch { }
        }

        public void Delete(string Source)
        {
            try
            {
                if (System.IO.File.Exists(Source)) { System.IO.File.Delete(Source); }
            }
            catch { }
            try
            {
                if (System.IO.Directory.Exists(Source))
                {
                    try
                    {
                        foreach (string file in System.IO.Directory.GetFiles(Source)) { Delete(file); }
                    }
                    catch { }
                    try
                    {
                        foreach (string dir in System.IO.Directory.GetDirectories(Source)) { Delete(dir); }
                    }
                    catch { }
                    try
                    {
                        System.IO.Directory.Delete(Source);
                    }
                    catch { }
                }
            }
            catch { }
        }

        public void Move(string Source, string Destination)
        {
            Copy(Source, Destination);
            Delete(Source);
        }

        public void Copy(string Source, string Destination)
        {
            if (Source.StartsWith("http://") ||
                Source.StartsWith("https://") ||
                Source.StartsWith("ftp://"))
            {
                Dowload(Source, Destination);
                return;
            }

            if (System.IO.File.Exists(Source))
            {
                try
                {
                    Delete(Destination);
                    System.IO.File.Copy(Source, Destination);
                }
                catch { }
            }

            if (System.IO.Directory.Exists(Source))
            {
                try
                {
                    if (System.IO.File.Exists(Destination)) { Delete(Destination); }
                    if (!System.IO.Directory.Exists(Destination)) { System.IO.Directory.CreateDirectory(Destination); }
                    try
                    {
                        foreach (string file in System.IO.Directory.GetFiles(Source))
                        {
                            string name = System.IO.Path.GetFileName(file);
                            Copy(file, Destination + "/" + name);
                        }
                    }
                    catch { }
                    try
                    {
                        foreach (string file in System.IO.Directory.GetDirectories(Source))
                        {
                            string name = System.IO.Path.GetFileName(file);
                            Copy(file, Destination + "/" + name);
                        }
                    }
                    catch { }
                }
                catch { }
            }
        }

        public void Dowload(string URL, string Location)
        {
            try
            {
                System.Net.WebClient WebClient = new System.Net.WebClient();
                WebClient.DownloadFile(URL, Location);
            }
            catch { }
        }

        public bool RunProgram(string Program, string Arguments)
        {
            Program = System.IO.Path.GetFullPath(Program);
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.Arguments = Arguments;
                process.StartInfo.FileName = Program;
                process.StartInfo.UseShellExecute = false;
                process.Start();
                process.WaitForExit();
                return process.ExitCode == 0;
            }
            catch { return false; }
            return true;
        }

        public bool CheckCondition(Fact.Parser.XML.Node Condition)
        {
            try
            {
                string conditiontype = Condition.Type.ToLower().Trim();
                switch (conditiontype)
                {
                    case "if-windows":
                        return System.Environment.OSVersion.Platform == PlatformID.Win32NT ||
                               System.Environment.OSVersion.Platform == PlatformID.Win32S ||
                               System.Environment.OSVersion.Platform == PlatformID.Win32Windows ||
                               System.Environment.OSVersion.Platform == PlatformID.WinCE;
                    case "if-unix":
                        return System.Environment.OSVersion.Platform == PlatformID.MacOSX ||
                               System.Environment.OSVersion.Platform == PlatformID.Unix;
                    case "if-file-exists":
                    case "if-file-exist":
                        return System.IO.File.Exists(Condition["file"]);
                    case "if-directory-exists":
                    case "if-directory-exist":
                        return System.IO.Directory.Exists(Condition["directory"]);
                }
                return false;
            }
            catch { return false; }
        }

        delegate void SimpleAction();
        public Job.Action CreateActionList(List<Fact.Parser.XML.Node> Nodes)
        {
            List<SimpleAction> actions = new List<SimpleAction>();
            foreach (Fact.Parser.XML.Node node in Nodes)
            {
                string nodetype = node.Type.ToLower().Trim();
                switch (nodetype)
                {
                    case "download": actions.Add(() => { Dowload(node["url"], node["destination"]); }); break;
                    case "copy": actions.Add(() => { Copy(node["source"], node["destination"]); }); break;
                    case "delete": actions.Add(() => { Delete(node["source"]); }); break;
                    case "move": actions.Add(() => { Move(node["source"], node["destination"]); }); break;
                    case "set-registry-key": actions.Add(() => { SetRegistryKey(node["key"], node["name"], node["value"]); }); break;
                    default: break;
                }
            }
            return (Job.Status status)
                =>
                {
                    status.Progress = 0;
                    foreach (SimpleAction action in actions) { action(); }
                    status.Progress = 100;
                };
        }
         
        public Slide RunSlide(Fact.Parser.XML.Node Slide)
        {
            bool nextbutton = false;
            bool eulabutton = false;

            Slide output = new Slide();
            List<Fact.Parser.XML.Node> actions = new List<Fact.Parser.XML.Node>();
            output.Title = Slide["title"];
            output.Type = global::Setup.Slide.DisplayType.Classic;
            foreach (Fact.Parser.XML.Node node in Slide.Children)
            {
                Fact.Parser.XML.Node noderef = node;
                string nodetype = node.Type.ToLower().Trim();
                switch (nodetype)
                {
                    case "eula":
                        output.AddChild(new Items.TextBlock("End User Licence Agreement", node["text"], int.Parse(node["size"])));
                        eulabutton = true;
                        break;
                    case "textblock":
                    case "text":
                        output.AddChild(new Items.TextBlock(node["title"], node["text"], int.Parse(node["size"])));
                        break;
                    case "next-button":
                        nextbutton = true;
                        break;
                    case "button":
                        {
                            Items.Button userbutton = new Items.Button(node["text"]);
                            Slide userslide = RunPackage(node);
                            userbutton.OnClick += (object sender, EventArgs e) => { if (userslide != null) { output.Parent.CurrentSlide = userslide; } };
                            userbutton.Enabled = userslide != null;
                            output.AddChild(userbutton);
                        }
                        break;
                    case "copy":
                    case "delete":
                    case "download":
                    case "move":
                    case "set-registry-key":
                        actions.Add(noderef);
                        break;
                }
            }
            Items.Button button = new Items.Button("Next");
            if (nextbutton)
            {
                output.Bottom = button;
                button.OnClick += (object sender, EventArgs e) => { if (output.Next != null) { output.Parent.CurrentSlide = output.Next; } };
                button.Enabled = actions.Count == 0;
            }
            if (actions.Count > 0)
            {
                Job.Action actionList = CreateActionList(actions);
                output.Processing = true;
                output.OnLoad += (object sender, EventArgs e) => 
                {
                    Job.Create((Job.Status Status) =>
                    {
                        actionList(Status);
                        if (nextbutton)
                        {
                            output.Processing = false;
                            button.Enabled = true;
                        }
                        else
                        {
                            output.Parent.CurrentSlide = output.Next;
                        }
                    });
                };
            }
            return output;
        }

        public Slide RunPackage(Fact.Parser.XML.Node Package)
        {
            Slide startSlide = null;
            Slide current = null;
            foreach (Fact.Parser.XML.Node node in Package.Children)
            {
                string nodetype = node.Type.ToLower().Trim();
                switch(nodetype)
                {
                    case "slide":
                        {
                            Slide result = RunSlide(node);
                            if (startSlide == null) { startSlide = result; current = result; }
                            else { current.Next = result; current = result; }
                            break;
                        }
                    case "package":
                        {
                            Slide result = RunPackage(node);
                            if (startSlide == null) { startSlide = result; current = result; }
                            else { current.Next = result; current = result; }
                            break;
                        }
                    case "include":
                        {
                            string URL = node["url"];

                            if (System.IO.File.Exists(_TempFolder + "./temp-fact-include.xml")) { System.IO.File.Delete(_TempFolder + "./temp-fact-include.xml"); }
                            Dowload(URL, _TempFolder + "./temp-fact-include.xml");
                            if (!System.IO.File.Exists(_TempFolder + "./temp-fact-include.xml")) { goto downloadfail; }
                            string resultXML = System.IO.File.ReadAllText(_TempFolder + "./temp-fact-include.xml");
                            System.IO.File.Delete(_TempFolder + "./temp-fact-include.xml");
                            Slide result = Run(resultXML);
                            if (startSlide == null) { startSlide = result; current = result; }
                            else { current.Next = result; current = result; }
                            break;
                        downloadfail:
                            Slide error = new Slide();
                            error.Title = "Fact";
                            error.Type = Slide.DisplayType.Classic;
                            Items.TextBlock errordescription = new Items.TextBlock("Error", "Impossible to fetch the installation data from the server. Please check your internet connection and retry in few minutes.", 100);
                            error.AddChild(errordescription);
                            if (startSlide == null) { startSlide = error; current = error; }
                            else { current.Next = error; current = error; }
                            break;
                        }
                    default:
                        {
                            if (nodetype.StartsWith("if-"))
                            {
                                if (CheckCondition(node))
                                {
                                    Slide result = RunPackage(node);
                                    if (startSlide == null) { startSlide = result; current = result; }
                                    else { current.Next = result; current = result; }
                                    break;
                                }
                            }
                            break;
                        }
                }
            }
            return startSlide;
        }

        public Slide Run(string XML)
        {
            if (XML == "") { goto fail; }
            Fact.Parser.XML XMLParser = new Fact.Parser.XML();
            XMLParser.Parse(XML);
            Fact.Parser.XML.Node root = XMLParser.Root;
            if (root == null) { goto fail; }
            // We try to grab the first packages node
            Fact.Parser.XML.Node packagesnode = root.FindNode("packages", false);
            Fact.Parser.XML.Node mainpackage = null;
            if (packagesnode == null) { goto fail; }
            List<Fact.Parser.XML.Node> packages = new List<Fact.Parser.XML.Node>();
            foreach ( Fact.Parser.XML.Node node in packagesnode.Children)
            {
                if (node.Type.ToLower() == "package")
                {
                    Fact.Parser.XML.Node package = node;
                    packages.Add(package);
                    if (package["type"].ToLower().Trim() == "main") { mainpackage = package; }
                }
            }
            if (mainpackage == null) { goto fail; }
            Slide output = RunPackage(mainpackage);
            if (output == null) { goto fail; }
            return output;
        fail:
            Slide error = new Slide();
            error.Title = "Fact";
            error.Type = Slide.DisplayType.Classic;
            Items.TextBlock errordescription = new Items.TextBlock("Error", "Impossible to parse the information file, The data may be corrupted.", 100);
            error.AddChild(errordescription);
            return error;
        }
    }
}
