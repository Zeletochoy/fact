﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Setup.Player
{
    class Job
    {
        public class Status
        {
            int _Progress = 0;
            public int Progress { get { lock (this) { return _Progress; } } set { lock (this) { _Progress = value; } } }
        }
        public delegate void Action(Status Status);
        Action _Action;
        void _Call(Status Status)
        {
            if (_Action != null) { _Action(Status); }
        }

        public Status Run()
        {
            Status status = new Status();
            System.Threading.Thread thread = new System.Threading.Thread(() => { _Call(status); });
            thread.IsBackground = true;
            thread.Start();
            return status;
        }
        private Job(Action Action)
        {
            _Action = Action;
        }
        public static Status Create(Action Action) { Job job = new Job(Action); return job.Run(); }
    }
}
