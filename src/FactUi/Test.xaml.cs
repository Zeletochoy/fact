﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactUi
{
    /// <summary>
    /// Logique d'interaction pour Test.xaml
    /// </summary>
    public partial class Test : UserControl
    {
        public Test()
        {
            InitializeComponent();
        }

        private void expander1_Expanded(object sender, RoutedEventArgs e)
        {
            this.Height = 222; rectangle2.Visibility = System.Windows.Visibility.Visible;
            rectangle1.Width = 200;
        }

        private void expander1_Collapsed(object sender, RoutedEventArgs e)
        {
            this.Height = 45; rectangle2.Visibility = System.Windows.Visibility.Collapsed;
            rectangle1.Width = 500;
        }
    }
}
