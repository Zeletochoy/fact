﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactUi
{
    /// <summary>
    /// Logique d'interaction pour TestList.xaml
    /// </summary>
    public partial class TestList : UserControl
    {
        public TestList()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            stackPanel1.Children.Add(new Test());
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            stackPanel1.Children.Add(new TestGroup());
        }
    }
}
