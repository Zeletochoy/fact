﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;                                                                          
using System.Windows.Shapes;

namespace FactUi
{
    /// <summary>
    /// Logique d'interaction pour TestGroup.xaml
    /// </summary>
    public partial class TestGroup : UserControl
    {
        public TestGroup()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            stackPanel1.Children.Add(new Test());
        }

        private void Compute_Height()
        {
            if (expander1 != null && !expander1.IsExpanded)
            { this.Height = 50; return; }
            if (stackPanel1 == null) { return; }
            double Height = 0;
            foreach (object _ in stackPanel1.Children)
            {
                if (_ is Test)
                 Height += ((Test)_).Height;
                if (_ is TestGroup)
                    Height += ((TestGroup)_).Height;
            }
            this.Height = Height + 100;
        }

        private void Grid_LayoutUpdated(object sender, EventArgs e)
        {
            Compute_Height();
        }

        private void expander1_Expanded(object sender, RoutedEventArgs e)
        {
            if (stackPanel1 == null) { return; }
            Compute_Height();
            stackPanel1.Visibility = System.Windows.Visibility.Visible;
            rectangle2.Visibility = System.Windows.Visibility.Visible;
            button1.Visibility = System.Windows.Visibility.Visible;
            button2.Visibility = System.Windows.Visibility.Visible;

        }

        private void expander1_Collapsed(object sender, RoutedEventArgs e)
        {
            if (stackPanel1 == null) { return; }
            Compute_Height();
            stackPanel1.Visibility = System.Windows.Visibility.Hidden;
            rectangle2.Visibility = System.Windows.Visibility.Hidden;
            button1.Visibility = System.Windows.Visibility.Hidden;
            button2.Visibility = System.Windows.Visibility.Hidden;

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            stackPanel1.Children.Add(new TestGroup());
        }
    }
}
