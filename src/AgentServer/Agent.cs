﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AgentServer
{
    public class Agent : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Agent";
            }
        }

        public Agent()  { }

        internal class Action
        {
            internal string _Type = "";
            internal List<string> _Content = new List<string>();
        }

        internal class Job
        {
            internal Dictionary<string, Fact.Processing.File> _JobLocalStorage = new Dictionary<string, Fact.Processing.File>();
            internal Dictionary<string, string> _JobLocalStorageHash = new Dictionary<string, string>();

            public Fact.Processing.File GetFileInJobLocalStorage(string File)
            {
                if (!_JobLocalStorageHash.ContainsKey(File)) { return null; }

                if (_JobLocalStorage.ContainsKey(File) && _JobLocalStorage[File] != null)
                { return _JobLocalStorage[File]; }

                Fact.Processing.File FileData = _Client.CallWithTimeout("AgentManager.GetFileInJobLocalStorage", 60000, _Token, File) as Fact.Processing.File;

                if (FileData != null)
                {
                    if (!_JobLocalStorage.ContainsKey(File)) { _JobLocalStorage.Add(File, FileData); }
                    else { _JobLocalStorage.Add(File, FileData); }
                }
                return FileData;

            }
            internal List<Action> _Actions = new List<Action>();
            internal Fact.Network.Client _Client = null;
            internal int _Token = 0;
            public string ToXML()
            {
                StringBuilder jobxml = new StringBuilder();
                jobxml.Append("<job>");
                jobxml.Append("<token>"); jobxml.Append(_Token.ToString()); jobxml.Append("</token>");
                jobxml.Append("<local>");
                foreach (KeyValuePair<string, Fact.Processing.File> obj in _JobLocalStorage)
                {
                    jobxml.Append("<element>");
                    jobxml.Append("<name>"); jobxml.Append(obj.Key); jobxml.Append("</name>");
                    jobxml.Append("<data>"); jobxml.Append(System.Convert.ToBase64String(obj.Value.GetBytes())); jobxml.Append("</data>");
                    jobxml.Append("</element>");
                }
                jobxml.Append("</local>");
                jobxml.Append("<actions>");
                foreach (Action action in _Actions)
                {
                    jobxml.Append("<action>");
                    jobxml.Append("<type>");
                    jobxml.Append(action._Type);
                    jobxml.Append("</type>");
                    jobxml.Append("<contents>");
                    foreach (string content in action._Content)
                    {
                        jobxml.Append("<content>");
                        jobxml.Append(Fact.Network.Tools.EscapeURIString(content));
                        jobxml.Append("</content>");
                    }
                    jobxml.Append("</contents>");
                    jobxml.Append("</action>");
                }
                jobxml.Append("</actions>");
                jobxml.Append("</job>");
                return jobxml.ToString();
            }
        }

        internal Job ParseJob(string JobXML)
        {
            try
            {
                Fact.Parser.XML XML = new Fact.Parser.XML();
                XML.Parse(JobXML);
                if (XML.Root == null) { Fact.Log.Error("Job parsing error: No root node"); return null; }
                Fact.Parser.XML.Node JobNode = XML.Root.FindNode("job", false);
                if (JobNode == null) { Fact.Log.Error("Job parsing error: No job node"); return null; }
                Fact.Parser.XML.Node Local = JobNode.FindNode("local", false);
                Fact.Parser.XML.Node Actions = JobNode.FindNode("actions", false);
                Fact.Parser.XML.Node Token = JobNode.FindNode("token", false);
                if (Actions == null) { Fact.Log.Warning("Job parsing error: No actions node"); return null; }
                Agent.Job Job = new Job();
                if (Local != null)
                {
                    foreach (Fact.Parser.XML.Node node in Local.Children)
                    {
                        if (node.Type.ToLower() == "element")
                        {
                            Fact.Parser.XML.Node Name = node.FindNode("name", false);
                            Fact.Parser.XML.Node Hash = node.FindNode("hash", false);
                            Fact.Parser.XML.Node Data = node.FindNode("data", false);
                            if (Name != null && Name.Children.Count == 1 && Data != null && Data.Children.Count == 1)
                            {
                                try
                                {
                                    string FileName = Name.Children[0].Text;
                                    byte[] FileData = Convert.FromBase64String(Data.Children[0].Text);
                                    Fact.Processing.File factfile = new Fact.Processing.File(FileData, FileName);
                                    Job._JobLocalStorage.Add(FileName, factfile);
                                    Job._JobLocalStorageHash.Add(FileName, factfile.ContentHash);
                                }
                                catch { }
                            }
                            else if (Name != null && Name.Children.Count == 1 && Hash != null && Hash.Children.Count == 1)
                            {
                                try
                                {
                                    string FileName = Name.Children[0].Text;
                                    string HashText = Hash.Children[0].Text;
                                    Job._JobLocalStorageHash.Add(FileName, HashText);
                                }
                                catch { }
                            }
                        }
                    }
                }
                {
                    foreach (Fact.Parser.XML.Node node in Actions.Children)
                    {
                        if (node.Type.ToLower() == "action")
                        {
                            Fact.Parser.XML.Node Type = node.FindNode("type", false);
                            Fact.Parser.XML.Node Contents = node.FindNode("contents", false);
                            if (Type != null && Type.Children.Count == 1 && Contents != null)
                            {
                                string ActionType = Type.Children[0].Text;
                                List<string> ActionContent = new List<string>();
                                foreach (Fact.Parser.XML.Node contentnode in Contents.Children)
                                {
                                    if (contentnode.Type.ToLower() == "content" && contentnode.Children.Count == 1)
                                    {
                                        ActionContent.Add(Uri.UnescapeDataString(contentnode.Children[0].Text));
                                    }
                                }
                                Job._Actions.Add(new Action() { _Type = ActionType,  _Content = ActionContent });
                            }
                        }
                    }
                }
                if (Token != null && Token.Children.Count > 0) { int.TryParse(Token.Children[0].Text, out Job._Token); }
                return Job;
            }
            catch { return null; }
        }

        internal string PathSolver(string Path, string WorkingDir)
        {
            if (Path.StartsWith("@//")) { return WorkingDir + "/" + Path.Substring("@//".Length); }
            if (Path.StartsWith("./")) { return WorkingDir + "/" + Path.Substring("./".Length); }
            return Path;
        }

        internal string FindFactExe()
        {
            foreach (System.Reflection.Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.GetName().Name.ToLower().Contains("factexe") && assembly.EntryPoint != null)
                {
                    return assembly.Location;
                }
            }
            return "";
        }

        internal void DoJob(Job Job, Fact.Network.Client Client)
        {
            if (Job._Client == null) { Job._Client = Client; }
            if (Job == null) { return; }
            Fact.Log.Verbose("Running Job " + Job._Token.ToString() + " ...");
            string tmp_dir = Fact.Tools.CreateTempDirectory();
            if (!System.IO.Directory.Exists(tmp_dir)) { Fact.Log.Error("Failed to create temp working directory"); return; }
            try
            {
                foreach (Action action in Job._Actions)
                {
                    switch (action._Type.ToLower())
                    {
                        case "copyfilefromjoblocalstorage":
                            if (action._Content.Count != 2 || !(
                                Job._JobLocalStorage.ContainsKey(action._Content[1]) ||
                                Job._JobLocalStorageHash.ContainsKey(action._Content[1]))
                                )
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            Job.GetFileInJobLocalStorage(action._Content[1]).Extract(PathSolver(action._Content[0], tmp_dir));
                            Fact.Log.Verbose("File " + action._Content[1] + " extracted at " + PathSolver(action._Content[0], tmp_dir));
                            break;
                        case "runcommand":
                        case "runcommandandredirectresult":
                            bool saveresult = (action._Type.ToLower() == "runcommandandredirectresult");
                            if (action._Content.Count == 0)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            string arguments = "";
                            int n = 1;
                            string stdout = "";
                            string stderr = "";

                            if (saveresult)
                            {
                                stdout = action._Content[n]; n++; stdout = PathSolver(stdout, tmp_dir);
                                stderr = action._Content[n]; n++; stderr = PathSolver(stderr, tmp_dir);
                            }
                            
                            for (; n < action._Content.Count; n++) { arguments += " \"" + action._Content[n] + "\" "; }
                            string program = action._Content[0];
                            if (program.Trim().ToLower() == "fact") { program = FindFactExe(); }
                            if (!System.IO.File.Exists(program)) { program = Fact.Internal.Information.Where(action._Content[0]); }
                            if (!System.IO.File.Exists(program)) { Fact.Log.Warning("Executable not found " + action._Content[0]); break; }
                            if (Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix ||
                                Fact.Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX)
                            {
                                // Custom path for .NET Executable using mono
                                string mono = Fact.Internal.Information.GetMonoBinary();
                                if (Fact.Internal.Information.IsDotNetExecutable(program))
                                {
                                    if (System.IO.File.Exists(mono))
                                    {
                                        // Use the same runtime as the one used by this command
                                        string runtimename = "CLR 2.0";
                                        string runtime = "--runtime=\"v2.0\"";
                                        if (System.Environment.Version.Major == 2) { runtime = "--runtime=\"v2.0\""; runtimename = "CLR 2.0"; }
                                        else if (System.Environment.Version.Major >= 4) { runtime = "--runtime=\"v4.0\""; runtimename = "CLR 4.0"; }
                                        using (Fact.Runtime.Process Process = new Fact.Runtime.Process(mono, System.IO.Path.GetFullPath(tmp_dir), runtime + " " + program + " " + arguments))
                                        {
                                            Fact.Log.Verbose("Run command " + program + " " + arguments + " (" + runtimename + ")...");
                                            Fact.Runtime.Process.Result result = Process.Run(-1);
                                            if (Client != null) { Client.Call("AgentManager.SendJobLog", Job._Token, result.StdOut, result.StdErr); }
                                            Fact.Log.Auto(result.StdOut);
                                            if (saveresult)
                                            {
                                                if (stdout != "") { System.IO.File.WriteAllText(stdout, result.StdOut); }
                                                if (stderr != "") { System.IO.File.WriteAllText(stderr, result.StdErr); }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }

                            using (Fact.Runtime.Process Process = new Fact.Runtime.Process(program, System.IO.Path.GetFullPath(tmp_dir), arguments))
                            {
                                Fact.Log.Verbose("Run command " + program + " " + arguments + "...");
                                Fact.Runtime.Process.Result result = Process.Run(-1);
                                if (Client != null) { Client.Call("AgentManager.SendJobLog", Job._Token, result.StdOut, result.StdErr); }
                                Fact.Log.Auto(result.StdOut);
                                if (saveresult)
                                {
                                    if (stdout != "") { System.IO.File.WriteAllText(stdout, result.StdOut); }
                                    if (stderr != "") { System.IO.File.WriteAllText(stderr, result.StdErr); }
                                }
                            }

                           
                            break;
                        case "copyfiletojoblocalstorage":
                            if (action._Content.Count != 2)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            string localpath = PathSolver(action._Content[0], tmp_dir);
                            if (!System.IO.File.Exists(localpath))
                            { Fact.Log.Warning("File not found " + localpath); break; }
                            Fact.Processing.File FileToCopy = new Fact.Processing.File(System.IO.File.ReadAllBytes(localpath), action._Content[1]);
                            Job._JobLocalStorage.Add(action._Content[1], FileToCopy);
                            Job._JobLocalStorageHash.Add(action._Content[1], FileToCopy.ContentHash);

                            Fact.Log.Verbose("File " + action._Content[1] + " saved");
                            break;
                        case "deletefilefromjoblocalstorage":
                            if (action._Content.Count != 1)
                            { Fact.Log.Warning("Invalid parameters in action " + action._Type); break; }
                            if (Job._JobLocalStorage.ContainsKey(action._Content[0]))
                            { Job._JobLocalStorage.Remove(action._Content[0]); Fact.Log.Verbose("File " + action._Content[0] + " deleted"); }
                            if (Job._JobLocalStorageHash.ContainsKey(action._Content[0]))
                            { Job._JobLocalStorageHash.Remove(action._Content[0]); }
                            break;
                        default: Fact.Log.Warning("Unknow action : " + action._Type); break;
                    }
                }
                Fact.Log.Verbose("Sending the result job the server");
                Client.CallWithTimeout("AgentManager.SendJobResult", 60000, Job._Token, Job.ToXML());
            }
            catch
            {
                Fact.Log.Error("Unexpected error while running the job");
                Client.CallWithTimeout("AgentManager.SendJobResult", Job._Token, "");
            }
            Fact.Tools.RecursiveDelete(tmp_dir);
        }

        public override int Run(params string[] args)
        {

            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.AddStringInput("server", "The address of the fact server hosting the agent manager", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("credential", "The credentials to use to reach the fact server", ""); CommandLine.AddShortInput("c", "credential");
            CommandLine.AddStringInput("group", "The group of the agent (leave empty if no group)", ""); CommandLine.AddShortInput("g", "group");
            CommandLine.AddStringInput("name", "The name of the agent", ""); CommandLine.AddShortInput("n", "name");

            CommandLine.AddBooleanInput("list", "List all the agent on the server [Does not create an agent]", false); CommandLine.AddShortInput("g", "group");

            CommandLine.AddBooleanInput("help", "Display this message", false); CommandLine.AddShortInput("h", "help");
            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            string _Server = CommandLine["server"].AsString();
            if (!_Server.ToLower().StartsWith("http://") &&
                !_Server.ToLower().StartsWith("https://"))
            {
                _Server = "http://" + _Server;  
            }
            string _CredentialFile = CommandLine["credential"].IsDefault ? "" : CommandLine["credential"];
            string group = CommandLine["group"].AsString();
            string name = CommandLine["name"].AsString();
            string token = "";
            Fact.Directory.Credential _Credential = null;
            if (_CredentialFile != "")
            {
                try
                {
                    _Credential = new Fact.Directory.Credential(_CredentialFile);
                }
                catch { Fact.Log.Error("Impossible to load the credential"); return 1; }
            }

            Fact.Log.Verbose("Creating agent client ...");
            Fact.Network.Client Client = null;

            // We should find a better way than keeping the password in memory
            string Username = "";
            string Password = "";
            if (_Credential == null)
            {
                Fact.Log.Login(out Username, out Password);
                Client = new Fact.Network.Client(_Server, Username, Password);
            }
            else
            {
                Client = new Fact.Network.Client(_Server, _Credential);
            }

            if (CommandLine["list"].AsBoolean())
            {
                try
                {
                    string[] list = Client.Call("AgentManager.GetAgentList") as string[];
                    if (list != null)
                    {
                        foreach (string agent in list)
                        {
                            if (agent == "") { break; }
                            string hostaddress = Client.Call("AgentManager.GetAgentHostAddress", agent) as string;
                            string userid = Client.Call("AgentManager.GetAgentUserId", agent) as string;
                            string agentname = Client.Call("AgentManager.GetAgentName", agent) as string;

                            Fact.Log.Info("agent: " + agentname + " " + userid + " " + hostaddress);
                        }
                    }
                    return 1;
                }
                catch
                {
                    Fact.Log.Error("Transmission error ...");
                    return 1;
                }
            }

            Fact.Log.Verbose("Register agent ...");
            try 
            {
                token = Client.Call("AgentManager.Register", name) as String;
            }
            catch
            {
                Fact.Log.Error("Impossible to register the agent");
                return 1;
            }
            if (token == null || token.Trim() == "")
            {
                Fact.Log.Error("Impossible to register the agent: The server has rejected the registration");
                return 1;
            }
            Fact.Log.Verbose("Agent registered as: {" + token + "}");
            int waittime = -10;
            bool failure = false;
            while (true)
            {
                try
                {
                    string job = Client.CallWithTimeout("AgentManager.GetJob", 60000, token, group) as string;
                    if (job == null)
                    {
                        failure = true;
                        Fact.Log.Warning("The server has rejected the request");
                    }
                    if (job != null && job != "")
                    {
                        failure = false;
                        waittime = -10;
                        try
                        {
                            DoJob(ParseJob(job), Client);
                        }
                        catch { Fact.Log.Error("Critical error during the execution of the process"); }

                        try
                        {
                            //Collect all the memory used by .NET
                            System.GC.Collect();
                        }
                        catch { }
                        continue;
                    }
                }
                catch (Exception e)
                {
                    Fact.Log.Warning("Communication error: " + e.Message);
                    // Count the communication error to be able to reinstigate the connection later
                    failure = true;
                    waittime = 1000;
                }

                if (failure)
                {
                    failure = false;
                    // Try to instigate a fresh connection
                    if (Client != null)
                    {
                        try { Client.Close(); } catch { }
                    }
                    if (_Credential == null)
                    {
                        Client = new Fact.Network.Client(_Server, Username, Password);
                    }
                    else
                    {
                        Client = new Fact.Network.Client(_Server, _Credential);
                    }
                    Fact.Log.Warning("Try to register the agent again");
                    try
                    {
                        token = Client.Call("AgentManager.Register", name) as String;
                        if (token == null || token.Trim() == "") { continue; }
                        Fact.Log.Verbose("Agent registered as: {" + token + "}");
                    }
                    catch { }
                }

                if (waittime > 0)
                {
                    System.Threading.Thread.Sleep(waittime);
                }
                if (waittime <= 250) { waittime++; }
                
            }

            return 0;
        }
    }
}
