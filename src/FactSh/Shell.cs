﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactSh
{
    class Shell
    {
        Fact.Processing.File _File;

        public Shell()
        { 
        }

        public void Run(string File)
        {
            _File = new Fact.Processing.File(new string[] { System.IO.File.ReadAllText(File)});
            TagFile(_File);
            Parser.AST AST = new FactSh.Parser.AST(_File);

        }
        void TagFile(Fact.Processing.File File)
        {
            Fact.Processing.Chain Chain = new Fact.Processing.Chain(File);
            Chain.Add(Fact.Processing.Prebuild.Lexer.Space.TagSpaces());
            Chain.Add(Fact.Processing.Prebuild.Lexer.Space.TagNewLines());
            Chain.Add(Fact.Processing.Prebuild.Lexer.Tag.ReTag(Fact.Character.MetaChar.BasicCharType.Space, Fact.Character.MetaChar.BasicSeparatorType.Separator));
            Chain.Add(Fact.Processing.Prebuild.Lexer.Tag.ReTag(Fact.Character.MetaChar.BasicCharType.EndOfLine, Fact.Character.MetaChar.BasicSeparatorType.Separator));
            Chain.Add(Fact.Processing.Prebuild.Lexer.Tag.TagAs(Fact.Character.MetaChar.BasicSeparatorType.Separator, "(", ")", ",", "."));
            Chain.Process();
        }
    }
}
