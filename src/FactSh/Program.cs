﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactSh
{
    class Program
    {
        public static void Usage()
        {
            Console.WriteLine("fact-sh script.fsh");
        }
        static void Main(string[] args)
        {
            for (int n = 0; n < args.Length; n++)
            {
                if (System.IO.File.Exists(args[n]))
                {
                    Shell Shell = new Shell();
                    Shell.Run(args[n]);
                    
                }
            }
        }
    }
}
