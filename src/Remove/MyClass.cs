using System;
using System.Collections.Generic;
using System.Text;

namespace Remove
{
	class Remove : Fact.Plugin.Plugin
	{
		public override string Name
		{
			get
			{
				return "Remove";
			}
		}

		public static string FindFactPackage(string fileName)
		{
			if (fileName == "") { return ""; }
			if (System.IO.Path.GetExtension(fileName).ToLower() == ".ff")
			{ return fileName; }
			return FindFactPackage(System.IO.Path.GetDirectoryName(fileName));
		}
		public override int Run(string[] args)
		{
			if (args.Length != 1)
			{
				Fact.Log.Error("Usage: Remove factpackage.ff/path/filename");
				Fact.Log.Error("  Remove plugin removes a file into an existing fact package");
			}
			string where = args [0];
			string package = FindFactPackage(args[0]);
			if (package == "")
			{
				Fact.Log.Error("No fact package specified");
				return 1;
			}
			if (!System.IO.File.Exists(package))
			{
				Fact.Log.Error("The package \"" + args[0] + "\" does not exist.");
				return 1;
			}

			string file = where.Substring(package.Length);
			while (file.StartsWith("/")) { file = file.Substring(1); }
			while (file.EndsWith("/")) { file = file.Substring(0, file.Length - 1); }
			Fact.Processing.Project Project = new Fact.Processing.Project("");
			Project.Load(package);
			Project.RemoveFile(file);
			Project.Save(package);
			return 0;
		}
	}
}
