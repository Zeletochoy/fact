﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manager
{

    class Client
    {
        System.Net.Sockets.TcpClient _TcpClient;
        List<Task> _Tasklist = new List<Task>();
        List<byte> _Buffer = new List<byte>();
        string _RemoteIP = "";
        string _LocalIP = "";
        public Client(System.Net.Sockets.TcpClient TcpClient, ref List<Task> Tasklist)
        {
            _TcpClient = TcpClient;
            _Tasklist = Tasklist;
            try
            {
                _RemoteIP = ((System.Net.IPEndPoint)_TcpClient.Client.RemoteEndPoint).Address.ToString();
            }
            catch { _RemoteIP = "0.0.0.0"; }
            try
            {
                _LocalIP = ((System.Net.IPEndPoint)_TcpClient.Client.LocalEndPoint).Address.ToString();
            }
            catch { _LocalIP = "0.0.0.0"; }
        }

        int filecount = 0;
        string file = "";
        int filesize = 0;
        // State machine
        public bool TryRun()
        {
            if (filecount == 0 && filesize == 0)
            {
                if (_TcpClient.Available >= 4)
                {
                    byte[] array = new byte[4];
                    _TcpClient.GetStream().Read(array, 0, 4);
                    filecount = BitConverter.ToInt32(array, 0);
                }

            }           
            else if (filecount > 0 && filesize == 0)
            {
                if (_TcpClient.Available >= 4)
                {
                    byte[] array = new byte[4];
                    _TcpClient.GetStream().Read(array, 0, 4);
                    filesize = BitConverter.ToInt32(array, 0);
                    _Buffer = new List<byte>();
                    if (filesize == 0)
                    {
                        filecount--;
                        filesize = 0;
                        file = "";
                        if (filecount == 0)
                            filecount = -1;
                    }
                }

            }
            else if (filesize > 0 && file == "")
            {
                if (_TcpClient.Available + _Buffer.Count >= filesize)
                {
                    while (filesize > _Buffer.Count)
                    {
                        byte[] array = new byte[filesize - _Buffer.Count];
                        int count = _TcpClient.GetStream().Read(array, 0, filesize - _Buffer.Count);
                        for (int n = 0; n < count; n++)
                        {
                            _Buffer.Add(array[n]);
                        }
                    }
                    
                    file = UTF8Encoding.UTF8.GetString(_Buffer.ToArray());
                    if (file.StartsWith("file://"))
                    {
                        Fact.Log.Info(" [TASK MGR] Load task " + file);
                        lock (_Tasklist)
                        {
                            Task task = new Task(file);
                            task.Variables.Add("LocalIP", _LocalIP);
                            task.Variables.Add("RemoteIP", _RemoteIP);
                            _Tasklist.Add(task);
                        }

                    }
                    else if (file.StartsWith("raw://"))
                    {
                        Fact.Log.Info(" [TASK MGR] Load network task");
                        lock (_Tasklist)
                        {
                            file = file.Replace("raw://", ""); 
                            string factfile = Fact.Internal.Information.GetTempFileName() + ".ft";
                            try
                            {
                                System.IO.File.WriteAllText(factfile, file);
                                lock (_Tasklist)
                                {
                                    Task task = new Task(factfile);
                                    task.Variables.Add("LocalIP", _LocalIP);
                                    task.Variables.Add("RemoteIP", _RemoteIP);
                                    _Tasklist.Add(task);
                                }
                                Fact.Tools.RecursiveDelete(factfile);
                            }
                            catch
                            {
                                try
                                {
                                    System.IO.File.WriteAllText("/tmp/" + factfile, file);
                                    lock (_Tasklist)
                                    {
                                        Task task = new Task("/tmp/" + factfile);
                                        task.Variables.Add("LocalIP", _LocalIP);
                                        task.Variables.Add("RemoteIP", _RemoteIP);
                                        _Tasklist.Add(task);
                                    }
                                    Fact.Tools.RecursiveDelete("/tmp/" + factfile);
                                }
                                catch
                                {
                                    Fact.Log.Error(" [TASK MGR] Impossible to create temp task.");
                                }
                            }
                        }
                    }
                    else
                    {
                        Fact.Log.Info(" [TASK MGR] Load task " + file + " (default action)");
                        lock (_Tasklist)
                        {
                            Task task = new Task(file);
                            task.Variables.Add("LocalIP", _LocalIP);
                            task.Variables.Add("RemoteIP", _RemoteIP);
                            _Tasklist.Add(task);
                        }
                    }

                    filecount--;
                    filesize = 0;
                    file = "";
                    if (filecount == 0)
                        filecount = -1;
                    _Buffer = new List<byte>();
                }
                else if (_TcpClient.Available > 64)
                {
					while (_TcpClient.Available > 64)
					{
						int sizemax = filesize - _Buffer.Count;
						if (sizemax > 1024)
							sizemax = 1024;
						byte[] tmp = new byte[sizemax];
						int cb = _TcpClient.GetStream ().Read (tmp, 0, sizemax);
						for (int n = 0; n < cb; n++) {
							_Buffer.Add (tmp [n]);
						}
					}
                }
                
            }
            else if (filecount == -4242)
            {
                _TcpClient.GetStream().WriteByte(0x42);
                Fact.Log.Verbose(" [TASK MGR] Answer to ping request.");
                _TcpClient.Close();
                return false;
            }
            else
            {
                _TcpClient.GetStream().WriteByte(0x42);
                Fact.Log.Verbose(" [TASK MGR] One client has been disconected.");
                _TcpClient.Close();
                return false;
            }
            return true;
        }
    }
}
