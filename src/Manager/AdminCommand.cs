using System;

namespace Manager
{
	public class AdminCommand
	{
        public static int SetWorker(string WorkingDirectory,
                 string[] Args,
                 System.IO.TextReader StdIn,
                 System.IO.TextWriter StdOut,
                 System.IO.TextWriter StdErr)
        {
            if (Args.Length != 1) { StdErr.Write("[ERROR] Invalid argument"); return 1; }
            try
            {
                
                int worker = int.Parse(Args[0]);
                if (worker < 1) { StdErr.Write("[ERROR] Invalid argument"); return 1; }
                Manager._WorkerCount = worker;
                Manager._Restart = true;
            }
            catch 
            {
                StdErr.Write("[ERROR] Invalid argument"); 
                return 1;
            }
            
            return 0;
        }

        public static int Restart(string WorkingDirectory,
                 string[] Args,
                 System.IO.TextReader StdIn,
                 System.IO.TextWriter StdOut,
                 System.IO.TextWriter StdErr)
        {
            if (Args.Length != 0) { StdErr.Write("[ERROR] Invalid argument"); return 1; }
            Manager._Restart = true;
            return 0;
        }

		public static int Exit(string WorkingDirectory,
		                          string[] Args,
		                          System.IO.TextReader StdIn,
		                          System.IO.TextWriter StdOut,
		                          System.IO.TextWriter StdErr)
		{
			if (Args.Length != 0) { StdErr.Write("[ERROR] Invalid argument"); return 1; }
			Manager._Exit = true;
			return 0;
		}

		public static int KillAllTasks(string WorkingDirectory,
		                       		   string[] Args,
		                       		   System.IO.TextReader StdIn,
		                       		   System.IO.TextWriter StdOut,
		                               System.IO.TextWriter StdErr)
		{
			if (Args.Length != 0) { StdErr.Write("[ERROR] Invalid argument"); return 1; }
			Manager._KillAllTasks = true;
			return 0;
		}
	}
}

