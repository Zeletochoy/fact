﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manager
{
    class Tools
    {
        public static void SendInt(Int32 value, System.IO.Stream Stream)
        {
            Stream.Write(BitConverter.GetBytes(value), 0, 4);

        }

        public static void SendString(string Message, System.IO.Stream Stream)
        {
            byte[] array = UTF8Encoding.UTF8.GetBytes(Message);
            SendInt(array.Length, Stream);
            Stream.Write(array, 0, array.Length);
        }
    }
}
