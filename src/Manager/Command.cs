﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manager
{
    static class Command
    {

        public static int Mkdir(string WorkingDirectory,
                         string[] Args,
                         System.IO.TextReader StdIn,
                         System.IO.TextWriter StdOut,
                         System.IO.TextWriter StdErr)
        {
            foreach (string dir in Args)
            {
                if (dir.StartsWith("/"))
                {
                    Fact.Tools.RecursiveMakeDirectory(dir);
                }
                else
                {
                    Fact.Tools.RecursiveMakeDirectory(WorkingDirectory + "/" + dir);
                }
            }
            return 0;
        }

		public static int Cp(string WorkingDirectory,
		                        string[] Args,
		                        System.IO.TextReader StdIn,
		                        System.IO.TextWriter StdOut,
		                        System.IO.TextWriter StdErr)
		{
			if (!WorkingDirectory.EndsWith ("/")) { WorkingDirectory += "/"; }
			if (Args.Length == 2) 
			{
				try
				{
					if (!System.IO.File.Exists(WorkingDirectory + Args[0]))
					{
						Fact.Log.Error ("File not found " + Args[0]);
					}
					else
					{
						System.IO.File.Copy(WorkingDirectory + Args[0], WorkingDirectory + Args[1]);
					}
				}
				catch (Exception e)
				{
					Fact.Log.Error ("Impossible to copy " + Args[0]);
					Fact.Log.Exception (e);
				}
			}
			else
			{
				Fact.Log.Error ("Usage: cp fileA fileB");
			}
			return 0;
		}

        public static int Echo(string WorkingDirectory,
                                string[] Args,
                                System.IO.TextReader StdIn,
                                System.IO.TextWriter StdOut,
                                System.IO.TextWriter StdErr)
        {
            for (int n = 0; n < Args.Length; n++)
            {
                StdOut.WriteLine(Args[n]);
            }
            return 0;
        }

        public static int Import(string WorkingDirectory,
                                string[] Args,
                                System.IO.TextReader StdIn,
                                System.IO.TextWriter StdOut,
                                System.IO.TextWriter StdErr)
        {
            foreach (string arg in Args)
            {
                string newarg = arg.Trim();
                if (!System.IO.File.Exists(newarg))
                {
					if (System.IO.Directory.Exists (newarg)) 
					{
						try
						{
							string destdir = newarg;
							try { destdir = System.IO.Path.GetFileName(destdir); } catch { }
							Fact.Log.Verbose("Import directory " + newarg + " ...");
							Fact.Tools.RecursiveCopyDirectory(newarg, WorkingDirectory + "/" + destdir);
							return 0;
						}
						catch
						{
							Fact.Log.Error("Impossible to copy the directory");
							return 1;
						}
					}
					else
					{
                    	Fact.Log.Error("Impossible to import \"" + newarg + "\": File not found.");
                    	return 1;
					}
                }
                else
                {
                    try
                    {
						string destfile = newarg;
						try { destfile = System.IO.Path.GetFileName(destfile); } catch { }
                        Fact.Log.Verbose("Import file " + newarg + " ...");
						System.IO.File.Copy(newarg, WorkingDirectory + "/" + destfile);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to copy the file");
                    }
                }
            }
            return 0;
        }

        public static int Export(string WorkingDirectory,
                        string[] Args,
                        System.IO.TextReader StdIn,
                        System.IO.TextWriter StdOut,
                        System.IO.TextWriter StdErr)
        {
            foreach (string arg in Args)
            {
				string newarg = arg.Trim();
                if (!System.IO.File.Exists(WorkingDirectory + "/" + newarg))
                {
                    Fact.Log.Error("Impossible to export \"" + newarg + "\": File not found.");
                    return 1;
                }
                else
                {
                    try
                    {
                        Fact.Log.Verbose("Export file " + newarg + " ...");
                        System.IO.File.Copy(WorkingDirectory + "/" + newarg, newarg);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to copy the file");
                    }
                }
            }
            return 0;
        }

		public static int Publish(string WorkingDirectory,
		                          string[] Args,
		                          System.IO.TextReader StdIn,
		                          System.IO.TextWriter StdOut,
		                          System.IO.TextWriter StdErr)
		{
			string sandboxfile = "";
			string outputfile = "";
			foreach (string arg in Args)
			{
				if (sandboxfile == "")
					sandboxfile = arg;
				else if (outputfile == "")
					outputfile = arg;

			}


			if (!System.IO.File.Exists(WorkingDirectory + "/" + sandboxfile))
			{
				Fact.Log.Error("Impossible to export \"" + sandboxfile + "\": File not found.");
				return 1;
			}
			else
			{
				try
				{
					Fact.Log.Verbose("Export file " + sandboxfile + " ...");
					System.IO.File.Move(WorkingDirectory + "/" + sandboxfile, outputfile);
				}
				catch
				{
					Fact.Log.Error("Impossible to copy the file");
				}
			}

			return 0;
		}

		public static int Exec(string WorkingDirectory,
		                       string[] Args,
		                       System.IO.TextReader StdIn,
		                       System.IO.TextWriter StdOut,
		                       System.IO.TextWriter StdErr)
		{
			bool _timeout = false;
			bool _outputfile = false;
			bool _errorfile = false;
			bool _executable = true;

			if (!WorkingDirectory.EndsWith ("/")) { WorkingDirectory += "/"; }

			int _timeoutval = -1;
			string _outputfileval = "";
			string _errorfileval = "";
			string _executableval = "";
			StringBuilder _progargs = new StringBuilder ();
			for (int n = 0; n < Args.Length; n++) 
			{
				if (_timeout) {	int.TryParse (Args [n], out _timeoutval); _timeout = false; }
				else if (_outputfile) { _outputfileval = Args[n];  _outputfile = false; }
				else if (_errorfile) { _errorfileval = Args[n]; _errorfile = false; }
				else if (_executable && Args[n].ToLower() == "--timeout" || Args[n].ToLower() == "-t") { _timeout = true; }
				else if (_executable && Args[n].ToLower () == "--outputfile" || Args[n].ToLower() == "-o") { _outputfile = true; }
				else if (_executable && Args[n].ToLower () == "--errorfile" || Args[n].ToLower() == "-e") { _errorfile = true; }
				else if (_executable) { _executableval = Args[n]; _executable = false;  }
				else { _progargs.Append(" " + Args[n] + " "); }

			}
            using (Fact.Runtime.Process Process = new Fact.Runtime.Process(WorkingDirectory + _executableval, _progargs.ToString()))
            {
                Fact.Runtime.Process.Result Result = Process.Run(_timeoutval);
                if (_outputfileval != "")
                {
                    System.IO.File.WriteAllText(WorkingDirectory + _outputfileval, Result.StdOut);
                }
                else
                {
                    StdOut.Write(Result.StdOut);
                }

                if (_errorfileval != "")
                {
                    System.IO.File.WriteAllText(WorkingDirectory + _errorfileval, Result.StdErr);
                }
                else
                {
                    StdErr.Write(Result.StdErr);
                }
                return Result.ExitCode;
            }
		}

        public static int Self(string WorkingDirectory,
                                string[] Args,
                                System.IO.TextReader StdIn,
                                System.IO.TextWriter StdOut,
                                System.IO.TextWriter StdErr)
        {
            foreach (System.Reflection.Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (assembly.GetName().Name.ToLower().Contains("factexe") && assembly.EntryPoint != null)
                {
                    Fact.Log.Verbose("Self call using current AppDomain");
                    string file = assembly.Location;
                    if (System.IO.File.Exists(file))
                    {
                        Fact.Log.Verbose("Fact executable found: " + file);
                        Fact.Log.Verbose("Change directory for fact runtime into " + WorkingDirectory + " ...");
                        string AllArgs = "";
                        foreach (string _ in Args)
                            AllArgs +=  " \"" + _ + "\" ";

						try
						{
                            using (Fact.Runtime.Process Process = new Fact.Runtime.Process(file, WorkingDirectory, AllArgs))
                            {
                                Process.IsTrustedProcess = true;
                                Fact.Runtime.Process.Result Result = Process.Run(-1);
                                Console.Write(Result.StdErr);
                                Console.Write(Result.StdOut);
                                return Result.ExitCode;
                            }
						}
						catch
						{
							// Try directly with mono
							string mono = Fact.Internal.Information.GetMonoBinary ();
							try
							{
                                using (Fact.Runtime.Process Process = new Fact.Runtime.Process(mono, WorkingDirectory, file + " " + AllArgs))
                                {
                                    Process.IsTrustedProcess = true;
                                    Fact.Runtime.Process.Result Result = Process.Run(-1);
                                    Console.Write(Result.StdErr);
                                    Console.Write(Result.StdOut);
                                    return Result.ExitCode;
                                }
							}
							catch (Exception e2)
							{
								Fact.Log.Warning ("Error while trying to create a fact subprocess:");
								Fact.Log.Warning (e2.ToString());
								return 1;
							}
						}

                    }
                    else
                    {
                        Fact.Log.Error("Impossible to found the FactExe associated to this appDomain");
                    }
                }
            }
            Fact.Log.Error("Impossible to found the FactExe associated to this appDomain");
            return 1;

        }

        public static int Base64(string WorkingDirectory,
                        string[] Args,
                        System.IO.TextReader StdIn,
                        System.IO.TextWriter StdOut,
                        System.IO.TextWriter StdErr)
        {
            if (Args.Length != 2) { Fact.Log.Error("Base64: Invalid argument"); return 1; }
            if (Args[0] != "-f") { Fact.Log.Error("Invalid option"); }
            if (System.IO.File.Exists(WorkingDirectory + "/" + Args[1]))
            {
                string base64 = System.Convert.ToBase64String(System.IO.File.ReadAllBytes(WorkingDirectory + "/" + Args[1]));
                StdOut.Write(base64);
                return 0;
            }
            Fact.Log.Error("File not found " + Args[1]);
            return 1;
        }

        #region Sharing_Primitives
        public static int Share_Out(string WorkingDirectory,
                 string[] Args,
                 System.IO.TextReader StdIn,
                 System.IO.TextWriter StdOut,
                 System.IO.TextWriter StdErr)
        {
			Fact.Log.Warning ("yeah");
			Fact.Log.Verbose(Args[1].Length.ToString());
            if (Args.Length != 2) { Fact.Log.Error("invalid sharing"); }
            try
            {

                System.IO.File.WriteAllBytes(WorkingDirectory + "/" + Args[0], Convert.FromBase64String(Args[1]));
            }
            catch { Fact.Log.Error("Impossible to unpack file " + Args[0]); return 1; }
            return 0;
        }
        #endregion
    }
}
