﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manager
{
    class Task
    {
		volatile string[] Instr = null;
        string _wrkdir = "";
        string _initwrkdir = "";

		string _refdir = "";
        volatile int index = 0;
        string _task = "";
		string _name = "";
		bool _killed = false;
        volatile bool _locked = false;

		DateTime _RegistrationTime = DateTime.Now;
		public DateTime RegistrationTime {get {return _RegistrationTime; } }
        Dictionary<string, Fact.Runtime.Process.EmbededProcess> _commands
            = new Dictionary<string, Fact.Runtime.Process.EmbededProcess>();

        StringBuilder _XMLOutput = new StringBuilder();
        bool _dumpXML = false;

        GearmanCommand.GearmanWorker.GearmanClient _GearmanClient = null;
        string _GearmanId = "";

        Dictionary<string, string> _Variables = new Dictionary<string, string>();

        public Dictionary<string, string> Variables
        {
            get { return _Variables; }
        }

        public void BindGearman(GearmanCommand.GearmanWorker.GearmanClient client, string id, string args)
        {
			Fact.Log.Verbose ("  [TASK MGR] [" + _name + "] Gearman binded:" + id);
            _GearmanClient = client;
            _GearmanId = id;
            _dumpXML = true;
			System.IO.File.WriteAllText (_wrkdir + "/gearman.dat", args);
			_RegistrationTime = DateTime.Now;
        }

        ~Task()
        {
            if (System.IO.Directory.Exists(_wrkdir))
            {
                Fact.Tools.RecursiveDelete(_wrkdir);
            }
        }
		Fact.Runtime.Process _CurrentProcess = null;
        public void ForceKill()
        {
            try
            {
                lock (this)
                {
                    if (_CurrentProcess != null)
                    {
                        _CurrentProcess.Kill();
                    }
                }
            }
            catch { }
            Kill();
        }
		public void Kill()
		{
			_killed = true;
            lock (this)
            {
                if (!_locked)
                {
                    if (System.IO.Directory.Exists(_wrkdir))
                    {
                        Fact.Tools.RecursiveDelete(_wrkdir);
                    }
                }
            }
			if (_GearmanClient != null) 
			{
				try
				{
					_GearmanClient.SendRequest(14, _GearmanId);
					try
					{
						if (index< Instr.Length)
						{
							if (_CurrentProcess != null)
							{ _CurrentProcess.Kill(); }
							Fact.Log.Warning("[TASK MGR] [" + _name + "] " + " Abort step: " + Instr[index]);
						}
					}
					catch
					{

					}
				}
				catch 
				{
				}
			}

		}



        public Task(string file)
        {
			try
			{
				string name = System.IO.Path.GetFileNameWithoutExtension(file);
				_name = name;
			}
			catch
			{
			}
            _commands = new Dictionary<string, Fact.Runtime.Process.EmbededProcess>();

            _commands["fact"] = Command.Self;
            _commands["import"] = Command.Import;
            _commands["export"] = Command.Export;
            _commands["echo"] = Command.Echo;
            _commands["mkdir"] = Command.Mkdir;
			_commands["publish"] = Command.Publish;
			_commands["exec"] = Command.Exec;
			_commands["cp"] = Command.Cp;
            _commands["base64"] = Command.Base64;

			_commands ["gearman_connect"] = GearmanCommand.GearmanConnect;
			_commands ["gearman_export"] = GearmanCommand.GearmanExport;

            _commands["admin_setworkers"] = AdminCommand.SetWorker;
			_commands["admin_restart"] = AdminCommand.Restart;
			_commands["admin_killalltasks"] = AdminCommand.KillAllTasks;
			_commands["admin_exit"] = AdminCommand.Exit;

            _commands["share_out"] = Command.Share_Out;

            _commands["cd"] =
                (string WorkingDirectory,
                 string[] Args,
                 System.IO.TextReader StdIn,
                 System.IO.TextWriter StdOut,
                 System.IO.TextWriter StdErr) =>
                {
                    if (Args.Length == 1)
                    {
                        if (System.IO.Directory.Exists(_wrkdir + "/" + Args[0]))
                        {
                            _wrkdir = _wrkdir + "/" + Args[0];
                            return 0;
                        }
                        else
                        {
                            Fact.Log.Error("The directory " + Args[0] + " does not exist");
                            return 1;
                        }
                    }
                    else if (Args.Length == 0)
                    {
                        _wrkdir = _initwrkdir;
                    }
                    return 1;
                };

            _commands["task_exec"] =
                (string WorkingDirectory,
				 string[] Args,
				 System.IO.TextReader StdIn,
				 System.IO.TextWriter StdOut,
				 System.IO.TextWriter StdErr) =>
			{
                if (Args.Length != 1)
                {
                    Fact.Log.Error("Invalid use of task_exec. One file expected");
                }
                try
                {
                    if (System.IO.File.Exists(WorkingDirectory + "/" + Args[0]))
                    {
                        Fact.Log.Info("Replacing current task with " + WorkingDirectory + "/" + Args[0]);
                        string[] list = System.IO.File.ReadAllLines(WorkingDirectory + "/" + Args[0]);
                        Instr = list;
                        index = -1;
                    }
                }
                catch
                { }
                return 0;
            };

			_commands["wrkdir"] =
				(string WorkingDirectory,
				 string[] Args,
				 System.IO.TextReader StdIn,
				 System.IO.TextWriter StdOut,
				 System.IO.TextWriter StdErr) =>
			{
				if (Args.Length != 1)
				{
					Fact.Log.Error("Invalid use of wrkdir. One directory expected");
				}
				try
				{
					Fact.Tools.RecursiveMakeDirectory(Args[0]);
				}
				catch
				{}
				while (_refdir.EndsWith("/")) { _refdir.Substring(0, _refdir.Length - 1); }
				_wrkdir = Args[0];
				return 0;
			};
			_commands ["name"] =
				(string WorkingDirectory,
				 string[] Args,
				 System.IO.TextReader StdIn,
				 System.IO.TextWriter StdOut,
				 System.IO.TextWriter StdErr) =>
			{
				if (Args.Length != 1)
				{
					Fact.Log.Error("Invalid name. One name expected");
				}
				try
				{
					_name = Args[0];
				}
				catch
				{}
				return 0;
			};

            _task = file;
			_wrkdir = Fact.Tools.CreateTempDirectory ();
            _initwrkdir = _wrkdir;

			// Test if fact can use the working directory
			string testfile = Fact.Internal.Information.GetTempFileName ();
			try
			{
				System.IO.File.WriteAllText (_wrkdir + "/" + testfile, "fact test file");
				Fact.Tools.RecursiveDelete(_wrkdir + "/" + testfile);
			}
			catch
			{
				Fact.Log.Warning ("Fact can't read in its temp directory " + _wrkdir +".");
				Fact.Log.Warning ("Fact will change the directory for the task " + _task + ".");

				_wrkdir = "/tmp/" + Fact.Internal.Information.GetTempFileName ();
                _initwrkdir = _wrkdir;
                try
				{
					Fact.Tools.RecursiveMakeDirectory(_wrkdir);
					System.IO.File.WriteAllText (_wrkdir + "/" + testfile, "fact test file");
					Fact.Tools.RecursiveDelete(_wrkdir + "/" + testfile);
				}
				catch
				{
					Fact.Log.Error ("Impossible to use the rescue temp directory " + _wrkdir + ".");
					Fact.Log.Error ("Some problems may occurs ...");

				}
			}

            try
            {
                Instr = System.IO.File.ReadAllLines(file);
            }
            catch
            {
                Fact.Log.Error("Impossible to load file " + file);
                Instr = new string[0];
            }
        }

        public bool Ended { get { lock (this) { return !_locked && (index >= Instr.Length); } } }

		public bool RunNext()
		{
			return RunNext("");
		}
        public bool RunNext(string Worker)
        {
            Fact.Runtime.Process Process = null;
            string instrvalue = "";
            lock (this)
            {
                if (_locked ||_killed)
					return false;
                if (index >= Instr.Length)
                {
                    if (System.IO.Directory.Exists(_wrkdir))
                    {
                        Fact.Tools.RecursiveDelete(_wrkdir);
                    }
                    return false;
                }
                _locked = true;
                if (Instr[index].Trim() == "")
                {
                    _locked = false;
                    index++;
                    return true;
                }
				if (_name == "")
					Fact.Log.Verbose(" [TASK MGR] Start: " + Instr [index]);
				else 
				{
					if (Worker == "")
					{
						if (Instr[index].Length < 512)
							Fact.Log.Verbose(" [TASK MGR] [" + _name + "] Start: " + Instr [index]);
						else
							Fact.Log.Verbose(" [TASK MGR] [" + _name + "] Start instruction");
							
					}
					else
					{
						if (Instr[index].Length < 512)
							Fact.Log.Verbose(" [TASK MGR] [" + _name + "] Start: " + Instr [index] + "(" + Worker + ")");
						else
							Fact.Log.Verbose(" [TASK MGR] [" + _name + "] Start instruction (" + Worker + ")");
							
					}
				}
                instrvalue = Instr[index];
				if (instrvalue.StartsWith ("share_out ")) {
					string[] args = instrvalue.Split (new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
					List<string> rargs = new List<string> ();
					for (int n = 1; n < args.Length; n++)
					{
						rargs.Add (args [n]);
					}
					_CurrentProcess = new Fact.Runtime.Process (_commands ["share_out"], _wrkdir, rargs.ToArray());
				} else {
					_CurrentProcess = Fact.Runtime.Process.CreateProcessFromCommand (instrvalue, 0, _wrkdir, _commands, _Variables);
				}
            }
            try
            {
				Fact.Runtime.Process.Result result = _CurrentProcess.Run(-1);
                _CurrentProcess.UnsafeCoerceDestroy();
				_CurrentProcess = null;
                Console.Write(result.StdOut);
                Console.Write(result.StdErr);
                if (_dumpXML)
                {
                    _XMLOutput.AppendLine("<process>");
                    _XMLOutput.AppendLine("  <cmd>" + instrvalue + "</cmd>");
                    _XMLOutput.AppendLine("  <stdout>" + result.StdOut + "</stdout>");
					_XMLOutput.AppendLine("  <stderr>" + result.StdErr + "</stderr>");
                    _XMLOutput.AppendLine("  <exitcode>" + result.ExitCode.ToString() + "</exitcode>");
                    _XMLOutput.AppendLine("</process>");
                }
				if (_name == "")
				{
					if (instrvalue.Length < 512)
                    	Fact.Log.Verbose(" [TASK MGR] Ended: " + instrvalue);
					else
						Fact.Log.Verbose(" [TASK MGR] Ended instruction");
						
				}
				else
				{
					if (instrvalue.Length < 512)
                    	Fact.Log.Verbose(" [TASK MGR] [" + _name + "] Ended: " + instrvalue);
					else
						Fact.Log.Verbose(" [TASK MGR] [" + _name + "] Ended instruction");
						
				}
            }
            catch (Exception e)
            {
                Fact.Log.Error(" [TASK MGR] Exception occurs while running " + instrvalue);
				Fact.Log.Error(e.ToString ());
            }

            if (index >= Instr.Length - 1)
            {
				try
				{
					if (System.IO.Directory.Exists(_wrkdir))
					{
						Fact.Tools.RecursiveDelete(_wrkdir);
					}
				}
				catch
				{
				}

                // End of task event
                if (_GearmanClient != null)
                {
					Fact.Log.Verbose ("  [TASK MGR] [" + _name + "] Gearman send response");
					_GearmanClient.SendRequest(13, _GearmanId, _XMLOutput.ToString());
                    _GearmanClient = null;
                    _GearmanId = "";
                    _dumpXML = false;
                }
            }

            lock (this)
            {
                index++;
                _locked = false;
            }
            return true;
        }
    }
}
