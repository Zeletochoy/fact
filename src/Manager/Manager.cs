﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manager
{
    public class Manager : Fact.Plugin.Plugin
    {
        const int FORK_LIMIT = (512);
        public override string Name
        {
            get
            {
                return "Manager";
            }
        }

        volatile int _threadcount = 0;

        static volatile bool _Singleton = false;
        static volatile internal bool _Restart = false;
		static volatile internal bool _Exit = false;
		static volatile internal bool _KillAllTasks = false;
        static volatile internal int _WorkerCount = 0;
		static volatile internal bool _ForkBombPaused = false;
		static List<Task> _Tasklist = new List<Task>();

		internal static int TaskListSize()
		{
			return _Tasklist.Count;
		}

	    internal static void AddTask(Task Task)
		{
			lock (_Tasklist) 
			{
				_Tasklist.Add (Task);
			}
		}

        bool _RecursiveKill(Fact.Internal.Monitor.ProcessInfo info)
        {
			bool mustkill = true;

            foreach (Fact.Internal.Monitor.ProcessInfo child in info.Children.Values)
            {
				if (!_RecursiveKill (child))
				{
					mustkill = false;
				}
            }
			if (mustkill)
			{
				if (info.Id == _FactManagerID) { Fact.Log.Error("Don't kill me " + info.Id); return false; }
	            //Fact.Log.Error("Emergency kill " + info.Id);
	            Fact.Runtime.Process.Kill(info.Id);
			}
			return mustkill;
        }

        int _RecurseKillForkBomb(Fact.Internal.Monitor.ProcessInfo info)
        {
			if ((int)info.Tag > 0)
				return (int)info.Tag;
            info.Tag = 1;
            int count = 0;
            foreach (Fact.Internal.Monitor.ProcessInfo child in info.Children.Values)
            {
                int value = _RecurseKillForkBomb(child);
                if (value < 0) { return value; }
                count += value;
            }
            if (count > FORK_LIMIT)
            {
                Fact.Log.Error("Potential fork bomb detected");
                Fact.Log.Error("Process tree associated to " + info.Name + " will be killed");
				_RecursiveKill(info);
				return -1;
            }
			info.Tag = count + 1;
            return count + 1;
        }

		public void GarbageCollect(Fact.Internal.Monitor Monitor)
		{
			restart:
			Monitor.Refresh();
			foreach (Fact.Internal.Monitor.ProcessInfo info in Monitor.Processes.Values)
			{

			}
		}

        public void KillForkBomb(Fact.Internal.Monitor Monitor)
        {
			restart:
			Monitor.Refresh();
			foreach (Fact.Internal.Monitor.ProcessInfo info in Monitor.Processes.Values)
			{
				info.Tag = 0;
			}
            foreach (Fact.Internal.Monitor.ProcessInfo info in Monitor.Processes.Values)
            {
                if (_RecurseKillForkBomb(info) < 0)
				{
					_ForkBombPaused = true;
					goto restart;
				}
            }
			if (_ForkBombPaused == true) 
			{
				Fact.Log.Warning ("Fork bomb seems under control restarting workers ...");
			}
			_ForkBombPaused = false;
        }

        int _FactManagerID = 0;

		public void Help()
		{

		}

        public override int Run(string[] args)
        {
            Fact.Log.Warning ("*** Deprecated ***");

            _FactManagerID = Fact.Internal.Monitor.GetCurrentProcess().Id;
			string ip = "127.0.0.1";
            bool setip = false;
			bool atomic = false;
			bool exec = false;
            bool interactive = false;
			bool nocheck = false;
			bool expiration = false;
            bool pingf = false;
            bool sendtask = false;
            int port = 1750;

			TimeSpan expirationvalue = TimeSpan.FromSeconds(1);

			int ProcessorCount = System.Environment.ProcessorCount;

			// Leave one core for the operating system and fact manager
			if (ProcessorCount > 8)
				ProcessorCount-=2;

            _WorkerCount = ProcessorCount;

			if (atomic) 
			{
                _WorkerCount *= 2;
			}

			for (int n = 0; n < args.Length; n++)
			{
				if (n < args.Length - 1)
				{
					try
					{
						if (args [n] == "--ip") { ip = args [n + 1]; args [n] = ""; args [n + 1] = ""; setip = true;}
                        if (args[n] == "--worker") { _WorkerCount = int.Parse(args[n + 1]); args[n] = ""; args[n + 1] = ""; }
						if (args[n] == "--atomic") { atomic = true; args[n] = ""; }
						if (args[n] == "--interactive") { interactive = true; args[n] = ""; }
						if (args[n] == "--exec") { exec = true; args[n] = ""; }
						if (args[n] == "--nocheck") { nocheck = true; args[n] = ""; }
                        if (args[n] == "--sendtask") { sendtask = true; args[n] = ""; }
                        if (args[n] == "--ping") { pingf = true; args[n] = ""; }
                        if (args[n] == "--port") { args[n] = ""; try { port = int.Parse(args[n + 1]); } catch { } args[n + 1] = ""; }
						if (args[n] == "--expirationtime")
						{ 
							args[n] = "";
							if (TimeSpan.TryParse(args[n + 1], out expirationvalue))
							{

								expiration = true;
							}
							else
							{
								Fact.Log.Error ("Invalid TimeSpan: " + args[n + 1]);
							}
							args[n + 1] = "";
						}
                    }
					catch { }
				}
				else
				{
					try
					{
						if (args[n] == "--atomic") { atomic = true; args[n] = ""; }
						if (args[n] == "--interactive") { interactive = true; args[n] = ""; }
						if (args[n] == "--exec") { exec = true; args[n] = ""; }
						if (args[n] == "--nocheck") { nocheck = true; args[n] = ""; }
                        if (args[n] == "--ping") { pingf = true; args[n] = ""; }
                        if (args[n] == "--sendtask") { sendtask = true; args[n] = ""; }
						if (args[n] == "--help" || args[n] == "-h") { Help(); }
					}
					catch { }
				}
			}

            
            if (ip != "")
            {
                if (ip.Contains(":"))
                {
                   string[] items = ip.Split(':');
                   if (items.Length == 2)
                   {
                       port = int.Parse(items[1]);
                       ip = items[0];
                   }
                }
            }
            

			if (exec)
			{
				for (int n = 0; n < args.Length; n++) 
				{
					if (args[n] == "")
						continue;
					try
					{
						Task t = new Task(args[n]);
						while (!t.Ended)
						{
							t.RunNext();
						}
					}
					catch { }
				}
				return 0;
			}
           
            if (args.Length > 0 && args[0] == "--ip") 
            {
                args[0] = "";
                if (args.Length > 1)
                {
                    ip = args[1];
                }
                args[1] = "";
            }

        tryconnect:
            string interactivecommand = "";
            if (interactive)
            {
                interactivecommand = Console.ReadLine();
            }
            if (!pingf)
            {
                Fact.Log.Verbose(" [TASK MGR] Starting fact task manager ...");
                Fact.Log.Verbose(" [TASK MGR] Checking if a server already exists ...");
            }
            try
            {
                
                System.Net.Sockets.TcpClient client =
                    new System.Net.Sockets.TcpClient(ip, port);
               
                
				int connectionTimeout = 3000;
				while (!client.Connected)
				{
					connectionTimeout -= 10;
					System.Threading.Thread.Sleep(10);
                    
				}

                if (client.Connected)
                {
                    Fact.Log.Verbose(" [TASK MGR] Sending command to existing fact manager ...");

                    if (interactive)
                    {
                        Tools.SendInt(1, client.GetStream());
                        Tools.SendString("raw://" + interactivecommand, client.GetStream());
                    }
                    else if (sendtask && !pingf)
                    {
                        int cb = 0;
                        for (int n = 0; n < args.Length; n++)
                        {
                            if (args[n] != "") { cb++; }
                        }
                        Tools.SendInt(cb, client.GetStream());

                        for (int n = 0; n < args.Length; n++)
                        {
                            if (args[n] == "") { }
                            else
                            {
                                string cmd = "";
                                try
                                {
                                    cmd = System.IO.File.ReadAllText(args[n]);
                                   
                                }
                                catch { }
                                try
                                {
                                    Tools.SendString("raw://" + FileSharing.ParseShare(cmd), client.GetStream());
                                }
                                catch { }
                            }
                        }
                    }
                    else
                    {
                        if (pingf) { Tools.SendInt(-4242, client.GetStream()); }
                        else
                        {
                            Tools.SendInt(args.Length, client.GetStream());
                            for (int n = 0; n < args.Length; n++)
                            {
                                Tools.SendString(args[n], client.GetStream());
                            }
                        }
                    }
                    Fact.Log.Verbose(" [TASK MGR] Waiting for server Ack ...");
                    int Timeout = 10000;
                    while (client.Available < 1)
                    {
                        System.Threading.Thread.Sleep(100);
                        Timeout -= 100;
                        if (Timeout == 0)
                        {
                            Fact.Log.Error(" [TASK MGR] Timeout exit");
                            return 1;
                        }
                    }
                    int ack = client.GetStream().ReadByte();
                    try { client.Close(); } catch { }
                    if (ack == 0x42)
                    {
                        if (pingf) { return 0; }
                        if (interactive) { goto tryconnect; }
                        return 0;
                    }
                    else
                    {
                        Fact.Log.Error(" [TASK MGR] Fact manager has not accepted the request");
                        if (interactive) { goto tryconnect; }
                        return 1;
                    }
                }

            }
            catch 
            {

            }

            if (pingf) { return 1; }
            if (setip) { return 1; }

            System.Net.Sockets.TcpListener Listener = null;
            System.Net.Sockets.TcpListener AdminListener = null;

            try
            {
                if (_Singleton)
                {
                    Fact.Log.Error(" [TASK MGR] Try to start more than one manager in the same context ...");
                }
                else
                {
                    Listener = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, port);
                    Listener.Start();
                    Fact.Log.Verbose(" [TASK MGR] Starting task manager server ...");
                    _Singleton = true;
                }
            }
            catch
            {
                Fact.Log.Error(" [TASK MGR] Impossible to start the server.");
                return 1;
            }

            if (System.Environment.ProcessorCount <= 0)
                Fact.Log.Warning(" [TASK MGR] Processor count not found ...");
            else
                Fact.Log.Verbose(" [TASK MGR] Run on " + System.Environment.ProcessorCount + " core.");



            
            foreach (string task in args)
            {
				if (task == "")
					continue;
                Fact.Log.Verbose(" [TASK MGR] Load task: " + task);
                if (System.IO.File.Exists(task))
                {
					_Tasklist.Add(new Task(task));
                }
                else
                {
                    Fact.Log.Error(" [TASK MGR] Impossible to load file: " + task);
                }
            }


            createthreadpool:
            Fact.Log.Verbose(" [TASK MGR] Creating workers ...");
            List<System.Threading.Thread> threadpool = new List<System.Threading.Thread>();

            for (int n = 0; n < _WorkerCount; n++)
            {
                Fact.Log.Verbose(" [TASK MGR] Creating worker " + n + " ...");

                int refn = n;
				System.Threading.Interlocked.Increment(ref _threadcount);
                (new System.Threading.Thread(() =>
                {    
					string workerName = "Worker" + n.ToString();
					try
					{
						while (!_Restart && !_Exit)
	                    {
	                        List<Task> localtasklist;
							lock (_Tasklist)
	                        {
								localtasklist = new List<Task>(_Tasklist);
	                        }
							for (int x = 0; x < localtasklist.Count; x++)
	                        {
								if (localtasklist[x].RunNext(workerName))
	                            {
									if (_Tasklist.Count > _WorkerCount)
									{
										// More task than workers task must ends quickly
										x = 0;
									}
	                            }
								while (_ForkBombPaused)
								{
									System.Threading.Thread.Sleep(1000);
								}
	                        }
							while (_ForkBombPaused)
							{
								System.Threading.Thread.Sleep(1000);
							}
							if (_Tasklist.Count < _WorkerCount)
							{
								if (atomic)
								{
									System.Threading.Thread.Sleep(5);
								}
								else
								{
	                        		System.Threading.Thread.Sleep(25);
								}
							}
							else
							{
								System.Threading.Thread.Sleep(0);
							}
	                    }
	                    System.Threading.Interlocked.Decrement(ref _threadcount);
					}
					catch (Exception e)
					{
                        System.Threading.Interlocked.Increment(ref _threadcount);
						Fact.Log.Exception(e);
						Fact.Log.Error(workerName + " is dead. RIP " + workerName + " x.x");
					}
                }
                ) { IsBackground = true }).Start();
				System.Threading.Thread.Sleep (25);
            }

            List<Client> clients =
                new List<Client>();
            Fact.Internal.Monitor Monitor = new Fact.Internal.Monitor();
			int ping = 500;
            while (true)
            {
				ping--;
				if (ping == 0) 
				{
					ping = 500;
					Fact.Log.Info ("[PING] Fact system manager: ping");
				}
				if (!nocheck)
				{
                    if (_Tasklist.Count > 0)
                    {
                        Monitor.Refresh();
                        KillForkBomb(Monitor);
                    }
				}
				if (expiration)
				{
					lock (_Tasklist)
					{
						for (int n = 0; n < _Tasklist.Count; n++)
						{
							if (DateTime.Now - _Tasklist [n].RegistrationTime > expirationvalue)
							{
								Fact.Log.Warning ("Drop task (expired)");
								_Tasklist [n].Kill ();
								_Tasklist.RemoveAt (n); 
							}
						}
					}
				}
                while (Listener.Pending())
                {
                    Fact.Log.Verbose(" [TASK MGR] New client connected to task manager ...");
					clients.Add(new Client(Listener.AcceptTcpClient(), ref _Tasklist));
                }

                for (int n = 0; n < clients.Count; n++)
                {
                    if (!clients[n].TryRun())
                    { clients.RemoveAt(n); }
                }
                if (_threadcount == 0)
                {
                    System.Threading.Thread.Sleep(25);
                    break;
                }
				lock (_Tasklist)
                {
					for (int n = 0; n < _Tasklist.Count; n++)
                    {
						if (_Tasklist[n].Ended) { _Tasklist.RemoveAt(n); }
                    }
                }
				if (atomic)
				{
					System.Threading.Thread.Sleep (5);
				}
				else
				{
					System.Threading.Thread.Sleep (25);
				}

               
                
				if (_Exit) 
				{
					Fact.Log.Info (" [TASK MGR] Stopping fact task manager ...");
					while (_threadcount > 0) 
					{
						System.Threading.Thread.Sleep(1000);
						Fact.Log.Error(" [TASK MGR] Waiting for " + _threadcount + " threads");
					}
					goto end;
				}

				if (_KillAllTasks) 
				{
					lock (_Tasklist) 
					{
						try
						{
							foreach (Task t in _Tasklist)
							{
								t.Kill();
							}
						}
						catch
						{
						}
					}
					_KillAllTasks = false;
				}

                if (_Restart)
                {
                    while (_threadcount > 0) 
                    {
						if (!nocheck)
						{
							Monitor.Refresh ();
							KillForkBomb (Monitor);
						}
                        System.Threading.Thread.Sleep(100);
                    }
                    _threadcount = 0;
                    _Restart = false;
                    System.Threading.Thread.Sleep(10);
                    goto createthreadpool;
                }
            }
		end:
            Fact.Log.Verbose(" [TASK MGR] Exit 0");
            return 0;
		}


    }
}
