﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Manager
{
    class FileSharing
    {
        public static string ParseShare(string script)
        {
            string[] list = script.Split('\n', '\r');
            list = ParseShare(list);
            StringBuilder result = new StringBuilder();
            for (int n = 0; n < list.Length; n++)
            {
                result.AppendLine(list[n]);
            }
            return result.ToString();
        }
        public static string[] ParseShare(string[] script)
        {
            List<string> array = new List<string>();

            for (int n = 0; n < script.Length; n++)
            {
                string[] split = script[n].Split(new char[]{' '}, StringSplitOptions.RemoveEmptyEntries);
                if (split.Length == 2 && split[0] == "share_in")
                {
                    try
                    {
                        string base64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(split[1]));
                        string name = System.IO.Path.GetFileName(split[1]);
                        array.Add("share_out " + name + " " + base64);
                    }
                    catch
                    {
                        Fact.Log.Error("Impossible to open the file " + split[1]);
                    }
                }
                else
                {
                    array.Add(script[n]);
                }
            }
            return array.ToArray();
        }
    }
}
