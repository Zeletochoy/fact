﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Parser
{
    [Fact.Attribute.Test.Group("Command line basic Sanity")]
    class CommandLine
    {
        [Fact.Attribute.Test.Test("No argument")]
        public void NoArguement()
        {
            Fact.Parser.CommandLine parser = new Fact.Parser.CommandLine();
            parser.AddBooleanInput("off", "", false);
            Fact.Assert.Basic.IsTrue(parser.Parse());
        }

        [Fact.Attribute.Test.Test("Bad argument")]
        public void BadArgument()
        {
            Fact.Parser.CommandLine parser = new Fact.Parser.CommandLine();
            parser.AddBooleanInput("off", "", false);
            Fact.Assert.Basic.IsFalse(parser.Parse("--on"));
        }

        [Fact.Attribute.Test.Test("Bad argument without --")]
        public void BadArgument2()
        {
            Fact.Parser.CommandLine parser = new Fact.Parser.CommandLine();
            parser.AddBooleanInput("off", "", false);
            Fact.Assert.Basic.IsFalse(parser.Parse("off"));
        }

        [Fact.Attribute.Test.Test("Boolean argument")]
        public void BooleanArgument()
        {
            Fact.Parser.CommandLine parser = new Fact.Parser.CommandLine();
            parser.AddBooleanInput("off", "", false);
            parser.AddBooleanInput("on", "", false);
            Fact.Assert.Basic.IsTrue(parser.Parse("--on"));
            Fact.Assert.Basic.IsTrue((bool)parser["on"]);
            Fact.Assert.Basic.IsFalse((bool)parser["off"]);

        }

        [Fact.Attribute.Test.Test("String argument")]
        public void StringArgument()
        {
            Fact.Parser.CommandLine parser = new Fact.Parser.CommandLine();
            parser.AddStringInput("inputA", "", "");
            parser.AddStringInput("inputB", "", "");
            Fact.Assert.Basic.IsTrue(parser.Parse("--inputA", "A", "--inputB", "B"));
            Fact.Assert.Basic.AreEqual("A", (string)parser["inputA"]);
            Fact.Assert.Basic.AreEqual("B", (string)parser["inputB"]);
        }
    }
}
