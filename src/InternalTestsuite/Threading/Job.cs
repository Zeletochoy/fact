﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Threading
{
    [Fact.Attribute.Test.Group("Job Sanity")]
    public class JobSanity
    {
        [Fact.Attribute.Test.Test("Job Abort")]
        public void JobAbort()
        {
            for (int n = 0; n < 4096; n++)
            {
                Fact.Threading.Job.Status Status = Fact.Threading.Job.CreateJob(() =>
                {
                    System.Threading.Thread.Sleep(1000 * 60);
                });
                // Most of the launch will be optimized out
                Status.Abort();
                Status.Join();
            }
        }

        [Fact.Attribute.Test.Test("Job Suicide")]
        public void JobSuicide()
        {
            for (int n = 0; n < 4096; n++)
            {
                Fact.Threading.Job.Status Status = Fact.Threading.Job.CreateJob(() =>
                {
                    Fact.Threading.Job.CurrentJob.Abort();
                });
                Status.Join();
            }
        }

        [Fact.Attribute.Test.Test("Job Timeout")]
        public void JobTimeout()
        {
            int timeoutcount = 0;
            int pass = 0;
            for (int n = 0; n < 10; n++)
            {
                Fact.Threading.Job.Status Status = Fact.Threading.Job.CreateJob(() =>
                {
                    System.Threading.Thread.Sleep(1000 * 60);
                    pass++;
                }, 100, () => { timeoutcount++; });
                Status.Join();
            }
            Fact.Assert.Basic.AreEqual(10, timeoutcount);
            Fact.Assert.Basic.AreEqual(0, pass);

            timeoutcount = 0;
            for (int n = 0; n < 10; n++)
            {
                Fact.Threading.Job.Status Status = Fact.Threading.Job.CreateJob(() =>
                {
                    System.Threading.Thread.Sleep(100);
                    pass++;
                }, 10000, () => { timeoutcount++; });
                Status.Join();
            }
            Fact.Assert.Basic.AreEqual(0, timeoutcount);
            Fact.Assert.Basic.AreEqual(10, pass);
        }
    }
}
