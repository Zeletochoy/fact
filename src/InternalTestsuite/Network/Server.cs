﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Network
{
    class DummyService : Fact.Network.Service
    {
        public DummyService() : base("DummyService") { }

        [Fact.Network.ServiceExposedMethod]
        public string Test1(Fact.Network.Service.RequestInfo RequestInfo) 
        {
            return "Test1:Ok";
        }

        [Fact.Network.ServiceExposedMethod]
        public string Test2(Fact.Network.Service.RequestInfo RequestInfo, string Input)
        {
            if (Input == "Test2") { return "Test2:Ok"; }
            return "Test2:Ko";
        }

        [Fact.Network.ServiceExposedMethod]
        public string Test3(Fact.Network.Service.RequestInfo RequestInfo, string Client)
        {
            if (RequestInfo.User.Login == Client) { return "Test3:Ok"; }
            return "Test3:Ko";
        }
    }

    [Fact.Attribute.Test.Group("Server Sanity")]
    public class Server
    {
        DummyService _Service = null;
        Fact.Network.Server _Server = null;
        Fact.Directory.Credential _Client2Credential = null;
        int _Port = 0;
        [Fact.Attribute.Test.Setup("Server Startup")]
        public void Server_Startup()
        {
            _Port = Fact.Network.Tools.FindAvailablePort(9089, 40000);
            _Server = new Fact.Network.Server(_Port, 0);
            _Server.Start();
            _Server.AllowedUsers.AddUser(new Fact.Directory.User("testAgent1", "password"));
            _Server.AllowedUsers.AddUser(new Fact.Directory.User("testClient1", "password"));
            {
                Fact.Directory.User client2 = new Fact.Directory.User("testClient2", "password");
                _Client2Credential = new Fact.Directory.Credential(client2, DateTime.Now + new TimeSpan(1, 0, 0));
                _Server.AllowedUsers.AddUser(client2);
            }


            Fact.Assert.Basic.IsTrue(_Server.IsListening);
        }

        [Fact.Attribute.Test.Test("Simple Client")]
        public void Simple_Client()
        {
            if (_Service == null)
            {
                _Service = new DummyService();
                _Server.LoadService(_Service);
            }
            Fact.Network.Client Client = new Fact.Network.Client("http://127.0.0.1:9089", "testClient1", "password");
            Fact.Assert.Basic.AreEqual("Test1:Ok", Client.Call("DummyService.Test1") as string);
            Fact.Assert.Basic.AreEqual("Test2:Ok", Client.Call("DummyService.Test2", "Test2") as string);
            Fact.Assert.Basic.AreEqual("Test3:Ok", Client.Call("DummyService.Test3", "testClient1") as string);

        }

        [Fact.Attribute.Test.Test("Client with credential")]
        public void Client_With_Credential()
        {
            if (_Service == null)
            {
                _Service = new DummyService();
                _Server.LoadService(_Service);
            }
            Fact.Network.Client Client = new Fact.Network.Client("http://127.0.0.1:9089", _Client2Credential);
            Fact.Assert.Basic.AreEqual("Test1:Ok", Client.Call("DummyService.Test1") as string);
            Fact.Assert.Basic.AreEqual("Test2:Ok", Client.Call("DummyService.Test2", "Test2") as string);
            Fact.Assert.Basic.AreEqual("Test3:Ok", Client.Call("DummyService.Test3", "testClient2") as string);

        }

        [Fact.Attribute.Test.Test("Server Stress Test")]
        public void StressTest()
        {
            if (_Service == null)
            {
                _Service = new DummyService();
                _Server.LoadService(_Service);
            }
            List<Fact.Network.Client> clients = new List<Fact.Network.Client>();
            for (int x = 0; x < 32; x++)
            {
                Fact.Network.Client Client = new Fact.Network.Client("http://127.0.0.1:9089", "testClient1", "password");
                clients.Add(Client);
            }
            for (int n = 0; n < 32; n++)
            {
                for (int x = 0; x < 32; x++)
                {
                    Fact.Assert.Basic.AreEqual("Test1:Ok", clients[x].Call("DummyService.Test1") as string);
                }
            }
        }

        [Fact.Attribute.Test.Test("Server Authenticate Rush Test")]
        public void AuthenticateTest()
        {
            if (_Service == null)
            {
                _Service = new DummyService();
                _Server.LoadService(_Service);
            }
            List<Fact.Network.Client> clients = new List<Fact.Network.Client>();
            for (int x = 0; x < 1024; x++)
            {
                Fact.Network.Client Client = new Fact.Network.Client("http://127.0.0.1:9089", "testClient1", "password");
                clients.Add(Client);
            }
            for (int n = 0; n < 2; n++)
            {
                for (int x = 0; x < 1024; x++)
                {
                    Fact.Assert.Basic.AreEqual("Test1:Ok", clients[x].Call("DummyService.Test1") as string);
                }
            }
        }

        [Fact.Attribute.Test.Teardown("Server Shutdown")]
        public void Server_Shutdown()
        {
            _Server.Stop();
            Fact.Assert.Basic.IsFalse(_Server.IsListening);
        }
    }
}
