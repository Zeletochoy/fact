﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Internal
{
    [Fact.Attribute.Test.Group("Os Path")]
    class Path
    {
        [Fact.Attribute.Test.Test("Find Fact")]
        public void FindFact()
        {
            string libfact = Fact.Internal.Information.GetLibFact();
            Fact.Assert.IO.FileExists(libfact);
        }

        [Fact.Attribute.Test.Test("Find Fact Directory")]
        public void FindFactDirectory()
        {
            string factdir = Fact.Internal.Information.GetFactDirectory();
            Fact.Assert.IO.DirectoryExists(factdir);
        }
    }
}
