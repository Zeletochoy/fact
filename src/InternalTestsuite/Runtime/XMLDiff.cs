﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InternalTestsuite.Processing
{
    [Fact.Attribute.Test.Group("XML Diff Sanity")]
    public class XMLDiff
    {
        [Fact.Attribute.Test.Test("Simple Xml diff: Equal")]
        public void BasicEqual()
        {
            string XML1 = "<typeA></typeB></typeB></typeB></typeA>";
            string XML2 = "<typeA> </typeB>   </typeB>   </typeB>   </typeA>";
            
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsTrue(diff.Run(0).AreEqual);
        }

        [Fact.Attribute.Test.Test("Simple Xml diff: Not Equal")]
        public void BasicNotEqual()
        {
            string XML1 = "<typeA><typeB/><typeB/><typeB/></typeA>";
            string XML2 = "<typeA> <typeB/>   <typeC/>   <typeB/>   </typeA>";

            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsFalse(diff.Run(0).AreEqual);
        }

        [Fact.Attribute.Test.Test("Simple Xml diff: Unordered nodes Equal")]
        public void BasicUnorderedEqual()
        {
            string XML1 = "<typeA><typeC/><typeB/><typeB/></typeA>";
            string XML2 = "<typeA> <typeB/>   <typeC/>   <typeB/>   </typeA>";

            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsTrue(diff.Run(0).AreEqual);
        }

        [Fact.Attribute.Test.Test("Advanced Xml diff: Unordered nodes Equal")]
        public void AdvancedUnorderedEqual()
        {
            string XML1 = "<typeA><typeB><typeC/><typeA/></typeB><typeB><typeA/></typeB></typeA>";
            string XML2 = "<typeA><typeB><typeA/></typeB><typeB><typeC/><typeA/></typeB></typeA>";

            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsTrue(diff.Run(0).AreEqual);
        }

        [Fact.Attribute.Test.Test("Advanced Xml diff: Unordered nodes Not Equal")]
        public void AdvancedUnorderedNotEqual()
        {
            string XML1 = "<typeA><typeB><typeC/><typeA/></typeB><typeB><typeA/></typeB></typeA>";
            string XML2 = "<typeA><typeB><typeA/></typeB><typeB><typeC/><typeD/></typeB></typeA>";

            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsFalse(diff.Run(0).AreEqual);
        }

        [Fact.Attribute.Test.Test("Advanced Xml diff: Directory Equal")]
        public void AdvancedDirectoryEqual()
        {
            string XML1 = @"
<Directory>
    <User>
        <Name>John</Name>
        <Password>passw0rd</Password>
    </User>
    <User>
        <Name>Michael</Name>
        <Password>pAAssw0rd</Password>
    </User>
</Directory>";
            string XML2 = @"
<Directory>
    <User>
        <Password>pAAssw0rd</Password>
        <Name>Michael</Name>
    </User>
    <User>
        <Name>John</Name>
        <Password>passw0rd</Password>
    </User>
</Directory>";
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsTrue(diff.Run(0).AreEqual);
        }

        [Fact.Attribute.Test.Test("Advanced Xml diff: Directory Not Equal")]
        public void AdvancedDirectoryNotEqual()
        {
            string XML1 = @"
<Directory>
    <User>
        <Name>John</Name>
        <Password>passw0rd</Password>
    </User>
    <User>
        <Name>Michael</Name>
        <Password>pAAssw0rd</Password>
    </User>
</Directory>";
            string XML2 = @"
<Directory>
    <User>
        <Password>pAAssw0rd</Password>
        <Name>Michael</Name>
    </User>
    <User>
        <Name>John</Name>
        <Password>pAssw0rd</Password>
    </User>
</Directory>";
            Fact.Runtime.Diff diff = new Fact.Runtime.Diff(
                new Fact.Runtime.Process.Result(XML1, "", 0),
                new Fact.Runtime.Process.Result(XML2, "", 0));
            diff.StdOutIsXML = true;
            Fact.Assert.Basic.IsFalse(diff.Run(0).AreEqual);
        }
    }
}
