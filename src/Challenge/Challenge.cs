﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge
{
    public class Challenge : Fact.Network.Service
    {
        class Lobby
        {
            string _Name = "";
            internal bool _Open = false;
            internal HashSet<Team> _WaitingTeams = new HashSet<Team>();
        }

        class Team
        {
            string _Name = "";
        }

        Dictionary<string, Lobby> _ChallengeInstances = new Dictionary<string, Lobby>();
        Dictionary<string, Team> _Teams = new Dictionary<string, Team>();

        public Challenge() : base("Challenge")
        {
        }

        [Fact.Network.ServiceExposedMethod]
        public bool CreateLobby(Fact.Network.Service.RequestInfo Info, string ChallengeName)
        {
            ChallengeName = ChallengeName.Trim();
            if (ChallengeName.Trim() == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(ChallengeName)) { return false; }
                Lobby lobby = new Lobby();
                _ChallengeInstances.Add(ChallengeName, lobby);
                return true;
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public bool DeleteLobby(Fact.Network.Service.RequestInfo Info, string ChallengeName)
        {
            ChallengeName = ChallengeName.Trim();
            if (ChallengeName.Trim() == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(ChallengeName)) { return false; }
                _ChallengeInstances.Remove(ChallengeName);
                return true;
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public bool OpenLobby(Fact.Network.Service.RequestInfo Info, string ChallengeName)
        {
            ChallengeName = ChallengeName.Trim();
            if (ChallengeName.Trim() == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(ChallengeName))
                {
                    lock (_ChallengeInstances[ChallengeName])
                    {
                        _ChallengeInstances[ChallengeName]._Open = true;
                        return true;
                    }
                }

            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool CloseLobby(Fact.Network.Service.RequestInfo Info, string ChallengeName)
        {
            ChallengeName = ChallengeName.Trim();
            if (ChallengeName.Trim() == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(ChallengeName))
                {
                    lock (_ChallengeInstances[ChallengeName])
                    {
                        _ChallengeInstances[ChallengeName]._Open = false;
                        return true;
                    }
                }

            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool CreateTeam(Fact.Network.Service.RequestInfo Info, string TeamName)
        {
            TeamName = TeamName.Trim();
            if (TeamName == "") { return false; }
            lock (_Teams)
            {
                if (_Teams.ContainsKey(TeamName)) { return false; }
                Team team = new Team();
                _Teams.Add(TeamName, team);
                return true;
            }
        }

        [Fact.Network.ServiceExposedMethod]
        public bool DeleteTeam(Fact.Network.Service.RequestInfo Info, string TeamName)
        {
            TeamName = TeamName.Trim();
            if (TeamName == "") { return false; }
            lock (_Teams)
            {
                if (_Teams.ContainsKey(TeamName)) { return false; }
                _Teams.Remove(TeamName);
            }
            return false;
        }

        [Fact.Network.ServiceExposedMethod]
        public bool RegisterIntoLobby(Fact.Network.Service.RequestInfo Info, string LobbyName, string TeamName)
        {
            TeamName = TeamName.Trim();
            LobbyName = LobbyName.Trim();
            if (TeamName == "") { return false; }
            if (LobbyName == "") { return false; }
            lock (_ChallengeInstances)
            {
                if (_ChallengeInstances.ContainsKey(LobbyName))
                {
                    lock (_ChallengeInstances[LobbyName])
                    {
                        if (!_ChallengeInstances[LobbyName]._Open)
                        {
                            lock (_Teams)
                            {
                                if (_Teams.ContainsKey(TeamName))
                                {
                                    _ChallengeInstances[LobbyName]._WaitingTeams.Add(_Teams[TeamName]);
                                }
                            }
                        }
                    }
                }

            }
            return false;
        }
    }
}
