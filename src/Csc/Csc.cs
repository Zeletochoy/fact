﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Csc
{
    class Csc : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Csc";
            }
        }

        public static int CompileFromSource(string AssemblyName, string[] References, string[] Defines, bool Debug, params string[] Sources)
        {
            if (System.CodeDom.Compiler.CodeDomProvider.IsDefinedLanguage("cs") || System.CodeDom.Compiler.CodeDomProvider.IsDefinedLanguage("csharp"))
            {
                System.CodeDom.Compiler.CodeDomProvider provider;
                if (System.CodeDom.Compiler.CodeDomProvider.IsDefinedLanguage("cs"))
                {
                    provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("cs");
                }
                else
                {
                    provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("csharp");
                }

                System.CodeDom.Compiler.CompilerParameters Parameters = new System.CodeDom.Compiler.CompilerParameters();


				foreach (string d in Defines)
				{
					Parameters.CompilerOptions += " /define:" + d + " ";
				}


                Parameters.GenerateInMemory = false;
                if (AssemblyName.EndsWith(".dll") || AssemblyName.EndsWith(".DLL"))
                {
                    Parameters.GenerateExecutable = false;
                }
                else
                {
                    Parameters.GenerateExecutable = true;
                }
                
                foreach (string _ in References)
                {
                    Parameters.ReferencedAssemblies.Add(_);
                }

				Parameters.OutputAssembly = AssemblyName;

				if (Debug)
				{
					Parameters.IncludeDebugInformation = true;
				}

                System.CodeDom.Compiler.CompilerResults Results = provider.CompileAssemblyFromFile(Parameters, Sources);
                
                if (!Results.Errors.HasErrors)
                {
                    return 0;
                }
                else
                {
                    {
                        bool systemFound = false;
                        foreach (string reference in References)
                        {
                            if (reference == "System.dll") { systemFound = true; break; }
                        }
                        if (!systemFound)
                        {
                            List<string> newReferences = new List<string>(References);
                            newReferences.Add("System.dll");
                            return CompileFromSource(AssemblyName, newReferences.ToArray(), Defines, Debug, Sources);
                        }
                    }

                    {
                        bool systemDataFound = false;
                        foreach (string reference in References)
                        {
                            if (reference == "System.Data.dll") { systemDataFound = true; break; }
                        }
                        if (!systemDataFound)
                        {
                            List<string> newReferences = new List<string>(References);
                            newReferences.Add("System.Data.dll");
							return CompileFromSource(AssemblyName, newReferences.ToArray(), Defines, Debug, Sources);
                        }
                    }

                    foreach (System.CodeDom.Compiler.CompilerError error in Results.Errors)
                    {
                        if (error.IsWarning)
                        {
                            Fact.Log.Warning(error.FileName + " (l " + error.Line + ", c " + error.Column + ") : " + error.ErrorText);
                        }
                        else
                        {
                            Fact.Log.Error(error.FileName + " (l " + error.Line + ", c " + error.Column + ") : " + error.ErrorText);
                        }
                    }
                    return 1;
                }
            }
            else
            {
                Fact.Log.Error("CSharp compiler not found.");
                return 1;
            }

        }

        public override int Run(string[] args)
        {
            bool inputMode = false;
            bool outputMode = false;
            bool refMode = false;
			bool defMode = false;
			bool debug = false;
            List<string> inputFile = new List<string>();
            List<string> referenceFile = new List<string>();
			List<string> def = new List<string>();

            string output = "out.exe";

            for (int n = 0; n < args.Length; n++)
            {
                if (args[n] == "-i" || args[n] == "--Input")
				{ defMode = false; inputMode = true; outputMode = false; refMode = false; continue; }
                if (args[n] == "-r" || args[n] == "--References")
				{ defMode = false; inputMode = false; outputMode = false; refMode = true; continue; }
                if (args[n] == "-o" || args[n] == "--Output")
				{ defMode = false; inputMode = false; outputMode = true; refMode = false; continue; }
				if (args[n] == "-d" || args[n] == "--Define")
				{ defMode = true; inputMode = false; outputMode = false; refMode = false; continue; }
				if (args [n] == "--Debug")
				{ debug = true; }
                if (outputMode) { output = args[n]; }
                if (inputMode) { inputFile.Add(args[n]); }
                if (refMode) { referenceFile.Add(args[n]); }
			    if (defMode) { def.Add(args[n]); }
            }

            for(int n = 0; n < referenceFile.Count; n++)
            {
                if (referenceFile[n].ToLower() == "fact.dll")
                {
                    referenceFile[n] = Fact.Internal.Information.GetLibFact();
                }
            }

			return CompileFromSource(output, referenceFile.ToArray(), def.ToArray(), debug, inputFile.ToArray());
        }
    }
}
