﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Grades
{
    class Grades : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Grades";
            }
        }


        public int GetTestScore(Fact.Test.Result.Result Test)
        {
            if (Test is Fact.Test.Result.Group) { return GetGroupScore(Test as Fact.Test.Result.Group); }
            return Test.Score;
        }

        public int GetGroupScore(Fact.Test.Result.Group Group)
        {
            int Sum = 0;
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                Sum += GetTestScore(result);
            }
            return Sum;
        }

        public void CollectTest(string Namespace, HashSet<string> Testpresent, Fact.Test.Result.Result Test)
        {
            if (Test is Fact.Test.Result.Group) { CollectGroup(Namespace, Testpresent, Test as Fact.Test.Result.Group); return; }
            string id = Namespace == "" ? Test.Text : Namespace + "/" + Test.Text;
            Testpresent.Add(id);
        }

        public void CollectGroup(string Namespace, HashSet<string> Testpresent, Fact.Test.Result.Group Group)
        {
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                string id = Namespace == "" ?  Group.Text : Namespace + "/" + Group.Text;
                CollectTest(id, Testpresent, result);
            }
        }

        public string GenerateCSVForTest(string Namespace, HashSet<string> Testpresent, Fact.Test.Result.Result Test)
        {
            StringBuilder builder = new StringBuilder();
            if (Test is Fact.Test.Result.Group) { return GenerateCSVForGroup(Namespace, Testpresent, Test as Fact.Test.Result.Group); }
            string testcode = Namespace + "/" + Test.Text;
            Testpresent.Add(testcode);
            builder.Append(Namespace);
            builder.Append(",");
            builder.Append(Test.Text);
            builder.Append(",");
            if (Test is Fact.Test.Result.Passed) { builder.Append("PASS"); }
            else if (Test is Fact.Test.Result.Error) { builder.Append("FAIL"); }
            else if (Test is Fact.Test.Result.Warning) { builder.Append("WARNING"); }
            else { builder.Append("OTHER"); }
            builder.Append(",");
            builder.AppendLine(Test.Score.ToString());
            return builder.ToString();
        }

        public string GenerateCSVForGroup(string Namespace, HashSet<string> Testpresent, Fact.Test.Result.Group Group)
        {
            StringBuilder builder = new StringBuilder();
            foreach (Fact.Test.Result.Result result in Group.GetTestResults())
            {
                if (result is Fact.Test.Result.Group)
                {
                    string id = Namespace == "" ? Group.Text : Namespace + "/" + Group.Text;
                    builder.Append(GenerateCSVForGroup(id, Testpresent, result as Fact.Test.Result.Group));
                }
                else
                {
                    string id = Namespace == "" ? Group.Text : Namespace + "/" + Group.Text;
                    builder.Append(GenerateCSVForTest(id, Testpresent, result));
                }
            }
            return builder.ToString();
        }

        public Fact.Test.Result.Result ProjectFromXMLParseNode(Fact.Parser.XML.Node Node)
        {
            if (Node.Type == "group")
            {
                Fact.Test.Result.Group group = new Fact.Test.Result.Group(Node["name"]);
                foreach (Fact.Parser.XML.Node child in Node.Children)
                {
                    group.AddTestResult(ProjectFromXMLParseNode(child));
                }
                return group;
            }
            else if (Node.Type == "eval")
            {
                string name = Node["name"];
                string type = "none";
                if (name == "")
                {
                    foreach (Fact.Parser.XML.Node nodename in Node.Children)
                    {

                        if (nodename.Type == "name")
                        {
                            name = nodename.Text.Trim();
                            if (name == "")
                            {
                                string result = "";
                                foreach (Fact.Parser.XML.Node children in nodename.Children)
                                { result += children.Text + " "; }
                                name = result.Trim();
                            }
                            break;
                        }
                    }
                }
                foreach (Fact.Parser.XML.Node status in Node.Children)
                {
                    
                    if (status.Type == "status")
                    {
                        type = status.Text.ToLower().Trim();
                        if (type == "") 
                        {
                            if (status.Children.Count > 0) { type = status.Children[0].Text.ToLower().Trim(); }
                        }
                        break;
                    }
                }
                int value = 0;
                foreach (Fact.Parser.XML.Node nodevalue in Node.Children)
                {
                    if (nodevalue.Type == "value")
                    {
                        string valuetext = nodevalue.Text.Trim();
                        if (valuetext == "")
                        {
                            if (nodevalue.Children.Count > 0)
                            {
                                valuetext = nodevalue.Children[0].Text.ToLower();
                            }
                        }
                        if (int.TryParse(valuetext, out value))
                        {
                            break;
                        }
                    }
                }
                Fact.Test.Result.Result Result = null;
                switch (type)
                {
                    case "pass": case "passed": Result = new Fact.Test.Result.Passed(name); break;
                    case "fail": case "failled": case "error": Result = new Fact.Test.Result.Error(name); break;
                    case "warn": case "warning": Result = new Fact.Test.Result.Warning(name); break;
                }
                Fact.Log.Debug(name);
                if (Result != null) { Result.Score = value; }

                return Fact.Log.Auto(Result);
            }
            else
            {
                return null;
            }
            return null;
        }

        public Fact.Processing.Project ProjectFromXML(string File)
        {
            try
            {
                if (System.IO.File.Exists(File))
                {
                    Fact.Parser.XML xmlparser = new Fact.Parser.XML();
                    xmlparser.Parse_File(File);
                    Fact.Parser.XML.Node traceNode = xmlparser.Root;
                    if (traceNode == null) { return new Fact.Processing.Project(""); }
                    while (traceNode.Type != "trace" && traceNode.Type != "group")
                    {
                        foreach (Fact.Parser.XML.Node child in traceNode.Children)
                        {
                            if (child.Type == "trace" || child.Type == "group")
                            {
                                traceNode = child;
                                break;
                            }
                        }
                        if (traceNode.Children.Count == 0)
                        { return new Fact.Processing.Project(""); }
                        traceNode = traceNode.Children[0];
                    }

                    {
                        string projectname = "";
                        try { projectname = System.IO.Path.GetFileNameWithoutExtension(File); }
                        catch { }
                        Fact.Processing.Project project = new Fact.Processing.Project(projectname);
                        foreach (Fact.Parser.XML.Node node in traceNode.Children)
                        {
                            Fact.Test.Result.Result result = ProjectFromXMLParseNode(node);
                            if (result != null)
                            {
                                Fact.Log.Debug("Add Test");
                                project.AddTestResult(result);
                            }

                        }
                        return project;
                    }
                }
            }
            catch {  }
            return new Fact.Processing.Project("");
        }

        public override int Run(params string[] args)
        {
            if (args.Length <= 1)
            {
                Fact.Log.Error("Usage: [StudentPackages]+ [OutputDirectory]");
            }

            List<string> files = new List<string>();
            for (int n = 0; n < args.Length - 1; n++)
            {
                if (!args[n].EndsWith(".ff") && !args[n].EndsWith(".xml"))
                {
                    if (System.IO.Directory.Exists(args[n]))
                    {
                        foreach (string file in System.IO.Directory.GetFiles(args[n]))
                        {
                            if (file.EndsWith(".ff") || file.EndsWith(".xml"))
                            {
                                files.Add(file);
                            }
                        }
                    }
                }
                else { files.Add(args[n]); }
            }

            List<Fact.Processing.Project> Projects = new List<Fact.Processing.Project>();
            foreach (string file in files)
            {
                try
                {
                    if (file.EndsWith(".xml"))
                    {
                        Projects.Add(ProjectFromXML(file));
                    }
                    else
                    {
                        Fact.Processing.Project project = new Fact.Processing.Project("");
                        project.Load(file);
                        Projects.Add(project);
                    }
                }
                catch
                {
                    Fact.Log.Error("Impossible to load the package " + file);
                }
            }

            string output = args[args.Length - 1];
            Fact.Tools.RecursiveMakeDirectory(output);


            HashSet<string> TestList = new HashSet<string>();
            foreach (Fact.Processing.Project project in Projects)
            {
                Fact.Log.Verbose("Collecting test in " + project.Name);
                foreach (Fact.Test.Result.Result result in project.GetTestResults())
                {
                    
                    CollectTest("", TestList, result);
                }
            }
            Dictionary<string, int> StudentScore = new Dictionary<string, int>();
            int MaxScore = 0;
            foreach (Fact.Processing.Project project in Projects)
            {
                Fact.Log.Verbose("Generating report for " + project.Name);
                try
                {
                HashSet<string> TestPresent = new HashSet<string>();
                StringBuilder CSV = new StringBuilder();
                foreach (Fact.Test.Result.Result result in project.GetTestResults())
                {
                    Fact.Log.Verbose("Generating report for " + result.Text);

                    CSV.Append(GenerateCSVForTest("", TestPresent, result));
                }
                int Score = 0;
                foreach (Fact.Test.Result.Result result in project.GetTestResults())
                {
                    Score += GetTestScore(result);
                }
                StudentScore.Add(project.Name, Score);
                if (MaxScore < Score) { MaxScore = Score; }
                foreach (string missingtest in TestList.Except(TestPresent))
                {
                    string[] groups = missingtest.Split('/');
                    string groupname = "";
                    for (int n = 0; n < groups.Length - 1; n++)
                    {
                        groupname += groups[n];
                        if (n != groups.Length - 2) { groupname += "/"; }
                    }
                    CSV.Append(groupname);
                    CSV.Append(",");
                    CSV.Append(groups[groups.Length - 1]);
                    CSV.Append(",");
                    CSV.Append("MISSING");
                    CSV.Append(",");
                    CSV.AppendLine("0");
                }
                try
                {
                    System.IO.File.WriteAllText(output + "/" + project.Name + ".csv", CSV.ToString());
                }
                catch 
                {
                    Fact.Log.Error("Impossible to save the file " + output + "/" + project.Name + ".csv");
                }
            }
                catch
                {
                    Fact.Log.Error("Unexpected error while generating CSV for " + project.Name);
                }
            }

            StringBuilder finalresult = new StringBuilder();
            if (MaxScore == 0) { MaxScore = 1; }
            foreach (KeyValuePair<string, int> result in StudentScore)
            {
                finalresult.Append(result.Key);
                finalresult.Append(",");
                finalresult.AppendLine(((((float)result.Value * 100.0f) / (float)MaxScore)).ToString().Replace(',', '.'));
            }
            System.IO.File.WriteAllText(output + "/result.csv", finalresult.ToString());

            return 0;
        }
    }
}
