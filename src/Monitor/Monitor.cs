﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Monitor
{
    public class MonitorPlugin : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Monitor";
            }
        }

         public override int Run(string[] args)
         {
             if (args.Length < 1)
             {
				Fact.Log.Error("Usage: monitor Executable");
                 return 1;
             }

             Fact.Log.Verbose("Starting monitor gui ...");
			 bool parameters = true;
		     
             MonitorGUI GUI = new MonitorGUI();
			 string pargs = "";
			 bool memlimit = false;
			 long memlimitv = -1;
			 bool timeout = false;
			 int timeoutv = -1;
			 bool nogui = false;
			 bool ldpreload = false;
			 string ldpreloadv = ""; 
			 string prog = "";
			 for (int n = 0; n < args.Length; n++)
			 {
				if (parameters) 
				{
					if (memlimit) 
					{ 
						long scaler = 1;
						string value = args [n].ToLower();
						if (value.EndsWith ("ko")) 
						{
							value = value.Substring (0, value.Length - "ko".Length);
							scaler = 1024;
						}
						else if (value.EndsWith ("mo"))
						{
							value = value.Substring (0, value.Length - "mo".Length);
							scaler = 1024 * 1024;
						}
						long.TryParse (value, out memlimitv);

						memlimit = false;
						memlimitv *= scaler;
						continue; 
					}
					if (args [n] == "--memlimit") { memlimit = true; continue; }
					if (timeout) { int.TryParse (args[n], out timeoutv); timeout = false; continue; }
					if (args [n] == "--timeout") { timeout = true; continue; }
					if (ldpreload) { ldpreloadv = args[n]; ldpreload = false; continue; }
					if (args [n] == "--ldpreload") { ldpreload = true; continue; }
				}
				if (prog == "") 
				{
					prog = args [n];
					if (!System.IO.File.Exists(prog))
					{
						Fact.Log.Error("File " + prog + " not found.");
						return 1;
					}
					continue;
				}
				pargs += " " + args [n] + " ";
				parameters = false;
			 }
			if (ldpreloadv != "") 
			{
				Fact.Internal.Information.SetEnvVariable ("LD_PRELOAD", ldpreloadv);
			}
			 Fact.Runtime.Process Process = new Fact.Runtime.Process(prog, "", pargs);
             Fact.Log.Verbose("Starting process ...");
             Fact.Runtime.Process.Result Result = Process.RunAsync(timeoutv);
			 GUI.Attach(Result, Process, memlimitv);
             System.Windows.Forms.Application.Run(GUI);
             Process.Kill();

		     Fact.Log.Info("StdOut=" + Result.StdOut);
			 Fact.Log.Info("StdErr=" + Result.StdErr);
			 Fact.Log.Info("ExitCode=" + Result.ExitCode);
			try
			{
				Fact.Log.Info("MemoryPeak=" + (Result.Statistic.MemoryUsedPeak / 1024).ToString() + "Ko");
			}
			catch {
			}
             return 0;
         }
    }
}
