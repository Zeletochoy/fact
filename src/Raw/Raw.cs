/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;

namespace Raw
{
	public class Raw : Fact.Plugin.Plugin
	{
		public override string Name
		{
			get
			{
				return "Raw";
			}
		}

		void Indent(System.Text.StringBuilder builder, int value)
		{
			for (int n = 0; n < value; n++)
			{
				builder.Append(' ');
			}
		}

		void AddTest(System.Text.StringBuilder builder, Fact.Test.Result.Result result, int indent)
		{
			if (result is Fact.Test.Result.Group)
			{
				Indent(builder, indent); builder.Append("group: ");
				builder.Append(result.Text); builder.AppendLine();
				foreach (Fact.Test.Result.Result subresult in ((Fact.Test.Result.Group)result).GetTestResults())
				{
					AddTest (builder, subresult, indent + 2);
				}
				return;
			}

			Indent(builder, indent); builder.Append("test: "); builder.Append(result.Text);
			builder.Append("    ");
			if (result is Fact.Test.Result.Passed)
			{
				builder.Append("   [PASS]");
			}
			else if (result is Fact.Test.Result.Error)
			{
				builder.Append("   [FAIL]");
			}
			builder.AppendLine();
			Indent(builder, indent); builder.Append("  ");  builder.Append(result.Info);
			builder.AppendLine();
		}

		public override int Run(string[] args)
		{
			if (args.Length != 2) { Fact.Log.Error("Usage: Raw package. output"); }
			if (!System.IO.File.Exists(args[0]))
			{
				Fact.Log.Error("File not found: " + args[0]);
				return 1;
			}
			Fact.Processing.Project Project = new Fact.Processing.Project("");
			Project.Load(args[0]);

			System.Text.StringBuilder builder = new System.Text.StringBuilder();
			foreach (Fact.Test.Result.Result result in Project.GetTestResults())
			{
				AddTest(builder, result, 0);
			}
			foreach (Fact.Processing.File file in Project)
			{
				bool init = false;
				foreach (Fact.Test.Result.Result result in file.GetTestResults()) 
				{
					if (!init) 
					{
						builder.Append("file " + file.Name + ":");
						builder.AppendLine();
						init = true;
					}
					AddTest(builder, result, 2);
				}
			}
			System.IO.File.WriteAllText(args[1], builder.ToString());
			return 0;
		}
	}
}

