﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AST
{
    public class AST : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "AST";
            }
        }

        public override int Run(string[] args)
        {
            if (args.Length != 1) { Fact.Log.Error("Usage: AST package"); }
            if (!System.IO.File.Exists(args[0]))
            {
                Fact.Log.Error("File not found: " + args[0]);
                return 1;
            }
            Fact.Processing.Project Project = new Fact.Processing.Project("");
            Project.Load(args[0]);
            foreach (Fact.Processing.File file in Project)
            {
                Fact.Log.Info("<file name=\"" + file.Name + "\">");
                foreach (Fact.Code.CodeElement element in file.GetCodeElements())
                {
                    Fact.Log.Info(element.ToString());
                }
                Fact.Log.Info("</file>");
            }
            return 0;
        }
    }
}
