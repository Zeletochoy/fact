﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactCXX.Parser
{
    public class Function
    {
        public static void Mark_Function_Declaration(Fact.Processing.File File)
        {
            File.Rebuild();
            List<Fact.Character.MetaToken> Tokens = File.GetAsToken();
            for (int iter = 0; iter < Tokens.Count; iter++)
            {
                int n = iter;
                if (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment)
                    continue;
                if (Tokens[n].TokenType == Fact.Character.MetaChar.BasicTokenType.Type)
                {
                    int StartToken = n;
                    n++;
                    for (; n < Tokens.Count &&
                        Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                        Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                        (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment) ||
                        Tokens[n] == "*";
                        n++) ;

                    if (n < Tokens.Count)
                    {
                        string name = Tokens[n].ToString();
                        name = name.Trim();
                        if (name.Length <= 0) { continue; }
                        if (!char.IsLetter(name[0]) && name[0] != '_')
                        { continue; }
                        n++;
                        for (; n < Tokens.Count &&
                            Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                            Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                            (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment);
                            n++) ;


                        if (n < Tokens.Count && Tokens[n] == "(")
                        {
                            n++;
                            int count = 1;
                            for (; n < Tokens.Count && count > 0; n++)
                            {
                                if (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment)
                                    continue;
                                if (Tokens[n] == "(") { count++; }
                                if (Tokens[n] == ")") { count--; }
                            }

                            for (; n < Tokens.Count &&
                                Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                                Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                                (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment);
                                n++) { };

                            if (n < Tokens.Count && Tokens[n] == ";")
                            {
                                Fact.Code.Function Function = new Fact.Code.Function();
                                Function.FromLine = (int)Tokens[StartToken][0].Line;
                                Function.ToLine = (int)Tokens[n][0].Line;
                                Function.FromColumn = (int)Tokens[StartToken][0].Column;
                                Function.ToColumn = (int)Tokens[n][0].Column;
                                Function.FunctionName = name;
                                File.AddCodeElement(Function);
                                iter = n;
                            }
                        }
                    }
                }
            }
        }

        public static void Mark_Function(Fact.Processing.File File)
        {
            File.Rebuild();
            List<Fact.Character.MetaToken> Tokens = File.GetAsToken();
            for (int iter = 0; iter < Tokens.Count; iter++)
            {
                int n = iter;
                if (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment)
                    continue;
                if (Tokens[n].TokenType == Fact.Character.MetaChar.BasicTokenType.Type)
                {
                    int StartToken = n;
                    n++;
                    for (; n < Tokens.Count &&
                        Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                        Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                        (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment) ||
                        Tokens[n] == "*";
                        n++) ;

                    if (n < Tokens.Count)
                    {
                        string name = Tokens[n].ToString();
                        name = name.Trim();
                        if (name.Length <= 0) { continue; }
                        if (!char.IsLetter(name[0]) && name[0] != '_')
                        { continue; }
                        n++;
                        for (; n < Tokens.Count &&
                            Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                            Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                            (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment);
                            n++) ;


                        if (n < Tokens.Count && Tokens[n] == "(")
                        {
                            n++;
                            int count = 1;
                            for (; n < Tokens.Count && count > 0; n++)
                            {
                                if (Tokens[n] == "(") { count++; }
                                if (Tokens[n] == ")") { count--; }
                            }

                            for (; n < Tokens.Count &&
                                Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Separator ||
                                Tokens[n].SeparatorType == Fact.Character.MetaChar.BasicSeparatorType.Implicit ||
                                (Tokens[n].Length > 0 && Tokens[n][0].Type == Fact.Character.MetaChar.BasicCharType.Comment);
                                n++) { };

                            if (n < Tokens.Count && Tokens[n] == "{")
                            {
                                int StartBody = n;
                                count = 1;
                                Fact.Code.Scope Body = Scope.ParseScope(ref n, Tokens);

                                // Go back to the end of scope
                                n--;
                                if (n < Tokens.Count)
                                {
                                    Fact.Code.Function Function = new Fact.Code.Function();
                                    Function.FromLine = (int)Tokens[StartToken][0].Line;
                                    Function.ToLine = (int)Tokens[n][0].Line;
                                    Function.FromColumn = (int)Tokens[StartToken][0].Column;
                                    Function.ToColumn = (int)Tokens[n][0].Column;
                                    Function.FunctionName = name;
                                    File.AddCodeElement(Function);

                                    Function.FunctionBody = Body;
                                    iter = n;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
