﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactCXX
{
    public static class Analyzer
    {
        public static void Process(Fact.Processing.Project Project)
        {
            List<Fact.Processing.File.FileType> Ftype = new List<Fact.Processing.File.FileType>();
            Ftype.Add(Fact.Processing.File.FileType.CPPHeader);
            Ftype.Add(Fact.Processing.File.FileType.CPPSource);

            Fact.Processing.Chain LexerChain = new Fact.Processing.Chain(Project);
            LexerChain.Add((Fact.Processing.Step.Step_On_Text)Lexer.Basic.String.Mark_String_And_Comment);
            LexerChain.Add((Fact.Processing.Step.Step_On_Text)Lexer.Basic.Preprocessor.Mark_Preporcessor);
            LexerChain.Add((Fact.Processing.Step.Step_On_Text)Lexer.Basic.Separator.Mark_Separators);
            LexerChain.Add((Fact.Processing.Step.Step_On_Token)Lexer.Basic.Keyword.Mark_Keywords);
            
            LexerChain.Process(Ftype);
           /*
            Fact.Processing.Chain ParserChain = new Fact.Processing.Chain(Project);
            ParserChain.Add((Fact.Processing.Step.Step_On_File)Parser.Function.Mark_Function_Declaration);
            ParserChain.Add((Fact.Processing.Step.Step_On_File)Parser.Function.Mark_Function);
            ParserChain.Process(Ftype);
             * */
        }
    }
}
