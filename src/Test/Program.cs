﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test
{
    class Program
    {
        
        static void CreateProject(Fact.Processing.Project Project, string path)
        {
            foreach (string _ in System.IO.Directory.GetDirectories(path))
            { CreateProject(Project, _); }
            foreach (string _ in System.IO.Directory.GetFiles(path))
            {
                if (_.EndsWith(".c")) { Project.AddFile(new Fact.Processing.File(System.IO.File.ReadAllLines(_), System.IO.Path.GetFileName(_))); }
            }
        }
        static void Main(string[] args)
        {
            //Fact.Processing.File file = new Fact.Processing.File(System.IO.File.ReadAllLines(@"C:\Users\Raphaël\Desktop\Fact\fact\test\FactC\ioccc\1985\sicherman.c"), "main.cc");
            Fact.Processing.Project Project = new Fact.Processing.Project("MyProject");

            //Project.AddFile(file);
            CreateProject(Project, @"C:\Users\Raphaël\Desktop\Fact\fact\test\FactC\ioccc\");
            FactC.Analyzer.Process(Project);
            Fact.Processing.Chain Chain = new Fact.Processing.Chain(Project);
            Chain.Add(Fact.Processing.Prebuild.Lexer.Debug.DebugColor());
            
            Chain.Process();
        }
    }
}
