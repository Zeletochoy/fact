﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    class Elf
    {
        unsafe class Method_Elf : Method
        {
            bool _ThisCall = false;
            IntPtr _MethodPtr = new IntPtr(0);
            internal Method_Elf(Assembly_Elf.Symbol Symbol, Assembly_Elf Parent)
            {
                _Name = Symbol._Name;
                _ParentAssembly = Parent;
            }

            protected override object _Invoke(object This, params object[] Parameters)
            {
                if (_MethodPtr.ToPointer() == null)
                {
                    Assembly assembly = this.ParentAssembly;
                    if (assembly is Assembly_Elf)
                    {
                        if ((assembly as Assembly_Elf)._AssemblyPtr.ToPointer() == null)
                        {
                            if ((assembly as Assembly_Elf).File != "")
                            {
                                (assembly as Assembly_Elf)._AssemblyPtr = Internal.Os.Generic.OpenLibrary((assembly as Assembly_Elf).File);
                            }
                            else if ((assembly as Assembly_Elf).Data != null)
                            {
                                string emptyfolder = Fact.Tools.CreateTempDirectory();
                                System.IO.File.WriteAllBytes(emptyfolder + "/libloaded.so", assembly.Data);
                                (assembly as Assembly_Elf)._AssemblyPtr = Internal.Os.Generic.OpenLibrary(emptyfolder + "/libloaded.so");
                            }
                        }
                    }
                    IntPtr asmptr = (assembly as Assembly_Elf)._AssemblyPtr;
                    if (asmptr.ToPointer() == null)
                    {
                        _MethodPtr = Internal.Os.Generic.GetMethod(asmptr, _Name);
                    }
                }
                if (!_ThisCall)
                {
                    return Tools.NativeInvoke<int>(_MethodPtr, Parameters);
                }
                return null;
            }
        }
        class Assembly_Elf : Assembly
        {
            internal IntPtr _AssemblyPtr = new IntPtr(0);
            internal bool _LittleEndian = true;
            internal bool _64BitsImage = false;
            internal List<Symbol> _Symbols = new List<Symbol>();
            internal List<Symbol> _FuntionSymbols = new List<Symbol>();

            internal List<Section> _Sections = new List<Section>();

            internal List<string> _NeededDynamicDependencis = new List<string>();

            internal class Symbol
            {
                internal ulong __st_name = 0;
                internal ulong __st_value = 0;
                internal ulong __st_size = 0;
                internal ulong __st_info = 0;
                internal ulong __st_info_bind = 0;
                internal ulong __st_info_type = 0;
                internal ulong __st_other = 0;
                internal ulong __st_shndx = 0;

                internal string _Name = "";

                public override string ToString()
                {
                    return _Name;
                }
            }
            internal class Section
            {
                internal bool __StringTable = false;
                internal ulong __sh_name = 0;
                internal ulong __sh_type = 0;
                internal ulong __sh_flags = 0;
                internal ulong __sh_addr = 0;
                internal ulong __sh_offset = 0;
                internal ulong __sh_size = 0;
                internal ulong __sh_link = 0;
                internal ulong __sh_info = 0;
                internal ulong __sh_addralign = 0;
                internal ulong __sh_entsize = 0;

                internal string _Name = "";

                public Section()
                { }
                public override string ToString()
                {
                    return _Name;
                }
            }

            internal void _AddMethod(Symbol Symbol)
            {
                if (!_Methods.ContainsKey(Symbol._Name))
                {
                    Method_Elf method = new Method_Elf(Symbol, this);
                    _Methods.Add(Symbol._Name, method);
                }
            }

            internal void _AddDependency(Assembly Assembly)
            {
                _Depends.Add(Assembly.Name, Assembly);
            }

            internal void _AddImportedMethod(Symbol Symbol)
            {
                Assembly parent = null;
                foreach (Assembly assembly in _Depends.Values)
                {
                    foreach (Method method in assembly.Methods)
                    {
                        if (method.Name == Symbol._Name)
                        {
                            _ImportedMethods.Add(method);
                        }
                    }
                }
            end: ;
                {
                    Method_Elf method = new Method_Elf(Symbol, null);
                    _ImportedMethods.Add( method);
                }
            }

            ~Assembly_Elf()
            {
                Internal.Os.Generic.CloseLibrary(_AssemblyPtr);
            }
        }

        internal static Assembly ReadElf(byte[] AssemblyData)
        {
            Assembly_Elf elf = new Assembly_Elf();
            return ReadElf(AssemblyData, elf);
        }

        static ulong ReadElf_Word(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            if (Elf._LittleEndian)
            {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100;
            }
            else
            {
                return (ulong)AssemblyData[Offset] * 0x100 +
                       (ulong)AssemblyData[Offset + 1];
            }
        }

        static ulong ReadElf_DWord(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            if (Elf._LittleEndian)
            {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000 +
                       (ulong)AssemblyData[Offset + 3] * 0x1000000;
            }
            else
            {
                return (ulong)AssemblyData[Offset + 3] +
                       (ulong)AssemblyData[Offset + 2] * 0x100 +
                       (ulong)AssemblyData[Offset + 1] * 0x10000 +
                       (ulong)AssemblyData[Offset] * 0x1000000;
            }
        }

        static ulong ReadElf_QWord(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            if (Elf._LittleEndian)
            {
                return (ulong)AssemblyData[Offset] +
                       (ulong)AssemblyData[Offset + 1] * 0x100 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000 +
                       (ulong)AssemblyData[Offset + 3] * 0x1000000 +
                       (ulong)AssemblyData[Offset + 4] * 0x100000000 +
                       (ulong)AssemblyData[Offset + 5] * 0x10000000000 +
                       (ulong)AssemblyData[Offset + 6] * 0x1000000000000 +
                       (ulong)AssemblyData[Offset + 7] * 0x100000000000000;
            }
            else
            {
                return (ulong)AssemblyData[Offset + 7] +
                       (ulong)AssemblyData[Offset + 6] * 0x100 +
                       (ulong)AssemblyData[Offset + 5] * 0x10000 +
                       (ulong)AssemblyData[Offset + 4] * 0x1000000 +
                       (ulong)AssemblyData[Offset + 3] * 0x100000000 +
                       (ulong)AssemblyData[Offset + 2] * 0x10000000000 +
                       (ulong)AssemblyData[Offset + 1] * 0x1000000000000 +
                       (ulong)AssemblyData[Offset] * 0x100000000000000;
            }
        }

        static Assembly ReadElf(byte[] AssemblyData, Assembly_Elf Elf)
        {
            if (AssemblyData.Length < 0x32) { return null; }
            int bits = 0;
            bool LittleEndian = true;
            switch (AssemblyData[4])
            {
                case 1: Elf._64BitsImage = false; break;
                case 2: Elf._64BitsImage = true; break;
                default: return null;
            }
            if (AssemblyData.Length < 0x3E) { return null; }
            switch (AssemblyData[5])
            {
                case 1: Elf._LittleEndian = true; break;
                case 2: Elf._LittleEndian = false; break;
                default: return null;
            }

            ulong SectionTablePtr = 0;
            ulong SectionTableEntrySize = 0;
            ulong SectionTableEntryCount = 0;
            ulong SectionStringIndex = 0;

            if (Elf._64BitsImage) { SectionTablePtr = ReadElf_QWord(AssemblyData, 0x28, Elf); }
            else { SectionTablePtr = ReadElf_DWord(AssemblyData, 0x20, Elf); }
            if (Elf._64BitsImage) { SectionTableEntrySize = ReadElf_Word(AssemblyData, 0x3A, Elf); }
            else { SectionTableEntrySize = ReadElf_Word(AssemblyData, 0x2E, Elf); }
            if (Elf._64BitsImage) { SectionTableEntryCount = ReadElf_Word(AssemblyData, 0x3C, Elf); }
            else { SectionTableEntryCount = ReadElf_Word(AssemblyData, 0x30, Elf); }
            if (Elf._64BitsImage) { SectionStringIndex = ReadElf_Word(AssemblyData, 0x3E, Elf); }
            else { SectionStringIndex = ReadElf_Word(AssemblyData, 0x32, Elf); }
            ReadElf_Sections(AssemblyData, (int)SectionTablePtr, SectionTableEntrySize, SectionTableEntryCount, SectionStringIndex, Elf);
            ReadElf_Symbols(AssemblyData, Elf);
            ReadELF_Depends(AssemblyData, Elf);
            ReadElf_SolveDependencies(Elf);
            return Elf;
        }

        static string ReadElf_ASCIIZ(byte[] AssemblyData, ulong Offset)
        {
            string value = "";
            for (int n = (int)Offset; n < AssemblyData.Length; n++)
            {
                if (AssemblyData[n] == 0) { return value; }
                value += (char)(AssemblyData[n]);
            }
            return value;
        }

        static void ReadElf_Sections(byte[] AssemblyData, int Offset, ulong EntrySize, ulong EntryCount, ulong StringTableIndex, Assembly_Elf Elf)
        {
            for (ulong n = 0; n < EntryCount; n++)
            {
                ReadElf_Section(AssemblyData, Offset, Elf);
                Offset += (int)EntrySize;
            }
            if (Elf._Sections.Count > (int)StringTableIndex)
            {
                Assembly_Elf.Section StringSection = Elf._Sections[(int)StringTableIndex];
                for (int n = 0; n < Elf._Sections.Count; n++)
                {
                    if (n == 0 && Elf._Sections[0].__sh_name == 0) { continue; }
                    ulong offset = (StringSection.__sh_offset) + (Elf._Sections[n].__sh_name);
                    Elf._Sections[n]._Name = ReadElf_ASCIIZ(AssemblyData, offset);
                }
            }
        }

        static void ReadElf_Section(byte[] AssemblyData, int Offset, Assembly_Elf Elf)
        {
            Assembly_Elf.Section Section = new Assembly_Elf.Section();
            if (Elf._64BitsImage)
            {
                Section.__sh_name = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_type = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_flags = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_addr = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_offset = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_size = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_link = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_info = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_addralign = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
                Section.__sh_entsize = ReadElf_QWord(AssemblyData, Offset, Elf); Offset += 8;
            }
            else
            {
                Section.__sh_name = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_type = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_flags = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_addr = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_offset = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_size = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_link = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_info = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_addralign = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
                Section.__sh_entsize = ReadElf_DWord(AssemblyData, Offset, Elf); Offset += 4;
            }
            Elf._Sections.Add(Section);
        }

        class DepTag { internal ulong Tag; internal ulong Value; }
        static void ReadELF_Depends(byte[] AssemblyData, Assembly_Elf Elf)
        {
            Assembly_Elf.Section Dynamic = null;
            Assembly_Elf.Section String = null;

            for (int n = 0; n < Elf._Sections.Count; n++)
            {
                if (Elf._Sections[n]._Name == ".dynamic")
                {
                    Dynamic = Elf._Sections[n];
                    break;
                }
            }

            if (Dynamic == null) { return; }
            ulong Offset = Dynamic.__sh_offset;
            ulong End = Offset + Dynamic.__sh_size;
            List<DepTag> Entries = new List<DepTag>();
            while (Offset < End)
            {
                ulong tag = 0;
                ulong value = 0;

                if (Elf._64BitsImage)
                {
                    tag = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    value = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                }
                else
                {
                    tag = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    value = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                }
                if (tag == 0) { continue; }
                DepTag Tag = new DepTag();
                Tag.Tag = tag;
                Tag.Value = value;
                Entries.Add(Tag);
            }
            
            // Find the string section
            for (int n = 0; n < Entries.Count; n++)
            {
                if (Entries[n].Tag == 5) 
                {
                    for (int x = 0; x < Elf._Sections.Count; x++)
                    {
                        if (Elf._Sections[x].__sh_offset == Entries[n].Value) { String = Elf._Sections[x]; break; }
                    }
                }
            }

            // Find the dependencies
            if (String != null)
            {
                for (int n = 0; n < Entries.Count; n++)
                {
                    if (Entries[n].Tag == 1)
                    {
                        Elf._NeededDynamicDependencis.Add(
                            ReadElf_ASCIIZ(AssemblyData, String.__sh_offset + Entries[n].Value)
                            );
                    }
                }
            }

            // Solve the dependencies
            for (int n = 0; n < Elf._NeededDynamicDependencis.Count; n++)
            {
                string executable = Internal.Information.Where(Elf._NeededDynamicDependencis[n]);
                if (System.IO.File.Exists(executable))
                {
                    Elf._AddDependency(Assembly.LoadFromFile(executable));
                }
                else if (System.IO.File.Exists(Elf._NeededDynamicDependencis[n]))
                {
                    Elf._AddDependency(Assembly.LoadFromFile(Elf._NeededDynamicDependencis[n]));
                }
            }
        }

        static void ReadElf_Symbols(byte[] AssemblyData, Assembly_Elf Elf)
        {
            // Find the dynamic sym section
            Assembly_Elf.Section SymTable = null;
            Assembly_Elf.Section DynSym = null;

            for (int n = 0; n < Elf._Sections.Count; n++)
            {
                if (Elf._Sections[n]._Name == ".dynsym")
                {
                    DynSym = Elf._Sections[n];
                    continue;
                }
                if (Elf._Sections[n]._Name == ".symtab")
                {
                    SymTable = Elf._Sections[n];
                    continue;
                }
            }
            if (SymTable != null)
            {
                ReadElf_SymbolsTable(AssemblyData, SymTable.__sh_offset, SymTable.__sh_size, SymTable, Elf);
            }
            if (DynSym != null)
            {
                ReadElf_SymbolsTable(AssemblyData, DynSym.__sh_offset, DynSym.__sh_size, DynSym, Elf);
            }
        }

        static void ReadElf_SymbolsTable(byte[] AssemblyData, ulong Offset, ulong Size, Assembly_Elf.Section Parent, Assembly_Elf Elf)
        {
            // Find the symbol string table
            Assembly_Elf.Section StringTable = null;

            for (int n = 0; n < Elf._Sections.Count; n++)
            {
                if (Parent._Name == ".symtab" && Elf._Sections[n]._Name == ".strtab")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
                if (Parent._Name == ".dynsym" && Elf._Sections[n]._Name == ".dynstr")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
                /* As specified in the arm documentation */
                if (Parent._Name == ".dynsym" && Elf._Sections[n]._Name == ".dynstrtab")
                {
                    StringTable = Elf._Sections[n];
                    break;
                }
            }
            ulong end = Offset + Size;
            while (Offset < end)
            {
                Assembly_Elf.Symbol Symbol = new Assembly_Elf.Symbol();
                if (Elf._64BitsImage)
                {
                    Symbol.__st_name = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_info = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_other = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_shndx = ReadElf_Word(AssemblyData, (int)Offset, Elf); Offset += 2;
                    Symbol.__st_value = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;
                    Symbol.__st_size = ReadElf_QWord(AssemblyData, (int)Offset, Elf); Offset += 8;

                    Symbol.__st_info_bind = Symbol.__st_info >> 4;
                    Symbol.__st_info_type = Symbol.__st_info & 0xF;

                }
                else
                {
                    Symbol.__st_name = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_value = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_size = ReadElf_DWord(AssemblyData, (int)Offset, Elf); Offset += 4;
                    Symbol.__st_info = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_other = AssemblyData[(int)Offset]; Offset += 1;
                    Symbol.__st_shndx = ReadElf_Word(AssemblyData, (int)Offset, Elf); Offset += 2;

                    Symbol.__st_info_bind = Symbol.__st_info >> 4;
                    Symbol.__st_info_type = Symbol.__st_info & 0xF;
                }
                if (Symbol.__st_name != 0 && StringTable != null)
                {
                    ulong nameoffset = StringTable.__sh_offset + Symbol.__st_name;
                    Symbol._Name = ReadElf_ASCIIZ(AssemblyData, nameoffset);
                }
                Elf._Symbols.Add(Symbol);
            }
        }

        static void ReadElf_SolveDependencies(Assembly_Elf Elf)
        {
            for (int n = 0; n < Elf._Symbols.Count; n++)
            {
                if (Elf._Symbols[n]._Name != "" && Elf._Symbols[n].__st_info_type == 0x02)
                {
                    if (Elf._Symbols[n].__st_value == 0 && Elf._Symbols[n].__st_size == 0)
                    {
                        // Undefined symbols
                        Elf._AddImportedMethod(Elf._Symbols[n]);
                    }
                    else
                    {
                        Elf._AddMethod(Elf._Symbols[n]);
                    }
                }
            }
        }
    }
}
