﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Assembly
    {
        public enum LoadingStatus
        {
            NotLoaded,
            LoadedForReflection,
            Loaded
        }

        protected LoadingStatus _LoadingStatus = LoadingStatus.NotLoaded;
        public LoadingStatus CurrentLoadingStatus { get { return _LoadingStatus; } }

        protected Dictionary<string, Type> _types = new Dictionary<string, Type>();
        protected Dictionary<string, Namespace> _Namespaces = new Dictionary<string, Namespace>();
        protected Dictionary<string, Assembly> _Depends = new Dictionary<string, Assembly>();
        protected Dictionary<string, Method> _Methods = new Dictionary<string, Method>();
        protected List<Method> _ImportedMethods = new List<Method>();

        protected string _Name = "";
        public string Name { get { return _Name; } }

        protected string _File = "";
        public string File { get { return _File; } }
        protected byte[] _Data = null;
        public byte[] Data { get { return _Data; } }


        protected virtual void _Load() { }
        internal void __Load() { _Load(); }
        protected virtual void _LoadForReflection() { }
        internal void __LoadForReflection() { _LoadForReflection(); }

        public List<Method> Methods
        {
            get
            {
                if (_LoadingStatus == LoadingStatus.NotLoaded)
                { _LoadForReflection(); }
                return new List<Method>(_Methods.Values);
            }
        }
        public List<Method> ImportedMethods
        {
            get
            {
                if (_LoadingStatus == LoadingStatus.NotLoaded)
                { _LoadForReflection(); }
                return new List<Method>(_ImportedMethods);
            }
        }

        public static Assembly LoadFromFile(string File)
        {
            byte[] data = System.IO.File.ReadAllBytes(File);
            Assembly Assembly = LoadFromBytes(data);
            if (Assembly != null)
            {
                if (Assembly._Name == "")
                {
                    Assembly._Name = System.IO.Path.GetFileName(File);
                    Assembly._File = File;
                    Assembly._Data = data;
                }
            }
            return Assembly;
        }

        public static Assembly LoadFromFile(Fact.Processing.File File)
        {
            byte[] data = File.GetBytes();
            Assembly Assembly = LoadFromBytes(data);
            if (Assembly != null)
            {
                if (Assembly._Name == "")
                {
                    Assembly._Name = File.Name;
                    Assembly._Data = data;
                }
            }
            return Assembly;
        }

        public static Assembly LoadFromBytes(byte[] AssemblyData)
        {
            System.IO.MemoryStream Stream = new System.IO.MemoryStream(AssemblyData);
            Assembly Assembly = null;
            switch (Internal.File.GetExecutableType(Stream))
            {
                case Processing.File.ExecutableFormat.ExecutableAndLinkableFormat:
                    Assembly = Elf.ReadElf(AssemblyData);
                    break;
                case Processing.File.ExecutableFormat.PortableExecutable:
                    Assembly = Pe.ReadPE(AssemblyData);
                    break;
            }
            if (Assembly != null) { Assembly._Data = AssemblyData; }
            return Assembly;
        }

        public override string ToString()
        {
            return _Name;
        }
    }
}
