﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Method
    {
        protected Assembly _ParentAssembly = null;
        public Assembly ParentAssembly { get { return _ParentAssembly; } }
        protected string _Name = "";
        public string Name { get { return _Name; } }
        public object Invoke(object This, params object[] Parameters)
        {
            if (_ParentAssembly != null)
            {
                if (_ParentAssembly.CurrentLoadingStatus != Assembly.LoadingStatus.Loaded)
                {
                    _ParentAssembly.__Load();
                }
            }
            return _Invoke(This, Parameters);
        }
        protected virtual object _Invoke(object This, params object[] Parameters) { throw new NotImplementedException(); }

        protected Dictionary<string, object> _attributes = new Dictionary<string, object>();

        public override string ToString()
        {
            return _Name;
        }
    }
}
