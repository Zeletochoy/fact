﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public static unsafe class Tools
    {
        // Unfortunatelly calling the delegate with VA_ARGS does not work as we excpect
        // default calling conv will push a pointer to the array onto the stack
        // internal delegate void* __rawfunction(params void*[] Parameters);

        internal delegate void* __rawfunction0();
        internal delegate void* __rawfunction1(void* P1);
        internal delegate void* __rawfunction2(void* P1, void* P2);
        internal delegate void* __rawfunction3(void* P1, void* P2, void* P3);
        internal delegate void* __rawfunction4(void* P1, void* P2, void* P3, void* P4);
        internal delegate void* __rawfunction5(void* P1, void* P2, void* P3, void* P4, void* P5);

        static unsafe T _NativeInvoke_Specialized<T>(void* Function) where T : class
        {
            return System.Runtime.InteropServices.Marshal.GetDelegateForFunctionPointer(
                new IntPtr(Function),
                typeof(T)) as T;
        }

        static unsafe void* _NativeInvoke(void* Function, void*[] Parameters)
        {
            switch (Parameters.Length)
            {
                case 0: return _NativeInvoke_Specialized<__rawfunction0>(Function)();
                case 1: return _NativeInvoke_Specialized<__rawfunction1>(Function)(Parameters[0]);
                case 2: return _NativeInvoke_Specialized<__rawfunction2>(Function)(Parameters[0], Parameters[1]);
                case 3: return _NativeInvoke_Specialized<__rawfunction3>(Function)(Parameters[0], Parameters[1], Parameters[2]); 
                case 4: return _NativeInvoke_Specialized<__rawfunction4>(Function)(Parameters[0], Parameters[1], Parameters[2], Parameters[3]);
                case 5: return _NativeInvoke_Specialized<__rawfunction5>(Function)(Parameters[0], Parameters[1], Parameters[2], Parameters[3], Parameters[4]);            
            }
            throw new Exception("To many parameters");
        }


        static unsafe void* ObjectToPointer(object Parameter)
        {
            if (Parameter == null) { return null; }
            if (Parameter is Int16) { return (void*)((Int16)Parameter); }
            if (Parameter is Int32) { return (void*)((Int32)Parameter); }
            if (Parameter is Int64) { return (void*)((Int64)Parameter); }
            if (Parameter is UInt16) { return (void*)((UInt16)Parameter); }
            if (Parameter is UInt32) { return (void*)((UInt32)Parameter); }
            if (Parameter is UInt64) { return (void*)((UInt64)Parameter); }

            throw new Exception("Do not know how to unmanage type: " + Parameter.GetType().Name);

        }

        static unsafe bool IsType<A, B>()
        {
            return typeof(A) == typeof(B);
        }

        static unsafe T Cast<T>(object Input)
        {
            return (T)Input;
        }

        static unsafe T PointerToObject<T>(void* Parameter)
        {
            if (Parameter == null) { return Cast<T>(null); }

            if (IsType<T, UInt16>()) { return Cast<T>((UInt16)Parameter); }
            if (IsType<T, UInt32>()) { return Cast<T>((UInt32)Parameter); }
            if (IsType<T, UInt64>()) { return Cast<T>((UInt64)Parameter); }

            if (IsType<T, Int16>()) { return Cast<T>((Int16)Parameter); }
            if (IsType<T, Int32>()) { return Cast<T>((Int32)Parameter); }
            if (IsType<T, Int64>()) { return Cast<T>((Int64)Parameter); }

            if (IsType<T, String>()) { return Cast<T>(new String((char*)Parameter)); }

            throw new Exception("Do not know how to manage type from pointer: " + typeof(T).Name);
        }

        public static unsafe T NativeInvoke<T>(IntPtr Function, params object[] Parameters)
        {
            void*[] pointers = new void*[Parameters.Length];
            for (int n = 0; n < Parameters.Length; n++)
            {
                pointers[n] = ObjectToPointer(Parameters[n]);
            }
            return PointerToObject<T>(_NativeInvoke(Function.ToPointer(), pointers));
        }


    }
}
