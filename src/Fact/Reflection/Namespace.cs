﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Reflection
{
    public class Namespace
    {
        protected Assembly _ParentAssembly = null;
        public Assembly ParentAssembly { get { return _ParentAssembly; } }
        protected string _Name = "";
        public string Name { get { return _Name; } }

        protected Dictionary<string, Type> _types = new Dictionary<string, Type>();
        protected Dictionary<string, Method> _methods = new Dictionary<string, Method>();
        protected Dictionary<string, Namespace> _Namespaces = new Dictionary<string, Namespace>();

        public List<Method> Methods { get { return new List<Method>(_methods.Values); } }

    }
}
