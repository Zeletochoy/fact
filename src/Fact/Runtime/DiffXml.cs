﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Runtime
{
    class DiffXml
    {
        bool _Ordered = false;
        public bool Ordered { get { return _Ordered; } set { _Ordered = value; } }
        public bool Xml(string refxmlA, string myxmlB, StringBuilder why)
        {
            Parser.XML xmlParserA = new Fact.Parser.XML(); bool ParseErrorA = false;
            Parser.XML xmlParserB = new Fact.Parser.XML(); bool ParseErrorB = false;
            try
            {
                xmlParserA.Parse(refxmlA);
            }
            catch { ParseErrorA = true; }
            try
            {
                xmlParserB.Parse(myxmlB);
            }
            catch { ParseErrorB = true; }

            if (ParseErrorA || ParseErrorB)
            { return TryNoXml(refxmlA, myxmlB, why); }
            else { return DiffXmlNodes(xmlParserA.Root, xmlParserB.Root, why); }

        }

        public bool TryNoXml(string xmlA, string xmlB, StringBuilder builder)
        {
            if (xmlA.Trim() != xmlB.Trim()) 
            {
                builder.Append("Due to an xml parse error the output a classic diff has been used:");
                builder.Append("  ref: "); builder.Append(xmlA);
                builder.Append("  my: "); builder.Append(xmlB);
                return false;
            }
            return true;
        }

        public string formatNode(Parser.XML.Node node) 
        {
            string strnode = node.ToString(); if (strnode.Length < 80) { return strnode; }
            else
            {
                return "<" + node.Type + " ...> ... </" + node.Type + ">";
            }
        }

        public bool DiffXmlNodes(Parser.XML.Node nodeA, Parser.XML.Node nodeB, StringBuilder why)
        {
            bool same = true;
            if (nodeA == null && nodeB != null)
            {
                why.AppendLine("Expected an empty node");
                why.AppendLine("ref: Empty");
                why.AppendLine("my: " + formatNode(nodeB));
                return false;
            }
            if (nodeB == null && nodeA != null)
            {
                why.AppendLine("Got an empty node");
                why.AppendLine("ref: " + formatNode(nodeA));
                why.AppendLine("my: Empty");
                return false;
            }
            if (nodeA == null && nodeB == null) { return true; }

            if (nodeA.Type.Trim() != nodeB.Type.Trim())
            {
                why.AppendLine("Type missmatching");
                why.AppendLine("ref: <" + nodeA.Type + ">");
                why.AppendLine("my: <" + nodeB.Type + ">");
                same = false;
            }

            if (nodeA.Text.Trim() != nodeB.Text.Trim())
            {
                why.AppendLine("Text missmatching");
                why.AppendLine("ref: " + nodeA.Text);
                why.AppendLine("my: " + nodeB.Text);
                same = false;
            }

            if (same) // if the type are not the same it is useless to match the attributes (it overloads the output)
            {
                HashSet<string> BagA = new HashSet<string>(nodeA.AttributesName);
                HashSet<string> BagB = new HashSet<string>(nodeB.AttributesName);

                // Match the attributes
                foreach (string attr in BagA)
                {
                    if (BagB.Contains(attr)) { BagB.Remove(attr); }
                    if (nodeA[attr] != nodeB[attr])
                    {
                        why.AppendLine("Attribute missmatching");
                        why.AppendLine("ref: <" + nodeA.Type + " " + attr + "='" + nodeA[attr] + "' ...>");
                        why.AppendLine("my: <" + nodeB.Type + " " + attr + "='" + nodeB[attr] + "' ...>");                        
                        same = false;
                    }
                }
                if (BagB.Count > 0)
                {
                    why.AppendLine("Extra attributes");
                    why.AppendLine("ref: All the attributes have been found");
                    why.AppendLine("my: Some extra attribute that are not present in the reference node have been found");
                    foreach (string attr in BagB)
                    {
                        why.AppendLine("    - " + attr);
                    }
                }
            }

            if (nodeA.Children.Count > nodeB.Children.Count)
            {
                why.AppendLine("Missing children");
                why.AppendLine("ref: the node " + nodeA.Type + " has " + nodeA.Children.Count + " children");
                why.AppendLine("my: the node " + nodeB.Type + " has " + nodeB.Children.Count + " children");
                same = false;
            }

            if (nodeA.Children.Count < nodeB.Children.Count)
            {
                why.AppendLine("Extra children");
                why.AppendLine("ref: the node " + nodeA.Type + " has " + nodeA.Children.Count + " children");
                why.AppendLine("my: the node " + nodeB.Type + " has " + nodeB.Children.Count + " children");
                same = false;
            }
            if (_Ordered)
            {
                // Assuming the node are ordered
                int min = nodeA.Children.Count > nodeB.Children.Count ? nodeB.Children.Count : nodeA.Children.Count;
                for (int n = 0; n < min; n++)
                {
                    if (!DiffXmlNodes(nodeA.Children[n], nodeB.Children[n], why)) { same = false; }
                }
            }
            else
            {
                HashSet<Parser.XML.Node> BagA = new HashSet<Fact.Parser.XML.Node>(nodeA.Children);
                HashSet<Parser.XML.Node> NotFoundA = new HashSet<Fact.Parser.XML.Node>();
                HashSet<Parser.XML.Node> BagB = new HashSet<Fact.Parser.XML.Node>(nodeB.Children);
            next:
                foreach (Parser.XML.Node a in BagA)
                {
                    foreach (Parser.XML.Node b in BagB)
                    {
                        StringBuilder temp = new StringBuilder();
                        if (DiffXmlNodes(a, b, temp)) { BagA.Remove(a); BagB.Remove(b); goto next; }
                    }
                    NotFoundA.Add(a);
                    BagA.Remove(a);
                    goto next;
                }
                if (BagA.Count == 0 && BagB.Count == 0) { return same; }

                why.AppendLine("Extra children");
                if (NotFoundA.Count > 0)
                {
                    why.AppendLine("ref: The following children present in the reference and have not been found");
                    foreach (Parser.XML.Node a in NotFoundA)
                    {
                        why.AppendLine("    - " + formatNode(a));
                    }
                    same = false;
                }
                else
                {
                    why.AppendLine("ref: All the children of the node " + nodeA.Type + " has been found");
                }
                if (BagB.Count > 0)
                {
                    why.AppendLine("my: The following children have no equivalent in the reference node");
                    foreach (Parser.XML.Node b in BagB)
                    {
                        why.AppendLine("    - " + formatNode(b));
                    }
                    same = false;
                }
                else
                {
                    why.AppendLine("my: All the children of the node " + nodeA.Type + " has been found");
                }
            }
            return same;
        }
    }
}
