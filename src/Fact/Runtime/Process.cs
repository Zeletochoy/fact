/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Runtime
{
    public class Process : IDisposable
    {

		#region KeepAlive
		internal Library _keepalivelib = null;
		#endregion

        bool _IsTrustedProcess = false;
        public bool IsTrustedProcess { get { return _IsTrustedProcess; } set { _IsTrustedProcess = value; } }

        static List<string> ParseArguments(string Arguments)
        {
            bool isString = false;
            char strChar = '"';
            string arg = "";
            List<string> args = new List<string>();
            for (int n = 0; n < Arguments.Length; n++)
            {
                if (isString)
                {

                    if (Arguments[n] == strChar)
                    {
                        isString = false;
                    }
                    else
                    {
                        arg += Arguments[n];
                    }
                }
                else
                {

                    if (Arguments[n] == '"') { isString = true; strChar = '"'; }
                    else if (Arguments[n] == '\'') { isString = true; strChar = '\''; }
                    else if (Arguments[n] == ' ')
                    {
                        if (arg != "")
                        {
                            args.Add(arg);
                            arg = "";
                        }
                    }
                    else
                    {
                        arg += Arguments[n];
                    }
                }
            }

            if (arg != "")
            {
                args.Add(arg);
                arg = "";
            }

            return args;
        }

        static public Process CreateProcessFromCommand(string Command, int Offset, string WorkingDirectory, Dictionary<string, EmbededProcess> Builtins)
        {
            return CreateProcessFromCommand(Command, Offset, WorkingDirectory, Builtins, new Dictionary<string, string>());
        }
        static public Process CreateProcessFromCommand(string Command, int Offset, string WorkingDirectory, Dictionary<string, EmbededProcess> Builtins, Dictionary<string, string> Variables)
        {
            bool isString = false;
            char strChar = '"';
            string arg = "";
            List<string> args = new List<string>();
            for (int n = Offset; n < Command.Length; n++)
            {
                if (isString)
                {
                    arg += Command[n];
                    if (Command[n] == strChar)
                    {
                        isString = false;
                    }
                }
                else
                {
                    if (Command[n] == '|')
                    {
                        if (arg != "")
                        {
                            arg = arg.Trim();
                            args.Add(arg);
                            arg = "";
                        }
                        Process next = CreateProcessFromCommand(Command, n + 1, WorkingDirectory, Builtins);

                        if (args.Count > 0)
                        {
                            string allargs = "";
                            for (int x = 1; x < args.Count; x++)
                            {
                                string realarg = args[x].Trim();
                                if (realarg.StartsWith("$"))
                                {
                                    if (Variables.ContainsKey(realarg.Substring(1)))
                                    {
                                        realarg = "\"" + Variables[realarg.Substring(1)] + "\"";
                                    }
                                }
                                allargs += realarg + " ";
                            }
                            string program = args[0].Trim();
                            if (WorkingDirectory != "")
                            {
                                Process p;
                                if (Builtins != null && Builtins.ContainsKey(program))
                                    p = new Process(Builtins[program], WorkingDirectory, allargs);
                                else
                                    p = new Process(program, WorkingDirectory, allargs);
                                p.Pipes.Add(next);
                                return p;
                            }
                            else
                            {
                                Process p;
                                if (Builtins != null && Builtins.ContainsKey(program))
                                    p = new Process(Builtins[program], allargs);
                                else
                                    p = new Process(program, allargs);
                                p.Pipes.Add(next);
                                return p;
                            }
                        }
                    }
                    arg += Command[n];
                    if (Command[n] == '"') { isString = true; strChar = '"'; }
                    else if (Command[n] == '\'') { isString = true; strChar = '\''; }
                    else if (Command[n] == ' ')
                    {
                        arg = arg.Trim();
                        if (arg != "")
                        {
                            args.Add(arg);
                            arg = "";
                        }
                    }
                }
            }

            if (arg != "")
            {
                arg = arg.Trim();
                args.Add(arg);
                arg = "";
            }

            if (args.Count > 0)
            {
                string allargs = "";
                for (int x = 1; x < args.Count; x++)
                {
                    string realarg = args[x].Trim();
                    if (realarg.StartsWith("$"))
                    {
                        Fact.Log.Debug("Variable " + realarg.Substring(1));
                        if (Variables.ContainsKey(realarg.Substring(1)))
                        {
                            realarg = "\"" + Variables[realarg.Substring(1)] + "\"";
                        }
                    }
                    allargs += realarg + " ";
                }
                string program = args[0].Trim();

                if (WorkingDirectory != "")
                {
                    if (Builtins != null && Builtins.ContainsKey(program))
                        return new Process(Builtins[program], WorkingDirectory, allargs);
                    else
                        return new Process(program, WorkingDirectory, allargs);
                }
                else
                {
                    if (Builtins != null && Builtins.ContainsKey(program))
                        return new Process(Builtins[program], allargs);
                    else
                        return new Process(program, allargs);
                }
            }
            return null;
        }

		public static void Kill(int ID)
		{
			try
			{
				if (System.IO.Directory.Exists("/compat/linux/proc"))
				{
                    Execute(Internal.Information.Where("kill"), ID.ToString());
				}
			}
			catch
			{
			}

			try
			{
				if (System.IO.Directory.Exists("/compat/linux/proc"))
				{
                    Execute(Internal.Information.Where("kill"), "-9 " + ID.ToString());
				}
			}
			catch
			{
			}

			try
			{
                System.Diagnostics.Process.GetProcessById(ID).Kill();
			}
			catch
			{
				try
				{
					if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
					{
                        Execute(Internal.Information.Where("kill"), ID.ToString());
					}
				}
				catch
				{

				}
			}
		}

        public static string GetProcessName(int ID)
        {
            int n = 10;
            while (n > 0)
            {
                try
                {
					try
					{
						string linuxname = _LinuxProcessName(ID);
						if (linuxname != "") return linuxname;
					}
					catch { }

					return _ProcessName(System.Diagnostics.Process.GetProcessById(ID));
                }
                catch
                {
                }
                System.Threading.Thread.Sleep(10);
                n--;
            }
            return "";
        }

        public static List<int> GetProcessChildren(int ID)
        {
            if (Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindows ||
                Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftWindowsCE ||
                Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftXBox360 ||
                Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftXBoxOne ||
                Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.MicrosoftDOS)
            {
                return new List<int>();
            }
            List<int> PID = new List<int>();
            Internal.Monitor monitor = new Internal.Monitor();
            monitor.Refresh();
            if (monitor.Processes.ContainsKey(ID)) 
            {
                foreach (int cpid in monitor.Processes[ID].Children.Keys)
                {
                    PID.Add(cpid);
                }
            }
            return PID;
        }

        public static void Kill(string Name)
        {
            Kill(Name, 10);
        }

	    static void Kill(string Name, int Try)
		{
            if (Try < 0) { return; }
			bool Native = false;
			try
			{
				try
				{
					if (System.IO.Directory.Exists("/compat/linux/proc"))
					{
						foreach (string process in System.IO.Directory.GetDirectories("/compat/linux/proc"))
						{
							try
							{
								string value = System.IO.File.ReadAllText(process + "/stat");
								string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
								string name = info[1];
								if (name.StartsWith("(")) { name = name.Substring(1); }
								if (name.EndsWith(")")) { name = name.Substring(0, name.Length - 1); }
								if (name == Name)
								{
									Kill(int.Parse(info[0]));
								}

							}
							catch
							{
							}
						}
					}
				}
				catch
				{
				}
				foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName(Name))
				{
					try
					{
						process.Kill();
					}
					catch
					{
						Native = true;
					}
				}
			}
			catch
			{
				Native = true;
			}
            try
            {
                if (Native)
                {
                    if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
                    {
                        Execute(Internal.Information.Where("killall") + " " + Name);
                    }
                }
            }
            catch { }

            try
            {
                bool stillexist = false;
                foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName(Name))
                {
                    stillexist = true;
                    try
                    {
                        process.Kill();
                    }
                    catch
                    {
                    }
                }
                if (stillexist)
                {
                    System.Threading.Thread.Sleep(100);
                    Kill(Name, Try - 1);
                }
            }
            catch { }

		}

        public delegate int EmbededProcess(string WorkingDirectory,
                                           string[] Args,
                                           System.IO.TextReader StdIn,
                                           System.IO.TextWriter StdOut,
                                           System.IO.TextWriter StdErr);

        public enum KillMethods
        {
            KILL_PROCESS_ONLY,
            KILL_PROCESS_AND_CHILDREN
        };

        KillMethods _KillMethod = KillMethods.KILL_PROCESS_AND_CHILDREN;
        public KillMethods KillMethod { get { return _KillMethod; } set { _KillMethod = value; } }

        /// <summary>
        /// Provide information on an execution of the process.
        /// </summary>
        public class Result
        {
            public Result() { }
            public Result(
                string StdOut,
                string StdErr,
                int ExitCode)
            {
                _StdOut = StdOut;
                _StdErr = StdErr;
                _ExitCode = ExitCode;
            }

            internal volatile bool _Started = false;
            bool _Terminated = false;
            public bool Terminated { get { return _Terminated; } set { _Terminated = value; } }
            int _ExitCode = 0;
            public int ExitCode { get { return _ExitCode; } set { _ExitCode = value; } }
            bool _TimeOutExit = false;
            public bool TimeOutExit { get { return _TimeOutExit; } set { _TimeOutExit = value; } }
            internal string _StdOut = "";
            public string StdOut { get { return _StdOut; } set { _StdOut = value; } }
            internal string _StdErr = "";
            public string StdErr { get { return _StdErr; } set { _StdErr = value; } }
            Statistic _Statistic = new Statistic();
            public Statistic Statistic { get { return _Statistic; } }
			bool _OutOfMemoryExit = false;
			public bool OutOfMemoryExit { get { return _OutOfMemoryExit; } set { _OutOfMemoryExit = value; } }
            bool _OutOfChildrenExit = false;
            public bool OutOfChildrenExit { get { return _OutOfChildrenExit; } set { _OutOfChildrenExit = value; } }
            public void Input(string text)
            {
                if (_VirtualInput != null)
                {
                    _VirtualInput.Input(text);
                }
                else
                {
                    try
                    {
						if (!_Process.HasExited)
						{
                        	_Process.StandardInput.Write(text);
						}
                    }
                    catch
                    { }
                }
            }
            internal void BreakPipe(int timeout)
            {
                try
                {
                    if (_VirtualInput != null)
                    {
                        _VirtualInput.Break();
                    }
                    else
                    {
                        try
                        {
                            _Process.StandardInput.Flush();
                            _Process.StandardInput.Close();
                            _Process.StandardInput.Flush();
                            _Process.StandardInput.BaseStream.Close();
                        }
                        catch
                        {
                            System.Threading.Thread.Sleep(100);
                        }
                        if (_Process == null)
                            Fact.Log.Error("Internal Corruption");
                        while (!_Process.WaitForExit(10))
                        {
                            try
                            {
                                _Process.StandardInput.Flush();
                                _Process.StandardInput.Close();
                                _Process.StandardInput.Flush();
                                _Process.StandardInput.BaseStream.Close();
                            }
                            catch { }
                            if (timeout > 0)
                            {
                                timeout -= 10;
                                if (timeout <= 0)
                                {
                                    try
                                    {
                                        _Process.Kill();
                                    }
                                    catch { }
                                    return;
                                }
                            }
                        }
                    }
                }
                catch { }
            }

            internal System.Diagnostics.Process _Process;
            internal EmbededBufferR _VirtualInput = null;
        }

		ulong _MemoryLimit = 1024 * 1024 * 1024;
		public ulong MemoryLimit { get { return _MemoryLimit; } set { _MemoryLimit = value; } }

        ulong _ChildLimit = 512;
        public ulong ChildLimit { get { return _ChildLimit; } set { _ChildLimit = value; } }

        volatile System.Diagnostics.Process _Process;
        int _Timeout = 0;
        volatile Result _Result = new Result();
        string _ExecutableName = "";
        List<string> _TempExecutable = new List<string>();
        string _TempWorkingDirectory = "";
        string _WorkingDirectory = "";
        Dictionary<string, string> _EnvironmentVariables = new Dictionary<string, string>();

        public Dictionary<string, string> EnvironmentVariables { get { return _EnvironmentVariables; } }

        /// <summary>
        /// Get or set the working directory of this process.
        /// </summary>
        /// <remarks> The working directory cannot be set when the process is running.</remarks>
        public string WorkingDirectory
        {
            get 
            {
                if (_embededProcess != null) { return _embededWorkingDirectory; }
                return _WorkingDirectory;
            }
            set 
            {
                if (_embededProcess != null) { _embededWorkingDirectory = value; }
                _WorkingDirectory = value; 
            }
        }

        ~Process()
        {
            _CoerceDestroy();
        }

        public void UnsafeCoerceDestroy()
        {
            try
            {
                _CoerceDestroy();
            }
            catch{}
        }

        internal void _CoerceDestroy()
        {
            try
            {
                foreach (string file in _TempExecutable)
                {
                    try
                    {
                        if (file != "")
                            System.IO.File.Delete(file);
                    }
                    catch { }
                }
            }
            catch { }

            _TempExecutable = new List<string>();

            try
            {
                if (_TempWorkingDirectory != "")
                {
                    Tools.RecursiveDelete(_TempWorkingDirectory);
                }
            }
            catch { }

            try
            {
                _Process.Close();
            }
            catch { }
			try
			{
				Fact.Tools.RecursiveDelete(_FakeHome);
			}
			catch { }
            _TempWorkingDirectory = "";
        }

        string _shell = "";

        public string Shell
        {
            get { return _shell; }
			set
			{
				if (_shell != value && _shell != "")
				{
					throw new Exception("Impossible to change the shell used.");
				}

				if (value == "strace" || value == "truss") 
				{
					if (Internal.Information.CurrentOperatingSystem () == Fact.Internal.Information.OperatingSystem.Unix) 
					{

						string file = Internal.Information.Where ("strace");
						if (!System.IO.File.Exists (file)) {
							file = Internal.Information.Where ("truss");
						}
						if (!System.IO.File.Exists (file)) {
							Fact.Log.Error ("strace or truss not found.");
						}
						_shell = file;

						string fullprocessname = _Process.StartInfo.FileName;

						try 
						{
							fullprocessname = System.IO.Path.GetFullPath (_Process.StartInfo.FileName);
						} catch 
						{
						}


						if (Internal.Information.IsDotNetExecutable (fullprocessname)) 
						{
							Fact.Log.Info("Use .NET VM to run " + fullprocessname);
							fullprocessname = Internal.Information.GetMonoBinary () + " " + fullprocessname;
						}
						
						_Process.StartInfo.Arguments = fullprocessname + " " + _Process.StartInfo.Arguments;
						Fact.Log.Info("Change command to : " + file + " " + _Process.StartInfo.Arguments);
						_Process.StartInfo.FileName = file;
						return;
					}
				}

				throw new Exception("Don't know how to use " + _shell);
			}
        }


        /// <summary>
        /// Create a temporary folder and set it as the current working directory.
        /// </summary>
        /// <returns>The name of the temporary working directory.</returns>
        public string CreateTempWorkingDirectory()
        {
            if (_TempWorkingDirectory != "")
            {
                try
                {
                    Tools.RecursiveDelete(_TempWorkingDirectory);
                }
                catch { }
            }

            Random rnd = new Random((int)(DateTime.Now.Ticks % (long)(Int32.MaxValue)));
            rnd.Next();

            string directoryName = Internal.Information.GetTempFileName();

            if (_ExecutableName != "")
            {
                try
                {
                    string realName = System.IO.Path.GetFileNameWithoutExtension(_ExecutableName);
                    directoryName += "_" + realName.Trim();
                }
                catch { }
            }

            if (System.IO.Directory.Exists("/tmp"))
            {
                try
                {
                    _TempWorkingDirectory = "/tmp/" + directoryName;
                    System.IO.Directory.CreateDirectory("/tmp/" + directoryName);
                }
                catch
                {
                    _TempWorkingDirectory = "";
                    return "";
                }
            }
            else
            {
                try
                {
                    _TempWorkingDirectory = directoryName;
                    System.IO.Directory.CreateDirectory(directoryName);
                }
                catch
                {
                    _TempWorkingDirectory = "";
                    return "";
                }
            }
            if (_TempWorkingDirectory != "")
            {
                try
                {
                    _Process.StartInfo.WorkingDirectory = _TempWorkingDirectory;
                    _WorkingDirectory = _TempWorkingDirectory;
                }
                catch { }
            }
            return _TempWorkingDirectory;
        }

        /// <summary>
        /// Create a new process.
        /// 
        /// Currently it use the current directory as working directory.
        /// This behaviour will change and a Temp directory will be created.
        /// To create a process and une the current directory as working directory use
        /// Process(Executable, "", Arguements).</summary>
        /// <param name="Executable"></param>
        /// <param name="Arguements"></param>
        public Process(string Executable, string Arguements)
        {
            try
            {
                if (!System.IO.File.Exists(Executable))
                {
                    string file = Internal.Information.Where(Executable);
                    if (file != "")
                    {
                        Executable = file;
                    }
                }
            }
            catch
            { }

            try
            {
                System.IO.FileInfo info = new System.IO.FileInfo(Executable);
                Executable = info.FullName;
            }
            catch { }

            try
            {
                if (System.IO.Path.GetExtension(Executable) == ".jar")
                {
                    string ClassPath = System.IO.Path.GetDirectoryName(Executable);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + ClassPath + "\" -jar \"" + Executable + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = "";
                    CreateTempWorkingDirectory();
                }
                else if (System.IO.Path.GetExtension(Executable) == ".class")
                {
                    string ClassPath = System.IO.Path.GetDirectoryName(Executable);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + ClassPath + "\" \"" + System.IO.Path.GetFileNameWithoutExtension(Executable) + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = "";
                    CreateTempWorkingDirectory();
                }
                else if (System.IO.Path.GetExtension(Executable) == ".py")
                {


                    _Process = new System.Diagnostics.Process();

					
					string python = Internal.Information.GetPythonBinary();

					try
					{
						string[] lines = System.IO.File.ReadAllLines(Executable);
						if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
						{
							python = Internal.Information.SolveSheBang(lines[0].Trim());
							_shell = python;
							lines[0] = "";
							if (!System.IO.File.Exists(python))
								python = Internal.Information.GetPythonBinary();
						}
					}
					catch{}
					_Process.StartInfo.FileName = python;
					_shell = python;
                    Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = Executable + " " + Arguements;
                    _Process.StartInfo.WorkingDirectory = "";
                    _WorkingDirectory = ".";
                }
                else if (System.IO.Path.GetExtension(Executable) == ".sh")
                {
                    try
                    {
                        string sh = Internal.Information.Where("sh");
                        _shell = sh;
                        string tmpexe = Internal.Information.GetTempFileName() + ".sh";
                        _TempExecutable.Add(tmpexe);
                        string[] lines = System.IO.File.ReadAllLines(Executable);
                        string shell = Internal.Information.Where(sh);
                        if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
                        {
                            shell = Internal.Information.SolveSheBang(lines[0].Trim());
                            _shell = shell;
                            lines[0] = "";
                        }

                        _emptyProcess = true;
                        for (int n = 0; n < lines.Length; n++)
                            if (lines[n].Trim() != "")
                            {
                                _emptyProcess = false;
                                break;
                            }
                        if (_emptyProcess)
                            return;

                        string fullname = new System.IO.FileInfo(tmpexe).FullName;
                        System.IO.File.WriteAllLines(fullname, lines);
                        _Process = new System.Diagnostics.Process();
                        _Process.StartInfo.FileName = shell;
                        _Process.StartInfo.Arguments = "\"" + fullname + "\" " + Arguements;
                        _Process.StartInfo.WorkingDirectory = "";
                        CreateTempWorkingDirectory();
                        Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                        Tools.AddExecutionPermission(fullname);
                    }
                    catch { }
                }
                else
                {
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Executable;
                    Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = Arguements;
                    _Process.StartInfo.WorkingDirectory = "";
                    CreateTempWorkingDirectory();
                }
                _Process.StartInfo.CreateNoWindow = true;
                _Process.StartInfo.UseShellExecute = false;
                _Process.StartInfo.RedirectStandardOutput = true;
                _Process.StartInfo.RedirectStandardInput = true;
                _Process.StartInfo.RedirectStandardError = true;
            }
            catch { _Process = null; }
        }

        public Process(string Executable, string workingDirectory, string Arguements)
        {
            try
            {
                if (!System.IO.File.Exists(Executable))
                {

                    string file = Internal.Information.Where(Executable);
                    if (file != "")
                    {
                        Executable = file;
                    }
                }
            }
            catch
            { }

            try
            {
                if (!Executable.StartsWith("/"))
                {
                    try
                    {
                        string file = "";
                        if (workingDirectory.EndsWith("/") ||
                            workingDirectory.EndsWith("\\"))
                        { file = workingDirectory + Executable; }
                        else
                        { file = workingDirectory + "/" + Executable; }
                        if (System.IO.File.Exists(file))
                        {
                            Executable = file;
                        }
                    }
                    catch
                    { }
                }
            }
            catch { }

            try
            {
                System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(workingDirectory);
                workingDirectory = info.FullName;
            }
            catch { }
            try
            {
                System.IO.FileInfo info = new System.IO.FileInfo(Executable);
                Executable = info.FullName;
            }
            catch { }
            try
            {
                if (System.IO.Path.GetExtension(Executable) == ".jar")
                {
                    string ClassPath = System.IO.Path.GetDirectoryName(Executable);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + ClassPath + "\" -jar \"" + Executable + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                }
                else if (System.IO.Path.GetExtension(Executable) == ".class")
                {
                    string ClassPath = System.IO.Path.GetDirectoryName(Executable);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + ClassPath + "\" \"" + System.IO.Path.GetFileNameWithoutExtension(Executable) + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                }
                else if (System.IO.Path.GetExtension(Executable) == ".py")
                {
                    _Process = new System.Diagnostics.Process();
                    string python = Internal.Information.GetPythonBinary();

					try
					{
						string[] lines = System.IO.File.ReadAllLines(Executable);
						if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
						{
							python = Internal.Information.SolveSheBang(lines[0].Trim());
							_shell = python;
							lines[0] = "";
							if (!System.IO.File.Exists(python))
								python = Internal.Information.GetPythonBinary();
						}
					}
					catch{}
					_Process.StartInfo.FileName = python;
					Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = Executable + " " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = workingDirectory;
                }
                else if (System.IO.Path.GetExtension(Executable) == ".sh")
                {
                    try
                    {
                        string sh = Internal.Information.Where("sh");
                        
                        _shell = sh;
                        string tmpexecutable = Internal.Information.GetTempFileName();
                        _TempExecutable.Add(tmpexecutable);
                        string[] lines = System.IO.File.ReadAllLines(Executable);

                        string shell = Internal.Information.Where(tmpexecutable);
                        
                        if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
                        {
                            shell = Internal.Information.SolveSheBang(lines[0].Trim());
                            _shell = shell;
                            lines[0] = "";
                        }

                        _emptyProcess = true;
                        for (int n = 0; n < lines.Length; n++)
                            if (lines[n].Trim() != "")
                            {
                                _emptyProcess = false;
                                break;
                            }
                        if (_emptyProcess)
                            return;

                        string fullname = new System.IO.FileInfo(tmpexecutable).FullName;
                        System.IO.File.WriteAllLines(tmpexecutable, lines);
                        _Process = new System.Diagnostics.Process();
                        _Process.StartInfo.FileName = shell;
                        _Process.StartInfo.Arguments = "\"" + fullname + "\" " + Arguements;
                        _Process.StartInfo.WorkingDirectory = workingDirectory;
                        _WorkingDirectory = workingDirectory;
                        Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                        Tools.AddExecutionPermission(fullname);
                    }
                    catch { }
                }
                else
                {
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Executable;
                    Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = workingDirectory;
                }
                _Process.StartInfo.CreateNoWindow = true;
                _Process.StartInfo.UseShellExecute = false;
                _Process.StartInfo.RedirectStandardOutput = true;
                _Process.StartInfo.RedirectStandardInput = true;
                _Process.StartInfo.RedirectStandardError = true;
            }
            catch { _Process = null; }
        }

        #region Hook
        string _tempHook = "";
        public void AddHook(string ProcessName, string Command)
        {
            if (_Process != null)
            {
                if (_tempHook == "")
                {
					try
					{
                    	_tempHook = Tools.CreateTempDirectory();
						System.IO.FileInfo info = new System.IO.FileInfo (_tempHook);
						_tempHook = info.FullName;
					}
					catch
					{
						return;
					}
                }
                if (!_Process.StartInfo.EnvironmentVariables.ContainsKey("PATH"))
                {
                    _Process.StartInfo.EnvironmentVariables.Add("PATH", _tempHook);
                }
                else if (!_Process.StartInfo.EnvironmentVariables["PATH"].Contains(_tempHook))
                {
                    if (_Process.StartInfo.EnvironmentVariables["PATH"].Contains(":"))
                    {
						_Process.StartInfo.EnvironmentVariables["PATH"] =  _tempHook + ":" + _Process.StartInfo.EnvironmentVariables["PATH"];
                    }
                    else if (_Process.StartInfo.EnvironmentVariables["PATH"].Contains(";"))
                    {
						_Process.StartInfo.EnvironmentVariables["PATH"] =  _tempHook + ";" + _Process.StartInfo.EnvironmentVariables["PATH"];
                    }
                    else if (Internal.Information.GetEnvPath().Contains(":"))
                    {
						_Process.StartInfo.EnvironmentVariables["PATH"] = _tempHook + ":" + _Process.StartInfo.EnvironmentVariables["PATH"];
                    }
                    else if (Internal.Information.GetEnvPath().Contains(";"))
                    {
						_Process.StartInfo.EnvironmentVariables["PATH"] =  _tempHook + ";" + _Process.StartInfo.EnvironmentVariables["PATH"];
                    }
                }

				try
				{
					System.IO.File.WriteAllText(_tempHook + "/" + ProcessName, "#! /usr/bin/env sh\n" + Command + " $*");
					Fact.Log.Debug("Create file "  + _tempHook + "/" + ProcessName + " " + " Containing " + Command + " $*");
					Tools.AddExecutionPermission(_tempHook + "/" + ProcessName);
				}
				catch {
				}
            }
        }
        #endregion

        EmbededProcess _embededProcess = null;
        string _embededWorkingDirectory = "";
        string[] _embededArguments = null;
        bool _emptyProcess = false;
        public Process(EmbededProcess Process, string workingDirectory, string Arguements)
        {
            if (Process == null)
            {
                _embededProcess =
                (string workingdir,
                    string[] args,
                    System.IO.TextReader StdIn,
                    System.IO.TextWriter StdOut,
                    System.IO.TextWriter StdErr) => { return 0; };
            }
            else
            {
                _embededProcess = Process;
                _embededWorkingDirectory = workingDirectory;

                _embededArguments = ParseArguments(Arguements).ToArray();

            }
        }

		public Process(EmbededProcess Process, string workingDirectory, string[] Arguements)
		{
			if (Process == null)
			{
				_embededProcess =
					(string workingdir,
					 string[] args,
					 System.IO.TextReader StdIn,
					 System.IO.TextWriter StdOut,
					 System.IO.TextWriter StdErr) => { return 0; };
			}
			else
			{
				_embededProcess = Process;
				_embededWorkingDirectory = workingDirectory;

				_embededArguments = Arguements;

			}
		}

        public Process(EmbededProcess Process, string Arguements) :
            this(Process, ".", Arguements)
        {
        }

		string ExtractExecutable (Processing.File file)
		{
			return ExtractExecutable (file, ".");
		}


		string ExtractExecutable(Processing.File file, string Where)
        {
            string path = "";

            try
            {
                if (!System.IO.File.Exists(Where + "/" + file.Name) && Where != ".")
                {

                    path = Where + "/" + file.Name;
                    System.IO.File.WriteAllBytes(path, file.GetBytes());
                    _TempExecutable.Add(path);
                }
            }
            catch { }
            if (path == "")
			{
				
	            if (file.Name == "")
	            {
	                try
	                {

	                    path = Internal.Information.GetTempFileName() + System.IO.Path.GetExtension(file.Name);
						System.IO.File.WriteAllBytes(path, file.GetBytes());
	                    _TempExecutable.Add(path);
	                }
	                catch
	                {
	                    path = Internal.Information.GetTempFileName();
						System.IO.File.WriteAllBytes(path, file.GetBytes());
	                    _TempExecutable.Add(path);
	                }
	            }
	            else
	            {
	                
	                if (!System.IO.File.Exists(_WorkingDirectory + "/" + file.Name))
	                {
						try
						{
	                    	path = _WorkingDirectory + "/" + file.Name;
							System.IO.File.WriteAllBytes(path, file.GetBytes());
	                    	_TempExecutable.Add(path);
						}
						catch 
						{
						}
	                }
	                else if (!System.IO.File.Exists(file.Name))
	                {
	                    try
	                    {
	                        path = file.Name;
							System.IO.File.WriteAllBytes(path, file.GetBytes());
	                        _TempExecutable.Add(path);
	                    }
	                    catch
	                    {

	                    }
	                }
	                else
	                {
	                    try
	                    {
	                        path = Internal.Information.GetTempFileName() + System.IO.Path.GetExtension(file.Name);
							System.IO.File.WriteAllBytes(path, file.GetBytes());
	                        _TempExecutable.Add(path);
	                    }
	                    catch
	                    {
							try
							{
	                        	path = Internal.Information.GetTempFileName();
								System.IO.File.WriteAllBytes(path, file.GetBytes());
	                        	_TempExecutable.Add(path);
							}
							catch 
							{
							}
	                    }
	                }
	            }

			}
            if (path == "")
            {
                try
                {
                    if (!System.IO.File.Exists(Where + "/" + file.Name))
                    {

                        path = Where + "/" + file.Name;
                        System.IO.File.WriteAllBytes(path, file.GetBytes());
                        _TempExecutable.Add(path);
                    }
                }
                catch 
				{ 
					try
					{
						string tmpdir = Tools.CreateTempDirectory();
						path = tmpdir + "/" + file.Name;
						System.IO.File.WriteAllBytes(tmpdir + "/" + file.Name, file.GetBytes());
						_TempExecutable.Add(path);
						Fact.Log.Warning("Impossible to find a place to extract executable.");
						Fact.Log.Warning("Fact will be forced to leak the temp directory " + tmpdir);
					}
					catch 
					{
					}
				}
            }
			Tools.AddExecutionPermission(path);
			return path;
        }

        public Process(Processing.File file, string workingDirectory, string Arguements)
        {
            try
            {
                System.IO.DirectoryInfo info = new System.IO.DirectoryInfo(workingDirectory);
                workingDirectory = info.FullName;
				_WorkingDirectory = workingDirectory;
            }
            catch { }


            try
            {
                if (file.Type == Fact.Processing.File.FileType.JavaPackage)
                {
                    _TempExecutable = file.Project.ExtractDirectory(file.Directory, workingDirectory, Processing.File.FileType.JavaPackage);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + workingDirectory + "\" -jar \"" + workingDirectory + "/" + file.Name + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = "";
                }
                else if (file.Type == Fact.Processing.File.FileType.JavaClass)
                {
                    _TempExecutable = file.Project.ExtractDirectory(file.Directory, workingDirectory, Processing.File.FileType.JavaClass);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + workingDirectory + "\" \"" + System.IO.Path.GetFileNameWithoutExtension(file.Name) + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = "";
                }
                else if (file.Type == Fact.Processing.File.FileType.PythonSource)
                {
                    _TempExecutable = file.Project.ExtractDirectory(file.Directory, workingDirectory, Processing.File.FileType.PythonSource);
                    _Process = new System.Diagnostics.Process();
					string python = Internal.Information.GetPythonBinary();
					try
					{
						string[] lines = file.GetText(false).Split('\n');
						if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
						{
							python = Internal.Information.SolveSheBang(lines[0].Trim());
							_shell = python;
							lines[0] = "";
							if (!System.IO.File.Exists(python))
								python = Internal.Information.GetPythonBinary();
						}
					}
					catch{}
					_Process.StartInfo.FileName = python;

                    _shell = Internal.Information.GetPythonBinary();
                    Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = file.Name + " " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = "";
                }
                else if (file.Type == Fact.Processing.File.FileType.ShSource)
                {
                    string shell = Internal.Information.Where("sh");
                    _shell = shell;
                    string[] lines = file.GetLines(true);

                    if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
                    {
                        shell = Internal.Information.SolveSheBang(lines[0].Trim());
                        _shell = shell;
                        lines[0] = "";
                    }

                    _emptyProcess = true;
                    for (int n = 0; n < lines.Length; n++)
                        if (lines[n].Trim() != "")
                        {
                            _emptyProcess = false;
                            break;
                        }
                    if (_emptyProcess)
                        return;
                    string tmpexecutable = Internal.Information.GetTempFileName();
                    _TempExecutable.Add(tmpexecutable);
                    string fullname = new System.IO.FileInfo(tmpexecutable).FullName;
                    System.IO.File.WriteAllLines(tmpexecutable, lines);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = shell;
                    _Process.StartInfo.Arguments = "\"" + fullname + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = "";
                    Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    Tools.AddExecutionPermission(fullname);
                }
                else
                {

                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = ExtractExecutable(file);
					Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = Arguements;
                    _Process.StartInfo.WorkingDirectory = workingDirectory;
                    _WorkingDirectory = "";
                }
                _Process.StartInfo.CreateNoWindow = true;
                _Process.StartInfo.UseShellExecute = false;
                _Process.StartInfo.RedirectStandardOutput = true;
                _Process.StartInfo.RedirectStandardInput = true;
                _Process.StartInfo.RedirectStandardError = true;
            }
            catch (Exception e)
            {
				if (file != null) 
				{
					Fact.Log.FatalInternalError (e);
				}
                _Process = null;
            }
        }

        void _Process_OutputDataReceived(object sender, System.Diagnostics.DataReceivedEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Create a process. A tempory directory is automatically created and set as working directory.
        /// </summary>
        /// <param name="file"></param>
        /// <param name="Arguements"></param>
        public Process(Processing.File file, string Arguements)
        {
            _WorkingDirectory = CreateTempWorkingDirectory();
            try
            {
                if (file.Type == Fact.Processing.File.FileType.JavaPackage)
                {
                    file.Project.ExtractDirectory(file.Directory, _WorkingDirectory, Processing.File.FileType.JavaPackage);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + _WorkingDirectory + "\" -jar \"" + _WorkingDirectory + "/" + file.Name + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = _WorkingDirectory;
                }
                else if (file.Type == Fact.Processing.File.FileType.JavaClass)
                {
                    file.Project.ExtractDirectory(file.Directory, _WorkingDirectory, Processing.File.FileType.JavaClass);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = Internal.Information.GetJAVABinary();
                    _Process.StartInfo.Arguments = "-classpath \"" + _WorkingDirectory + "\" \"" + System.IO.Path.GetFileNameWithoutExtension(file.Name) + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = _WorkingDirectory;
                }
                else if (file.Type == Fact.Processing.File.FileType.PythonSource)
                {
                    file.Project.ExtractDirectory(file.Directory, _WorkingDirectory, Processing.File.FileType.PythonSource);
                    _Process = new System.Diagnostics.Process();
					string python = Internal.Information.GetPythonBinary();
					try
					{
						string[] lines = file.GetText(false).Split('\n');
						if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
						{
							python = Internal.Information.SolveSheBang(lines[0].Trim());
							_shell = python;
							lines[0] = "";
							if (!System.IO.File.Exists(python))
								python = Internal.Information.GetPythonBinary();
						}
					}
					catch{}
					_Process.StartInfo.FileName = python;
                    _shell = Internal.Information.GetPythonBinary();
                    _Process.StartInfo.Arguments = file.Name + " " + Arguements;
                    _Process.StartInfo.WorkingDirectory = _WorkingDirectory;
                }
                else if (file.Type == Fact.Processing.File.FileType.ShSource)
                {
                    string shell = Internal.Information.Where("sh");
                    _shell = shell;
                    string[] lines = file.GetLines(true);

                    if (lines.Length > 0 && lines[0].Trim().StartsWith("#!"))
                    {
                        shell = Internal.Information.SolveSheBang(lines[0].Trim());
                        _shell = shell;
                        lines[0] = "";
                    }

                    _emptyProcess = true;
                    for (int n = 0; n < lines.Length; n++)
                        if (lines[n].Trim() != "")
                        {
                            _emptyProcess = false;
                            break;
                        }
                    if (_emptyProcess)
                        return;

                    System.IO.File.WriteAllLines(_WorkingDirectory + "/" + file.Name, lines);
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = shell;
                    _Process.StartInfo.Arguments = "\"" + file.Name + "\" " + Arguements;
                    _Process.StartInfo.WorkingDirectory = _WorkingDirectory;

                    Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    Tools.AddExecutionPermission(_WorkingDirectory + "/" + file.Name);
                }
                else
                {
                    _Process = new System.Diagnostics.Process();
                    _Process.StartInfo.FileName = ExtractExecutable(file);
					Tools.AddExecutionPermission(_Process.StartInfo.FileName);
                    _Process.StartInfo.Arguments = Arguements;
                    _Process.StartInfo.WorkingDirectory = _WorkingDirectory;

                }
                _Process.StartInfo.CreateNoWindow = true;
                _Process.StartInfo.UseShellExecute = false;
                _Process.StartInfo.RedirectStandardOutput = true;
                _Process.StartInfo.RedirectStandardInput = true;
                _Process.StartInfo.RedirectStandardError = true;
            }
            catch (Exception e)
			{
				if (file != null) 
				{
					Fact.Log.FatalInternalError (e);
				}
				_Process = null;
			}
        }

        volatile bool _BackgroundRunning = false;
        System.Threading.Thread _Worker = null;
        System.Threading.Thread _ErrorWorker = null;
        void _DumpProcessInfo()
        {
            try
            {
                try
                {
                    string value = _Process.StandardOutput.ReadToEnd();
                    _Result.StdOut += value;
                    try
                    {
                        foreach (Result result in _PipesResults)
                        {
                            try
                            {
                                result.Input(value);
                            }
                            catch
                            { }
                        }
                    }
                    catch
                    { }
                }
                catch { }
            }
            catch { }
        }
        void _BackgroundWorker()
        {

            while (_BackgroundRunning)
            {
                _DumpProcessInfo();
                System.Threading.Thread.Sleep(10);

            }
        }

        void _BackgroundErrorWorker()
        {

            while (_BackgroundRunning)
            {
                try
                {
                    try { _Result.StdErr += _Process.StandardError.ReadToEnd(); }
                    catch { }
                }
                catch { }
                System.Threading.Thread.Sleep(10);

            }
        }

        void _ForegroundWorker()
        {
            int PID = 0;
            if (_emptyProcess)
            {
                _Result.ExitCode = 0;
                _Result.StdOut = "";
                _Result.StdErr = "";
                _Result.TimeOutExit = false;
                return;
            }


			try
			{
				if (_Process != null)
				{
					if (System.IO.File.Exists(_Process.StartInfo.FileName))
					{

						System.IO.FileInfo Info = new System.IO.FileInfo(_Process.StartInfo.FileName);
						_Process.StartInfo.FileName = Info.FullName;
					}
				}
			}
			catch { }

			try
			{
				if (_Process != null)
				{
					Tools.AddExecutionPermission(_Process.StartInfo.FileName);
				}
			}
			catch { }

            try
            {
                if (!_Process.StartInfo.UseShellExecute)
                {
                    if (_EnvironmentVariables.Count > 0)
                    {
                        foreach (KeyValuePair<string, string> var in _EnvironmentVariables)
                        {
                            _Process.StartInfo.EnvironmentVariables.Add(var.Key, var.Value);
                        }
                    }
                }
            }
            catch { }


            if (_Process != null)
            {
				if (Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.AppleMacOSX ||
				    Internal.Information.CurrentOperatingSystem() == Fact.Internal.Information.OperatingSystem.Unix)
				{
					try
					{
						if (Internal.Information.GetEnvVariable("LD_LIBRARY_PATH") == "")
							Internal.Information.SetEnvVariable("LD_LIBRARY_PATH", ".");
					}
					catch { }

					try
					{
						_Process.StartInfo.EnvironmentVariables.Add("LD_LIBRARY_PATH", ".");
					}
					catch { }
				}
                _Process.Start();
                try { Internal.Os.UserModeDriver.Driver.RefuseProcessProtection(_Process.Id); }
                catch { }
                try { Internal.Os.UserModeDriver.Driver.DeclareChildProcess(_Process.Id); }
                catch { }
				if (_StartupInput != "") 
                {
					try
					{
                    	_Process.StandardInput.Write(_StartupInput);
                        try { _Process.StandardInput.Flush(); }
                        catch { }
					}
					catch
					{
						try
						{
							_Process.StandardInput.Write(_StartupInput);
						}
						catch
						{
						}
                        try { _Process.StandardInput.Flush(); }
                        catch { }
					}
                    _StartupInput = "";
                }
                _Worker.Start();
                _ErrorWorker.Start();
                _Result.TimeOutExit = false;
                _Result._Started = true;
                int CurrentTimeOut = _Timeout;
                {
                    try
                    {
                        long OldUCPU = 0;
                        long OldSCPU = 0;
                        long OldMsSCPU = 0;
                        long TreeRefreshInterval = 500;
                        PID = _Process.Id;
                        while (!_Process.WaitForExit(10))
                        {
                            if (_BreakInput)
                            {
                                bool broken = true;
                                try { _Process.StandardInput.Flush(); } catch { broken = false; }
                                try { _Process.StandardInput.Close(); }catch { broken = false; }
                                try { _Process.StandardInput.BaseStream.Close(); } catch { }
                                if (broken) { _BreakInput = false; }
                            }
                            _Process.Refresh();
                            if (PID == 0) { PID = _Process.Id; }
                            try
                            {
                                TreeRefreshInterval -= 10;
                                if (TreeRefreshInterval < 0)
                                {
                                    TreeRefreshInterval = 500;
                                    if (Fact.Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.AppleMacOSX ||
                                        Fact.Internal.Information.CurrentOperatingSystem() == Internal.Information.OperatingSystem.Unix)
                                    {
                                        if (_ChildLimit > 0 && _ChildLimit != ulong.MaxValue)
                                        {
                                            if ((ulong)GetProcessChildren(PID).Count > _ChildLimit)
                                            {
                                                _Result.OutOfChildrenExit = true;
                                                _Result.ExitCode = -4646;
                                                try { _Process.Kill(); } catch { }
                                                try
                                                {
                                                    foreach (int child in GetProcessChildren(PID))
                                                    {
                                                        Kill(child);
                                                    }
                                                }
                                                catch { }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            catch { }

                            try
                            {
                                long value = _VirtualMemoryUsed(_Process);
								if ((ulong)value > _MemoryLimit && MemoryLimit > 0)
								{
									_Result.OutOfMemoryExit = true;
									try
									{
										_Result.ExitCode = -4545;
										try
										{
											_Process.Kill();
										}
										catch { }
										break;
									}
									catch
									{
									}
								}
                                if (value > _Result.Statistic.MemoryUsedPeak)
                                    _Result.Statistic.MemoryUsedPeak = value;
                                _Result.Statistic.MemoryUsed.Add(DateTime.Now, value);
                            }
                            catch { }
                            try
                            {
                                long value = _Process.WorkingSet64;
                                if (value > _Result.Statistic.SwapUsedPeak)
                                    _Result.Statistic.SwapUsedPeak = value;
                                _Result.Statistic.SwapUsed.Add(DateTime.Now, value);
                            }
                            catch { }

                            try
                            {
                                long value = _Process.PeakVirtualMemorySize64;
                                if (value > _Result.Statistic.SwapUsedPeak)
                                    _Result.Statistic.SwapUsedPeak = value;
                            }
                            catch { }

                            try
                            {
                                long value = _Process.PeakWorkingSet64;
                                if (value > _Result.Statistic.MemoryUsedPeak)
                                    _Result.Statistic.MemoryUsedPeak = value;
                            }
                            catch { }

                            try
                            {
                                long value = _Process.Threads.Count;
                                _Result.Statistic.ThreadCount.Add(DateTime.Now, value);
                            }
                            catch { }
                            try
                            {
                                long value = 0;
                                foreach (System.Diagnostics.ProcessThread Thread in _Process.Threads)
                                {
                                    if (
                                        Thread.ThreadState == System.Diagnostics.ThreadState.Wait)
                                    {
                                        value++;
                                    }
                                }
                                _Result.Statistic.WaitingThreadCount.Add(DateTime.Now, value);
                            }
                            catch { }
                            try
                            {

								long value = _LinuxGetUserTimeTick(_Process.Id) + _LinuxGetKernelTimeTick(_Process.Id);

								long totalvaluems = 0;
								if (value != 0)
								{
									totalvaluems = (value * 1000) / (long)Fact.Internal.Os.Generic.GetClockTickPerSecond();

								}
								else
								{
									value = (long)_Process.TotalProcessorTime.Ticks;
									totalvaluems = (long)_Process.TotalProcessorTime.TotalMilliseconds;
								}
								_Result.Statistic.UserProcessorTime.Add(DateTime.Now, value - OldUCPU);
                                _Result.Statistic.TotalUserProcessorTime = value;

                                if (_Timeout > 0)
                                {
                                    try
                                    {
										if (totalvaluems > _Timeout)
                                        {
                                            CurrentTimeOut = -1;
                                        }
										else if (totalvaluems == 0 || totalvaluems == OldMsSCPU)
                                        {
                                            CurrentTimeOut -= 5;
                                        }
										OldMsSCPU = totalvaluems;
                                    }
                                    catch
                                    {
                                        CurrentTimeOut -= 10;
                                    }
                                }
                                OldUCPU = value;
                            }
                            catch
                            {
                                if (_Timeout > 0)
                                {
                                    CurrentTimeOut -= 10;
                                }
                            }
                            try
                            {
								long value = _LinuxGetKernelTimeTick(_Process.Id);
								if (value == 0) { value = (long)_Process.PrivilegedProcessorTime.Ticks; }
                                _Result.Statistic.SystemProcessorTime.Add(DateTime.Now, value - OldSCPU);
                                _Result.Statistic.TotalSystemProcessorTime = value;
                                OldSCPU = value;
                            }
                            catch 
                            {
                            }
                            try
                            {
                                if (_Timeout > 0 && CurrentTimeOut <= 0)
                                {
                                    _BackgroundRunning = false;
                                    _Result.TimeOutExit = true;
                                    _Result.ExitCode = -4242;
                                    try
                                    {
                                        _Process.Kill();
                                    }
                                    catch { }
                                    break;
                                }
                            }
                            catch { }
                        }
                    }
                    catch
                    {
                        _BackgroundRunning = false;
                        try { if (!_Process.HasExited) { _Process.Kill(); } }
                        catch 
                        {
                            
                        }
                        lock (_Result)
                        {
                            _Result.ExitCode = -2323;
                        }
						_Result._Process = null;
                        if (PID != 0)
                        {
                            try
                            {
                                if (_KillMethod == KillMethods.KILL_PROCESS_AND_CHILDREN)
                                {
                                    foreach (int child in GetProcessChildren(PID))
                                    {
                                        Kill(child);
                                    }
                                }
                            }
                            catch { }
                        }
                        return;
                    }
                }
            exit:
                _BackgroundRunning = false;
				try { if (!_Worker.Join(1000)){ try {_Worker.Abort(); _Result.TimeOutExit = true; return;} catch {}} }
                catch { }
				try { if (!_ErrorWorker.Join(1000)){try {_ErrorWorker.Abort(); _Result.TimeOutExit = true; return;} catch {}}}
                catch { }

                
                lock (_Result)
                {
                    try { _Result.ExitCode = _Process.ExitCode; }
                    catch { }
                    try { _Result.StdErr += _Process.StandardError.ReadToEnd(); }
                    catch { }
                    try { _Result.StdOut += _Process.StandardOutput.ReadToEnd(); }
                    catch { }
                }
            }
            else
			{
                _BackgroundRunning = false;
                lock (_Result)
                {
                    _Result.ExitCode = -2424;
                }
            }
            _BackgroundRunning = false;
            try
            {
                lock (_Result)
                {
                    _Result.Terminated = true;
                }
            }
            catch { }
            try
            {
                foreach (Result _ in _PipesResults)
                {
                    _.BreakPipe(_Timeout);
                }
            }
            catch { }
			_Result._Process = null;
            try
            {
                if (_KillMethod == KillMethods.KILL_PROCESS_AND_CHILDREN)
                {
                    foreach (int child in GetProcessChildren(PID))
                    {
                        Kill(child);
                    }
                }
            }
            catch { }
        }
        /// <summary>
        /// Input send on stdin when the program start.
        /// </summary>
        string _StartupInput = "";
        bool _BreakInput = false;
        List<Process> _Pipes = new List<Process>();
        List<Process> _CpyPipes = new List<Process>();
        List<Result> _PipesResults = new List<Result>();
        public List<Process> Pipes { get { return _Pipes; } }

		string _FakeHome = "";
		public void CreateFakeHome()
		{
			if (_FakeHome != "")
			{
				Fact.Tools.RecursiveDelete (_FakeHome);
			}
			_FakeHome = Fact.Tools.CreateTempDirectory ();
			Fact.Internal.Information.SetEnvVariable("HOME", _FakeHome);
			Fact.Internal.Information.SetEnvVariable("Home", _FakeHome);
		}

        public static Result Execute(string Executable)
        {
            return Execute(Executable, "", "", -1);
        }

        public static Result Execute(string Executable, string Args)
        {
            return Execute(Executable, "", Args, -1);
        }

        public static Result Execute(string Executable, string Args, int Timeout)
        {
            return Execute(Executable, "", Args, Timeout);
        }

        public static Result Execute(string Executable, string WorkingDirectory, string Args, int Timeout)
        {
			if (!System.IO.File.Exists (Executable))
				Executable = Fact.Internal.Information.Where (Executable);

            using (Process process = new Process(Executable, WorkingDirectory, Args))
            {
                return process.Run(Timeout);
            }
        }

		/// <summary>
		/// Use the shell to execute command.
		/// </summary>
		/// <returns>The process result</returns>
		/// <param name="Command">Command passed to the shell</param>
		/// <param name="WorkingDirectory">Working directory</param>
		/// <param name="TimeOut">Time out</param>
		public static Result ShellExecute(string Command, string WorkingDirectory, int TimeOut)
		{
			string shell = Internal.Information.Where ("sh");
			if (!System.IO.File.Exists (shell))
				shell = Internal.Information.Where ("ksh");
			if (!System.IO.File.Exists (shell))
				shell = Internal.Information.Where ("bash");
			if (!System.IO.File.Exists (shell))
				shell = Internal.Information.Where ("zsh");
			return Execute (shell, " -c \"" + Parser.Tools.EscapeString (Command) + "\"", WorkingDirectory, TimeOut);
		}

        /// <summary>
        /// Run the process with the specified timeout.
        /// </summary>
        /// <param name="Timeout">Specified the timeout or -1 if no timeout must be used.</param>        
        /// <returns>The result of the execution.</returns>
        public Result Run(int Timeout)
        {
            return Run(Timeout, "");
        }

        public Result Run(int Timeout, string Input)
        {
            return Run(Timeout, Input, false);
        }

        /// <summary>
        /// Run the process with the specified timeout.
        /// </summary>
        /// <param name="Timeout">Specified the timeout or -1 if no timeout must be used.</param>
        /// <param name="Input">Specify the value that must be send to stdin after startup.</param>        
        /// <returns>The result of the execution.</returns>
        public Result Run(int Timeout, string Input, bool CloseInput)
        {
            _BreakInput = CloseInput;
            _StartupInput = Input;
            _PipesResults = new List<Result>();
            _CpyPipes = new List<Process>(_Pipes);
            foreach (Process process in _CpyPipes)
            {
                if (Timeout == -1)
                    _PipesResults.Add(process.RunAsync(-1));
                else
                    _PipesResults.Add(process.RunAsync(Timeout * 4));
                // Spin lock to wait while the other process is not started
                while (!_PipesResults[_PipesResults.Count - 1]._Started)
                {
                    System.Threading.Thread.Sleep(10);
                }

            }
            _Result = new Result();
            if (_CpyPipes.Count > 0)
            {
				_ReturnedResult = _CpyPipes[0]._ReturnedResult;
            }
            else
            {
                _ReturnedResult = _Result;
            }

            _Timeout = Timeout;
            if (_embededProcess == null)
            {

                _Result._Process = _Process;
                _BackgroundRunning = true;
                _Worker = new System.Threading.Thread(_BackgroundWorker);
                _Worker.IsBackground = true;
                _ErrorWorker = new System.Threading.Thread(_BackgroundErrorWorker);
                _ErrorWorker.IsBackground = true;
                _ForegroundWorker();
            }
            else
            {
                _EmbededForegroundWorker(_Result, _PipesResults);
            }

            if (_CpyPipes.Count > 0)
            {
                return _CpyPipes[0].WaitForExit();
            }
            _PipesResults.Clear();
            return _ReturnedResult;
        }

        System.Threading.Thread _Async = null;

        public Result RunAsync(int Timeout)
        {
            return RunAsync(Timeout, "");
        }


        public Result RunAsync(int Timeout, string Input)
        {
            return RunAsync(Timeout, Input, false);
        }

        /// <summary>
        /// Run the process with the specified timeout.
        /// </summary>
        /// <param name="Timeout">Specified the timeout or -1 if no timeout must be used.</param>
        /// <param name="Input">Specify the value that must be send to stdin after startup.</param>
        /// <param name="CloseInput">Specify that stdin must be close just after sending the input</param>
        /// <returns>The result of the execution.</returns>
        public Result RunAsync(int Timeout, string Input, bool CloseInput)
        {
            _BreakInput = CloseInput;
            _StartupInput = Input;
            _PipesResults = new List<Result>();
            _CpyPipes = new List<Process>(_Pipes);
            foreach (Process process in _CpyPipes)
            {
                if (Timeout == -1)
                    _PipesResults.Add(process.RunAsync(-1));
                else
                    _PipesResults.Add(process.RunAsync(Timeout * 4));
                // Spin lock to wait while the other process is not started
                while (!_PipesResults[_PipesResults.Count - 1]._Started)
                {
                    System.Threading.Thread.Sleep(10);
                }
            }
            _Timeout = Timeout;
            _Result = new Result();
            if (_CpyPipes.Count > 0)
            {
                _ReturnedResult = _CpyPipes[0]._ReturnedResult;
            }
            else
            {
                _ReturnedResult = _Result;
            }
            if (_embededProcess == null)
            {
                _Result._Process = _Process;
                _BackgroundRunning = true;
                _Worker = new System.Threading.Thread(_BackgroundWorker);
                _Worker.IsBackground = true;
                _ErrorWorker = new System.Threading.Thread(_BackgroundErrorWorker);
                _ErrorWorker.IsBackground = true;
                _Async = new System.Threading.Thread(_ForegroundWorker);
                _Async.IsBackground = true;
                _Async.Start();
            }
            else
            {

                _Async = new System.Threading.Thread(() => { _EmbededForegroundWorker(_Result, _PipesResults); });
                _Async.IsBackground = true;
                _Async.Start();
            }
            return _ReturnedResult;
        }

        public void Kill()
        {
            try
            {
                _Process.Kill();
            }
            catch { }
            try
            {
                _Worker.Abort();
            }
            catch { }

            try
            {
                _ErrorWorker.Abort();
            }
            catch { }

            try
            {
                foreach (Process _ in _CpyPipes) { _.Kill(); }
            }
            catch { }
        }


        Result _ReturnedResult = null;
        public Result WaitForExit()
        {
            if (_Async != null)
            {
                try
                {
                    _Async.Join();
                }
                catch { }
            }
            try
            {
                foreach (Process _ in _CpyPipes)
                {
                    try
                    {
                        _.WaitForExit();
                    }
                    catch { }
                }
            }
            catch { }
            return _ReturnedResult;
        }

        #region EmbededProcess
        class EmbededBufferW : System.IO.TextWriter
        {
            Result _result;
            bool _stdErr = false;
            List<Result> _pipeResults = new List<Result>();
            public EmbededBufferW(Result Result, bool StdErr, List<Result> Pipes)
            {
                _result = Result;
                _stdErr = StdErr;
                _pipeResults = Pipes;
            }
            public override Encoding Encoding
            {
                get { return System.Text.Encoding.ASCII; }
            }

			public override void WriteLine (string value)
			{
				Write(value + "\n");
			}

            public override void Write(char value)
            {
                Write((string)value.ToString());
            }

            public override void Write(string value)
            {
                if (_stdErr)
                {
                    _result._StdErr += value;
                }
                else
                {
                    _result._StdOut += value;
                    try
                    {
                        foreach (Result _ in _pipeResults)
                        {
							try
							{
								_.Input(value);
							}catch { }
                        }
                    }
                    catch
                    {

                    }
                }
            }

            public override void Flush() { }
            public override string ToString()
            {
                if (_stdErr)
                {
                    return _result._StdErr;
                }
                else
                {
                    return _result._StdOut;
                }
            }
            public override void Close()
            {
            }
        }

        internal class EmbededBufferR : System.IO.TextReader
        {
            int at = 0;
            string _buffer = "";
            bool _broken = false;
            public void Break()
            {
                lock (this)
                {
                    _broken = true;

                    System.Threading.Monitor.Pulse(this);
                }
            }
            public void Input(string Value)
            {
                lock (this)
                {
                    _buffer += Value;
                    System.Threading.Monitor.Pulse(this);
                }
            }
            public override int Read(char[] buffer, int index, int count)
            {
                lock (this)
                {
                    if (_broken)
                        return 0;
                    int start = at;
                    int cb = 0;
                    for (int n = start; n < _buffer.Length && cb < count; n++)
                    {
                        buffer[cb + index] = _buffer[n];
                        cb++;
                        at++;
                    }
                    return count;
                }
            }
            public override int ReadBlock(char[] buffer, int index, int count)
            {
                return Read(buffer, index, count);
            }

            public override string ReadToEnd()
            {
                lock (this)
                {
                    if (_broken)
                        return "";
                    string output = _buffer.Substring(at);
                    at = _buffer.Length;
                    return output;
                }
            }
            public override int Peek()
            {
                lock (this)
                {

                    while (at >= _buffer.Length)
                    {
                        if (_broken)
                            return -1;
                        System.Threading.Monitor.Pulse(this);
                        System.Threading.Monitor.Wait(this);
                    }
                    return _buffer[at];
                }
            }
            public override int Read()
            {
                lock (this)
                {
                    while (at >= _buffer.Length)
                    {
                        if (_broken)
                            return -1;
                        System.Threading.Monitor.Pulse(this);
                        System.Threading.Monitor.Wait(this);
                    }
                    at++;
                    return _buffer[at - 1];
                }
            }
            public override void Close()
            {
                _broken = true;
            }
        }
        void _EmbededForegroundWorker(Result Result, List<Result> Pipes)
        {

            EmbededBufferW StdOut = new EmbededBufferW(Result, false, Pipes);
            EmbededBufferW StdErr = new EmbededBufferW(Result, true, Pipes);
            EmbededBufferR StdIn = new EmbededBufferR();
			System.Threading.Thread.Sleep (100);
            if (_StartupInput != "") { StdIn.Input(_StartupInput); _StartupInput = ""; }
            Result._VirtualInput = StdIn;
            Result._Started = true;
			Result.ExitCode = _embededProcess(_embededWorkingDirectory, _embededArguments, StdIn, StdOut, StdErr);

            try
            {
                foreach (Result _ in _PipesResults)
                {
                    _.BreakPipe(_Timeout);
                }
            }
            catch { }

        }
        #endregion

		#region PROC_INFO
		static long _VirtualMemoryUsed(System.Diagnostics.Process Process)
		{
			try
			{
				long info = _LinuxVirtualMemoryUsed(Process.Id);
				if (info > 0)
				{
					return info;
				}
			}
			catch 
			{
			}

			try
			{
				return Process.PrivateMemorySize64;
			}
			catch 
			{
			}
			return 0;
		}

		static string _ProcessName(System.Diagnostics.Process Process)
		{
			try
			{
				string info = _LinuxProcessName(Process.Id);
				if (info != "")
				{
					return info;
				}
			}
			catch 
			{
			}

			try
			{
				return Process.ProcessName;
			}
			catch 
			{
			}
			return "";
		}



		#endregion

		#region LINUX_PROC_INFO

		static long _LinuxVirtualMemoryUsed(int PID)
		{
			try
			{
				if (System.IO.File.Exists ("/compat/linux/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/compat/linux/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					return long.Parse(info[23] )* 4096;
				}
				else if (System.IO.File.Exists ("/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					return long.Parse(info[23] )* 4096;
				}
			}
			catch
			{
			}
			return 0;
		}

		static long _LinuxGetUserTimeTick(int PID)
		{
			try
			{

				if (System.IO.File.Exists ("/compat/linux/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/compat/linux/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					return long.Parse(info[13]);
				}
				else if (System.IO.File.Exists ("/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					return long.Parse(info[13]);
				}
			}
			catch
			{
			}
			return 0;
		}

		static long _LinuxGetKernelTimeTick(int PID)
		{
			try
			{
				if (System.IO.File.Exists ("/compat/linux/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/compat/linux/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					return long.Parse(info[14]);
				}
				else if (System.IO.File.Exists ("/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					return long.Parse(info[14]);
				}
			}
			catch
			{
			}
			return 0;
		}

		static string _LinuxProcessName(int PID)
		{
			try
			{
				if (System.IO.File.Exists ("/compat/linux/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/compat/linux/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					string name = info[1];
					if (name.StartsWith("(")) { name = name.Substring(1); }
					if (name.EndsWith(")")) { name = name.Substring(0, name.Length - 1); }
					return (name);
				}
				else if (System.IO.File.Exists ("/proc/" + PID.ToString () + "/stat")) 
				{
					string value = System.IO.File.ReadAllText("/proc/" + PID.ToString () + "/stat");
					string[] info = value.Split(new char[]{' ', '\t', '\n', '\r'}, System.StringSplitOptions.RemoveEmptyEntries);
					string name = info[1];
					if (name.StartsWith("(")) { name = name.Substring(1); }
					if (name.EndsWith(")")) { name = name.Substring(0, name.Length - 1); }
					return (name);
				}
			}
			catch
			{
			}
			return "";
		}
		#endregion

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            _CoerceDestroy();
        }

        #endregion
    }
}
