﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Runtime
{
    class Sandbox
    {
        class Bridge : MarshalByRefObject
        {

        }

        AppDomain _LocalAppDomain = null; 
        public Sandbox(string Name)
        {
            // Ok so first we will create an empty appdomain
            AppDomainSetup AppDomainSetup = new AppDomainSetup();
            AppDomainSetup.DisallowBindingRedirects = false;
            AppDomainSetup.DisallowCodeDownload = true;
            AppDomainSetup.ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            AppDomainSetup.ConfigurationFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            _LocalAppDomain = AppDomain.CreateDomain(Name, null, AppDomainSetup);

            // Bind the load assembly event so we can take the control on the whitelisted assemblies
            _LocalAppDomain.AssemblyLoad += _LocalAppDomain_AssemblyLoad;
            _LocalAppDomain.AssemblyResolve += _LocalAppDomain_AssemblyResolve;
            _LocalAppDomain.TypeResolve += _LocalAppDomain_TypeResolve;
            _LocalAppDomain.UnhandledException += _LocalAppDomain_UnhandledException;
            _LocalAppDomain.ResourceResolve += _LocalAppDomain_ResourceResolve;
            _LocalAppDomain.ReflectionOnlyAssemblyResolve += _LocalAppDomain_ReflectionOnlyAssemblyResolve;
            _LocalAppDomain.ProcessExit += _LocalAppDomain_ProcessExit;

            // Now load fact
        }

        void Dispose()
        {

        }

        void _LocalAppDomain_ProcessExit(object sender, EventArgs e)
        {
        }

        System.Reflection.Assembly _LocalAppDomain_ReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        System.Reflection.Assembly _LocalAppDomain_ResourceResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        void _LocalAppDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
        }

        System.Reflection.Assembly _LocalAppDomain_TypeResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        System.Reflection.Assembly _LocalAppDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return null;
        }

        void _LocalAppDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
        }


    }
}
