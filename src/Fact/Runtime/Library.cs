﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Runtime
{

    public class Library
    {
        string _file = "";
        string _generatedWrapper = "";
		string _deleteFolder = "";
        ~Library()
        {
            try
            {
                if (System.IO.File.Exists(_generatedWrapper))
                {
                    System.IO.File.Delete(_generatedWrapper);
                }

            }
            catch
            { }
			try
			{
				if (_deleteFolder != "")
				{
					Tools.RecursiveDelete(_deleteFolder);
				}
			}
			catch 
			{ }
        }

        public Library(string File, params string[] Prototypes)
        {
            _file = File;
            _generateWrapper(Prototypes);
        }

		public Library(Processing.File File, params string[] Prototypes)
		{
			_deleteFolder = Tools.CreateTempDirectory ();
			_file = File.ExtractAt (_deleteFolder);
			_file = File.Name;
			_generateWrapper(Prototypes);
		}

        volatile int uid = 0;

        public Process Call(params string[] Instructions)
        {
			string instr = "";
			for (int n = 0; n < Instructions.Length; n++) 
			{
				instr += " \"" + Parser.Tools.EscapeString (Instructions [n]) + "\" ";
			}
            if (System.IO.File.Exists(_generatedWrapper))
            {
                Process process = new Process(_generatedWrapper,
				                              instr);
				try
				{
					Tools.Copy(Internal.Information.GetLibFact(), _deleteFolder + "/Fact.dll");
				}
				catch
				{}
				//Keep a handle on lib in the process for the GC
				process._keepalivelib = this;
				return process;
            }
            else
            { }
			return null;
        }

		#region ProtoHandler

		public static string _Proto_CSharp_To_C(string Prototype)
		{
			Prototype += Prototype.Trim ();

			string rettype = "";
			int n = 0;

			for (; n < Prototype.Length; n++)
			{
				if (Prototype [n] == ' ' ||
					Prototype [n] == '\t')
				{
					break;
				}
				rettype += Prototype[n];
			}

			for (; n < Prototype.Length; n++)
			{
				if (Prototype [n] != ' ' &&
				    Prototype [n] != '\t')
				{
					break;
				}
			}
            return "";

		}

		public static string _Proto_Type_CSharp_To_C(string Prototype)
		{
			//TODO
			return "";
		}

		#endregion

        #region Wrapper
        public static int SubprogramMain(object Wrapper, string[] args)
        {
            Dictionary<string, object> variables = new Dictionary<string, object>();
            Type type = Wrapper.GetType();
            for (int n = 0; n < args.Length; n++)
            {
                string funcName = "";
				string varName = "";
                object[] parameters = _extractCallInfo(args[n], variables, out funcName, out varName);
                bool found = false;

				if (funcName == "@print")
				{
					foreach (object obj in parameters)
					{
						System.Console.WriteLine (obj);
					}
					continue;
				}

				if (funcName == "@return")
				{
					foreach (object obj in parameters)
					{
						if (obj is int)
							return (int)obj;
					}
					return 0;
				}

                foreach (System.Reflection.MethodInfo method in type.GetMethods())
                {
                    
                    if (method.Name == funcName) 
                    {
                        found = true;
                        try
                        {
                            object result = method.Invoke(null, parameters);
							if (varName != "")
							{
								if (!variables.ContainsKey(varName))
									variables.Add(varName, result);
								else
									variables[varName] = result;
							}
                        }
                        catch  (Exception e)
                        {
                            Fact.Log.Error(e.ToString());
                            return 1;
                        }
                        break ;
                    }
                }
                if (!found)
                {
					Fact.Log.Error("Function not found " + funcName);
                    //return 1;
                }
            }
            return 0;
        }

        static object[] _extractCallInfo(string prototype, Dictionary<string, object> variables, out string name, out string variable)
        {
            prototype = prototype.Trim();
            int n = 0;
			variable = "";
		restart:
            name = "";
            for (; n < prototype.Length; n++)
            {
                if (prototype[n] == ' ' ||
                    prototype[n] == '\n' ||
                    prototype[n] == '\r' ||
				    prototype[n] == '(' ||
				    prototype[n] == '=')
                {
                    break;
                }
                name += prototype[n];
            }

            for (; n < prototype.Length; n++)
            {
				if (prototype [n] == '=')
				{
					variable = name;
					name = "";
					n++;

					while (n < prototype.Length &&
						   (prototype[n] == ' ' ||
					       prototype[n] == '\n' ||
					       prototype[n] == '\r'))
					{
						n++;
					}
					goto restart;

				}
                if (prototype[n] == '(') { break; }
            }
            n++;
            List<object> objects = new List<object>();

            for (; n < prototype.Length; n++)
            {
                for (; n < prototype.Length; n++)
                {
                    if (prototype[n] != ' ' &&
                        prototype[n] != '\n' &&
                        prototype[n] != '\r')
                    { break; }
                }
                if (n >= prototype.Length)
                    break;
                if (prototype[n] == ',')
                    continue;
                if (prototype[n] == ')')
                    break;
                string objectDecl = "";
                int parenthesis = 0;
				bool str = false;
				bool chr = false;
				bool esc = false;
                for (; n < prototype.Length; n++)
                {
					if (str)
					{
						objectDecl += prototype[n];
						if (esc)
						{
							esc = false;
							continue;
						}
						if (prototype [n] == '"')
							str = false;
						if (prototype [n] == '\\')
							esc = true;
						continue;
					}
					if (chr)
					{
						objectDecl += prototype[n];
						if (esc)
						{
							esc = false;
							continue;
						}
						if (prototype [n] == '\'')
							chr = false;
						if (prototype [n] == '\\')
							esc = true;
						continue;
					}
					if (prototype [n] == '"')
					{
						str = true;
					}
					if (prototype [n] == '\'')
					{
						chr = true;
					}
                    if (prototype[n] == ')')
                    {
                        parenthesis--;
                        if (parenthesis < 0)
                        { break; }
                    }
                    if (prototype[n] == '(')
                    {
                        parenthesis++;
                    }
                    if (prototype[n] == ',')
                    {
                        break;
                    }
                    objectDecl += prototype[n];
                }
                object obj = _extractObjectInfo(objectDecl, variables);
                if (obj == null)
                    obj = new IntPtr(0);
                objects.Add(obj);
                if (n >= prototype.Length)
                    break;
            }

            return objects.ToArray();
        }

        static object _extractObjectInfo(string prototype, Dictionary<string, object> variables)
        {

            int At = 0;
            prototype = prototype.Trim();

            if (prototype == "")
                return null;

            if (prototype == "NULL" ||
                prototype == "nullptr")
                return new IntPtr(0);
            //Cast like syntax
            if (prototype.ToLower().StartsWith("(uint)"))
                return (uint)Parser.Tools.ParseInt(ref At, prototype.Substring("(uint)".Length));
            if (prototype.ToLower().StartsWith("(uint32)"))
                return (UInt32)Parser.Tools.ParseInt(ref At, prototype.Substring("(uint32)".Length));
            if (prototype.ToLower().StartsWith("(long)"))
                return (long)Parser.Tools.ParseInt(ref At, prototype.Substring("(long)".Length));
            if (prototype.ToLower().StartsWith("(ulong)"))
                return (ulong)Parser.Tools.ParseInt(ref At, prototype.Substring("(ulong)".Length));

            if (prototype.StartsWith("\""))
                return Parser.Tools.ParseString(ref At, prototype);
			if (prototype.StartsWith("\'"))
				return Parser.Tools.ParseChar(ref At, prototype);
            if (prototype.Contains('.'))
                if (prototype.EndsWith("f"))
                    return Parser.Tools.ParseFloat(ref At, prototype);
                else
                    return Parser.Tools.ParseDouble(ref At, prototype);
			else if (char.IsDigit(prototype[0]) || prototype[0] == '-')
                return Parser.Tools.ParseInt(ref At, prototype);
            if (variables.ContainsKey(prototype))
            {
				return variables [prototype];
			}

            return null;
        }

        string _generateWrapperCSCode(out string WrapperName, params string[] protoype)
        {

            WrapperName = "___fact_wrap_" + (uid++).ToString();
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("using System;");
            builder.AppendLine("namespace " + WrapperName);
            builder.AppendLine("{");
            builder.AppendLine("  class wrap");
            builder.AppendLine("  {");
            foreach (string proto in protoype)
            {
                 builder.AppendLine("    [System.Runtime.InteropServices.DllImport(\"" + _file + "\")]");
                 builder.AppendLine("    public static extern " + proto + ";");
            }
            builder.AppendLine("  }");
            builder.AppendLine("  class Program");
            builder.AppendLine("  {");
            builder.AppendLine("    static int Main(string[] args)");
            builder.AppendLine("    {");
            builder.AppendLine("       return Fact.Runtime.Library.SubprogramMain(new wrap(), args);");
            builder.AppendLine("    }");
            builder.AppendLine("  }");
            builder.AppendLine("}");
            return builder.ToString();
        }

        void _generateWrapper(params string[] protoype)
        {
           if (System.CodeDom.Compiler.CodeDomProvider.IsDefinedLanguage("cs") || System.CodeDom.Compiler.CodeDomProvider.IsDefinedLanguage("csharp"))
           {
               System.CodeDom.Compiler.CodeDomProvider provider;
               if (System.CodeDom.Compiler.CodeDomProvider.IsDefinedLanguage("cs"))
               {
                   provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("cs");
               }
               else
               {
                   provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("csharp");
               }

               System.CodeDom.Compiler.CompilerParameters Parameters = new System.CodeDom.Compiler.CompilerParameters();
               Parameters.GenerateInMemory = false;
               Parameters.GenerateExecutable = true;
               Parameters.ReferencedAssemblies.Add(Internal.Information.GetLibFact());
               string filename = Fact.Internal.Information.GetTempFileName() + ".exe";

               Parameters.OutputAssembly = filename;
               

               // Patch made for cygwin
               // A bug in .NET Frameworks CodeDOM force fact to crash
               // when $TMP or $TEMP variables are set in Cygwin env
               string cygwinTMP = Internal.Information.GetEnvVariable("TMP");
               string cygwinTEMP = Internal.Information.GetEnvVariable("TEMP");

			   // Hack for linux and FreeBSD
				if (Internal.Information.CurrentOperatingSystem() ==
				    Fact.Internal.Information.OperatingSystem.Unix)
				{
					// Fix the path
					if (System.IO.Directory.Exists("/usr/local/sbin"))
						Internal.Information.AppendEnvPath("/usr/local/sbin");
					if (System.IO.Directory.Exists("/usr/local/bin"))
						Internal.Information.AppendEnvPath("/usr/local/bin");
					if (System.IO.Directory.Exists("/usr/bin"))
						Internal.Information.AppendEnvPath("/usr/bin");
					if (System.IO.Directory.Exists("/usr/sbin"))
						Internal.Information.AppendEnvPath("/usr/sbin");
				}

               try
               {
                   if (cygwinTMP != "")
                       Internal.Information.SetEnvVariable("TMP", "");
                   if (cygwinTEMP != "")
                       Internal.Information.SetEnvVariable("TEMP", "");
               }
               catch
               {
               }

               string namespaceid = "";
               string code = _generateWrapperCSCode(out namespaceid, protoype);
               Parameters.MainClass = namespaceid + ".Program";
               System.CodeDom.Compiler.CompilerResults result;
               
                try
               {
                   result = provider.CompileAssemblyFromSource(Parameters, code);
                   try
                   {
                       foreach(System.CodeDom.Compiler.CompilerError error in result.Errors)
                       {
                           Fact.Log.Error("Internal error in fact wrapper: ");
                           Fact.Log.Error("================================");
                           Fact.Log.Error(code);
                           Fact.Log.Error("================================");
                           Fact.Log.Error(error.Line + ":" + error.Column + ":" + error.ErrorText);
                       }
                   }
                   catch { }
               }
               catch
               {
               }
                if (!System.IO.File.Exists(filename))
                {
                    Fact.Log.Error("Internal error: fact has failled to generate the library");
                }
                else
                {
                    _generatedWrapper = filename;
					if (_deleteFolder == "")
					{
						_deleteFolder = Tools.CreateTempDirectory ();
					}
					try
					{
						Tools.Move(filename, _deleteFolder + "/" + filename);
						_generatedWrapper = _deleteFolder + "/" + filename;
					}
					catch{ }
                }


               try
               {
                   if (cygwinTMP != "")
                       Internal.Information.SetEnvVariable("TMP", cygwinTMP);
                   if (cygwinTEMP != "")
                       Internal.Information.SetEnvVariable("TEMP", cygwinTEMP);
               }
               catch { }
           }
        }
        #endregion
    }
}
