﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Runtime
{
	public class Diff : IDisposable
    {

		#region IDisposable Members

		void IDisposable.Dispose()
		{
			if (_A != null)
				_A._CoerceDestroy();
			if (_B != null)
				_B._CoerceDestroy();
		}

		#endregion

        Process _A = null;
        Process.Result _PrecomputedA;
		Process _B = null;
		Process.Result _PrecomputedB;
        public class Result 
        {
            bool _Equal = true;

            public bool AreEqual
            {
                get { return _Equal; }
                set { _Equal = value; }
            }

            public string Text = "";
            string _AStdOut = "";
            string _BStdOut = "";

            string _AStdErr = "";
            string _BStdErr = "";

            int _ARCode = 0;
            int _BRCode = 0;
            bool _Timeout = false;
			bool _OutOfMemory = false;
            bool _OutOfChildren = false;
            bool _StdOutIsXml = false;
            string _XmlWhy = "";

            string _StdOutName = "Standard outputs";

            public string StdOutName
            {
                get { return _StdOutName; }
                set { _StdOutName = value; }
            }
            string _StdErrName = "Error outputs";

            public string StdErrName
            {
                get { return _StdErrName; }
                set { _StdErrName = value; }
            }



            internal Result(
                string AStdOut, string BStdOut, bool StdOutIsXml,
                string AStdErr, string BStdErr,
                int ARCode, int BRCode,
                bool ATimeOut, bool BTimeOut,
				bool AOutOfMemory, bool BOutOfMemory,
                bool AOutOfChildren, bool BOutOfChildren)
            {
                _StdOutIsXml = StdOutIsXml;
                _AStdOut = AStdOut; _AStdErr = AStdErr; _ARCode = ARCode;
                _BStdOut = BStdOut; _BStdErr = BStdErr; _BRCode = BRCode;

				if (ATimeOut || AOutOfMemory || AOutOfChildren)
                { _Equal = true; }
                else if (BTimeOut)
                { _Equal = false; _Timeout = true; }
				else if (BOutOfMemory)
				{ _Equal = false; _OutOfMemory = true; }
                else if (BOutOfChildren)
                { _Equal = false; _OutOfChildren = true; }
                else
				{
                    if (_StdOutIsXml)
                    {
                        DiffXml xml = new DiffXml();
                        StringBuilder Summary = new StringBuilder();
                        bool result = xml.Xml(AStdOut, BStdOut, Summary);
                        _XmlWhy = Summary.ToString();
                        _Equal = result &&  _ErrCmp(_AStdErr, _BStdErr) && (ARCode == BRCode);
                    }
                    else
                    {
                        _Equal = (AStdOut == BStdOut) && _ErrCmp(_AStdErr, _BStdErr) && (ARCode == BRCode);
                    }
                }

            }
            

			static void _formatDiff(string _ref, string _my, StringBuilder Builder)
            {
				if (_my != _ref)
				{
                    if (_my.Length > 90 || _ref.Length > 90)
                    {
                        string[] mylines = _my.Split('\n');
                        string[] reflines = _ref.Split('\n');

                        if (mylines.Length > 4 && reflines.Length > 4)
                        {
                            int endmy = mylines.Length - 1;
                            int endref = reflines.Length - 1;

                            while (endmy > 0 && endref > 0)
                            {
                                if (mylines[endmy] != reflines[endref])
                                {
                                    endmy += 2;
                                    endref += 2;
                                    if (endmy >= mylines.Length) { endmy = mylines.Length - 1; }
                                    if (endref >= reflines.Length) { endref = reflines.Length - 1; }
                                    break;
                                }
                                endmy--;
                                endref--;
                            }
                            int startmy = 0;
                            int startref = 0;
                            while (startmy < mylines.Length && startref < reflines.Length)
                            {
                                if (mylines[startmy] != reflines[startref])
                                {
                                    startmy -= 2;
                                    startref -= 2;
                                    if (startmy < 0) { startmy = 0; }
                                    if (startref < 0) { startref = 0; }
                                    break;
                                }
                                startmy++;
                                startref++;
                            }
                            string newmy = "";
                            string newref = "";

                            if (startmy > 0) { newmy = "[...]\n"; }
                            if (startref > 0) { newref = "[...]\n"; }
                            for (int x = startmy; x <= endmy; x++)
                                newmy += mylines[x] + "\n";
                            for (int x = startref; x <= endref; x++)
                                newref += reflines[x] + "\n";
                            if (endmy < mylines.Length - 1) { newmy += "[...]\n"; }
                            if (endref < reflines.Length - 1) { newref += "[...]\n"; }

                            if (newmy != newref)
                            {
                                _my = newmy;
                                _ref = newref;
                            }
                        }
                    }

                    if (_my.Replace ("\n", "").Replace("\r", "") == _ref.Replace ("\n", "").Replace("\r", ""))
					{
						Builder.AppendLine("End of lines are replaced by '$'");
						Builder.AppendLine("   ++ref " + _ref.Replace ("\n", "$").Replace("\r", "^M"));
                        Builder.AppendLine("   --my  " + _my.Replace("\n", "$").Replace("\r", "^M"));
					}
                    else if (_my.Replace (" ", "") == _ref.Replace (" ", ""))
				    {
					    Builder.AppendLine ("Spaces are replaced by '.'");
					    Builder.AppendLine("   ++ref " + _ref.Replace (" ", "."));
					    Builder.AppendLine("   --my  " + _my.Replace (" ", "."));
				    }
					else
					{

                        if (_ref.Length > 2048) { Builder.AppendLine("  --ref  [Too long]"); }
                        else { Builder.AppendLine("   ++ref " + _ref); }
						
                        if (_my.Length > 2048) { Builder.AppendLine("   --my  [Too long]"); }
                        else { Builder.AppendLine("   --my  " + _my); }
					}
				}
				else
				{

				}
            }

			
			/// <summary>
			/// Check if both output are assertion
			/// </summary>
			/// <returns><c>true</c>, if the inputs are asserts, <c>false</c> otherwise.</returns>
			/// <param name="A">A.</param>
			/// <param name="B">B.</param>
			bool _AssertCmpExt(string A, string B)
			{
				if (A != B) 
				{
					if (A.ToLower ().Contains ("assertion"))
					{
						if (B.ToLower ().Contains ("assertion"))
						{
							if (A.ToLower ().Contains ("failed"))
							{
								if (B.ToLower ().Contains ("failed"))
								{
									return true;
								}
							}
						}
					}
					return false;
				}
				return true;
			}

			bool _ErrCmp(string A, string B)
			{
				return _AssertCmpExt (A, B);
			}

            public string Summary
            {
                get
                {
                    if (_Equal)
                    { return "No differences"; }
                    else if (_Timeout)
                    { return "Timeout"; }
					else if (_OutOfMemory)
					{ return "Out of memory"; }
                    else if (_OutOfChildren)
                    { return "Out of child handles"; }
                    else
                    {
                        StringBuilder Builder = new StringBuilder();
                        if (_StdOutIsXml)
                        {
                            if (_XmlWhy.Trim() != "")
                            {
                                Builder.AppendLine("XML are not equals :");
                                Builder.AppendLine(_XmlWhy);
                            }
                        }
                        else if (_AStdOut != _BStdOut)
                        {
                            Builder.AppendLine(_StdOutName + " are not equals :");
							_formatDiff (_AStdOut, _BStdOut, Builder);
                        }
						if (!_ErrCmp(_AStdErr, _BStdErr))
                        {
                            Builder.AppendLine(_StdErrName + " are not equals :");
							_formatDiff (_AStdErr, _BStdErr, Builder);
                        }
                        if (_ARCode != _BRCode)
                        {
                            Builder.AppendLine("Exit Codes are not equals :");
							Builder.AppendLine("   ++ref " + _ARCode.ToString());
							Builder.AppendLine("   --my  " + _BRCode.ToString());
                        }
                        string result = Builder.ToString();
                        if (result.EndsWith("\n") || result.EndsWith("\r"))
                        { result = result.Substring(0, result.Length - 1); }
                        return result;
                    }
                }
            }

            public static implicit operator Test.Result.Result(Result Result)
            {
                if (Result.AreEqual)
                {
                    return new Test.Result.Passed(Result.Text, Result.Summary);
                }
                else
                {
                    return new Test.Result.Error(Result.Text, Result.Summary);
                }
            }

            public static implicit operator bool(Result Result)
            { return Result._Equal; }
        }

		bool _checkTimeOut = true;
		public bool CheckTimeout
		{
			get { return _checkTimeOut; }
			set { _checkTimeOut = value; }
		}

        bool _checkStdOut = true;

        public bool CheckStdOut
        {
            get { return _checkStdOut; }
            set { _checkStdOut = value; }
        }

        bool _checkStdErr = true;

        public bool CheckStdErr
        {
            get { return _checkStdErr; }
            set { _checkStdErr = value; }
        }

        bool _checkExitCode = true;

        public bool CheckExitCode
        {
            get { return _checkExitCode; }
            set { _checkExitCode = value; }
        }

        bool _formatTrim = true;

        public bool FormatTrim
        {
            get { return _formatTrim; }
            set { _formatTrim = value; }
        }

        bool _formatEndOfLine = true;

        public bool FormatEndOfLine
        {
            get { return _formatEndOfLine; }
            set { _formatEndOfLine = value; }
        }

        bool _formatSpace = true;

        public bool FormatSpace
        {
            get { return _formatSpace; }
            set { _formatSpace = value; }
        }

		bool _formatStdOutOnlySet = false;
		public bool FormatStdOutOnlySet
		{
			get { return _formatStdOutOnlySet; }
			set { _formatStdOutOnlySet = value; }
		}

		bool _formatStdErrOnlySet = false;
		public bool FormatStdErrOnlySet
		{
			get { return _formatStdErrOnlySet; }
			set { _formatStdErrOnlySet = value; }
		}

        bool _StdOutIsXML = false;
        public bool StdOutIsXML
        {
            get { return _StdOutIsXML; }
            set { _StdOutIsXML = value; }
        }

        string _internalFormatSpace(string input)
        {
            StringBuilder output = new StringBuilder();
            bool space = false;
            for (int n = 0; n < input.Length; n++)
            {
                if (space)
                {
                    if (input[n] != ' ' && input[n] != '\t')
                    {
                        output.Append(input[n]);
                        space = false;
                    }
                }
                else
                {
                    output.Append(input[n]);
                    if (input[n] == ' ' || input[n] == '\t')
                    {
                        space = true;
                    }
                }
            }

            return output.ToString();
        }

		string _formatStdout(string input)
		{
			input = _format (input);
			if (_formatStdOutOnlySet) 
			{
				if (input == "") 
				{
					input = "[Empty]";
				}
				else
				{
					input = "[Not Empty]";
				}
			}
			return input;
		}

		string _formatStderr(string input)
		{
			input = _format (input);
			if (_formatStdErrOnlySet) 
			{
				if (input == "") 
				{
					input = "[Empty]";
				}
				else
				{
					input = "[Not Empty]";
				}
			}
			return input;
		}

        string _format(string input)
        {
            if (_formatTrim) { input = input.Trim(); }
            if (_formatEndOfLine) { input = input.Replace("\n\r", "\n"); }
            if (_formatSpace) { input = _internalFormatSpace(input); }
            return input;
        }

        string _StartupInput = "";

        public Diff(Process Ref, Process Test)
        {
            _PrecomputedA = null;
			_PrecomputedB = null;
            _A = Ref;
            _B = Test;
        }

        public Diff(Process.Result RefResul, Process Test)
        {
            _PrecomputedA = RefResul;
            _A = null;
            _B = Test;
			_PrecomputedB = null;
        }

		public Diff(Process.Result RefResul, Process.Result TestResult)
		{
			_PrecomputedA = RefResul;
			_A = null;
			_B = null;
			_PrecomputedB = TestResult;
		}

        public Result Run(int Timeout)
        {
            return Run(Timeout, "");
        }
        public Result Run(int Timeout, string Input)
        {
            return Run(Timeout, Input, false);
        }

		public Result Run(int Timeout, string Input, bool CloseInput)
		{
			return Run (Timeout, Input, CloseInput, 3);
		}

        private Result Run(int Timeout, string Input, bool CloseInput, int tries)
        {
                Process.Result resultA = null;
                if (_A != null)
                {
					try
					{
                    	resultA = _A.Run(Timeout, Input, CloseInput);
                    	_A._CoerceDestroy();
					}
					catch (Exception e)
					{
						resultA = new Process.Result ();
						Fact.Log.Error ("Reference process crash ...");
						if (tries > 0)
						{
							System.GC.WaitForPendingFinalizers ();
							System.Threading.Thread.Sleep(1000);
							return Run(Timeout, Input, CloseInput, tries - 1);
						}
					}
                }
                else if (_PrecomputedA != null)
                    resultA = _PrecomputedA;

                Process.Result resultB = null;
				if (_B != null)
				{
					try
					{
	                	resultB = _B.Run(Timeout, Input, CloseInput);
						_B._CoerceDestroy();
					}
					catch 
					{
						resultB = new Process.Result ();
						Fact.Log.Error ("Student process crash ...");
						if (tries > 0)
						{
							System.GC.WaitForPendingFinalizers ();
							System.Threading.Thread.Sleep(1000);
							return Run(Timeout, Input, CloseInput, tries - 1);
						}
					}
				}
				else if (_PrecomputedB != null)
					resultB = _PrecomputedB;

				string AStdErr = _checkStdErr ? _formatStderr(resultA.StdErr) : "";
				string BStdErr = _checkStdErr ? _formatStderr(resultB.StdErr) : "";

				string AStdOut = _checkStdOut ? _formatStdout(resultA.StdOut) : "";
				string BStdOut = _checkStdOut ? _formatStdout(resultB.StdOut) : "";

                int AExitCode = _checkExitCode ? resultA.ExitCode : 0;
                int BExitCode = _checkExitCode ? resultB.ExitCode : 0;

                return new Result(AStdOut, BStdOut, _StdOutIsXML, AStdErr, BStdErr, AExitCode, BExitCode, resultA.TimeOutExit, resultB.TimeOutExit, resultA.OutOfMemoryExit, resultB.OutOfMemoryExit, resultA.OutOfChildrenExit, resultB.OutOfChildrenExit);
        }

        public static bool DiffFile(string A, string B)
        {
            try
            {
                if (System.IO.File.Exists(A))
                {
                    if (System.IO.File.Exists(B))
                    {
                        byte[] ArrayA = System.IO.File.ReadAllBytes(A);
                        byte[] ArrayB = System.IO.File.ReadAllBytes(B);
                        if (ArrayA.Length != ArrayB.Length)
                        { return false; }
                        int length = ArrayA.Length;
                        for (int n = 0; n < length; n++)
                        {
                            if (ArrayA[n] != ArrayB[n])
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                    else
                    { return false; }
                }
                else
                {
                    if (!System.IO.File.Exists(B))
                    { return true; }
                    return false;
                }
            }
            catch { return false; }
        }
    }
}
