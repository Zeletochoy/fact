﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Script
{
    public class Script
    {
        Dictionary<string, Function> _functions = new Dictionary<string, Function>();

        public Dictionary<string, Function> Functions
        {
            get { return _functions; }
        }

        Dictionary<string, Test> _tests = new Dictionary<string, Test>();

        public Dictionary<string, Test> Tests
        {
            get { return _tests; }
        }

        Dictionary<string, TestSuite> _testsuites = new Dictionary<string, TestSuite>();

        public Dictionary<string, TestSuite> TestSuites
        {
            get { return _testsuites; }
        }

        public static Script LoadFromFile(string file)
        {
            return Parser.Parse(System.IO.File.Open(file, System.IO.FileMode.Open));
        }
    }
}
