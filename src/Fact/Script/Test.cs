﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Script
{
    public class Test
    {
        string _Name = "";

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        internal string _Refbin = "";
        internal string _Testbin = "";
        internal string _args = "";
        internal string _input = "";

        internal string _exists = "";

        internal int _timeout = 2000;

        internal bool _checkstdout = true;
        internal bool _checkstderr = false;
        internal bool _checkexitcode = false;


        public void Run(Processing.Project RefProject, Processing.Project TestedProject)
        {
            if (TestedProject != null && _exists != "")
            {
                Fact.Processing.File exist = TestedProject.GetFile(_exists);
                if (exist == null)
                {
                    Fact.Log.TestFail("File " + _exists + " is not present");
                    TestedProject.AddTestResult(
                        new Fact.Test.Result.Error(
                            _Name,
                            "File " + _exists + " is not present"));
                }
                else
                {
                    Fact.Log.TestPass("File " + _exists + " is present");
                    TestedProject.AddTestResult(
                        new Fact.Test.Result.Passed(
                            _Name,
                            "File " + _exists + " is present"));
                }
            }
            if (RefProject == null)
            {
                Fact.Log.Error("[" + _Name + "]" + ": missing reference package.");
                return;
            }
            if (TestedProject == null)
            {
                Fact.Log.Error("[" + _Name + "]" + ": missing test package.");
                return;
            }
            if (_Refbin != "" && _Testbin != "")
            {
                Fact.Processing.File refbin = RefProject.GetFile(_Refbin);
                Fact.Processing.File testbin = TestedProject.GetFile(_Testbin);
                if (refbin == null)
                {
                    Fact.Log.Error("[" + _Name + "]" + ": file not found " + _Refbin + " in reference package");
                    return;
                }

                if (testbin == null)
                {
                    Fact.Log.TestFail("file not found " + _Testbin + " in tested package");
                    return;
                }

                Fact.Log.Info("[" + _Name + "]" + " start testing:");
                Fact.Runtime.Process refProcess = new Fact.Runtime.Process(refbin, _args);
                Fact.Runtime.Process testProcess = new Fact.Runtime.Process(testbin, _args);
                Fact.Runtime.Diff diff = new Fact.Runtime.Diff(refProcess, testProcess);
                diff.CheckStdOut = _checkstdout;
                diff.CheckStdErr = _checkstderr;
                diff.CheckExitCode = _checkexitcode;
                Fact.Runtime.Diff.Result result = diff.Run(_timeout, _input);
                if (result.AreEqual)
                {
                    Fact.Log.TestPass(result.Summary);
                    testbin.AddTestResult(
                        new Fact.Test.Result.Passed(
                            _Name,
                            result.Summary));
                }
                else
                {
                    Fact.Log.TestFail(result.Summary);
                    testbin.AddTestResult(
                        new Fact.Test.Result.Error(
                            _Name,
                            result.Summary));
                } 


            }
            else
            {
                Fact.Log.Warning("[" + _Name + "]" + ": empty test.");
 
            }

        }
    }
}
