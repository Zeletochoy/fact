﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Script
{
    class Lexer
    {
        System.IO.Stream _stream;
        string buffer = "";
        string next = "";
        string waitToken = "";
        char nextChar = (char)0;
        int pos = 0;

        public Lexer(System.IO.Stream Stream)
        {
            _stream = Stream;
        }
        void LoadBuffer()
        {
            if (pos >= buffer.Length)
            {
                pos = 0;
                buffer = "";
                byte[] data = new byte[1024];
                int cb = _stream.Read(data, 0, data.Length);
                buffer = System.Text.UTF8Encoding.UTF8.GetString(data, 0, cb);
            }
        }
        public char EatChar()
        {
            if (nextChar != 0)
            {
                char tmp = nextChar;
                nextChar = (char)0;
                return tmp;
            }
            if (pos >= buffer.Length)
                LoadBuffer();
            while (buffer.Length == 0)
            {
                try
                {
                    if (_stream.Position >= _stream.Length)
                        return (char)0;
                }
                catch { }
                System.Threading.Thread.Sleep(10);
                LoadBuffer();
            }
            char value = buffer[pos];
            pos++;
            return value;
        }
        public char GetChar()
        {
            char chr = EatChar();
            nextChar = chr;
            return chr;
        }
        public bool EatBool()
        {
            string value = EatToken();
            if (value.ToLower() == "yes") { return true; }
            if (value.ToLower() == "no") { return false; }
            if (value.ToLower() == "true") { return true; }
            if (value.ToLower() == "false") { return false; }
            if (value.ToLower() == "1") { return true; }
            if (value.ToLower() == "0") { return false; }
            throw new Exception("Expected boolean got " + value);
        }
        public int EatInt()
        {
            string value = EatToken();
            try
            {
                
                return int.Parse(value);
            }
            catch
            {
                throw new Exception("Expected integer got " + value);
            }
        }
        public void EatSpace()
        {
            char value;
            do
            {
                value = GetChar();
                if (value == ' ' || value == '\t' || value == '\n' || value == '\r')
                {
                    EatChar();
                    continue;
                }
                break;
            } while (value > 0);
        }
        public string GetToken()
        {
            string token = EatToken();
            waitToken = token;
            return token;
        }

        public string EatToken()
        {
            if (waitToken != "")
            {
                waitToken = waitToken.Trim();
                if (waitToken != "")
                {
                    string tmp = waitToken;
                    waitToken = "";
                    return tmp;
                }
            }
            if (next != "")
            {
                next = next.Trim();
                if (next != "")
                {
                    string tmp = next;
                    next = "";
                    return tmp;
                }
            }
            StringBuilder builder = new StringBuilder();
            char value;
            bool isstring = false;
            bool esc = false;
            do
            {
                value = EatChar();
                if (isstring)
                {
                    if (esc)
                    {
                        switch (value)
                        {
                            case '\\': builder.Append('\\'); break;
                            case 'n': builder.Append('\n'); break;
                            case 'r': builder.Append('\r'); break;
                            case 't': builder.Append('\t'); break;
                            case '0': builder.Append('\0'); break;
                            default: builder.Append(value); break;
                        }
                        esc = false;
                        continue;
                    }
                    else if (value == '"')
                    {
                        isstring = false;
                        continue;
                    }
                    else if (value == '\\')
                    {
                        esc = true;
                        continue;
                    }
                    builder.Append(value);
                    continue;
                }
                if (value == '"')
                {
                    isstring = true;
                    continue;
                }
                else if (value == ' ' || value == '\t' || value == '\n' || value == '\r')
                {
                    EatSpace();
                    if (builder.Length > 0)
                    {
                        return builder.ToString();
                    }
                }
                else if (value == ';' ||
                    value == ',' ||
                    value == '|' ||
                    value == '(' ||
                    value == ')' ||
                    value == '{' ||
                    value == '}' ||
                    value == '[' ||
                    value == '=' ||
                    value == ']')
                {
                    if (builder.Length > 0)
                    {
                        next = value.ToString();
                        return builder.ToString();
                    }
                    else
                    {
                        return value.ToString();
                    }
                }
                else
                {
                    builder.Append(value);
                }

            } while (value > 0);
            return "";
        }

        public void Expect(string Token)
        {
            string realToken = EatToken();
            if (realToken != Token)
            { throw new Exception("Expected " + Token + " got " + realToken); }
        }
    }
}
