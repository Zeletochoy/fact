﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Script
{
    class Parser
    {
        static Function ParseFunction(Lexer lexer, Script script)
        {
            lexer.Expect("}");
            return null;
        }

        static Test ParseTest(Lexer lexer, Script script)
        {
            Test test = new Test();
            string Token = lexer.EatToken();
            while (Token != "}")
            {
                if (Token == "refbin") { lexer.Expect("="); test._Refbin = lexer.EatToken(); lexer.Expect(";"); }
                else if (Token == "testbin") { lexer.Expect("="); test._Testbin = lexer.EatToken(); lexer.Expect(";"); }
                else if (Token == "checkstdout") { lexer.Expect("="); test._checkstdout = lexer.EatBool(); lexer.Expect(";"); }
                else if (Token == "checkstderr") { lexer.Expect("="); test._checkstderr = lexer.EatBool(); lexer.Expect(";"); }
                else if (Token == "checkexitcode") { lexer.Expect("="); test._checkexitcode = lexer.EatBool(); lexer.Expect(";"); }
                else if (Token == "timeout") { lexer.Expect("="); test._timeout = lexer.EatInt(); lexer.Expect(";"); }
                else if (Token == "exists") { lexer.Expect("="); test._exists = lexer.EatToken(); lexer.Expect(";"); }
				else if (Token == "import") { lexer.Expect("="); test._exists = lexer.EatToken(); lexer.Expect(";"); }
                else if (Token == "finput")
                {
                    lexer.Expect("=");
                    string file = lexer.EatToken();
                    if (!System.IO.File.Exists(file))
                        Fact.Log.Error("Impossible to find file " + file);
                    else
                        test._input = System.IO.File.ReadAllText(file);
                    lexer.Expect(";");
                }
                else if (Token == "fargs")
                {
                    lexer.Expect("=");
                    string file = lexer.EatToken();
                    if (!System.IO.File.Exists(file))
                        Fact.Log.Error("Impossible to find file " + file);
                    else
                        test._args = System.IO.File.ReadAllText(file);
                    lexer.Expect(";");
                }
                else if (Token == "args")
                {
                    lexer.Expect("=");
                    string args = "";
                    Token = lexer.EatToken();
                    while (Token != ";" && Token != "}")
                    {
                        args += Token + " ";
                        Token = lexer.EatToken();
                    }
                    test._args = args;
                }
                else if (Token == "input")
                {
                    lexer.Expect("=");
                    string input = "";
                    Token = lexer.EatToken();
                    while (Token != ";" && Token != "}")
                    {
                        input += Token;
                        Token = lexer.EatToken();
                    }
                    test._input = input;
                }
                else
                {
                    Fact.Log.Warning("Ignored token: " + Token);
                }

                Token = lexer.EatToken();
            }
            return test;
        }

        static TestSuite ParseTestSuite(Lexer lexer, Script script)
        {
            TestSuite testsuite = new TestSuite();
            string Token = lexer.EatToken();
            while (Token != "}")
            {
                string blockType = Token;
                string blockName = lexer.EatToken();
                lexer.Expect("{");
                switch (blockType)
                {
                    case "test":
                        {
                            Test test = ParseTest(lexer, script);
                            if (test != null)
                            {
                                test.Name = blockName;
                                testsuite.Tests.Add(test.Name, test);
                            }
                        }
                        break;
					case "mount":
						{
							lexer.Expect("=");
							string args = lexer.EatToken ();
							lexer.Expect(";");
							if (System.IO.File.Exists(args))
							{
								try
								{
									Fact.Processing.Project project = new Fact.Processing.Project("");
									project.Load(args);
								}
								catch
								{
									Fact.Log.Error ("Impossible to load project " + args);
								}
							}
							else
							{
								Fact.Log.Error ("The file " + args + " does not exist");
							}
						}
						break;
                }
                Token = lexer.EatToken();
            }
            return testsuite;
        }

        static void ParseBlock(Lexer lexer, Script script)
        {
            string blockType = lexer.EatToken();
            string blockName = lexer.EatToken();
            lexer.Expect("{");
            switch (blockType)
            {
                case "function":
                    {
                        Function function = ParseFunction(lexer, script);
                        if (function != null)
                        {
                            function.Name = blockName;
                            script.Functions.Add(function.Name, function);
                        }
                    }
                    break;
                case "test":
                    {
                        Test test = ParseTest(lexer, script);
                        if (test != null)
                        {
                            test.Name = blockName;
                            script.Tests.Add(test.Name, test);
                        }
                    }
                    break;
                case "testsuite":
                    {
                        TestSuite testsuite = ParseTestSuite(lexer, script);
                        if (testsuite != null)
                        {
                            testsuite.Name = blockName;
                            script.TestSuites.Add(testsuite.Name, testsuite);

                        }
                    }
                    break;
            }
        }

        public static Script Parse(System.IO.Stream Stream)
        {
            Script script = new Script();
            Lexer lexer = new Lexer(Stream);
            while (lexer.GetToken() != "")
            {
                ParseBlock(lexer, script);
            }
            return script;
        }
    }
}
