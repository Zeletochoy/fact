﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Script
{
    public class TestSuite
    {
        string _Name = "";

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        Dictionary<string, Test> _tests = new Dictionary<string, Test>();
        public Dictionary<string, Test> Tests
        {
            get { return _tests; }
        }
    }
}
