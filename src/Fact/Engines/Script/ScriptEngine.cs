﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Engines.Script
{
    public class ScriptEngine
    {
        Scope _GlobalScope = null;
        public Scope GlobalScope { get { return _GlobalScope; } }
        public ScriptEngine()
        {
            _GlobalScope = new Scope(this);
        }
    }
}
