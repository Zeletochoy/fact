﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Engines.Script
{
    public class Scope
    {
        ScriptEngine _Engine = null;
        public Scope(ScriptEngine Engine)
        {
            _Engine = Engine;
        }

        Scope _Parent = null;
        public Scope Parent { get { return _Parent; } }

        List<Scope> _Children = new List<Scope>();
        public List<Scope> Children { get { return _Children; } }

        Dictionary<string, object> _Variables = new Dictionary<string, object>();
        public object GetVariableValue(string Variable)
        {
            if (_Variables.ContainsKey(Variable)) { return _Variables[Variable]; }
            if (_Parent != null) { return GetVariableValue(Variable); }
            return null;
        }

        Dictionary<string, string> ToEnvironment()
        {
            Dictionary<string, string> environment = new Dictionary<string, string>();
            {
                foreach (KeyValuePair<string, object> var in _Variables)
                {
                    if (var.Value == null) { environment.Add(var.Key, ""); }
                    else { environment.Add(var.Key, var.Value.ToString()); }
                }
            }
            if (_Parent != null)
            {
                foreach (KeyValuePair<string, string> var in _Parent.ToEnvironment())
                {
                    if (!environment.ContainsKey(var.Key)) { environment.Add(var.Key, var.Value); }
                }
            }
            return environment;
        }

        public Fact.Runtime.Process.Result InvokeProcess(string File, string Arguments)
        {
            Fact.Runtime.Process Process = new Runtime.Process(File, Arguments);
            try
            {
                foreach (KeyValuePair<string, string> var in ToEnvironment())
                {
                    Process.EnvironmentVariables.Add(var.Key, var.Value);
                }
            }
            catch { }
            return Process.Run(-1);

        }
    }
}
