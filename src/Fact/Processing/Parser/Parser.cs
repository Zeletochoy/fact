﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Processing.Parser
{
    public class Parser
    {
        public class ParseElement
        {
            public virtual bool Match(Character.MetaToken Token) { return false; }
            public static implicit operator ParseElement(string String)
            {
                return new ParseElementRawString() { String = String };
            }
            public static implicit operator ParseElement(Character.MetaChar.BasicTokenType Type)
            {
                return new ParseElementTokenType() { Type = Type };
            }
            public static implicit operator ParseElement(Parser Parser)
            {
                return new ParseElementParser() { SubParser = Parser };
            }
            public static ParseElement Any { get { return new ParseElementAny(); } }
            public static ParseElement AnyExcept(params string[] Except)
            {
                return new ParseElementAny() { Except = Except }; 
            }
            public static ParseElement Identifier { get { return new ParseElementId(); } }

            public static ParseElement Star { get { return new ParseElementStar(); } }
            public static ParseElement StarExcept(params string[] Except)
            {
                return new ParseElementStar() { Except = Except }; 
            }
            public static ParseElement GreadyStarExcept(params string[] Except)
            {
                return new ParseElementStar() { Except = Except, Gready = true};
            }
            public static ParseElement Braces(string Open, string Close)
            {
                return new ParseElementBraces() { Open = Open, Close = Close };
            }

            public static ParseElement List(Parser Parser)
            {
                return new ParseElementParser() { SubParser = Parser, Once = false};
            }

            public override string ToString()
            {
                return "";
            }
            
        }
        class ParseElementRawString : ParseElement
        {
            internal string String = "";
            public override bool Match(Character.MetaToken Token)
            {
                return Token == String;
            }
            public override string ToString()
            {
                return String;
            }
        }

        class ParseElementTokenType : ParseElement
        {
            internal Character.MetaChar.BasicTokenType Type = Character.MetaChar.BasicTokenType.None;
            public override bool Match(Character.MetaToken Token)
            {
                return Token.TokenType == Type;
            }
            public override string ToString()
            {
                return Type.ToString();
            }
        }

        class ParseElementParser : ParseElement
        {
            internal bool Once = true;
            internal Parser SubParser = null;
            public override bool Match(Character.MetaToken Token)
            {
                return true;
            }
            public override string ToString()
            {
                return "( " + SubParser.ToString() + " )";
            }
        }


        class ParseElementBraces : ParseElement
        {
            internal string Open = "";
            internal string Close = "";
            public override bool Match(Character.MetaToken Token)
            {
                return Token == Open;
            }
            public override string ToString()
            {
                return Open + "*" + Close;
            }
        }

        class ParseElementAny : ParseElement
        {
            internal string[] Except = new string[0];
            public override bool Match(Character.MetaToken Token)
            {
                for (int n = 0; n < Except.Length; n++)
                {
                    if (Except[n] == Token) { return false; }
                }
                return true;
            }
            public override string ToString()
            {
                return "?";
            }
        }

        class ParseElementId : ParseElement
        {
            public override bool Match(Character.MetaToken Token)
            {
                if (Token.TokenType == Character.MetaChar.BasicTokenType.KeyWord) { return false; }
                if (Token.TokenType == Character.MetaChar.BasicTokenType.Null) { return false; }

                string str = Token.ToString();
                if (str.Length == 0) { return false; }
                if (str[0] >= 'a' && str[0] <= 'z' ||
                    str[0] >= 'A' && str[0] <= 'Z' ||
                    str[0] == '_' ||
                    str[0] == '@')
                {
                    for (int n = 1; n < str.Length; n++)
                    {
                        if (str[n] >= 'a' && str[n] <= 'z' ||
                            str[n] >= 'A' && str[n] <= 'Z' ||
                            str[n] >= '0' && str[n] <= '9' ||
                            str[n] == '_' ||
                            str[n] == '@')
                        {
                            continue;
                        }
                        return false;
                    }
                    return true;
                }
                return false;
            }
            public override string ToString()
            {
                return "Id";
            }
        }

        class ParseElementStar : ParseElement
        {
            internal string[] Except = new string[0];
            internal bool Gready = false;
            public override bool Match(Character.MetaToken Token)
            {
                for (int n = 0; n < Except.Length; n++)
                {
                    if (Except[n] == Token) { return false; }
                }
                return true;
            }
            public override string ToString()
            {
                return "*";
            }
        }

        public delegate void Callback(Lexer.Lexer[] Captures);
        public Parser()
        {
        }
        List<KeyValuePair<ParseElement[], Callback>> _Rules = new List<KeyValuePair<ParseElement[], Callback>>();
        public void AddRule(Callback Callback, params ParseElement[] Elements)
        {
            if (Elements.Length == 0) { return; }
            _Rules.Add(new KeyValuePair<ParseElement[], Callback>(Elements, Callback));
        }
        public void AddRule(params ParseElement[] Elements)
        {
            if (Elements.Length == 0) { return; }
            _Rules.Add(new KeyValuePair<ParseElement[], Callback>(Elements, (Lexer.Lexer[] ignored) => { }));
        }
        bool Match(Lexer.Lexer Lexer, ParseElement[] Rule, int RuleOffset, ref int TokenOffset, List<Processing.Lexer.Lexer> Captures)
        {
            next:
            if (RuleOffset >= Rule.Length) { return false; }
            Character.MetaToken token = Lexer.GetToken(TokenOffset);
            if ((object)token == null) { return false; }
            if (!Rule[RuleOffset].Match(token)) { return false; }
            if (Rule[RuleOffset] is ParseElementRawString ||
                Rule[RuleOffset] is ParseElementTokenType ||
                Rule[RuleOffset] is ParseElementAny ||
                Rule[RuleOffset] is ParseElementId) 
            {
                if (Rule[RuleOffset] is ParseElementAny ||
                    Rule[RuleOffset] is ParseElementId)
                { Captures.Add(Lexer.CreateSubLexer(TokenOffset, TokenOffset + 1)); }
                TokenOffset++;
                if (RuleOffset == Rule.Length - 1) { return true; }
                RuleOffset++;
                goto next;
            }
            if (Rule[RuleOffset] is ParseElementStar)
            {
                bool gready = (Rule[RuleOffset] as ParseElementStar).Gready;
                // Just keep the minimal capture
                if (!gready)
                {
                    if (RuleOffset == Rule.Length - 1)
                    {
                        Captures.Add(Lexer.CreateSubLexer(TokenOffset, TokenOffset));
                        return true;
                    }
                }
                List<Lexer.Lexer> newCaptures = new List<Processing.Lexer.Lexer>();
                List<Lexer.Lexer> lastCaptures = new List<Processing.Lexer.Lexer>();
                int lastTokenOffset = -1;
                int lastStartTokenOffset = -1;

                int startToken = TokenOffset;

                while (true)
                {
                    int newTokenOffset = TokenOffset;
                    if (gready)
                    {
                        TokenOffset++;
                        token = Lexer.GetToken(TokenOffset);
                        if ((object)token == null ||
                            !Rule[RuleOffset].Match(token))
                        {
                            if (RuleOffset == Rule.Length - 1)
                            {
                                Captures.Add(Lexer.CreateSubLexer(startToken, TokenOffset));
                                return true;
                            }
                            else
                            {
                                // The gready match has failed and we have no backup
                                if (lastTokenOffset < 0) { return false; }
                                // If we have a backupt then we use it
                                Captures.Add(Lexer.CreateSubLexer(startToken, lastStartTokenOffset));
                                Captures.AddRange(lastCaptures);
                                TokenOffset = lastTokenOffset;
                                return true;
                            }
                        }
                        if (Match(Lexer, Rule, RuleOffset + 1, ref newTokenOffset, newCaptures))
                        {
                            lastCaptures = new List<Lexer.Lexer>(lastCaptures);
                            lastTokenOffset = newTokenOffset;
                            lastStartTokenOffset = TokenOffset;
                            return true;
                        }
                    }
                    else
                    {
                        if (Match(Lexer, Rule, RuleOffset + 1, ref newTokenOffset, newCaptures))
                        {
                            Captures.Add(Lexer.CreateSubLexer(startToken, TokenOffset));
                            TokenOffset = newTokenOffset;
                            Captures.AddRange(newCaptures);
                            return true;
                        }
                        else
                        {
                            TokenOffset++;
                            token = Lexer.GetToken(TokenOffset);
                            if ((object)token == null) { return false; }
                            if (!Rule[RuleOffset].Match(token)) { return false; }
                        }
                    }
                }
            }
            if (Rule[RuleOffset] is ParseElementBraces)
            {
                string open = (Rule[RuleOffset] as ParseElementBraces).Open;
                string close = (Rule[RuleOffset] as ParseElementBraces).Close;
                TokenOffset++;
                int StartToken = TokenOffset;
                int count = 1;
                while (true)
                {
                    Character.MetaToken newtoken = Lexer.GetToken(TokenOffset);
                    TokenOffset++;
                    if ((object)newtoken == null) { return false; }
                    if (newtoken == open) { count++; }
                    else if (newtoken == close)
                    {
                        count--;
                        if (count <= 0) { break; }
                    }
                }
                Captures.Add(Lexer.CreateSubLexer(StartToken, TokenOffset - 1));
                if (RuleOffset == Rule.Length - 1) { return true; }
                RuleOffset++;
                goto next;
            }
            if (Rule[RuleOffset] is ParseElementParser)
            {
                bool once = (Rule[RuleOffset] as ParseElementParser).Once;
                int SubLength = 0;
                if (!once)
                {
                    if (!(Rule[RuleOffset] as ParseElementParser).SubParser._MatchAll(Lexer.CreateSubLexer(TokenOffset), out SubLength))
                    { return false; }
                    else
                    {
                        Captures.Add(Lexer.CreateSubLexer(TokenOffset, SubLength + TokenOffset));
                        TokenOffset += SubLength;
                        if (RuleOffset == Rule.Length - 1) { return true; }
                        RuleOffset++;
                        goto next;
                    }
                }
                else
                {
                    Callback Ignore1 = null;
                    Lexer.Lexer[] Ignore2 = null;
                    if (!(Rule[RuleOffset] as ParseElementParser).SubParser._MatchOne(Lexer.CreateSubLexer(TokenOffset),out Ignore1, out Ignore2, out SubLength))
                    { return false; }
                    else
                    {
                        Captures.Add(Lexer.CreateSubLexer(TokenOffset, SubLength + TokenOffset));
                        TokenOffset += SubLength;
                        if (RuleOffset == Rule.Length - 1) { return true; }
                        RuleOffset++;
                        goto next;
                    }
                }
            }
            return false;
        }

        bool _MatchOne(Lexer.Lexer Lexer, out Callback Callback, out Lexer.Lexer[] Captures, out int Length)
        {
            bool matched = false;
            List<Processing.Lexer.Lexer> SelectedCaptures = new List<Processing.Lexer.Lexer>();
            int SelectedLength = int.MaxValue;
            if (_biggestMatch)
            {
                SelectedLength = -1;
            }
            Callback SelectedCallback = null;
            List<KeyValuePair<ParseElement[], Callback>> potentialRules = new List<KeyValuePair<ParseElement[], Callback>>();
            foreach (KeyValuePair<ParseElement[], Callback> rule in _Rules)
            {
                List<Processing.Lexer.Lexer> newCaptures = new List<Processing.Lexer.Lexer>();
                int offset = 0;
                if (Match(Lexer, rule.Key, 0, ref offset, newCaptures))
                {
                    if ((!_biggestMatch && offset < SelectedLength && offset > 0) ||
                        (_biggestMatch && offset > SelectedLength && offset > 0))
                    {
                        SelectedCallback = rule.Value;
                        SelectedCaptures = new List<Lexer.Lexer>(newCaptures);
                        SelectedLength = offset;
                        matched = true;
                    }
                }
            }
            if (matched) 
            {
                Callback = SelectedCallback;
                Length = SelectedLength;
                Captures = SelectedCaptures.ToArray();
                return true;
            }
            Callback = null;
            Length = 0;
            Captures = null;
            return false;
        }

        bool _MatchAll(Lexer.Lexer Lexer, out int Length)
        {
            Length = 0;
            Lexer = Lexer.Clone(); 
            while ((object)(Lexer.GetToken(0)) != null)
            {
                int SubLength = 0;
                Callback Callback = null;
                Lexer.Lexer[] Captures = null;
                if (_MatchOne(Lexer, out Callback, out Captures, out SubLength))
                {
                    Length += SubLength;
                    Callback(Captures);
                    if (SubLength == 0) { SubLength = 1; }
                    while (SubLength > 0) { Lexer.EatToken(); SubLength--; }
                }
                else
                {
                    return Length > 0;
                }
            }
            return true;
        }

        bool _biggestMatch = false;
        public bool BiggestMatch { get { return _biggestMatch; } set { _biggestMatch = value; } }

        public void Parse(Lexer.Lexer Lexer)
        {
            while ((object)(Lexer.GetToken(0)) != null)
            {
                int Length = 0;
                Callback Callback = null;
                Lexer.Lexer[] Captures = null;
                if (_MatchOne(Lexer, out Callback, out Captures, out Length))
                {
                    Callback(Captures);
                    if (Length == 0) { Length = 1; }
                    while (Length > 0) { Lexer.EatToken(); Length--; }
                }
                else
                {
                    Lexer.EatToken();

                }
            }
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<ParseElement[], Callback> rule in _Rules)
            {
                foreach (ParseElement elm in rule.Key)
                {
                    builder.Append(elm.ToString());
                    builder.Append(" ");

                }
                builder.Append("| ");

            }
            string result = builder.ToString();
            if (result.EndsWith("| ")) { result = result.Substring(0, result.Length - 2); }
            return result.Trim();
        }
    }
}
