﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Scope : CodeElement
    {
        public override string Name
        {
            get
            {
                return "Scope";
            }
        }

        List<CodeElement> _CodeElement = new List<CodeElement>();

        public List<CodeElement> Children
        {
            get { return _CodeElement; }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(
                "<" + Name + " line=\"" +
                FromLine +
                ":" +
                ToLine +
                "\" column=\"" +
                FromColumn +
                ":" +
                ToColumn +
                "\" " +
                ">");
            for (int n = 0; n < _CodeElement.Count; n++)
            {
                builder.AppendLine(_CodeElement[n].ToString());
            }
            builder.AppendLine("</" + Name + ">");
            return builder.ToString();
        }

        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, "scope");
            GeneralSave(Stream);
            Internal.StreamTools.WriteInt32(Stream, _CodeElement.Count);
            for (int n = 0; n < _CodeElement.Count; n++)
            {
                _CodeElement[n].Save(Stream);
            }
        }

        internal static Scope Load(System.IO.Stream Stream)
        {
            Scope scope = new Scope();
            scope.GeneralLoad(Stream);
            int count = Internal.StreamTools.ReadInt32(Stream);
            while (count > 0)
            {
                scope._CodeElement.Add(CodeElement.Load(Stream));
                count--;
            }
            return scope;
        }

    }
}
