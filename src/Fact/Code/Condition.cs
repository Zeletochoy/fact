﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Condition : CodeElement
    {
        public override string Name
        {
            get
            {
                return "Condition";
            }
        }

        CodeElement _Condition = new Raw("true");


        Scope _OnTrue = new Scope();
        public Scope OnTrue
        {
            get { return _OnTrue; }
            set { _OnTrue = value; }
        }

        Scope _OnFalse = new Scope();
        public Scope OnFalse
        {
            get { return _OnFalse; }
            set { _OnFalse = value; }
        }

        public CodeElement ConditionExpr
        { get { return _Condition; } set { _Condition = value; } }

        internal static Condition Load(System.IO.Stream Stream)
        {
            Condition function = new Condition();
            function.GeneralLoad(Stream);
            function._Condition = CodeElement.Load(Stream);
            function._OnTrue = CodeElement.Load(Stream) as Scope;
            function._OnFalse = CodeElement.Load(Stream) as Scope;
            return function;
        }
        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, "condition");
            GeneralSave(Stream);
            _Condition.Save(Stream);
            _OnTrue.Save(Stream);
            _OnFalse.Save(Stream);

        }
        public override string ToString()
        {
            return "<condition " +
                "line=\"" +
                FromLine +
                ":" +
                ToLine +
                "\" cols=\"" +
                FromColumn +
                ":" +
                ToColumn +
                "\">\n" + "<conditionexp>" + _Condition.ToString() + "</conditionexp>" + "<ontrue>\n" + _OnTrue.ToString() + "</ontrue>\n" + "<onfalse>\n" + _OnFalse.ToString() + "</onfalse>\n" + "</condition>";
        }
    }
}
