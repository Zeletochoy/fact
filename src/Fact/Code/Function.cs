﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Function : CodeElement
    {
        public override string Name
        {
            get
            {
                return "Function";
            }
        }
        string _FunctionName = "";
        public string FunctionName
        {
            get { return _FunctionName; }
            set { _FunctionName = value; }
        }
        Scope _Scope = new Scope();
        public Scope FunctionBody
        {
            get { return _Scope; }
            set { _Scope = value; }
        }
        internal static Function Load(System.IO.Stream Stream)
        {
            Function function = new Function();
            function.GeneralLoad(Stream);
            function._FunctionName = Internal.StreamTools.ReadUTF8String(Stream);
            function._Scope = CodeElement.Load(Stream) as Scope;
            return function;
        }
        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, "function");
            GeneralSave(Stream);
            Internal.StreamTools.WriteUTF8String(Stream, _FunctionName);
            _Scope.Save(Stream);
        }
        public override string ToString()
        {
            return "<function " +
                "name=\""+ _FunctionName + "\" " +
                "line=\"" +
                FromLine +
                ":" +
                ToLine +
                "\" cols=\"" +
                FromColumn +
                ":" +
                ToColumn +
                "\">" + _Scope.ToString() + "</function>";
        }
    }
}
