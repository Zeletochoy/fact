﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class While : CodeElement
    {
        public override string Name
        {
            get
            {
                return "While";
            }
        }
        Scope _Scope = new Scope();
        public Scope Body
        {
            get { return _Scope; }
            set { _Scope = value; }
        }
        CodeElement _Condition;
        public CodeElement ConditionExpr
        { get { return _Condition; } set { _Condition = value; } }

        internal static While Load(System.IO.Stream Stream)
        {
            While while_ = new While();
            while_.GeneralLoad(Stream);
            while_._Scope = CodeElement.Load(Stream) as Scope;
            while_._Condition = CodeElement.Load(Stream);
            return while_;
        }
        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, "while");
            GeneralSave(Stream);
            _Scope.Save(Stream);
            _Condition.Save(Stream);
        }

        public override string ToString()
        {
            return "<while " +
                "conditionexp=\"" + _Condition.ToString() + "\" " +
                "line=\"" +
                FromLine +
                ":" +
                ToLine +
                "\" cols=\"" +
                FromColumn +
                ":" +
                ToColumn +
                "\">" + _Scope.ToString() + "</while>";
        }
    }
}
