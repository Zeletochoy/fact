﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class CodeElement
    {
        int _fromLine = 0;
        int _fromColumn = 0;
        int _ToLine = 0;
        int _ToColumn = 0;

        public int FromLine
        {
            get { return _fromLine; }
            set { _fromLine = value; }
        }

        public int FromColumn
        {
            get { return _fromColumn; }
            set { _fromColumn = value; }
        }

        public int ToLine
        {
            get { return _ToLine; }
            set { _ToLine = value; }
        }

        public int ToColumn
        {
            get { return _ToColumn; }
            set { _ToColumn = value; }
        }

        public virtual string Name { get { return "Unknow"; } }
        public override string ToString()
        {
            return
                "<" + Name + " line=\"" +
                _fromLine +
                ":" +
                _ToLine +
                "\" column=\"" +
                _fromColumn +
                ":" +
                _ToColumn +
                "\" " +
                "/>";
        }
        internal static CodeElement Load(System.IO.Stream Stream)
        {
            string _Name = Internal.StreamTools.ReadUTF8String(Stream);
            
            switch (_Name.ToLower())
            {
                case "raw": return Raw.Load(Stream); break;
                case "function": return Function.Load(Stream); break;
                case "scope": return Scope.Load(Stream); break;
                case "condition": return Condition.Load(Stream); break;
                case "return": return Return.Load(Stream); break;
                case "while": return While.Load(Stream); break;
            }
            return new CodeElement();
        }
        internal void GeneralSave(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteInt32(Stream, _fromLine);
            Internal.StreamTools.WriteInt32(Stream, _ToLine);
            Internal.StreamTools.WriteInt32(Stream, _fromColumn);
            Internal.StreamTools.WriteInt32(Stream, _ToColumn);
        }

        internal void GeneralLoad(System.IO.Stream Stream)
        {
            _fromLine = Internal.StreamTools.ReadInt32(Stream);
            _ToLine = Internal.StreamTools.ReadInt32(Stream);
            _fromColumn = Internal.StreamTools.ReadInt32(Stream);
            _ToColumn = Internal.StreamTools.ReadInt32(Stream);
        }
        internal virtual void Save(System.IO.Stream Stream) 
        {
            Internal.StreamTools.WriteUTF8String(Stream, Name);
        }
    }
}
