﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Raw : CodeElement
    {
        public override string Name
        {
            get
            {
                return "Raw";
            }
        }
        string _Data = "";
        public string Data { get { return _Data; } }
        public Raw() { }
        public Raw(string Value) { _Data = Value; }
        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, "raw");
            GeneralSave(Stream);
            Internal.StreamTools.WriteUTF8String(Stream, _Data);
        }
        internal static Raw Load(System.IO.Stream Stream)
        {
            Raw raw = new Raw();
            raw.GeneralLoad(Stream);
            raw._Data = Internal.StreamTools.ReadUTF8String(Stream);
            return raw;
        }
        public override string ToString()
        {
            return "<raw>" + _Data + "</raw>";
        }
    }
}
