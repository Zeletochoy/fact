﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Code
{
    public class Return : CodeElement
    {
        public override string Name
        {
            get
            {
                return "Return";
            }
        }

        CodeElement _expr = new Raw("0");
        CodeElement Expression
        { get { return _expr; } set { _expr = value; } }

        public Return() { }
        public Return(string Value) { _expr = new Raw(Value); }
        internal override void Save(System.IO.Stream Stream)
        {
            Internal.StreamTools.WriteUTF8String(Stream, "return");
            GeneralSave(Stream);
            _expr.Save(Stream);
        }
        internal static Return Load(System.IO.Stream Stream)
        {
            Return ret = new Return();
            ret.GeneralLoad(Stream);
            ret._expr = CodeElement.Load(Stream);
            return ret;
        }
        public override string ToString()
        {
            return "<return>" + _expr.ToString() + "</return>";
        }
    }
}
