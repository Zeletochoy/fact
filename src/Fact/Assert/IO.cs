﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Assert
{
    static public class IO
    {
        public static void FileExists(string File) { FileExists(File, Assert.Trigger.FATAL, "Assertion"); }
        public static void FileExists(string File, Assert.Trigger Trigger, string AssertionName)
        {
            if (!System.IO.File.Exists(File))
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, "The file " + File + " does not exist")
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, "The file " + File + " exists")
                });
            }
        }

        public static void FileNotExists(string File) { FileNotExists(File, Assert.Trigger.FATAL, "Assertion"); }
        public static void FileNotExists(string File, Assert.Trigger Trigger, string AssertionName)
        {
            if (System.IO.File.Exists(File))
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, "The file " + File + " exists")
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, "The file " + File + " does not exists")
                });
            }
        }

        public static void DirectoryExists(string Directory) { DirectoryExists(Directory, Assert.Trigger.FATAL, "Assertion"); }
        public static void DirectoryExists(string Directory, Assert.Trigger Trigger, string AssertionName)
        {
            if (!System.IO.Directory.Exists(Directory))
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, "The directory " + Directory + " does not exist")
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, "The directory " + Directory + " exists")
                });
            }
        }

        public static void DirectoryNotExists(string Directory) { DirectoryNotExists(Directory, Assert.Trigger.FATAL, "Assertion"); }
        public static void DirectoryNotExists(string Directory, Assert.Trigger Trigger, string AssertionName)
        {
            if (System.IO.Directory.Exists(Directory))
            {
                Assert Assert = new Assert()
                {
                    Result = new Fact.Test.Result.Error(AssertionName, "The directory " + Directory + " exists")
                };
                if (Trigger == Fact.Assert.Assert.Trigger.FATAL || Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL)
                {
                    throw Assert;
                }
                else
                {
                    Fact.Assert.Assert.PushThreadAssert(Assert);
                }
            }
            else if (Trigger == Fact.Assert.Assert.Trigger.FATAL_RECORD_ALL || Trigger == Fact.Assert.Assert.Trigger.NON_FATAL_RECORD_ALL)
            {
                Fact.Assert.Assert.PushThreadAssert(new Assert()
                {
                    Result = new Fact.Test.Result.Passed(AssertionName, "The directory " + Directory + " does not exist")
                });
            }
        }
    }
}
