/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Runtime.InteropServices;

namespace Fact.Internal.Os
{
	internal unsafe class Linux : Generic.GenericIn
	{

        abstract unsafe class SupportedLibDl
        {
            public virtual void* call_dlopen(string FileName, int Flags) { return (void*)0; }
            public virtual void* call_dlsym(void* Handle, string Symbol) { return (void*)0; }
            public virtual int call_dlclose(void* Handle) { return 0; }
        }

		abstract unsafe class SupportedLibC
		{
			public virtual long call_sysconf(int value) { return 0; }
            public virtual void call_signal(int signal, void* ptr) { return; }
		}



		unsafe class GenericLibC : SupportedLibC
		{
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
            extern static void signal(int signal, void* ptr);
			[System.Runtime.InteropServices.DllImport("/usr/lib/libc.so")]
			extern static long sysconf(int value);

			public override long call_sysconf (int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
		}

		unsafe class LibC2_18 : SupportedLibC
		{
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
            extern static void signal(int signal, void* ptr);
			[System.Runtime.InteropServices.DllImport("/usr/lib/libc-2.18.so")]
			extern static long sysconf(int value);

			public override long call_sysconf (int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
		}

        unsafe class LibC6 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/usr/lib/libc.so.6")]
            extern static long sysconf(int value);

            public override long call_sysconf(int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
        }

        unsafe class RawLibC6 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/lib/libc.so.6")]
            extern static long sysconf(int value);

            public override long call_sysconf(int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
        }

        unsafe class GNULibC6_x64 : SupportedLibC
        {
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static void signal(int signal, void* ptr);
            [System.Runtime.InteropServices.DllImport("/lib/x86_64-linux-gnu/libc.so.6")]
            extern static long sysconf(int value);

            public override long call_sysconf(int value) { return sysconf(value); }
            public override void call_signal(int signalnum, void* ptr) { signal(signalnum, ptr); }
        }


		static SupportedLibC libc = null;
        static SupportedLibDl libdl = null;


        class GNULibDL : SupportedLibDl
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern void* dlopen(string FileName, int Flags);

            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern void* dlsym(void* Handle, string Symbol);

            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern int dlclose(void* Handle);

            public override void* call_dlsym(void* Handle, string Symbol) { return dlsym(Handle, Symbol); }
            public override void* call_dlopen(string FileName, int Flags) { return dlopen(FileName, Flags); }
            public override int call_dlclose(void* Handle) { return dlclose(Handle); }
        }

        class GenericLibDL : SupportedLibDl
        {
            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern void* dlopen(string FileName, int Flags);

            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern void* dlsym(void* Handle, string Symbol);

            [System.Runtime.InteropServices.DllImport("/usr/lib/x86_64-linux-gnu/libdl.so")]
            public static extern int dlclose(void* Handle);

            public override void* call_dlsym(void* Handle, string Symbol) { return dlsym(Handle, Symbol); }
            public override void* call_dlopen(string FileName, int Flags) { return dlopen(FileName, Flags); }
            public override int call_dlclose(void* Handle) { return dlclose(Handle); }
        }
		public Linux ()
		{
            
            if (System.IO.File.Exists("/lib/x86_64-linux-gnu/libc.so.6")) { libc = new GNULibC6_x64(); }
            else if (System.IO.File.Exists ("/usr/lib/libc.so.6")) { libc = new LibC6(); }
            else if (System.IO.File.Exists("/lib/libc.so.6")) { libc = new RawLibC6(); }
            else if (System.IO.File.Exists("/usr/lib/libc-2.18.so")) { libc = new LibC2_18(); }			
            else if (System.IO.File.Exists ("/usr/lib/libc.so")) { libc = new GenericLibC (); }

            if (System.IO.File.Exists("/usr/lib/x86_64-linux-gnu/libdl.so")) { libdl = new GNULibDL(); }
            if (System.IO.File.Exists("/usr/lib/libdl.so")) { libdl = new GenericLibDL(); }

		}

		public override long GetClockTickPerSecond()
		{
			if (libc == null) { return 1; }
			return libc.call_sysconf (2); /* 2 is _SC_CLK_TCK */
		}

        public override void HookSignal(int Signal, Generic.SignalHookFunction Hook)
        {
            if (libc == null) { return; }
            libc.call_signal(Signal, System.Runtime.InteropServices.Marshal.GetFunctionPointerForDelegate((Generic.SignalHookFunction)Hook).ToPointer());
        }

        public override IntPtr MapMemory(int Size, Generic.Protection Protection)
        {
            if (libc == null) { return new IntPtr(0); }
            //FIX ME call mmap
            return new IntPtr(0);
        }

        public override IntPtr OpenLibrary(string Library)
        {
            if (libdl == null) { return new IntPtr(0); }
            if (!System.IO.File.Exists(Library)) { return new IntPtr(0); }
            return new IntPtr(libdl.call_dlopen(Library, 0));
        }

        public override void CloseLibrary(IntPtr LibraryHandle)
        {
            if (libdl == null) { return; }
            if (LibraryHandle.ToPointer() == (void*)0) { return; }
            libdl.call_dlclose(LibraryHandle.ToPointer());
        }

        public override IntPtr GetMethod(IntPtr LibraryHandle, string Method)
        {
            if (libdl == null) { return new IntPtr(0); }
            if (LibraryHandle.ToPointer() == (void*)0) { return new IntPtr(0); }
            if (Method.Trim() == "") { return new IntPtr(0); }
            return new IntPtr(libdl.call_dlsym(LibraryHandle.ToPointer(), Method));
        }
	}
}

