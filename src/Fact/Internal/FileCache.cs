﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    public class FileCache
    {
        string _Folder = "";

        Dictionary<string, string> _DiskFiles = new Dictionary<string, string>();
        ulong _DiskPoolSize = 0;
        ulong _MaxDiskPoolSize = 1024 * 1024 * 1024;

        Dictionary<string, Fact.Processing.File> _RamFiles = new Dictionary<string, Processing.File>();
        Dictionary<string, int> _TemperatureFiles = new Dictionary<string, int>();
        ulong _RamPoolSize = 0;
        ulong _MaxRamPoolSize = 1024 * 1024 * 32;


        public void AddFile(string Path, Fact.Processing.File File)
        {
            lock (this)
            {
                string hash = File.ContentHash;
                if (_RamFiles.ContainsKey(hash)) { return; }
                if (_DiskFiles.ContainsKey(hash)) { return; }
                if (_MaxRamPoolSize == 0 || _MaxRamPoolSize < File.Size)
                {

                }
                _RamFiles.Add(hash, File);
                _TemperatureFiles.Add(hash, 0);
                _RamPoolSize += File.Size;
                if (_RamPoolSize > _MaxRamPoolSize) { OffLoad(); }
            }
        }

        void SaveBlobToDisk(string Hash, Fact.Processing.File File)
        {
            if (_Folder == "") { throw new Exception("No space on disk specified"); }
            string folder1 = Hash.Substring(0, 2);
            string folder2 = Hash.Substring(2);
            try
            {
                Fact.Tools.RecursiveMakeDirectory(_Folder + "/" + folder1);
                System.IO.FileStream stream = System.IO.File.Open(_Folder + "/" + folder1 + "/" + folder2 + ".dat", System.IO.FileMode.OpenOrCreate);
                File.Save(stream);
                stream.Close();
                _DiskFiles.Add(Hash, _Folder + "/" + folder1 + "/" + folder2);
            }
            catch { throw new Exception("Error during the disk saving");  }
        }

        void OffLoad()
        {
            int min = int.MaxValue;
            string filetokill = "";
            foreach (KeyValuePair<string, int> file in _TemperatureFiles)
            {
                if (file.Value < min) { min = file.Value; filetokill = file.Key; }
            }
            if (_RamFiles.ContainsKey(filetokill))
            {
                if (!_DiskFiles.ContainsKey(filetokill))
                {
                    SaveBlobToDisk(filetokill, _RamFiles[filetokill]);
                }
                else
                {
                    // Check if the file has been modified
                    if (_RamFiles[filetokill].Hash != filetokill)
                    {
                        throw new Exception("The file has been modified without being register again in the cache pool");
                    }
                }
                _RamFiles.Remove(filetokill);
                _TemperatureFiles.Remove(filetokill);
            }
        }

        void RemoveBlob(string Hash, Fact.Processing.File File)
        {
            if (_RamFiles.ContainsKey(Hash))
            {
                _RamFiles.Remove(Hash);
                _TemperatureFiles.Remove(Hash);
            }

            if (_DiskFiles.ContainsKey(Hash))
            {
                Tools.RecursiveDelete(_DiskFiles[Hash]);
            }
        }

        public Fact.Processing.File GetFileFromHash(string Hash)
        {
            lock (this)
            {
                if (_RamFiles.ContainsKey(Hash))
                {
                    _TemperatureFiles[Hash]++;
                    return _RamFiles[Hash];
                }
            }
            return null;
        }
    }
}
