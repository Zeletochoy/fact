﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public class MSILOpCode
    {
        System.Reflection.Emit.OpCode _OpCode = System.Reflection.Emit.OpCodes.Unaligned;
        object _Data = null;
        public System.Reflection.Emit.OpCode OpCode { get { return _OpCode; } }
        public object Data { get { return _Data; } }
        public MSILOpCode(System.Reflection.Emit.OpCode OpCode, object Data)
        { _Data = Data; _OpCode = OpCode; }
    }
}
