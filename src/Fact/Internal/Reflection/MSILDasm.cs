﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    static class MSILDasm
    {
        static Dictionary<string, System.Reflection.Emit.OpCode> opcodes_names = new Dictionary<string,System.Reflection.Emit.OpCode>();
        static Dictionary<short, System.Reflection.Emit.OpCode> opcodes_bytes = new Dictionary<short,System.Reflection.Emit.OpCode>();


        static MSILDasm()
        {
            foreach (System.Reflection.FieldInfo _ in typeof(System.Reflection.Emit.OpCodes).GetFields())
            {
                if (_.IsStatic && _.IsPublic)
                {
                    System.Reflection.Emit.OpCode opcode = (System.Reflection.Emit.OpCode)_.GetValue(null);
                    opcodes_names[opcode.Name] = opcode;
                    opcodes_bytes[opcode.Value] = opcode;
                }
            }
        }

        internal static MSILOpCode GetOpCodeInfo(byte[] bytecode, ref int offset)
        {
            if (offset < 0 || offset >= bytecode.Length)
                return null;
            short Key = bytecode[offset];
            if (Key == 0xFF || Key == 0xFE) { Key *= 0x100; offset++; Key += bytecode[offset]; }
            offset++;
            if (!opcodes_bytes.ContainsKey(Key)) { return null; }
            System.Reflection.Emit.OpCode Code = opcodes_bytes[Key];
            try
            {
                switch (Code.OperandType)
                {
                    case System.Reflection.Emit.OperandType.InlineNone: return new MSILOpCode(Code, null);
                    case System.Reflection.Emit.OperandType.InlineMethod: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4).ToString());
                    case System.Reflection.Emit.OperandType.InlineI8: offset += 8; return new MSILOpCode(Code, System.BitConverter.ToUInt64(bytecode, offset - 8));
                    case System.Reflection.Emit.OperandType.InlineI: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4));
                    case System.Reflection.Emit.OperandType.InlineField: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4));
                    case System.Reflection.Emit.OperandType.InlineBrTarget: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4));
                    case System.Reflection.Emit.OperandType.InlineString: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4));
                    case System.Reflection.Emit.OperandType.InlineSwitch: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4));
                    case System.Reflection.Emit.OperandType.InlineType: offset += 4; return new MSILOpCode(Code, System.BitConverter.ToUInt32(bytecode, offset - 4));
                    case System.Reflection.Emit.OperandType.InlineVar: offset += 2; return new MSILOpCode(Code, System.BitConverter.ToUInt16(bytecode, offset - 2));
                    case System.Reflection.Emit.OperandType.ShortInlineBrTarget:
                        offset += 1;
                        int ShortOffSet = bytecode[offset - 1];
                        if (ShortOffSet < 0x80) { return new MSILOpCode(Code, offset + ShortOffSet); }
                        ShortOffSet -= 0x80; ShortOffSet = 0x80 - ShortOffSet;
                        return new MSILOpCode(Code, offset - ShortOffSet);
                    case System.Reflection.Emit.OperandType.ShortInlineI: offset += 1; return new MSILOpCode(Code, bytecode[offset - 1]);
                    case System.Reflection.Emit.OperandType.ShortInlineR: offset += 4; return new MSILOpCode(Code, bytecode[offset - 4]);
                    case System.Reflection.Emit.OperandType.ShortInlineVar: offset += 1; return new MSILOpCode(Code, bytecode[offset - 1]);
                }
            }
            catch { }
            return null;
        }
    }
}
