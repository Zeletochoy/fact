﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal.Reflection
{
    public static class MSILReader
    {
        static public List<MSILOpCode> ReadMethod(System.Delegate Method)
        {
            return ReadMethod(Method.Method);
        }

        static public List<MSILOpCode> ReadMethod(System.Reflection.MethodInfo Method)
        {
            List<MSILOpCode> opcodes = new List<MSILOpCode>();
            byte[] data = Method.GetMethodBody().GetILAsByteArray();
            int offset = 0;
            while (offset < data.Length)
            {
                opcodes.Add(MSILDasm.GetOpCodeInfo(data, ref offset));
            }
            return opcodes;
        }
    }
}
