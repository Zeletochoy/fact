﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Internal
{
    static internal class StreamTools
    {
        static internal void Serialize(System.IO.Stream stream, object obj)
        {
            if (obj == null) { WriteUTF8String(stream, "@@null"); return; }
            if (obj is string || obj is String) { WriteUTF8String(stream, "@@string"); WriteUTF8String(stream, (string)obj); return; }
            if (obj is bool || obj is Boolean) { WriteUTF8String(stream, "@@bool"); WriteBoolean(stream, (bool)obj); return; }
            if (obj is Int32) { WriteUTF8String(stream, "@@Int32"); WriteInt32(stream, (Int32)obj); return; }
            if (obj is Int64) { WriteUTF8String(stream, "@@Int64"); WriteInt64(stream, (Int64)obj); return; }
            if (obj is Int16) { WriteUTF8String(stream, "@@Int16"); WriteInt16(stream, (Int16)obj); return; }
            if (obj is DateTime) { WriteUTF8String(stream, "@@DateTime"); WriteDateTime(stream, (DateTime)obj); return; }
            if (obj is Version) { WriteUTF8String(stream, "@@Version"); WriteVersion(stream, (Version)obj); return; }

            if (obj is Fact.Processing.File) { WriteUTF8String(stream, "@Fact@File"); ((Fact.Processing.File)obj).Save(stream); return; }
            if (obj is Fact.Processing.Project) { WriteUTF8String(stream, "@Fact@Project"); ((Fact.Processing.Project)obj).Save(stream); return; }
            if (obj is Fact.Test.Result.Result) { WriteUTF8String(stream, "@Fact@Test"); ((Fact.Test.Result.Result)obj).Save(stream); return; }

            if (obj is Fact.Directory.User) { WriteUTF8String(stream, "@Fact@User"); ((Fact.Directory.User)obj).Save(stream); return; }
            if (obj is Fact.Directory.Group) { WriteUTF8String(stream, "@Fact@Group"); ((Fact.Directory.Group)obj).Save(stream); return; }
            if (obj is Fact.Directory.Directory) { WriteUTF8String(stream, "@Fact@Directory"); ((Fact.Directory.Directory)obj).Save(stream); return; }



            if (obj is string[])
            {
                WriteUTF8String(stream, "@@Array@String");
                string[] str = (string[])obj;
                WriteInt32(stream, str.Length);
                foreach (string s in str) { WriteUTF8String(stream, s); }
                return;
            }
            if (obj is byte[]) 
            {
                WriteUTF8String(stream, "@@Array@Byte");
                WriteByteArray(stream, (byte[])obj);
                return;
            }

            {
                // Check now with .NET serialization
                if (obj.GetType().IsSerializable)
                {
                    WriteUTF8String(stream, "@@#");
                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    formatter.Serialize(stream, obj);
                }
            }
            throw new Exception("Impossible to serialize the object");

        }

        static internal object Deserialize(System.IO.Stream stream)
        {
            string type = ReadUTF8String(stream);
            switch (type)
            {
                case "@@null": return null;
                case "@@string": return ReadUTF8String(stream);
                case "@@bool": return ReadBoolean(stream);
                case "@@Int32": return ReadInt32(stream);
                case "@@Int64": return ReadInt64(stream);
                case "@@Int16": return ReadInt16(stream);
                case "@@DateTime": return ReadDateTime(stream);
                case "@@Version": return ReadVersion(stream);

                case "@Fact@File": Fact.Processing.File file = new Fact.Processing.File(); file.Load(stream); return file;
                case "@Fact@Project": Fact.Processing.Project project = new Fact.Processing.Project(""); project.Load(stream); return project;
                case "@Fact@Test": return Fact.Test.Result.Result.Load(stream);

                case "@Fact@User": Fact.Directory.User user = new Fact.Directory.User("", ""); user.Load(stream); return user;
                case "@Fact@Group": Fact.Directory.Group group = new Fact.Directory.Group(""); group.Load(stream); return group;
                case "@Fact@Directory": Fact.Directory.Directory directory = new Fact.Directory.Directory(); directory.Load(stream); return directory;

                case "@@Array@String":
                    {
                        List<string> strings = new List<string>();
                        int length = ReadInt32(stream);
                        for (int n = 0; n < length; n++)
                        { strings.Add(ReadUTF8String(stream)); }
                        return strings.ToArray();
                    }
                case "@@Array@Byte": return ReadByteArray(stream);
                case "@@#":
                    {
                        // Serialized with .NET
                        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        return formatter.Deserialize(stream);
                    }
            }

            throw new Exception("Impossible to serialize the object");
        }

        static internal Int32 ReadInt32(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }
        static internal Int32 ReadInt32(System.IO.Stream stream)
        {
            byte[] array = new byte[4];
            stream.Read(array, 0, 4);
            return BitConverter.ToInt32(array, 0);
        }
        static internal void WriteInt32(System.IO.Stream stream, Int32 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }
        static internal Int16 ReadInt16(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 2);
            return BitConverter.ToInt16(buffer, 0);
        }
        static internal Int16 ReadInt16(System.IO.Stream stream)
        {
            byte[] array = new byte[2];
            stream.Read(array, 0, 2);
            return BitConverter.ToInt16(array, 0);
        }
        static internal void WriteInt16(System.IO.Stream stream, Int16 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 2);
        }
        static internal Int64 ReadInt64(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 8);
            return BitConverter.ToInt64(buffer, 0);
        }
        static internal Int64 ReadInt64(System.IO.Stream stream)
        {
            byte[] array = new byte[8]; 
            stream.Read(array, 0, 8);
            return BitConverter.ToInt64(array, 0);
        }
        static internal void WriteInt64(System.IO.Stream stream, Int64 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 8);
        }
        static internal UInt32 ReadUInt32(System.IO.Stream stream, byte[] buffer)
        {
            stream.Read(buffer, 0, 4);
            return BitConverter.ToUInt32(buffer, 0);
        }
        static internal UInt32 ReadUInt32(System.IO.Stream stream)
        {
            byte[] array = new byte[4];
            stream.Read(array, 0, 4);
            return BitConverter.ToUInt32(array, 0);
        }
        static internal void WriteUInt32(System.IO.Stream stream, UInt32 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 4);
        }
        static internal UInt64 ReadUInt64(System.IO.Stream stream)
        {
            byte[] array = new byte[8];
            stream.Read(array, 0, 8);
            return BitConverter.ToUInt64(array, 0);
        }
        static internal void WriteUInt64(System.IO.Stream stream, UInt64 value)
        {
            stream.Write(BitConverter.GetBytes(value), 0, 8);
        }
        static internal bool ReadBoolean(System.IO.Stream stream)
        {
			return stream.ReadByte() != 0x00;
        }
        static internal void WriteBoolean(System.IO.Stream stream, Boolean value)
        {
			stream.WriteByte(value ? (byte)0xFF : (byte)0x00);
        }

        static internal byte[] ReadByteArray(System.IO.Stream stream)
        {
            Int32 length = ReadInt32(stream);
            byte[] array = new byte[length];
            stream.Read(array, 0, length);
            return array;
        }
        static internal void WriteByteArray(System.IO.Stream stream, byte[] value)
        {
            // DO NOT USE LongLength
            WriteInt32(stream, value.Length);
            stream.Write(value, 0, value.Length);
        }

        static internal string ReadUTF8String(System.IO.Stream stream)
        {
            return System.Text.Encoding.UTF8.GetString(ReadByteArray(stream));
        }

        static internal void WriteUTF8String(System.IO.Stream stream, string value)
        {
            WriteByteArray(stream, System.Text.Encoding.UTF8.GetBytes(value));
        }


        static internal void WriteUTF8ShortString(System.IO.Stream stream, string value)
        {
            byte[] data = System.Text.Encoding.UTF8.GetBytes(value);
            if (data.Length == 1)
            {
                if (data[0] == 0)
                {
                    stream.WriteByte(0);
                    stream.WriteByte(1);
                    stream.WriteByte(0);
                    return;
                }
                else
                {
                    stream.WriteByte(data[0]);
                }
            }
            else if (data.Length == 0)
            {
                stream.WriteByte(0);
                stream.WriteByte(0);
                return;
            }
            else if (data.Length < 255)
            {
                stream.WriteByte(0);
                stream.WriteByte((byte)data.Length);
                stream.Write(data, 0, data.Length);
            }
            else
            {
                stream.WriteByte(0);
                stream.WriteByte(255);
                WriteUTF8String(stream, value);
            }
        }

        static internal string ReadUTF8ShortString(System.IO.Stream streams)
        {
            int value = streams.ReadByte();
            if (value < 0) { return ""; }
            if (value > 0) { return ((char)value).ToString(); }
            value = streams.ReadByte();
            if (value <= 0) { return ""; }
            if (value == 255) { return ReadUTF8String(streams); }
            byte[] array = new byte[value];
            streams.Read(array, 0, value);
            return System.Text.Encoding.UTF8.GetString(array);
        }

        static internal DateTime ReadDateTime(System.IO.Stream stream)
        {
            int Millisecond = ReadInt16(stream);
            int Second = ReadInt16(stream);
            int Minute = ReadInt16(stream);
            int Hour = ReadInt16(stream);
            int Day = ReadInt16(stream);
            int Month = ReadInt16(stream);
            int Year = ReadInt16(stream);
            DateTime date = new DateTime(Year, Month, Day, Hour, Minute, Second, Millisecond);
            return date;

        }

        static internal void WriteDateTime(System.IO.Stream stream, DateTime value)
        {
            WriteInt16(stream, (short)value.Millisecond);
            WriteInt16(stream, (short)value.Second);
            WriteInt16(stream, (short)value.Minute);
            WriteInt16(stream, (short)value.Hour);
            WriteInt16(stream, (short)value.Day);
            WriteInt16(stream, (short)value.Month);
            WriteInt16(stream, (short)value.Year);
        }

        static internal void WriteVersion(System.IO.Stream stream, Version value)
        {
            WriteInt32(stream, value.Major);
            WriteInt32(stream, value.Minor);
            WriteInt32(stream, value.Build);
            WriteInt32(stream, value.Revision);
        }


        static internal Version ReadVersion(System.IO.Stream stream)
        {
            int Major = ReadInt32(stream);
            int Minor = ReadInt32(stream);
            int Build = ReadInt32(stream);
            int Revision = ReadInt32(stream);
            return new Version(Major, Minor, Build, Revision);
        }
    }
}
