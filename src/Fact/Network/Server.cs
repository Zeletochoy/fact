﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class Server
    {
        static System.Security.Cryptography.SHA512 SHA512 = System.Security.Cryptography.SHA512.Create();
        class User
        {
            internal string _HostAddress = "";
            public string HostAddress { get { return _HostAddress; } }
            internal byte[] _Token = new byte[0];
            internal ulong _IncToken = 0;
            internal Fact.Directory.User _User = null;
            internal string _UserId = "";
            internal byte[] _Password = new byte[0];
            internal DateTime _LastMessage = DateTime.UtcNow;
            static long _Id = 0;

            static HashSet<string> _UsersId = new HashSet<string>();

            public void GenerateUserId() 
            {
                if (_UserId != "") { return; }
            retry:
                long value = 0;
                try
                {
                    unchecked
                    {
                        value = System.Threading.Interlocked.Increment(ref _Id);
                        if (value < 0) { value = 0; }
                    }
                }
                catch { }

                lock (_UsersId)
                {
                    _UserId = _User.Login + "_" + DateTime.UtcNow.Millisecond.ToString() + "_" + DateTime.UtcNow.Second.ToString() + "_" + value.ToString();
                    if (_UsersId.Contains(_UserId))
                    {
                        goto retry;
                    }
                    _UsersId.Add(_UserId);
                }
            }

            ~User()
            {
                lock (_UsersId)
                {
                    _UsersId.Remove(_UserId);
                }
            }
        }
        public class UserEventArgs : EventArgs
        {
            internal Server _Server = null; public Server Server { get { return _Server; } }
            internal string _UserId = ""; public string UserID { get { return _UserId; } }
        }
        public event EventHandler<UserEventArgs> Connected;
        public event EventHandler<UserEventArgs> Disconnected;

        System.Net.HttpListener _Listener = new System.Net.HttpListener();
        delegate void Action(object[] Input, Service.RequestInfo RequestInfo, out object Output);
        Dictionary<string, Action> _Actions = new Dictionary<string, Action>();
        Dictionary<string, System.Reflection.MethodInfo> _ActionsInfo = new Dictionary<string, System.Reflection.MethodInfo>();

        Dictionary<string, User> _Users = new Dictionary<string, User>();
        Directory.Group _AllowedUser = new Fact.Directory.Group();
        Dictionary<string, Fact.Directory.Group> _ActionAllowedUser = new Dictionary<string, Directory.Group>();

        public Dictionary <string, Fact.Directory.Group> ActionAllowedUser
        {
            get { return _ActionAllowedUser; }
        }

        public List<KeyValuePair<string, System.Reflection.MethodInfo>> ListActions()
        {
            List<KeyValuePair<string, System.Reflection.MethodInfo>> lst = new List<KeyValuePair<string, System.Reflection.MethodInfo>>();
            lock (_ActionsInfo)
            {
                foreach (KeyValuePair<string, System.Reflection.MethodInfo> pair in _ActionsInfo)
                {
                    lst.Add(new KeyValuePair<string, System.Reflection.MethodInfo>(pair.Key, pair.Value));
                }
            }
            return lst;
        }

        static Random rnd = new Random();
        int _InterfacePort = 0;
        int _SSLInterfacePort = 0;
        public Server(int InterfacePort, int SSLInterfacePort)
        {
            Connected += new EventHandler<UserEventArgs>(Default_UserEvent);
            Disconnected += new EventHandler<UserEventArgs>(Default_UserEvent);
            _InterfacePort = InterfacePort;
            _SSLInterfacePort = SSLInterfacePort;
            if (_SSLInterfacePort == _InterfacePort)
            {
                throw new Exception("HTTP and HTTPS connections can't be made on the same port");
            }
        }

        void Default_UserEvent(object sender, Server.UserEventArgs e) { }

        public bool IsListening { get { if (_Listener == null) { return false; } return _Listener.IsListening; } }

        public void LoadService(Service Service)
        {
            if (Service == null) { return; }
            List<System.Reflection.MethodInfo> methods = Service._GetExposedMethods();
            foreach (System.Reflection.MethodInfo method in methods)
            {
                if (method.GetParameters().Length == 0 || method.GetParameters()[0].ParameterType != typeof(Network.Service.RequestInfo))
                {
                    throw new Exception("The method " + method.Name + " is not a valid method.");
                }
                // Microsoft VS Scope bug WAR (fixed in the last VS version)
                System.Reflection.MethodInfo _WAR_method = method;
                string actionName = Service.Name + "." + method.Name;
                lock (_Actions)
                {
                    if (_Actions.ContainsKey(actionName))
                    {
                        _Actions.Remove(actionName);
                    }
                    _Actions.Add(actionName, (object[] parameters, Network.Service.RequestInfo RequestInfo, out object output) =>
                    {
                        List<object> plist = new List<object>();
                        plist.Add(RequestInfo);
                        plist.AddRange(parameters);
                        if (parameters == null) { parameters = new object[0]; }
                        try
                        {
                            output = _WAR_method.Invoke(Service, plist.ToArray());
                        }
                        catch { output = null; }
                    });
                }
                lock (_ActionsInfo)
                {
                    if (_ActionsInfo.ContainsKey(actionName))
                    {
                        _ActionsInfo.Remove(actionName);
                    }
                    _ActionsInfo.Add(actionName, _WAR_method);
                }
            }
        }
        public object InvokeAction(Directory.User User, string Action, params object[] Parameters)
        {
            Service.RequestInfo requestInfo = new Service.RequestInfo();
            requestInfo._User = User;
            requestInfo._UserId = "";
            requestInfo._Server = this;
            return InvokeAction(requestInfo, Action, Parameters);
        }

        public object InvokeAction(Service.RequestInfo Info, string Action, params object[] Parameters)
        {
            try
            {
                if (Info.User != null)
                {
                    lock (_ActionAllowedUser)
                    {
                        if (_ActionAllowedUser.ContainsKey(Action) &&
                            !_ActionAllowedUser[Action].Contains(Info.User))
                        {
                            return null;
                        }
                    }
                }
            }
            catch { return null; }
            try
            {
                lock (_Actions)
                {
                    if (!_Actions.ContainsKey(Action)) { return null; }
                    object response = null;
                    _Actions[Action](Parameters, Info, out response);
                    return response;
                }
            }
            catch { return null; }
        }
        volatile bool _Started = true;
        volatile bool _Running = false;
        public Directory.Group AllowedUsers { get { return _AllowedUser; } set { _AllowedUser = value; } }
        public void Restart()
        {
            if (_Listener != null)
            {
                Stop();
            }
            lock (this)
            {
                while (_Running) { _Started = false; System.Threading.Monitor.Wait(this); }
                _Listener = new System.Net.HttpListener();
                if (_InterfacePort > 0) { _Listener.Prefixes.Add("http://*:" + _InterfacePort + "/"); }
                if (_SSLInterfacePort > 0) { _Listener.Prefixes.Add("https://*:" + _SSLInterfacePort + "/"); }
                _Listener.Start();
                _Started = true;
                new System.Threading.Thread(Run) { IsBackground = true }.Start();
                while (!_Running) { System.Threading.Monitor.Wait(this); }
            }
        }

        public void Start() 
        {
            // Yes start is just an alias for restart beause restart
            // will also start the server if it is not started
            Restart();
        }
        public void Stop() 
        {
            lock (this)
            {
                _Started = false;
                try
                {
                    _Listener.Stop();
                    _Listener.Close();
                }
                catch { }
                System.Threading.Monitor.PulseAll(this);
            }

            lock (this)
            {
                while (_Running)
                {
                    try
                    {
                        _Listener.Abort();
                    }
                    catch { }
                    _Started = false;
                    System.Threading.Monitor.Wait(this);
                }
                _Listener = null;
            }
        }

        int _PendingRequest = 0;
        int _PendingProcessingRequest = 0;

        void ListenerCallback(IAsyncResult result)
        {
            try
            {
                System.Net.HttpListener listener = (System.Net.HttpListener)result.AsyncState;
                System.Net.HttpListenerContext Context = listener.EndGetContext(result);
                Threading.Job.CreateJob(DispatcherAgnostic, Context);
            }
            catch { }
            System.Threading.Interlocked.Decrement(ref _PendingRequest);
        }
        void Run()
        {
            int _Processor = System.Environment.ProcessorCount;
            if (_Processor <= 1) { _Processor = 1; }
            int _IOQueue = _Processor * 4;
            try
            {
                lock (this)
                {
                    _Running = true;
                    System.Threading.Monitor.PulseAll(this);
                }

                while (_Started)
                {
                    try
                    {
                        if (_PendingRequest < _IOQueue)
                        {
                            System.Threading.Interlocked.Increment(ref _PendingRequest);
                            _Listener.BeginGetContext(ListenerCallback, _Listener);
                        }
                        else
                        {
                            System.Threading.Thread.Sleep(10);
                        }
                    }
                    catch 
                    {
                        if (!_Listener.IsListening) { goto Exit; }
                    }
                }
                Exit:
                bool mustRestart = false;
                lock (this)
                {
                    _Running = false;

                    System.Threading.Monitor.PulseAll(this);
                    if (_Started)
                    {
                        mustRestart = true;
                    }
                    else
                    {
                        try
                        {
                            _Listener.Close();
                            _Listener.Stop();
                            _Listener.Close();
                        }
                        catch { }
                    }
                }
                Restart();
            }
            catch { }
        }

        DateTime _LastDeadUserCheck = DateTime.UtcNow;
        void RemoveDeadUsers()
        {
            List<string> deadUsers = new List<string>();
            lock (_Users)
            {
                DateTime now = DateTime.UtcNow;
                if ((now - _LastDeadUserCheck).TotalMinutes < 1) 
                {
                    return;
                }
                foreach (KeyValuePair<string, User> user in _Users)
                {
                    if ((now - user.Value._LastMessage).TotalSeconds >= 30) { deadUsers.Add(user.Key); }
                }
                _LastDeadUserCheck = now;
            }
            foreach (string dead in deadUsers)
            {
                try { Disconnected(this, new UserEventArgs() { _UserId = dead, _Server = this }); }
                catch { }
                lock (_Users) { _Users.Remove(dead); }
            }
        }

        public string GetHostAddressFromUserId(string UserId)
        {
            lock (_Users)
            {
                if (!_Users.ContainsKey(UserId)) { return ""; }
                return _Users[UserId]._HostAddress;
            }
            return "";
        }

        void DispatcherAgnostic(object Context)
        {
            Dispatcher(Context as System.Net.HttpListenerContext);
        }

        void Dispatcher(System.Net.HttpListenerContext Context)
        {
            if (Context == null) { return; }
            string url = Context.Request.RawUrl;
            System.Threading.Interlocked.Increment(ref _PendingProcessingRequest);
            try
            {
                ParseRequest(Context);
                Context.Response.Close();
            }
            catch { }
            System.Threading.Interlocked.Decrement(ref _PendingProcessingRequest);
        }

        void ParseRequest(System.Net.HttpListenerContext Context)
        {
            string response = "<Error/>";

            if (Context.Request.HttpMethod.ToLower() == "post")
            {
                response = "<Error/>";
                byte[] fullmessage = null;
                try
                {
                    int Remaining = (int)Context.Request.ContentLength64;
                    int offset = 0;
                    byte[] buffer = new byte[Remaining];
                    
                    // FIXME Implement a timeout
                    int GetTimeout = 15000;
                    while (Remaining > 0)
                    {
                        int length = Context.Request.InputStream.Read(buffer, offset, Remaining);
                        offset += length;
                        Remaining -= length;
                        if (length == 0)
                        {
                            if (GetTimeout <= 0)
                            {
                                // The client has not been able to send a single byte in 15s
                                // No way the client is sending it at 32 Bits per minutes
                                // don't try to look at it
                                return;
                            }
                            System.Threading.Thread.Sleep(10);
                            GetTimeout -= 10;
                        }
                        else if (length == -1)
                        {
                            // This must never happend: The length of the message missmatch the specified one
                            // In that case we have to discard the entire connection
                            return;
                        }
                        else
                        {
                            GetTimeout = 60000;
                        }
                    }
                    fullmessage = buffer;

                }
                catch (Exception e) { }

                string text = System.Text.Encoding.UTF8.GetString(fullmessage);
                Dictionary<string, string> Variables = new Dictionary<string, string>();
                {
                    int n = 0;
                    while (n < text.Length)
                    {
                        StringBuilder key = new StringBuilder();
                        StringBuilder value = new StringBuilder();
                        for (; n < text.Length; n++)
                        {
                            if (text[n] == '=') { n++; break; } else { key.Append(text[n]); }
                        }
                        for (; n < text.Length; n++)
                        {
                            if (text[n] == '&') { n++; break; } else { value.Append(text[n]); }
                        }
                        try
                        {
                            // FIXME Recode the UnescapeFunction
                            string ukey = "";
                            try { ukey = System.Uri.UnescapeDataString(key.ToString()); }
                            catch { ukey = key.ToString(); }
                            string udata = "";
                            try { udata = System.Uri.UnescapeDataString(value.ToString()); }
                            catch { udata = value.ToString(); }
                            Variables.Add(ukey, udata);
                        }
                        catch { }
                    }
                }
                if (Variables.ContainsKey("action"))
                {
                    string Action = Variables["action"];
                    if (Action == "@@authenticate")
                    {
                        if (Variables.ContainsKey("username") &&
                            Variables.ContainsKey("password"))
                        {
                            string Username = Variables["username"];
                            byte[] Password = Tools.FromBase64URL(Variables["password"]);
                            Fact.Directory.User authenticatedUser = null;
                            if (Variables.ContainsKey("credential"))
                            {
                                byte[] CredentialId = Tools.FromBase64URL(Variables["credential"]);
                                Directory.Credential credential = new Fact.Directory.Credential();
                                if (_AllowedUser.AuthenticateWithCredential(Username, CredentialId, Password, Fact.Directory.User.AuthenticationMethod.Fact_TimeSafe, out authenticatedUser, out credential))
                                {
                                    RemoveDeadUsers();
                                    string UserId = Username;
                                    User User = new User();
                                    lock (_Users)
                                    {
                                        User._IncToken = 0;
                                        User._Token = new byte[64];
                                        User._User = authenticatedUser;
                                        User._Password = credential._Password;
                                        try { User._HostAddress = Context.Request.RemoteEndPoint.Address.ToString(); }
                                        catch { }
                                        rnd.NextBytes(User._Token);

                                        User.GenerateUserId();
                                        UserId = User._UserId;
                                        _Users.Add(UserId, User);
                                    }
                                    if (authenticatedUser.Password_SHA512_Salt == null)
                                    {
                                        response = "<Error><message>The user object is not compatible to Fact time safe</message></Error>";
                                    }
                                    else
                                    {
                                        try { if (Connected != null) { Connected(this, new UserEventArgs() { _UserId = UserId, _Server = this }); } }
                                        catch { }
                                        response = "<Response><Token>" + Tools.ToBase64URL(_Users[UserId]._Token) + "</Token><UserId>" + UserId + "</UserId></Response>";
                                    }
                                }
                                else
                                {
                                    response = "<Error><message>Authentication failure</message></Error>";
                                }
                            }
                            else
                            {
                                if (_AllowedUser.Authenticate(Username, Password, Fact.Directory.User.AuthenticationMethod.Fact_TimeSafe, out authenticatedUser))
                                {
                                    RemoveDeadUsers();
                                    string UserId = Username;
                                    User User = new User();
                                    lock (_Users)
                                    {
                                        User._IncToken = 0;
                                        User._Token = new byte[64];
                                        User._User = authenticatedUser;
                                        User._Password = authenticatedUser.Password_SHA512_Salt;
                                        try { User._HostAddress = Context.Request.RemoteEndPoint.Address.ToString(); }
                                        catch { }
                                        rnd.NextBytes(User._Token);

                                        User.GenerateUserId();
                                        UserId = User._UserId;
                                        _Users.Add(UserId, User);
                                    }
                                    if (authenticatedUser.Password_SHA512_Salt == null)
                                    {
                                        response = "<Error><message>The user object is not compatible to Fact time safe</message></Error>";
                                    }
                                    else
                                    {
                                        try { if (Connected != null) { Connected(this, new UserEventArgs() { _UserId = UserId, _Server = this }); } }
                                        catch { }
                                        response = "<Response><Token>" + Tools.ToBase64URL(_Users[UserId]._Token) + "</Token><UserId>" + UserId + "</UserId></Response>";
                                    }
                                }
                                else
                                {
                                    response = "<Error><message>Authentication failure</message></Error>";
                                }
                            }
                        }
                        else
                        {
                            response = "<Error><message>Invalid parameters</message></Error>";
                        }
                    }
                    else
                    {
                       
                        if (Variables.ContainsKey("userid") &&
                           Variables.ContainsKey("token"))
                        {
                            byte[] content = new byte[0];
                            if (Variables.ContainsKey("content") && Variables["content"].Length > 0)
                            {
                                content = Tools.FromBase64URL(Variables["content"]);
                            }
                            byte[] token = Tools.FromBase64URL(Variables["token"]);
                            User user = null;
                            RemoveDeadUsers();
                            lock (_Users)
                            {
                                if (!_Users.ContainsKey(Variables["userid"]))
                                {
                                    response = "<Error><message>Invalid auth data</message></Error>";
                                    goto sendresponse;
                                }
                                else
                                {
                                    user = _Users[Variables["userid"]];
                                }
                            }
                            byte[] actionAsArray = System.Text.Encoding.UTF8.GetBytes(Action);

                            List<byte> expectedTokenBuilder = new List<byte>(user._Token);
                            expectedTokenBuilder.AddRange(user._Password);
                            expectedTokenBuilder.AddRange(BitConverter.GetBytes((Int64)user._IncToken));
                            DateTime now = DateTime.UtcNow;
                            expectedTokenBuilder.AddRange(Fact.Internal.Information.TimeCode(now));
                            expectedTokenBuilder.AddRange(content);
                            expectedTokenBuilder.AddRange(actionAsArray);
                            byte[] expectedToken = null;
                            lock (SHA512) { expectedToken = SHA512.ComputeHash(expectedTokenBuilder.ToArray()); }
                            if (expectedToken.Length != token.Length)
                                goto error;
                            for (int n = 0; n < expectedToken.Length; n++)
                            { if (expectedToken[n] != token[n]) { goto nextp1; } }
                            goto end;
                        nextp1:
                            List<byte> expectedTokenp1Builder = new List<byte>(user._Token);
                            expectedTokenp1Builder.AddRange(user._Password);
                            expectedTokenp1Builder.AddRange(BitConverter.GetBytes((Int64)user._IncToken));
                            DateTime nowp1 = now + new TimeSpan(0, 1, 0);
                            expectedTokenp1Builder.AddRange(Fact.Internal.Information.TimeCode(nowp1));
                            expectedTokenp1Builder.AddRange(content);
                            expectedTokenp1Builder.AddRange(actionAsArray);
                            byte[] expectedTokenp1 = null;
                            lock (SHA512) { expectedTokenp1 = SHA512.ComputeHash(expectedTokenp1Builder.ToArray()); }
                            for (int n = 0; n < expectedTokenp1.Length; n++)
                            { if (expectedTokenp1[n] != token[n]) { goto nextm1; } }
                            goto end;
                        nextm1:
                            List<byte> expectedTokenm1Builder = new List<byte>(user._Token);
                            expectedTokenm1Builder.AddRange(user._Password);
                            expectedTokenm1Builder.AddRange(BitConverter.GetBytes((Int64)user._IncToken));
                            DateTime nowm1 = now - new TimeSpan(0, 1, 0);
                            expectedTokenm1Builder.AddRange(Fact.Internal.Information.TimeCode(nowm1));
                            expectedTokenm1Builder.AddRange(content);
                            expectedTokenm1Builder.AddRange(actionAsArray);
                            byte[] expectedTokenm1 = null;
                            lock (SHA512) { expectedTokenm1 = SHA512.ComputeHash(expectedTokenm1Builder.ToArray()); }
                            for (int n = 0; n < expectedTokenm1.Length; n++)
                            { if (expectedTokenm1[n] != token[n]) { goto error; } }
                            goto end;
                        error:
                            lock (_Users) { _Users.Remove(Variables["userid"]); }
                            try { Disconnected(this, new UserEventArgs() { _UserId = Variables["userid"] }); }
                            catch { }
                            response = "<Error><message>Invalid token</message></Error>";
                            goto sendresponse;
                        end:
                            user._IncToken++;
                            user._LastMessage = DateTime.UtcNow;
                            List<object> parameters = new List<object>();
                            if (content.Length > 0)
                            {
                                System.IO.MemoryStream stream = new System.IO.MemoryStream(content);
                                int Count = Internal.StreamTools.ReadInt32(stream);
                                for (int n = 0; n < Count; n++)
                                {
                                    parameters.Add(Internal.StreamTools.Deserialize(stream));
                                }
                            }

                            if (Action.ToLower() == "@@postauthenticate") 
                            {
                                response = "<Authenticated/>"; 
                            }
                            else if (Action.ToLower() == "@@ping")
                            {
                                response = "<Pong/>";
                            }
                            else if (_Actions.ContainsKey(Action))
                            {
                                bool allowed = true;

                                lock (_ActionAllowedUser)
                                {
                                    if (_ActionAllowedUser.ContainsKey(Action) &&
                                        !_ActionAllowedUser[Action].Contains(user._User))
                                    {
                                        allowed = false;
                                    }
                                }
                                if (allowed)
                                {
                                    object obj_response = null;
                                    Service.RequestInfo Request = new Service.RequestInfo();
                                    Request._User = user._User;
                                    Request._Server = this;
                                    Request._UserId = user._UserId;
                                    Request._UserHostAddress = user._HostAddress;
                                    _Actions[Action](parameters.ToArray(), Request, out obj_response);
                                    System.IO.MemoryStream stream = new System.IO.MemoryStream();
                                    Internal.StreamTools.Serialize(stream, obj_response);
                                    //response = "<Content>" + Tools.ToBase64URL(stream.ToArray()) + "</Content>";
                                    // Send a raw response to  avoid using Base64
                                    Context.Response.ContentType = "application/octet-stream";
                                    byte[] array = stream.ToArray();
                                    Context.Response.ContentLength64 = array.Length;
                                    Context.Response.OutputStream.Write(array, 0, array.Length);
                                    return;
                                }
                            }
                            else
                            {
                                response = "<Error><message>Invalid action</message></Error>"; return;
                            }
                        }
                        else if (Action.ToLower() == "@@ping")
                        {
                            response = "<Pong/>";
                        }
                        else
                        { response = "<Error><message>Missing auth data</message></Error>"; return; }
                    }
                }
                else
                { response = "<Error><message>No action specified</message></Error>"; return; }
            }
            else
            {
                response = "<Error><message>A request can only be a post request</message></Error>"; return;
            }
            sendresponse:
            byte[] responsebyte = System.Text.Encoding.UTF8.GetBytes(response);
            Context.Response.ContentType = "application/xml";
            Context.Response.ContentLength64 = responsebyte.Length;
            Context.Response.OutputStream.Write(responsebyte, 0, responsebyte.Length);
        }
    }
}
