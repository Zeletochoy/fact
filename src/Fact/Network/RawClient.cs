﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class RawClient
    {
        string _ServerUrl = "";
        string _UserName = "";
        string _UserId = "";
        byte[] _Password = new byte[0];
        byte[] _CredentialId = null;
        static System.Security.Cryptography.SHA512 SHA512 = System.Security.Cryptography.SHA512.Create();
        
        bool _Authenticated = false;

        public bool Authenticated { get { return _Authenticated; } }

        public bool Started { get { return _Started; } }

        public RawClient(string Server, string UserName, string Password)
        {
            _ServerUrl = Server;
            _UserName = UserName;
            lock (SHA512)
            {
                _Password = SHA512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Password + "__fact__" + UserName));
            }
        }

        public RawClient(string Server, Directory.Credential Credential)
        {
            _ServerUrl = Server;
            _UserName = Credential._Username;
            _Password = Credential._Password;
            _CredentialId = Credential._CredId;
        }

        volatile bool _Running = false;
        volatile bool _Started = false;
        public void Restart()
        {
            lock (this)
            {
                while (_Running) { _Started = false; System.Threading.Monitor.Wait(this); }
                _Started = true;
                new System.Threading.Thread(Run) { IsBackground = true }.Start();
                while (!_Running) { System.Threading.Monitor.Wait(this); }
            }
        }

        public void Start()
        {
            // Yes start is just an alias for restart beause restart
            // will also start the server if it is not started
            Restart();
        }

        public void Stop()
        {
            lock (this)
            {
                while (_Running) { _Started = false; System.Threading.Monitor.Wait(this); }
            }
        }

        class Action
        {
            internal string ActionName = "";
            internal byte[] Content = new byte[0];
            internal Callback Callback = (object Response) => { };
            internal int Timeout = 5000;
        }

        Queue<Action> _ActionQueue = new Queue<Action>();
        public delegate void Callback(object Response);
        public void Do(string Action, Callback Callback, params object[] Parameters)
        {
            DoWithTimeout(Action, 5000, Callback, Parameters);
        }
        public void DoWithTimeout(string Action, int Timeout, Callback Callback, params object[] Parameters)
        {
            if (Parameters.Length == 0 || Parameters == null)
            {
                lock (_ActionQueue)
                {
                    _ActionQueue.Enqueue(new Action() { ActionName = Action, Content = new byte[0], Callback = Callback, Timeout = Timeout });
                }
                return;
            }
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            Internal.StreamTools.WriteInt32(stream, Parameters.Length);
            for (int n = 0; n < Parameters.Length; n++)
            {
                Internal.StreamTools.Serialize(stream, Parameters[n]);   
            }
            lock (_ActionQueue)
            {
                _ActionQueue.Enqueue(new Action() { ActionName = Action, Content = stream.ToArray(), Callback = Callback });
            }
        }

        void Run()
        {
            string userId = "";
            bool _PostAuthenticate = false;
            ulong tokenCount = 0;
            byte[] token = null;
            lock (this)
            {
                _Running = true;
                System.Threading.Monitor.PulseAll(this);
            }
            _Authenticated = false;
            _PostAuthenticate = false;
            int waittime = -10;
            while (_Started)
            {
                DateTime now = DateTime.UtcNow;
                DateTime lastPing = DateTime.UtcNow;
                try
                {
                    if (!_Authenticated && !_PostAuthenticate)
                    {
                        tokenCount = 0;
                        List<byte> password_timecode = new List<byte>(_Password);
                        password_timecode.AddRange(Internal.Information.TimeCode(DateTime.UtcNow));
                        string base64Password = "";
                        lock (SHA512) { base64Password = Tools.ToBase64URL(SHA512.ComputeHash(password_timecode.ToArray())); }

                            Dictionary<string, string> Variables = new Dictionary<string, string>();
                            Variables.Add("action", "@@authenticate");
                            Variables.Add("username", _UserName);
                            Variables.Add("password", base64Password);
                            if (_CredentialId != null)
                                Variables.Add("credential", Tools.ToBase64URL(_CredentialId));
                            string response = Tools.HttpPostRequest(_ServerUrl, Variables, 10000);
                            Fact.Parser.XML parser = new Fact.Parser.XML();
                            parser.Parse(response);
                            Fact.Parser.XML.Node TokenNode = parser.Root.FindNode("token", false);
                            Fact.Parser.XML.Node UserIdNode = parser.Root.FindNode("userid", false);
                            if (TokenNode != null && TokenNode.Children.Count > 0 &&
                                UserIdNode != null && UserIdNode.Children.Count > 0)
                            {
                                _PostAuthenticate = true;
                                token = Tools.FromBase64URL(TokenNode.Children[0].Text);
                                userId = UserIdNode.Children[0].Text.Trim();
                            }
                            else
                            {
                                userId = "";
                                _Authenticated = false;
                                _PostAuthenticate = false;
                                System.Threading.Thread.Sleep(1000);
                            }
                    }
                    else
                    {
                        List<byte> currenttokenBuilder = new List<byte>(token);
                        currenttokenBuilder.AddRange(_Password);
                        currenttokenBuilder.AddRange(BitConverter.GetBytes((Int64)tokenCount));
                        currenttokenBuilder.AddRange(Internal.Information.TimeCode(DateTime.UtcNow));
                        Dictionary<string, string> Variables = new Dictionary<string, string>();
                        Variables.Add("userid", userId);
                        if (_PostAuthenticate)
                        {
                            currenttokenBuilder.AddRange(System.Text.Encoding.UTF8.GetBytes("@@postauthenticate"));
                            byte[] currenttoken = null;
                            lock (SHA512) { currenttoken = SHA512.ComputeHash(currenttokenBuilder.ToArray()); }
                            Variables.Add("token", Tools.ToBase64URL(currenttoken));
                            Variables.Add("action", "@@postauthenticate");
                            string response = Tools.HttpPostRequest(_ServerUrl, Variables, 10000);
                            tokenCount++;
                            if (response != "")
                            {
                                bool valid = false;
                                Fact.Parser.XML parser = new Fact.Parser.XML();
                                parser.Parse(response);
                                Fact.Parser.XML.Node Node = parser.Root.FindNode("authenticated", false);
                                if (Node != null)
                                {
                                    _Authenticated = true; _PostAuthenticate = false;
                                }
                                else
                                {
                                    _Authenticated = false; _PostAuthenticate = false;
                                    System.Threading.Thread.Sleep(1000);
                                }
                            }
                        }
                        else
                        {
                            Action action = null;
                            bool mustPing = (DateTime.UtcNow - lastPing).TotalSeconds > 10;
                            lock (_ActionQueue)
                            {
                                if (_ActionQueue.Count != 0)
                                {
                                    action = _ActionQueue.Dequeue();                                
                                }
                            }
                            if (action != null) { mustPing = false; }
                            if (action == null && !mustPing)
                            {
                                if (waittime > 100) { waittime = 100; }
                                if (waittime > 0)
                                {
                                    System.Threading.Thread.Sleep(waittime);
                                }
                                waittime++;
                            }
                            else
                            {
                                waittime = -10;
                                if (mustPing)
                                {
                                    currenttokenBuilder.AddRange(new byte[0]);
                                    currenttokenBuilder.AddRange(System.Text.Encoding.UTF8.GetBytes("@@ping"));
                                    byte[] currenttoken = null;
                                    lock (SHA512) { currenttoken = SHA512.ComputeHash(currenttokenBuilder.ToArray()); }
                                    Variables.Add("token", Tools.ToBase64URL(currenttoken));
                                    Variables.Add("action", "@@ping");
                                }
                                else
                                {
                                    currenttokenBuilder.AddRange(action.Content);
                                    currenttokenBuilder.AddRange(System.Text.Encoding.UTF8.GetBytes(action.ActionName));
                                    byte[] currenttoken = null;
                                    lock (SHA512) { currenttoken = SHA512.ComputeHash(currenttokenBuilder.ToArray()); }
                                    Variables.Add("token", Tools.ToBase64URL(currenttoken));
                                    Variables.Add("action", action.ActionName);
                                }

                                if (action.Content.Length > 0)
                                    Variables.Add("content", Tools.ToBase64URL(action.Content));
                                string responsetype = "";
                                byte[] rawresponse = Tools.HttpPostRequestRawResult(_ServerUrl, Variables, action.Timeout, out responsetype);
                                tokenCount++;
                                if (rawresponse == null)
                                {
                                    _PostAuthenticate = false; _Authenticated = false;
                                    System.Threading.Thread.Sleep(1000);
                                }
                                else
                                {
                                    if (responsetype == "application/xml")
                                    {
                                        string response = System.Text.Encoding.UTF8.GetString(rawresponse);
                                        if (response != "")
                                        {
                                            lastPing = DateTime.UtcNow;
                                            Fact.Parser.XML parser = new Fact.Parser.XML();
                                            parser.Parse(response);
                                            if (mustPing)
                                            {
                                                Fact.Parser.XML.Node Node = parser.Root.FindNode("pong", false);
                                                if (Node == null)
                                                {
                                                    _PostAuthenticate = false; _Authenticated = false;
                                                    System.Threading.Thread.Sleep(1000);
                                                }
                                            }
                                            else
                                            {
                                                Fact.Parser.XML.Node Node = parser.Root.FindNode("content", false);
                                                if (Node != null && Node.Children.Count > 0)
                                                {
                                                    byte[] responseArray = Tools.FromBase64URL(Node.Children[0].Text);
                                                    System.IO.MemoryStream responseStream = new System.IO.MemoryStream(responseArray);
                                                    action.Callback(Internal.StreamTools.Deserialize(responseStream));

                                                }
                                                else
                                                {
                                                    _PostAuthenticate = false; _Authenticated = false;
                                                    System.Threading.Thread.Sleep(1000);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _PostAuthenticate = false; _Authenticated = false;
                                            System.Threading.Thread.Sleep(1000);
                                        }
                                    }
                                    else
                                    {
                                        lastPing = DateTime.UtcNow;
                                        System.IO.MemoryStream responseStream = new System.IO.MemoryStream(rawresponse);
                                        action.Callback(Internal.StreamTools.Deserialize(responseStream));
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    _Authenticated = false;
                    _PostAuthenticate = false;
                }
            }
            bool NeedRestart = false;
            lock (this)
            {
                _Running = false;
                if (_Started) 
                {
                    NeedRestart = true;
                }
                System.Threading.Monitor.PulseAll(this);
            }
            if (NeedRestart) { Restart(); }
        }
    }
}
