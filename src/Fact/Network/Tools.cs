/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Text;
using System.Collections.Generic;

namespace Fact
{
	namespace Network
	{
		public static class Tools
		{
            public static string HttpGetRequest(string URL, int Timeout)
            {
                // FIXME
                return "";
            }

            public static string EscapeURIString(string URI)
            {
                //FIX ME: Should be implemented correctly
                try
                { return System.Uri.EscapeDataString(URI); }
                catch { return URI; }
            }
            public static string ToBase64(byte[] Array)
            {
                return System.Convert.ToBase64String(Array);
            }
            public static string ToBase64URL(byte[] Array)
            {
                string base64 = ToBase64(Array);
                return base64.Replace('+', '-').Replace('/', '_');
            }
            public static byte[] FromBase64(string Text)
            {
                return System.Convert.FromBase64String(Text);
            }
            public static byte[] FromBase64URL(string Text)
            {
                return FromBase64(Text.Replace('_', '/').Replace('-', '+'));
            }

            public static string HttpPostRequest(string URL, Dictionary<string, string> Variables, int Timeout)
            {
                string useless = "";
                return HttpPostRequest(URL, Variables, Timeout, out useless);
            }

            public static string HttpPostRequest(string URL, Dictionary<string, string> Variables, int Timeout, out string ContentType)
            {
                return System.Text.Encoding.UTF8.GetString(HttpPostRequestRawResult(URL, Variables, Timeout, out ContentType));
            }

            public static byte[] HttpPostRequestRawResult(string URL, Dictionary<string, string> Variables, int Timeout, out string ContentType)
            {
                System.Net.HttpWebRequest webRequest = System.Net.HttpWebRequest.Create(URL) as System.Net.HttpWebRequest;
                webRequest.Method = "POST";
                bool first = true;
                List<byte> fullMessage = new List<byte>();
                foreach (string entry in Variables.Keys)
                {
                    string entryText = (first ? "" : "&") +
                        EscapeURIString(entry) +
                        "=" +
                        EscapeURIString(Variables[entry]);

                    first = false;
                    fullMessage.AddRange(System.Text.Encoding.UTF8.GetBytes(entryText));
                }
                webRequest.ContentLength = fullMessage.Count;
                webRequest.GetRequestStream().Write(fullMessage.ToArray(), 0, fullMessage.Count);
                System.Net.WebResponse webResponse = webRequest.GetResponse();
                ContentType = webResponse.ContentType;
                long remaining = webResponse.ContentLength;
                byte[] buffer = new byte[1024];
                List<byte> responseMessage = new List<byte>();
                while (remaining > 0)
                {
                    int length = webResponse.GetResponseStream().Read(buffer, 0, buffer.Length);
                    remaining -= length;
                    for (int n = 0; n < length; n++) { responseMessage.Add(buffer[n]); }
                }
                return responseMessage.ToArray();
            }

			public static Runtime.Process.Result WebRequest(string URL, int Timeout)
			{
				if (URL.StartsWith ("http://"))
				{
					try
					{
						System.Net.HttpWebRequest Request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create (URL);
						Request.Timeout = Timeout;
						Request.UserAgent = "Fact";
						System.Net.WebResponse response = Request.GetResponse();
						Runtime.Process.Result result = new Fact.Runtime.Process.Result ();
						byte[] array = new byte[response.ContentLength];
						int offset = 0;

						while (offset < array.Length && Timeout > 0)
						{
							offset += response.GetResponseStream().Read(array, offset, array.Length);
							System.Threading.Thread.Sleep(10);
							Timeout -= 10;
						}

						if (Timeout <= 0)
						{
							result.TimeOutExit = true;
							return result;
						}
						result.StdOut = "content type = " + response.ContentType + "\n" + System.Text.Encoding.UTF8.GetString(array);
						return result;
					}
					catch (Exception e)
					{
						Runtime.Process.Result result = new Fact.Runtime.Process.Result ();
						result.StdErr = e.Message; 
						return result;
					}
				}
				{
					Runtime.Process.Result result = new Fact.Runtime.Process.Result ();
					result.StdErr = "Protocol not supported";
					return result;
				}
			}

			public static int FindAvailablePort(int From, int To)
			{
				for (int x = 0; x < 10; x++)
				{
					for (int n = From; n <= To; n++)
					{
						if (IsPortAvailable (n))
							return n;
					}
				}
				return -1;
			}

			public static bool IsPortAvailable(int Port)
			{
				try
				{
					System.Net.Sockets.TcpListener listener = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Loopback, Port);
					listener.ExclusiveAddressUse = false;
					try
					{
						listener.Start();
					}
					catch 
					{
						try
						{
							listener.Stop();
						}
						catch 
						{
						}
						return false;
					}
					try
					{
						listener.Stop();
					}
					catch 
					{
						return false;
					}
					System.Threading.Thread.Sleep(10);
					return true;
				}
				catch 
				{
					return false;
				}
			}
		}
	}
}

