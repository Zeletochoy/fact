﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Network
{
    public class Client
    {
        string _server = "";
        public string Server { get { return _server; } }
        RawClient _Client = null;
        public Client(string Server, string UserName, string Password)
        {
            _server = Server;
            _Client = new RawClient(Server, UserName, Password);
        }

        public Client(string Server, Fact.Directory.Credential Credential)
        {
            _server = Server;
            _Client = new RawClient(Server, Credential);
        }

        public static bool Ping(string Server, int Timeout)
        {
            try
            {
                Dictionary<string, string> variables = new Dictionary<string, string>();
                variables.Add("action", "@@ping");
                string response = Tools.HttpPostRequest(Server, variables, Timeout);
                Parser.XML xml = new Parser.XML();
                xml.Parse(response);
                if (xml.Root.FindNode("pong", false) != null) { return true; }
                return false;
            }
            catch { return false; }
        }

        public object Call(string Method, params object[] Parameters)
        {
            return CallWithTimeout(Method, 5000, Parameters);
        }
        public object CallWithTimeout(string Method, int Timeout, params object[] Parameters)
        {
            if (_Client.Started == false) { _Client.Start(); }
            int CTimeout = Timeout;
            int waittime = -10;
            while (!_Client.Authenticated && CTimeout >= 0) 
            {
                waittime++;
                if (waittime > 0)
                {
                    if (waittime > 50) { waittime = 50; }
                    System.Threading.Thread.Sleep(waittime);
                    CTimeout -= waittime;
                }
            }
            if (!_Client.Authenticated) { throw new Exception("Authentication failure"); }
            object syncResponse = null;
            bool responded = false;
            CTimeout = Timeout;
            _Client.DoWithTimeout(Method, Timeout, (object response) => { responded = true; syncResponse = response; }, Parameters);
            waittime = -10;

            while (!responded && CTimeout >= 0)
            {
                waittime++;
                if (waittime > 0)
                {
                    if (waittime > 50) { waittime = 50; }
                    System.Threading.Thread.Sleep(waittime);
                    CTimeout -= waittime;
                }
            }
            if (!responded)
            {
                throw new Exception("Timeout");
            }
            return syncResponse;
        }

        /// <summary>
        ///  Force the client to close the link with the server
        /// </summary>
        public void Close()
        {
            _Client.Stop();
        }
    }
}
