﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor
{
    public class Instructions
    {
        [AttributeUsage(AttributeTargets.Method)]
        public class Jump : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class RelativeJump : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter1Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter2Byte : System.Attribute { }


        [AttributeUsage(AttributeTargets.Method)]
        public class Parameter4Byte : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class OpCode1Byte : System.Attribute
        {
            byte _OpCode = 0;
            byte _Mask = 0xFF;
            public byte OpCode { get { return _OpCode; } }
            public byte Mask { get { return _Mask; } }

            public OpCode1Byte(byte OpCode) { _OpCode = OpCode; }
            public OpCode1Byte(byte OpCode, byte Mask) { _OpCode = OpCode; _Mask = Mask; }

        }

        [AttributeUsage(AttributeTargets.Method)]
        public class ExecuteAndReturn : System.Attribute { }

        [AttributeUsage(AttributeTargets.Method)]
        public class Fetch : System.Attribute { }
    }
}
