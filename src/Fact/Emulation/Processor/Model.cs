﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Emulation.Processor
{
    unsafe public class Model
    {
        object _Instance = null;
        Dictionary<byte, System.Reflection.MethodInfo> _Method = new Dictionary<byte, System.Reflection.MethodInfo>();
        Dictionary<byte, System.Reflection.MethodInfo> _RetMethod = new Dictionary<byte, System.Reflection.MethodInfo>();
        Dictionary<byte, System.Reflection.MethodInfo> _MethodWithMask = new Dictionary<byte, System.Reflection.MethodInfo>();

        List<_8BOpCode> _8BOpCodes = new List<_8BOpCode>();

        // 0 -> IP
        ulong* _Scratchpad = null;

        class _8BOpCode
        {
            internal byte OpCode = 0x00;
            internal byte Mask = 0xFF;
            internal bool Return = false;
            internal bool Jump = false;
            internal bool RelativeJump = false;
            internal int ParameterSize = 0;
            internal System.Reflection.MethodInfo Method = null;
            public bool Match(byte Code)
            {
                return ((Code & Mask) == OpCode);
            }
        }

        internal System.Reflection.MethodInfo _FetchMethod = null;


        public Model(object Processor)
        {
            _Instance = Processor;
            Type proctype = Processor.GetType();
            _Scratchpad = (ulong*)System.Runtime.InteropServices.Marshal.AllocHGlobal(256 * sizeof(ulong)).ToPointer();
            foreach (System.Reflection.MethodInfo method in proctype.GetMethods())
            {
                System.Reflection.MethodInfo methodcpy = method;
                bool _ExecAndReturn = false;
                int _ParameterSize = 0;
                bool Jump = false;
                bool RelativeJump = false;
                bool Fetch = false;

                foreach ( object attr in method.GetCustomAttributes(false))
                {
                    if (attr is Instructions.ExecuteAndReturn) { _ExecAndReturn = true; }
                    if (attr is Instructions.Jump) { Jump = true; }
                    if (attr is Instructions.RelativeJump) { RelativeJump = true; }
                    if (attr is Instructions.Parameter1Byte) { _ParameterSize++; }
                    if (attr is Instructions.Parameter2Byte) { _ParameterSize += 2; }
                    if (attr is Instructions.Parameter4Byte) { _ParameterSize += 4; }
                    if (attr is Instructions.Fetch) { Fetch = true; }
                }
                foreach (object attr in method.GetCustomAttributes(false))
                {
                    if (attr is Instructions.OpCode1Byte)
                    {
                        _8BOpCode opcode = new _8BOpCode();
                        opcode.OpCode = (attr as Instructions.OpCode1Byte).OpCode;
                        opcode.Mask = (attr as Instructions.OpCode1Byte).Mask;
                        opcode.Method = methodcpy;
                        opcode.Return = _ExecAndReturn;
                        opcode.Jump = Jump;
                        opcode.RelativeJump = RelativeJump;
                        opcode.ParameterSize = _ParameterSize;
                        _8BOpCodes.Add(opcode);
                    }
                }
                if (Fetch)
                {
                    _FetchMethod = methodcpy;
                }
            }
            BuildOpCodeMap();
        }

        delegate void Action();
        Action[] _OpcodeMap = new Action[256];

        void BuildOpCodeMap()
        {
            for (int n = 0; n < 256; n++) { _OpcodeMap[n] = null; }
            for (int n = 0; n < _8BOpCodes.Count; n++)
            {
                _8BOpCode opcode = _8BOpCodes[n];
                for (int x = 0; x < 256; x++)
                {
                    int masked = x & opcode.Mask;
                    byte remain = (byte)(x & ~opcode.Mask);

                    if (masked == (int)opcode.OpCode)
                    {
                        switch (opcode.ParameterSize)
                        {
                            case 0:
                                _OpcodeMap[x] = () =>
                                {
                                    object returnvalue = opcode.Method.Invoke(_Instance, new object[] { (byte)remain });
                                    _Scratchpad[0] += 1;
                                    if (opcode.Return) { _Scratchpad[1] = 1; }
                                    if (opcode.Jump) { _Scratchpad[0] = (ulong)returnvalue; }
                                    if (opcode.RelativeJump) { _Scratchpad[0] = (ulong)((long)_Scratchpad[0] + (long)returnvalue); }
                                };
                                break;
                            case 1:
                                if (_FetchMethod == null) { throw new Exception("Missing fetch method"); }
                                _OpcodeMap[x] = () =>
                                {
                                    object returnvalue = opcode.Method.Invoke(_Instance, new object[]
                                        { (byte)remain,
                                         (byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 1 })
                                        });
                                    _Scratchpad[0] += 2;
                                    if (opcode.Return) { _Scratchpad[1] = 1; }
                                    if (opcode.Jump) { _Scratchpad[0] = (ulong)returnvalue; }
                                    if (opcode.RelativeJump) { _Scratchpad[0] = (ulong)((long)_Scratchpad[0] + (long)returnvalue); }
                                };
                                break;
                            case 2:
                                if (_FetchMethod == null) { throw new Exception("Missing fetch method"); }
                                _OpcodeMap[x] = () =>
                                {
                                    object returnvalue = opcode.Method.Invoke(_Instance, new object[]
                                      { (byte)remain,
                                        (ushort)((ushort)(byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 1 }) +
                                                 (ushort)(byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 2 }) * 0x100)
                                      });
                                    _Scratchpad[0] += 3;
                                    if (opcode.Return) { _Scratchpad[1] = 1; }
                                    if (opcode.Jump) { _Scratchpad[0] = (ulong)returnvalue; }
                                    if (opcode.RelativeJump) { _Scratchpad[0] = (ulong)((long)_Scratchpad[0] + (long)returnvalue); }
                                };
                                break;
                            case 4:
                                if (_FetchMethod == null) { throw new Exception("Missing fetch method"); }
                                _OpcodeMap[x] = () =>
                                {
                                    object returnvalue = opcode.Method.Invoke(_Instance, new object[]
                                    { (byte)remain,
                                        (ulong)((ulong)(byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 1 }) +
                                                (ulong)(byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 2 }) * 0x100 +
                                                (ulong)(byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 3 }) * 0x10000 +
                                                (ulong)(byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] + 4 }) * 0x1000000)
                                    });
                                    _Scratchpad[0] += 5;
                                    if (opcode.Return) { _Scratchpad[1] = 1; }
                                    if (opcode.Jump) { _Scratchpad[0] = (ulong)returnvalue; }
                                    if (opcode.RelativeJump) { _Scratchpad[0] = (ulong)((long)_Scratchpad[0] + (long)returnvalue); }
                                };
                                break;
                            default:
                                throw new InvalidInstruction();
                        }
                    }
                }
            }
            for (int n = 0; n < 256; n++) { if (_OpcodeMap[n] == null) { _OpcodeMap[n] = () => { throw new InvalidInstruction(); }; } }
        }
        public class InvalidInstruction : Exception
        {
            public override string Message
            {
                get
                {
                    return "The virtual processor has jump on an invalid instruction";
                }
            }
        }
        public void DynamicExecution(ulong EntryPoint)
        {
            if (_FetchMethod == null) { throw new Exception("Missing fetch method"); }
            _Scratchpad[0] = EntryPoint;
            _Scratchpad[1] = 0;
            while (true)
            {
                byte opcode = (byte)_FetchMethod.Invoke(_Instance, new object[] { _Scratchpad[0] });
                _OpcodeMap[opcode]();
                if (_Scratchpad[1] != 0) { return; }
            }
        }
    }
}
