﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Compilation.Assembly
{
    public class x86
    {
        enum Mode
        {
            _16_BITS_OLD,
            _32_BITS,
            _64_BITS
        }

        public Object Assemble(string Program)
        {
            List<string> errors = new List<string>();
            List<byte> result = new List<byte>();
            List<KeyValuePair<int, int>> reloc = new List<KeyValuePair<int, int>>();
            Dictionary<string, int> labels = new Dictionary<string, int>();
            Mode mode = Mode._16_BITS_OLD;

            string[] lines = Program.Split('\n', '\r');
            for (int n = 0; n < lines.Length; n++)
            {
                int x = 0;
                string line = "";
                string label = "";

                for (; x < lines[n].Length; x++)
                {
                    if (lines[n][x] == ':') { label = line; line = ""; x++; break; }
                    line += lines[n][x];
                }

                for (; x < lines[n].Length; x++)
                {
                    if (lines[n][x] == ';') { break; }
                    line += lines[n][x];
                }

                string[] elements = line.Split(new char[] { ' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (elements.Length >= 1)
                {
                    if (elements[0].ToLower() == "section" ||
                        elements[0].ToLower() == "segment")
                    {
                        if (elements.Length != 2) { errors.Add("Invalid " + elements[0] + " declaration"); }
                    }
                    else if (elements[0].ToLower() == "bits")
                    {
                        if (elements.Length != 2) { errors.Add("Invalid " + elements[0] + " declaration"); }
                        switch (elements[1].ToLower())
                        {
                            case "16": mode = Mode._16_BITS_OLD; break;
                            case "32": mode = Mode._32_BITS; break;
                            case "64": mode = Mode._64_BITS; break;
                        }
                    }
                    else
                    {
                        try
                        {
                            switch (mode)
                            {
                                case Mode._16_BITS_OLD: Assemble_16_BITS(elements, result); break;
                                case Mode._32_BITS: Assemble_32_BITS(elements, result); break;
                                case Mode._64_BITS: Assemble_64_BITS(elements, result); break;
                            }
                        }
                        catch (Exception e)
                        {
                            errors.Add(e.Message);
                        }
                    }
                }
            }

            return null;
        }

        void Assemble_16_BITS(string[] Elements, List<byte> Output)
        {
            switch (Elements[0])
            {
                case "mov": break;
                case "int": break;
                case "cmp": break;
                case "add": break;
                case "call": break;
                default: throw new Exception("Unsupported mnemonic");
            }
        }

        void Assemble_32_BITS(string[] Elements, List<byte> Output)
        {

        }

        int GetRegisterValue(string register)
        {
            switch (register)
            {
                case "rax": return 0;
                case "rbx": return 3;
                case "rcx": return 1;
                case "rdx": return 2;
                case "rsp": return 4;
                case "rbp": return 5;
                case "rsi": return 6;
                case "rdi": return 7;

                case "r8": return 0;
                case "r9": return 1;
                case "r10": return 2;
                case "r11": return 3;
                case "r12": return 4;
                case "r13": return 5;
                case "r14": return 6;
                case "r15": return 7;
            }
            return 0;
        }

        bool Is64BitsDefaultRegister(string input)
        {
            switch (input.ToLower())
            {
                case "rax":
                case "rbx":
                case "rcx":
                case "rdx":
                case "rsp":
                case "rbp":
                case "rsi":
                case "rdi":
                    return true;
            }
            return false;
        }

        bool Is64BitsExtendedRegister(string input)
        {
            switch (input.ToLower())
            {
                case "r8":
                case "r9":
                case "r10":
                case "r11":
                case "r12":
                case "r13":
                case "r14":
                case "r15":
                    return true;
            }
            return false;
        }

        void Assemble_64_BITS(string[] Elements, List<byte> Output)
        {
            switch (Elements[0])
            {
                case "mov":
                    {
                        if (Elements.Length != 3) { throw new Exception("Invalid syntax"); }
                        if (Is64BitsDefaultRegister(Elements[1]))
                        {
                            if (Is64BitsDefaultRegister(Elements[2]))
                            {
                                Output.Add(0x48);
                                Output.Add(0x89);
                                Output.Add((byte)((GetRegisterValue(Elements[2]) * 0x08) + 0xC0 + GetRegisterValue(Elements[1])));
                            }
                            else if (Is64BitsExtendedRegister(Elements[2]))
                            {
                                Output.Add(0x4C);
                                Output.Add(0x89);
                                Output.Add((byte)((GetRegisterValue(Elements[2]) * 0x08) + 0xC0 + GetRegisterValue(Elements[1])));
                            }
                        }
                    }
                    break;
            }
        }
    }
}
