﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fact.Parser
{
    static public class Tools
    {
        static public int ParseInt(ref int At, string String)
        {
			int value = 0;
			int neg = 1;

			if (String.StartsWith ("0x")) 
			{
				At += 2;
				for (; At < String.Length; At++)
				{
					value *= 16;
					if (String[At] < '0' || String[At] > '9')
					{
						if (String[At] < 'A' || String[At] > 'F')
						{
							if (String[At] < 'a' || String[At] > 'f')
							{
								return value;
							}
						}
					}

					if (String[At] >= '0' && String[At] <= '9')
					{
						value += String[At] - '0';
					}

					if (String[At] >= 'A' && String[At] <= 'F')
					{
						value += String[At] - 'A' + 10;
					}

					if (String[At] >= 'a' && String[At] <= 'f')
					{
						value += String[At] - 'a' + 10;
					}
				}
				return value;
			}

			if (String.StartsWith ("0"))
			{
				At++;
				for (; At < String.Length; At++)
				{
					value *= 8;
					if (String[At] < '0' || String[At] > '7')
					{ return neg * value; }
					value += String[At] - '0';
				}
				return value;
			}

            if (String[At] == '-')
            { neg = -1; At++; }
            for (; At < String.Length; At++)
            {
				value *= 10;
                if (String[At] < '0' || String[At] > '9')
                { return neg * value; }
                value += String[At] - '0';
            }
            return value * neg;
        }
        static public double ParseDouble(ref int At, string String)
        {
            double value = 0.0;
            try
            {
                int n = 0;
                double neg = 1.0;
                if (String[0] == '-')
                {
                    neg = -1.0;
                    n++;
                }
                for (; At < String.Length; At++)
                {
                    if (String[n] == '.')
                    { break; }
                    if (String[n] < '0' || String[n] > '9')
                    { return neg * value; }
                    value *= 10.0;
                    value += String[n] - '0';
                }
                n++;
                double decimalp = 0.1;
                for (; At < String.Length; At++)
                {
                    if (String[n] < '0' || String[n] > '9')
                    { return neg * value; }
                    decimalp /= 1.0;
                    value += (String[n] - '0') * decimalp;
                }
                return neg * value;
            }
            catch { return 0.0; }
        }

        static public float ParseFloat(ref int At, string String)
        {
            float value = 0.0f;
            try
            {
                int n = 0;
                float neg = 1.0f;
                if (String[0] == '-')
                {
                    neg = -1.0f;
                    n++;
                }
                for (; n < String.Length; n++)
                {
                    if (String[n] == '.')
                    { break; }
                    if (String[n] < '0' || String[n] > '9')
                    { return neg * value; }
                    value *= 10.0f;
                    value += String[n] - '0';
                }
                n++;
                float decimalp = 0.1f;
                for (; n < String.Length; n++)
                {
                    if (String[n] < '0' || String[n] > '9')
                    { return neg * value; }
                    decimalp /= 1.0f;
                    value += (String[n] - '0') * decimalp;
                }
                return neg * value;
            }
            catch { return 0.0f; }
        }

        static public string ParseString(ref int At, string String)
        {
			String = String.Trim ();
            StringBuilder builder = new StringBuilder();
            bool esc = false;
            if (String[At] == '"') 
            {
                At++;
            }
            for (; At < String.Length; At++)
            {
                if (esc)
                {
                    switch (String[At])
                    {
                        case 'n': builder.Append('\n'); break;
                        case 'r': builder.Append('\r'); break;
                        case 't': builder.Append('\t'); break;
                        case '0': builder.Append('\0'); break;
                        case '\'': builder.Append('\''); break;
                        case '"': builder.Append('"'); break;
                        case '\\': builder.Append('\\'); break;

                    }
                    esc = false;
                }
                else
                {
                    if (String[At] == '"')
                    {
                        At++;
                        return builder.ToString();
                    }
                    else if (String[At] == '\\')
                    {
                        esc = true;
                    }
                    else
                    {
                        builder.Append(String[At]);
                    }
                }
            }
            return builder.ToString();
        }

		static public char ParseChar(ref int At, string String)
		{
			String = String.Trim ();
			StringBuilder builder = new StringBuilder();
			bool esc = false;
			if (String[At] == '\'') 
			{
				At++;
			}
			for (; At < String.Length; At++)
			{
				if (esc)
				{
					switch (String[At])
					{
						case 'n': builder.Append('\n'); break;
						case 'r': builder.Append('\r'); break;
						case 't': builder.Append('\t'); break;
						case '0': builder.Append('\0'); break;
						case '\'': builder.Append('\''); break;
						case '"': builder.Append('"'); break;
						case '\\': builder.Append('\\'); break;

					}
					esc = false;
				}
				else
				{
					if (String[At] == '\'')
					{
						At++;
						return builder.ToString()[0];
					}
					else if (String[At] == '\\')
					{
						esc = true;
					}
					else
					{
						builder.Append(String[At]);
					}
				}
			}
			return builder.ToString()[0];
		}

        public static string EscapeString(string Value)
        {
            StringBuilder builder = new StringBuilder();
            for (int n = 0; n < Value.Length; n++)
            {
                if (Value[n] == '"') { builder.Append("\\\""); }
				else if (Value[n] == '\\') { builder.Append("\\\\"); }
                else { builder.Append(Value[n]); }
            }
            return builder.ToString();
        }
    }
}
