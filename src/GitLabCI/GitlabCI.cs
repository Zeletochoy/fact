﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitLabCI
{
    public class GitLabCI : Fact.Network.Service
    {
        void PeriodicCheck(ref Fact.Threading.Job.Status Status)
        {
            lock (this)
            {
                
            }
        }

        GitLabCI() : base("GitLabCI")
        {
            Fact.Threading.Job.CreateTimer(PeriodicCheck, 1000, true);
        }
    }
}
