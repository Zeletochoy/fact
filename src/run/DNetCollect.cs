﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace run
{
    class DNetCollect : Collector
    {
       Dictionary<string, string> _Parameters = new Dictionary<string, string>();
       System.Reflection.Assembly _Assembly = null;
       public DNetCollect(System.Reflection.Assembly Assembly, Dictionary<string, string> Parameters, Fact.Processing.Project Project) : base (Project)
       {
           _Parameters = Parameters;
           _Assembly = Assembly;
       }

       object ToTestMethod(System.Reflection.MethodInfo Method, object Instance, Fact.Test.Result.Result.VisibilityLevel Visibility)
       {
           if (Method.ReturnType == typeof(List<Fact.Test.Result.Result>))
           {
               if (Method.GetParameters().Length == 1 && Method.GetParameters()[0].ParameterType == typeof(Fact.Processing.Project))
               {
                   return new TestsProject((Fact.Processing.Project project) => { return Method.Invoke(Instance, new object[] { project }) as List<Fact.Test.Result.Result>; }, null, "") { Visibility = Visibility };
               }
               else if (Method.GetParameters().Length == 0)
               {
                   return new TestsProject((Fact.Processing.Project project) => { return Method.Invoke(Instance, new object[] { }) as List<Fact.Test.Result.Result>; }, null, "") { Visibility = Visibility };
               }
           }
           else if (Method.ReturnType == typeof(Fact.Test.Result.Result))
           {
               if (Method.GetParameters().Length == 1 && Method.GetParameters()[0].ParameterType == typeof(Fact.Processing.Project))
               {
                   return new TestProject((Fact.Processing.Project project) => { return Method.Invoke(Instance, new object[] { project }) as Fact.Test.Result.Result; }, null, "") { Visibility = Visibility };
               }
               else if (Method.GetParameters().Length == 0)
               {
                   return new TestProject((Fact.Processing.Project project) => { return Method.Invoke(Instance, new object[] { }) as Fact.Test.Result.Result; }, null, "") { Visibility = Visibility };
               }
           }
           else if (Method.ReturnType == typeof(void))
           {
               if (Method.GetParameters().Length == 1 && Method.GetParameters()[0].ParameterType == typeof(Fact.Processing.Project))
               {
                   return new TestProject((Fact.Processing.Project project) => { Method.Invoke(Instance, new object[] { project }); return null; }, null, "") { Visibility = Visibility };
               }
               else if (Method.GetParameters().Length == 0)
               {
                   return new TestProject((Fact.Processing.Project project) => { Method.Invoke(Instance, new object[] { }); return null; }, null, "") { Visibility = Visibility };
               }
           }
           throw new Exception("The method " + Method.Name + " is not a valid test.");
       }

       void GetDependencies()
       {
           List<System.Reflection.Assembly> depAssList = new List<System.Reflection.Assembly>();
           System.Reflection.AssemblyName[] assNames = _Assembly.GetReferencedAssemblies();
           System.Uri pUri = new Uri(_Assembly.CodeBase);
           string searchPath = System.IO.Path.GetDirectoryName(pUri.AbsolutePath);
           foreach (System.Reflection.AssemblyName assName in assNames)
           {
               if (assName.Name == "mscorlib" ||
                   assName.Name == "Fact" ||
                   assName.Name.StartsWith("System.")) // skip mscorlib.dll and Fact.dll and system libs
                   continue;
               string depDllPath = System.IO.Path.Combine(searchPath, assName.Name + ".dll");
               if (!System.IO.File.Exists(depDllPath))
               {
                   Fact.Log.Warning("Unable to load collect assembly dependency " + assName.Name);
                   continue;
               }
               else
               {
                   AddDependency(new Fact.Processing.File(System.IO.File.ReadAllBytes(depDllPath)),
                                 System.IO.Path.GetFileName(depDllPath));
               }
               depAssList.Add(System.Reflection.Assembly.LoadFile(depDllPath));
           }
       }

       void SolveParameter(object Object) { SolveParameter(Object, Object.GetType()); }
       void SolveParameter(object Object, Type type)
       {
           System.Reflection.BindingFlags StaticFlags = Object == null ?
               System.Reflection.BindingFlags.Static :
               System.Reflection.BindingFlags.Instance;
           foreach (System.Reflection.FieldInfo field in type.GetFields(System.Reflection.BindingFlags.Public |
                                                               System.Reflection.BindingFlags.NonPublic |
                                                               StaticFlags |
                                                               System.Reflection.BindingFlags.FlattenHierarchy))
           {
               foreach (object Attribute in field.GetCustomAttributes(true))
               {
                   if (Attribute is Fact.Attribute.Test.Parameter)
                   {
                       string name = (Attribute as Fact.Attribute.Test.Parameter).Name;
                       if (_Parameters.ContainsKey(name))
                       {
                           if (field.FieldType == typeof(string)) { field.SetValue(Object, _Parameters[name]); }
                           else if (field.FieldType == typeof(bool))
                           {
                               field.SetValue(Object,
                                   _Parameters[name].ToLower() == "yes" ||
                                   _Parameters[name].ToLower() == "true" ||
                                   _Parameters[name].ToLower() == "enable" ||
                                   _Parameters[name].ToLower() == "1");
                           }
                           else if (field.FieldType == typeof(Fact.Processing.Project))
                           {
                               if (System.IO.File.Exists(_Parameters[name]))
                               {
                                   Fact.Processing.Project project = new Fact.Processing.Project("");
                                   project.Load(_Parameters[name]);
                                   AddDependency(
                                       new Fact.Processing.File(
                                           System.IO.File.ReadAllBytes(_Parameters[name]),
                                           System.IO.Path.GetFileName(_Parameters[name])),
                                       _Parameters[name]);
                                   field.SetValue(Object, project);
                               }
                               else
                               {
                                   throw new Exception("File not found: " + _Parameters[name]);
                               }
                           }
                           else { throw new Exception("Unsupported parameter type: " + field.FieldType.FullName); }
                       }
                   }
               }
           }
       }

       public override void Setup()
       {
           GetDependencies();
           HashSet<string> _RecordedType = new HashSet<string>();
           Dictionary<string, string> groupalias = new Dictionary<string, string>();
           Dictionary<string, object> groupInstance = new Dictionary<string, object>();
           Dictionary<string, Type> groupType = new Dictionary<string, Type>();

       restart:
           bool _MustRestart = false;
           bool _Modified = false;
           try
           {
               foreach (System.Reflection.Module module in _Assembly.GetModules())
               {
                   try
                   {
                       foreach (Type type in module.GetTypes())
                       {
                           try
                           {

                               Fact.Attribute.Test.Group TestGroup = null;
                               string group = "";
                               string path = "";
                               foreach (object attr in type.GetCustomAttributes(true))
                               {
                                   if (attr is Fact.Attribute.Test.Group)
                                   {
                                       TestGroup = (Fact.Attribute.Test.Group)(attr);
                                       group = TestGroup.Name;
                                   }
                               }

                               if (TestGroup != null)
                               {
                                   try { if (_RecordedType.Contains(type.FullName.Replace('+', '.'))) { continue; } }
                                   catch { }
                                   // Create the group tree
                                   if (type.FullName.Contains('+'))
                                   {
                                       string[] subgroups = type.FullName.Split('+');
                                       string parent_group_name = subgroups[0];
                                       int n = 1;
                                       while (true)
                                       {
                                           if (n == subgroups.Length) { /* Must not occurs */ break; }
                                           string first_group_name = parent_group_name + "." + subgroups[n];
                                           n++;
                                           if (_RecordedType.Contains(parent_group_name))
                                           {
                                               if (groupalias.ContainsKey(parent_group_name))
                                               {
                                                   path +=  "/" + groupalias[parent_group_name];
                                               }
                                               // Nothing to do my parent is a group
                                               if (n == subgroups.Length)
                                               {
                                                   // Everything is ok we can continue
                                                   break;
                                               }
                                           }
                                           else
                                           {
                                               // The introspection tree is not yet ready to handle this type
                                               _MustRestart = true;
                                               goto nexttype;
                                           }
                                           parent_group_name = first_group_name;
                                       }
                                   }
                                   try { groupalias.Add(type.FullName.Replace('+', '.'), group); }
                                   catch { }
                                   
                                   // compute the fullname of the group
                                   group = path + "/" + group;

                                   // Format the group name 
                                   if (group.StartsWith("/")) { group = group.Substring(1); }
                                   if (path.StartsWith("/")) { path = path.Substring(1); }
                                   if (!groupType.ContainsKey(group))
                                   {
                                       Type t = type;
                                       groupType.Add(group, type);
                                   }

                                   // Collect the tests
                                   object instance = null;
                                   SolveParameter(null, type);

                                   // Check if we have to set the Parent<T> Link
                                   try
                                   {
                                       // Black magick start here

                                       // Don't refactor or make comment on this part of the code if you are not familiar with:
                                       // FlattenHiierachy reflection restriction, const type problem of generic, fact internal group tree

                                       // Now we have to make the virtual link for parent<T> if the user has requestedIt
                                       if (path != "" && path != "/")
                                       {
                                           // Ok so we do have a parent. Check if we want to have a parent link
                                           System.Reflection.FieldInfo parentField = type.GetField("_Parent",
                                               System.Reflection.BindingFlags.NonPublic |
                                               System.Reflection.BindingFlags.Instance |
                                               System.Reflection.BindingFlags.FlattenHierarchy);
                                           if (parentField != null)
                                           {
                                               // Now check the magick attribute
                                               foreach (object hiddenAttr in parentField.GetCustomAttributes(true))
                                               {
                                                   if (hiddenAttr.GetType().FullName == "Fact.Attribute.Test.Internal_ParentAccessTag")
                                                   {

                                                       // Create the instance of the class since it will be needed
                                                       try
                                                       {
                                                           instance = type.GetConstructor(new Type[] { }).Invoke(new object[] { });
                                                           SolveParameter(instance);
                                                           if (!groupInstance.ContainsKey(group))
                                                           { groupInstance.Add(group, instance); }
                                                       }
                                                       catch
                                                       {
                                                           Fact.Log.Error("Could not build an instance of type " + type.Name);
                                                           break;
                                                       }

                                                       object parentinstance = null;
                                                       if (groupInstance.ContainsKey(path)) { parentinstance = groupInstance[path]; }
                                                       else if (groupType.ContainsKey(path))
                                                       {
                                                           try
                                                           {
                                                               parentinstance = groupType[path].GetConstructor(new Type[] { }).Invoke(new object[] { });
                                                               SolveParameter(parentinstance);
                                                               if (!groupInstance.ContainsKey(path))
                                                               { groupInstance.Add(path, parentinstance); }
                                                           }
                                                           catch
                                                           {
                                                               Fact.Log.Error("Could not build an instance of type " + path + " for group " + group);
                                                               break;
                                                           }
                                                       }
                                                       if (parentinstance == null)
                                                       {
                                                           Fact.Log.Error("Could not build an instance of type " + path + " for group " + group);
                                                       }
                                                       else
                                                       {
                                                           try
                                                           {
                                                               parentField.SetValue(instance, parentinstance);

                                                           }
                                                           catch
                                                           {
                                                               Fact.Log.Error("Could not build link between group " + path + " and group " + group);
                                                           }
                                                       }
                                                       break;
                                                   }
                                               }
                                           }
                                       }
                                   }
                                   catch { }


                                   foreach (System.Reflection.MethodInfo method in type.GetMethods())
                                   {
                                       foreach (object attr in method.GetCustomAttributes(true))
                                       {
                                           if (!method.IsStatic && instance == null)
                                           {
                                               try
                                               {
                                                   instance = type.GetConstructor(new Type[] { }).Invoke(new object[] { });
                                                   SolveParameter(instance);
                                                   if (!groupInstance.ContainsKey(group))
                                                   { groupInstance.Add(group, instance); }
                                               }
                                               catch
                                               { throw new Exception("Impossible to generate the instace for the class " + type.FullName); }
                                           }
                                           if (attr is Fact.Attribute.Test.Setup) { AddSetup(group, (attr as Fact.Attribute.Test.Setup).Name, ToTestMethod(method, instance, (attr as Fact.Attribute.Test.Setup).Visibility)); }
                                           else if (attr is Fact.Attribute.Test.Teardown) { AddTeardown(group, (attr as Fact.Attribute.Test.Teardown).Name, ToTestMethod(method, instance, (attr as Fact.Attribute.Test.Teardown).Visibility)); }
                                           else if (attr is Fact.Attribute.Test.Test) { AddTest(group, (attr as Fact.Attribute.Test.Test).Name, ToTestMethod(method, instance, (attr as Fact.Attribute.Test.Test).Visibility)); }
                                       }
                                   }
                               }
                           }
                           catch { }
                           try { _RecordedType.Add(type.FullName.Replace('+', '.')); _Modified = true; }
                           catch { }
                       nexttype: ;
                       }
                   }
                   catch { }
               }
           }

           catch { }
           if (_MustRestart && _Modified) { goto restart; }
       }
    }
}
