﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace run
{
    public class IPyCollect : Collector
    {

        private System.IO.FileInfo scriptFileInfo;
        private Microsoft.Scripting.Hosting.ScriptEngine engine;
        private Microsoft.Scripting.Hosting.ScriptSource source;
        private Microsoft.Scripting.Hosting.ScriptScope scope;

        public IPyCollect(System.IO.FileInfo fileInfo, Fact.Processing.Project Project)
            : base(Project)
        {
            scriptFileInfo = fileInfo;
            // Python runtime init
            engine = IronPython.Hosting.Python.CreateEngine();
            engine.Runtime.LoadAssembly(typeof (Fact.Test.Result.Result).Assembly);
            source = engine.CreateScriptSourceFromFile(scriptFileInfo.FullName);
            scope = engine.CreateScope();
        }

        public override void Setup()
        {
            // FIXME Implement the new interface
            // Add the method to the collector here
            // AddSetup("Group", "Test", Py2FactTest(...));
            // AddTest("Group", "Test", Py2FactTest(...));

            // Insert fact test loader in the runtime
            List<IronPython.Runtime.PythonFunction> testFunctions = new List<IronPython.Runtime.PythonFunction>();
            List<IronPython.Runtime.PythonFunction> testsFunctions = new List<IronPython.Runtime.PythonFunction>();
            object testLoader = (Func<IronPython.Runtime.PythonFunction, IronPython.Runtime.PythonFunction>)
                                (fun =>
                                     {
                                         testFunctions.Add(fun);
                                         return fun;
                                     });
            object testsLoader = (Func<IronPython.Runtime.PythonFunction, IronPython.Runtime.PythonFunction>)
                                 (fun =>
                                      {
                                          testsFunctions.Add(fun);
                                          return fun;
                                      });
            // FIXME Now we have to handle the setup/teardown now
            // Moreover I will prefer something where we don't explicitly return a test list so we can trash FactTests
            // And it will be grat to have the same name as C#
            scope.SetVariable("FactTest", testLoader);
            scope.SetVariable("FactTests", testsLoader);

            // Execute the test script while loading  
            try
            {
                source.Execute(scope);
            }
            catch (Exception e)
            {
                if (e is Microsoft.Scripting.SyntaxErrorException ||
                    e is IronPython.Runtime.UnboundNameException)
                {
                    Fact.Log.Info(engine.GetService<Microsoft.Scripting.Hosting.ExceptionOperations>().FormatException(e));
                    return;
                }
                throw;
            }

            foreach (IronPython.Runtime.PythonFunction pythonFunction in testFunctions)
            {
                TestProject test = new TestProject(Py2FactTest(pythonFunction), null, "");
                test._name = pythonFunction.__name__;
                // FIXME Switch to the new one: Chain.AddCheck(test.Run);
                // It should just be AddTest("", pythonFunction.__name__, test) but I have not tested it
            }
            foreach (IronPython.Runtime.PythonFunction pythonFunction in testsFunctions)
            {
                TestsProject test = new TestsProject(Py2FactTests(pythonFunction), null, "");
                test._name = pythonFunction.__name__;
                // FIXME Switch to the new one Chain.AddMultiCheck(test.Run);
                // It should just be AddTest("", pythonFunction.__name__, test) but I have not tested it
            }
        }

        private Func<Fact.Processing.Project, Fact.Test.Result.Result> Py2FactTest(
            IronPython.Runtime.PythonFunction pythonTest)
        {
            return (stdProj => Fact.Test.Result.Result.Cast(engine.Operations.Invoke(pythonTest, stdProj)));;
        }

        private Func<Fact.Processing.Project, List<Fact.Test.Result.Result>> Py2FactTests(IronPython.Runtime.PythonFunction pythonTest)
        {
            return (stdProj =>
                        {
                            try
                            {
                                return ((IList<object>) engine.Operations.Invoke(pythonTest, stdProj)).Select(v => Fact.Test.Result.Result.Cast(v)).ToList();
                            }
                            catch (IronPython.Runtime.UnboundNameException e)
                            {
                                Fact.Log.Info(
                                    engine.GetService<Microsoft.Scripting.Hosting.ExceptionOperations>().FormatException
                                        (e));
                                return null;
                            }
                        });
        }
    }
}