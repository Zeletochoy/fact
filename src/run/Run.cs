﻿/*                                                                           *
 * Copyright © 2014, Raphaël Boissel, Jean-Luc Bounthong                     *  
 * Permission is hereby granted, free of charge, to any person obtaining     *
 * a copy of this software and associated documentation files, to deal in    *
 * the Software without restriction, including without limitation the        *
 * rights to use, copy, modify, merge, publish, distribute, sublicense,      *
 * and/or sell copies of the Software, and to permit persons to whom the     *
 * Software is furnished to do so, subject to the following conditions:      *
 *                                                                           *
 * - The above copyright notice and this permission notice shall be          *
 *   included in all copies or substantial portions of the Software.         *
 * - The Software is provided "as is", without warranty of any kind,         *
 *   express or implied, including but not limited to the warranties of      *
 *   merchantability, fitness for a particular purpose and noninfringement.  *
 *   In no event shall the authors or copyright holders. be liable for any   *
 *   claim, damages or other liability, whether in an action of contract,    *
 *   tort or otherwise, arising from, out of or in connection with the       *
 *   software or the use or other dealings in the Software.                  *
 * - Except as contained in this notice, the name of Raphaël Boissel or      *
 *   Jean-Luc Bounthong shall not be used in advertising or otherwise to     *
 *   promote the sale, use or other dealings in this Software without prior  *
 *   written authorization from Raphaël Boissel or Jean-Luc Bounthong.       *
 *                                                                           */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace run
{
    public class RunPlugin : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Run";
            }
        }

        public List<KeyValuePair<string, string>> GetTestPlan(string[] RequestedTestPlan)
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            foreach (string test in RequestedTestPlan)
            {
                StringBuilder fullgroup = new StringBuilder();
                string[] groups = test.Split('/');
                for (int n = 0; n < groups.Length - 1; n++)
                {
                    fullgroup.Append(groups[n]);
                    fullgroup.Append("/");
                }
                string testname = groups[groups.Length - 1];
                string groupname = fullgroup.ToString();
                if (groupname.EndsWith("/"))
                { groupname = groupname.Substring(0, groupname.Length - 1); }
                result.Add(new KeyValuePair<string, string>(groupname, testname));
            }
            return result;
        }

        
        Fact.Processing.Project RemoteGetResult(Remote.Job Job,
            out bool CommunicationError,
            out string StdOut,
            out string StdErr)
        {
            StdErr = "";
            StdOut = "";
            CommunicationError = false;
            try
            {
                if (Job == null) { CommunicationError = true; return null; }

                Job.Execute();
                Fact.Processing.File file = Job.GetFile("output.ff");
                StdOut = Job.StdOut;
                StdErr = Job.StdErr;
                Job.Delete();

                if (!Job.Valid) { CommunicationError = true; return null; }

                if (file != null)
                {
                    Fact.Processing.Project project = new Fact.Processing.Project("output");
                    project.Load(file);
                    return project;
                }
            }
            catch
            {
                CommunicationError = true;
                return null;
            }
            return null;
        }

        PendingJob RemoteRunTestPlan(
            List<KeyValuePair<string, string>> TestPlan,
            List<KeyValuePair<string, Fact.Processing.File>> Dependencies,
            Dictionary<string, string> Parameters,
            Fact.Network.Client Client,
            Fact.Processing.File Input,
            Fact.Processing.File Testsuite,
            string Prefix,
            string AgentGroup,
            string TestGroup)
        {
            //Generate the Testplan String
            StringBuilder testplanstring = new StringBuilder();
            foreach (KeyValuePair<string, string> test in TestPlan)
            {
                testplanstring.Append(test.Key);
                testplanstring.Append("/");
                testplanstring.Append(test.Value);
                testplanstring.Append(";");
            }

            // Create the arguments
            List<string> Arguments = new List<string>();
            Arguments.Add("--nologo");
            Arguments.Add("test");  Arguments.Add("run");
            Arguments.Add("--testsuite");  Arguments.Add("testsuite.dll");
            if (Input != null)
            {
                Arguments.Add("--input");  Arguments.Add("input.ff");
            }
            Arguments.Add("--output");  Arguments.Add("output.ff");
            if (Prefix != "")
            {
                Arguments.Add("--prefix");  Arguments.Add(Prefix);
            }
            foreach (KeyValuePair<string, string> parameter in Parameters)
            {
                Arguments.Add("--parameters");
                Arguments.Add(parameter.Key + "=" + Fact.Parser.Tools.EscapeString(parameter.Value));
            }

            Arguments.Add("--internal-testplan");  Arguments.Add(testplanstring.ToString());
            Arguments.Add("--internal-purge");
            Remote.Job Job = new Remote.Job(Client, AgentGroup);
            if (Input != null)
            {
                Job.SendFile(Input, "input.ff");
            }
            Job.SendFile(Testsuite, "testsuite.dll");
            foreach (KeyValuePair<string, Fact.Processing.File> dep in Dependencies)
            {
                Job.SendFile(dep.Value, dep.Key);
            }

            Job.Run("fact", Arguments.ToArray());
            Job.ExportFile("output.ff");
            
            // We don't want to get the file used as input in the local storage
            // It will overload the network
            Job.DeleteFile("testsuite.dll");
            if (Input != null) { Job.DeleteFile("input.ff"); }
            foreach (KeyValuePair<string, Fact.Processing.File> dep in Dependencies)
            {
                Job.DeleteFile(dep.Key);
            }
            Job.Publish();
            PendingJob pendingJob = new PendingJob();
            pendingJob.AgentGroup = AgentGroup;
            pendingJob.TestGroup = TestGroup;

            pendingJob.TestPlan = TestPlan;
            pendingJob.Job = Job;
            return pendingJob;
        }

        class PendingJob
        {
            internal string AgentGroup;
            internal string TestGroup;
            internal List<KeyValuePair<string, string>> TestPlan;
            internal Remote.Job Job;
        }

        public override int Run(string[] args)
        {
            Fact.Parser.CommandLine CommandLine = new Fact.Parser.CommandLine();
            CommandLine.ProgramName = "run";
            CommandLine.AddStringInput("input", "input fact package", ""); CommandLine.AddShortInput("i", "input");
            CommandLine.AddStringInput("output", "output fact package", ""); CommandLine.AddShortInput("o", "output");
            CommandLine.AddStringInput("testsuite", "testsuite containing the tests to run", ""); CommandLine.AddShortInput("t", "testsuite");
            CommandLine.AddStringInput("server", "when deployed on a cluster this is used for the main server address", ""); CommandLine.AddShortInput("s", "server");
            CommandLine.AddStringInput("credential", "credential used to cnnect to the server", ""); CommandLine.AddShortInput("c", "credential");
            CommandLine.AddStringInput("group", "set the agent group targeted by this testsuite (with --server)", "");
            
            CommandLine.AddStringInput("filter-in", "Select only some specific test to run using a regexp", ""); CommandLine.AddShortInput("f", "filter-in");
            CommandLine.AddStringInput("filter-out", "Exclude some test of the test plan using a regexp", "");
            CommandLine.AddStringInput("filter-group-in", "Select only some specific group to run using a regexp", "");
            CommandLine.AddStringInput("filter-group-out", "Exclude some group of the test plan using a regexp", "");

            CommandLine.AddStringListInput("parameters", "list of parameters injected into the test (format: parameter=value)", new List<string>());

            CommandLine.AddBooleanInput("dryrun", "Do everything except the launching of the test", false);

            CommandLine.AddStringInput("prefix", "Add a prefix to the name of the test", "");

            CommandLine.AddStringInput("internal-testplan", "internal use only", "");
            CommandLine.AddBooleanInput("internal-purge", "internal use only", false);

            CommandLine.AddStringInput("resources", "Specify the resources files that must be uploaded on the server (useless without --server)", "");

            CommandLine.AddBooleanInput("help", "display the help message", false); CommandLine.AddShortInput("h", "help");
            CommandLine.AddBooleanInput("verbose", "display more information", false); CommandLine.AddShortInput("v", "verbose");

            if (!CommandLine.Parse(args)) { CommandLine.PrintErrors(); return 1; }

            string testsuite = CommandLine["testsuite"].AsString();
            string inputpackage = CommandLine["input"].AsString();
            string outputpackage = CommandLine["output"].AsString();
            string server = CommandLine["server"].AsString();
            string prefix = CommandLine["prefix"].AsString();
            bool verbose = CommandLine["verbose"].AsBoolean();
            string filterin = CommandLine["filter-in"].AsString();
            string filterout = CommandLine["filter-out"].AsString();
            string filteringroup = CommandLine["filter-group-in"].AsString();
            string filteroutgroup = CommandLine["filter-group-out"].AsString();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            foreach (string parameter in CommandLine["parameters"].AsStringList())
            {
                int parameterN = 0;
                for (; parameterN < parameter.Length; parameterN++) { if (!char.IsWhiteSpace(parameter[parameterN])) { break; } }
                StringBuilder name = new StringBuilder();
                for (; parameterN < parameter.Length; parameterN++) { if (parameter[parameterN] == '=') { parameterN++; break; } name.Append(parameter[parameterN]); }
                StringBuilder value = new StringBuilder();
                for (; parameterN < parameter.Length; parameterN++) { value.Append(parameter[parameterN]); }
                parameters.Add(name.ToString().Trim(), value.ToString());      
            }

            string[] requestedtestplan =
                CommandLine["internal-testplan"].AsString().Split(
                    new char[] { ';' },
                    StringSplitOptions.RemoveEmptyEntries);

            List<string> filters = new List<string>();
            if (CommandLine["help"].AsBoolean()) { CommandLine.PrintUsage(); return 0; }
            if (testsuite == "") { CommandLine.PrintUsage(); return 1; }

            if (!System.IO.File.Exists(testsuite))
            {
                Console.WriteLine("Can't load " + testsuite + ".");
                return 1;
            }
            if (!CommandLine ["filter-in"].IsDefault)
            {
                filters.Add(CommandLine ["filter-in"].AsString());
            }

            Fact.Processing.Project project = new Fact.Processing.Project("");
            if (inputpackage != "")
            {
                if (!System.IO.File.Exists(inputpackage))
                {
                    Console.WriteLine("Can't load " + inputpackage + ".");
                    return 1;
                }
                project.Load(inputpackage);
            }
            else if (outputpackage != "")
            {
                project = new Fact.Processing.Project(System.IO.Path.GetFileNameWithoutExtension(outputpackage));
            }
            Fact.Log.Verbose("Load package ...");
            System.IO.FileInfo info = new System.IO.FileInfo(testsuite);

            Fact.Log.Verbose("Collect tests ...");
            List<KeyValuePair<string, string>> testPlan = new List<KeyValuePair<string, string>>();
            Collector collector = null;
            if (info.Extension.ToLower() == ".dll" || info.Extension.ToLower() == "dll")
            {
                collector = new DNetCollect(System.Reflection.Assembly.LoadFrom(info.FullName), parameters,  project);
            }
            else if (info.Extension.ToLower() == ".py" || info.Extension.ToLower() == "py")
            {
                collector = new IPyCollect(info, project);
            }

            if (collector != null)
            {
                collector.Setup();
                if (requestedtestplan.Length != 0)
                { testPlan = GetTestPlan(requestedtestplan); }
                else
                { testPlan = collector.ComputeTestPlan(filterin, filterout, filteringroup, filteroutgroup); }
            }

            if (CommandLine["server"].IsDefault == false)
            {
                if (CommandLine["resources"].IsDefault == false)
                {
                    // FIXME support string array
                    string ressource = CommandLine["resources"].AsString();
                    if (System.IO.File.Exists(ressource))
                    {
                        Fact.Processing.File ressourcefile =
                            new Fact.Processing.File(
                                System.IO.File.ReadAllBytes(ressource),
                                System.IO.Path.GetFileName(ressource));
                        collector.AddDependency(ressourcefile, ressource);
                    }
                }
                Fact.Log.Verbose("Connection to the server " + server + " ...");
                string credentialFile = CommandLine["credential"];
                Fact.Network.Client Client = null;
                if (credentialFile != "")
                {
                    Fact.Directory.Credential Cred = new Fact.Directory.Credential(credentialFile);
                    Client = new Fact.Network.Client(server, Cred);
                }
                else
                {
                    string username = "";
                    string password = "";
                    Fact.Log.Login(out username, out password);
                    Client = new Fact.Network.Client(server, username, password);
                }
                if (Client != null)
                {
                    string stdout = "";
                    string stderr = "";

                    bool CommunicationError;
                    Fact.Processing.File InputFile = null;
                    if (inputpackage != "")
                    {
                        InputFile = new Fact.Processing.File(new byte[] { }, "input.ff");
                        project.Save(InputFile, true);
                    }
                    Fact.Processing.File TestsuiteFile = null;
                    if (testsuite != "")
                    { TestsuiteFile = new Fact.Processing.File(System.IO.File.ReadAllBytes(testsuite), "testsuite.dll"); }

                    Queue<PendingJob> jobs = new Queue<PendingJob>();
                    foreach (string group in collector.ListAllGroups())
                    {
                        testPlan = collector.ComputeTestPlan(group, filterin, filterout, filteringroup, filteroutgroup);
                        if (testPlan.Count == 0) { continue; }
                        PendingJob projectjob =
                            RemoteRunTestPlan(testPlan,
                                              collector.ListDependenciesValue(),
                                              parameters,
                                              Client,
                                              InputFile,
                                              TestsuiteFile,
                                              prefix,
                                              CommandLine["group"].AsString(),
                                              group);
                        jobs.Enqueue(projectjob);
                    }

                    while (jobs.Count > 0)
                    {
                        PendingJob projectjob = jobs.Dequeue();
                        string group = projectjob.TestGroup;
                        testPlan = projectjob.TestPlan;
                        Fact.Processing.Project projectresult =
                            RemoteGetResult(projectjob.Job, out CommunicationError, out stdout, out stderr);


                        if (CommunicationError)
                        {
                            Fact.Log.Error("Impossible to comunicate with the server.");
                            return 1;
                        }
                        // append the result of the test in the project
                        if (project != null)
                        {
                            if (projectresult == null)
                            {
                                Fact.Processing.Chain skipchain = new Fact.Processing.Chain(project);
                                collector.CollectAndSkipTestInChain(testPlan, prefix, skipchain, "The agent has crash during the test.");
                                skipchain.Process();
                                // If we have no group it means that this is an isolated test
                                if (group != "")
                                {
                                    Fact.Log.Warning("The server has reported an error in the execution engine for the group: " + group);
                                    Fact.Log.Warning("The execution policy has been changed from fast to safe for the group: " + group);
                                    foreach (List<KeyValuePair<string, string>> isolatedTestPlan in
                                             collector.ComputeTestPlanWithIsolatedTest(group, filterin, filterout, filteringroup, filteroutgroup))
                                    {
                                        if (testPlan.Count == 0) { continue; }
                                        PendingJob isolatedprojectjob =
                                             RemoteRunTestPlan(isolatedTestPlan,
                                                               collector.ListDependenciesValue(),
                                                               parameters,
                                                               Client,
                                                               InputFile,
                                                               TestsuiteFile,
                                                               prefix,
                                                               projectjob.AgentGroup,
                                                               "");
                                        jobs.Enqueue(isolatedprojectjob);
                                    }
                                }
                            }
                            else
                            {
                                Fact.Log.Terminal.Write(stdout);
                                Fact.Log.Terminal.Write(stderr);
                                try
                                {
                                    project.Merge(projectresult);
                                }
                                catch { Fact.Log.Warning("Impossible to merge the output of the iteration"); }
                            }
                        }
                    }

                    if (outputpackage != "")
                    {
                        Fact.Log.Verbose("Save test result ...");
                        
                        project.Save(outputpackage);
                    }
                    return 0;
                }
            }

            Fact.Processing.Chain chain = new Fact.Processing.Chain(project);

            if (collector != null)
            {
                collector.Verbose = verbose;
                collector.CollectTestInChain(testPlan, prefix, chain);
            }
            else
            {
                Fact.Log.Error("No valid test collector found for the input file");
            }
            Fact.Log.Verbose("Run tests ...");
            if (CommandLine["dryrun"].AsBoolean())
            {
                foreach (KeyValuePair<string, string> test in testPlan)
                {
                    if (collector.IsSetup(test.Key, test.Value))
                    { Fact.Log.Info("(setup) " + test.Key + "/" + test.Value); }
                    else
                    { Fact.Log.Info("(test)  " + test.Key + "/" + test.Value); }
                }
                return 0;
            }
            chain.Process();
            if (outputpackage != "")
            {
                Fact.Log.Verbose("Save test result ...");
                if (CommandLine["internal-purge"].AsBoolean()) { project.Purge(true, false); }
                project.Save(outputpackage);
            }
            return 0;
        }
    }
}
