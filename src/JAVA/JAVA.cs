﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JAVA
{
    public class Java : Fact.Plugin.Plugin
    {
        public override string Name
        {
            get
            {
                return "Java";
            }
        }

        public override int Run(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine("Usage: Java InputDirectory OutputPackage.");
                return 1;
            }

            if (!System.IO.File.Exists(args[0]))
            {
                Console.WriteLine("Can't load " + args[0] + ".");
                return 1;
            }

            Fact.Processing.Project Project = new Fact.Processing.Project("");
            Project.Load(args[0]);
            //FactJAVA.Analyzer.Process(Project);
            Project.Save(args[1]);
            return 0;
        }
    }
}
